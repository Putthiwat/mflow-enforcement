package com.appworks.co.th.batchworker.job.audit;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.audit.*;
import com.appworks.co.th.batchworker.dto.audit.InvoiceDetail;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class AuditInvoiceNonMemberItemWriter implements ItemWriter<InvoiceNonMember> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private InvoiceListValueRepository invoiceListValueRepository;
    private MasterVehicleOfficeRepository masterVehicleOfficeRepository;
    private InvoiceMasterHqRepository invoiceMasterHqRepository;
    private MasterPlazaRepository masterPlazaRepository;
    private InvoiceMasterLaneRepository invoiceMasterLaneRepository;
    private InvoiceNonMemberRepository invoiceNonMemberRepository;
    private AuditServiceImp auditServiceImp;

    @Override
    public void write(List<? extends InvoiceNonMember> items) throws Exception {
        items.stream().forEach(a -> {
            try {
                Long id = a.getId();
                log.info("Start submit audit billing with id : {}",id);
                auditServiceImp.auditBillingWithId(id, "NONMEMBER",setAuditBillingNonMember(a));

            } catch (Exception e) {
                log.error("AuditInvoiceNonMemberItemWriter error : {}", e);
            }
        });
    }
    public AuditBillingReq setAuditBillingNonMember(InvoiceNonMember invoiceNonMember){

        /*Optional<InvoiceNonMember> invoiceNonMemberById =invoiceNonMemberRepository.findById(id);*/
        AuditBillingReq auditBillingReq = new AuditBillingReq();
        /*if(invoiceNonMemberById.isPresent()){
            InvoiceNonMember invoiceNonMember = invoiceNonMemberById.get();*/
            if (invoiceNonMember != null){
                auditBillingReq.setInvoiceNo(invoiceNonMember.getInvoiceNo());
                auditBillingReq.setInvoiceRefNo(invoiceNonMember.getInvoiceNoRef()!=null?invoiceNonMember.getInvoiceNoRef():null);
                InvoiceType invoiceType = new InvoiceType();
                if(invoiceNonMember.getInvoiceType()!=null){
                    invoiceType.setCode(invoiceNonMember.getInvoiceType().toString());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoiceNonMember.getInvoiceType().toString(), "T000012");
                    if (invoiceListValue != null) {
                        invoiceType.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                    }
                }
                auditBillingReq.setInvoiceType(invoiceType);
                auditBillingReq.setTransactionType("NONMEMBER");
                auditBillingReq.setFullName(invoiceNonMember.getFullName()!=null?invoiceNonMember.getFullName():null);
                auditBillingReq.setAddress(invoiceNonMember.getAddress()!=null?invoiceNonMember.getAddress():null);

                if(invoiceNonMember.getIssueDate()!=null)
                    auditBillingReq.setIssueDate(DateUtils.issueDateDueDateAudit(invoiceNonMember.getIssueDate()));
                if(invoiceNonMember.getDueDate()!=null)
                    auditBillingReq.setDueDate(DateUtils.issueDateDueDateAudit(invoiceNonMember.getDueDate()));

                auditBillingReq.setFeeAmount(invoiceNonMember.getFeeAmount()!=null?invoiceNonMember.getFeeAmount():null);
                auditBillingReq.setFineAmount(invoiceNonMember.getFineAmount()!=null?invoiceNonMember.getFineAmount():null);
                auditBillingReq.setCollectionAmount(invoiceNonMember.getCollectionAmount()!=null?invoiceNonMember.getCollectionAmount():null);
                auditBillingReq.setTotalAmount(invoiceNonMember.getTotalAmount()!=null?invoiceNonMember.getTotalAmount():null);
                InvoiceStatus invoiceStatus = new InvoiceStatus();
                if(invoiceNonMember.getStatus()!=null){
                    invoiceStatus.setCode(invoiceNonMember.getStatus());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoiceNonMember.getStatus(), "T000013");
                    if (invoiceListValue != null) {
                        invoiceStatus.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                    }
                }
                auditBillingReq.setInvoiceStatus(invoiceStatus);
                auditBillingReq.setErrorMessage(invoiceNonMember.getErrorMessage()!=null?invoiceNonMember.getErrorMessage():null);
                auditBillingReq.setInvoiceDetails(invoiceNonMember.getDetails().stream().map(item ->{
                    com.appworks.co.th.batchworker.dto.audit.InvoiceDetail invoiceDetail = new com.appworks.co.th.batchworker.dto.audit.InvoiceDetail();
                    invoiceDetail.setInvoiceNo(item.getInvoice().getInvoiceNo());
                    invoiceDetail.setTransactionId(item.getTransactionId());
                    invoiceDetail.setTransactionDate(DateUtils.transactionDate(item.getTransactionDate()));
                    invoiceDetail.setPlate1(item.getPlate1());
                    invoiceDetail.setPlate2(item.getPlate2());
                    Province province = new Province();
                    if(item.getProvince()!=null){
                        province.setCode(item.getProvince());
                        InvoiceMasterVehicleOffice invoiceMasterVehicleOffice = masterVehicleOfficeRepository.findByCode(item.getProvince());
                        if(invoiceMasterVehicleOffice!=null){
                            province.setDescription(invoiceMasterVehicleOffice.getDescription());
                        }
                    }
                    invoiceDetail.setProvice(province);
                    Origin origin = new Origin();
                    Hq hqOrigin = new Hq();
                    if(item.getHqCode()!=null){
                        hqOrigin.setCode(item.getHqCode());
                        InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getHqCode());
                        if(invoiceMasterHq!=null){
                            hqOrigin.setName(invoiceMasterHq.getDescription());
                        }
                    }
                    Plaza plazaOrigin = new Plaza();
                    if(item.getPlazaCode()!=null){
                        plazaOrigin.setCode(item.getPlazaCode());
                        InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getPlazaCode());
                        if(invoiceMasterPlaza!=null){
                            plazaOrigin.setName(invoiceMasterPlaza.getDescription());
                        }
                    }
                    Lane laneOrigin = new Lane();
                    if(item.getLaneCode()!=null){
                        laneOrigin.setCode(item.getLaneCode());
                        InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getLaneCode());
                        if(invoiceMasterLane!=null){
                            laneOrigin.setName(invoiceMasterLane.getDescription());
                        }
                    }
                    origin.setHq(hqOrigin);
                    origin.setLane(laneOrigin);
                    origin.setPlaza(plazaOrigin);
                    invoiceDetail.setOrigin(origin);
                    Destination destination = new Destination();
                    Hq hqDes = new Hq();
                    if(item.getDestHqCode()!=null){
                        hqDes.setCode(item.getDestHqCode());
                        InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getDestHqCode());
                        if(invoiceMasterHq!=null){
                            hqDes.setName(invoiceMasterHq.getDescription());
                        }
                    }
                    Plaza plazaDes = new Plaza();
                    if(item.getDestPlazaCode()!=null){
                        plazaDes.setCode(item.getDestPlazaCode());
                        InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getDestPlazaCode());
                        if(invoiceMasterPlaza!=null){
                            plazaDes.setName(invoiceMasterPlaza.getDescription());
                        }
                    }
                    Lane laneDes = new Lane();
                    if(item.getDestLaneCode()!=null){
                        laneDes.setCode(item.getDestLaneCode());
                        InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getDestLaneCode());
                        if(invoiceMasterLane!=null){
                            laneDes.setName(invoiceMasterLane.getDescription());
                        }
                    }
                    destination.setHq(hqDes);
                    destination.setLane(laneDes);
                    destination.setPlaza(plazaDes);
                    invoiceDetail.setDestination(destination);
                    invoiceDetail.setFeeAmount(item.getFeeAmount());
                    invoiceDetail.setFineAmount(item.getFineAmount());
                    invoiceDetail.setCollectionAmount(item.getCollectionAmount());
                    invoiceDetail.setTotalAmount(item.getTotalAmount());
                    OriginTranType originTranType = new OriginTranType();
                    if(item.getOriginTranType()!=null){
                        originTranType.setCode(item.getOriginTranType());
                        InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(item.getOriginTranType(), "T000018");
                        if (invoiceListValue != null) {
                            originTranType.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                        }
                    }
                    invoiceDetail.setOriginTranType(originTranType);
                    invoiceDetail.setEvidences(item.getEvidences().stream().map(evi ->{
                        Evidences evidences = new Evidences();
                        evidences.setType(evi.getType());
                        evidences.setFileId(evi.getFile());
                        return evidences;
                    }).collect(Collectors.toList()));

                    return invoiceDetail;
                }).collect(Collectors.toList()));
            }
        //}
        return auditBillingReq;
    }
}
