package com.appworks.co.th.batchworker.service;

import static com.appworks.co.th.batchworker.commons.Constants.CustomerType.JURISTIC;
import static com.appworks.co.th.batchworker.commons.Constants.NationalityCode.THAI;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.grpc.MasterServiceProviderClient;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceMasterJdbcRepository;
import com.appworks.co.th.batchworker.payload.*;
import com.appworks.co.th.customerinfoservice.MasterCustomerDataResponse;
import com.appworks.co.th.getserviceprovider.GetServiceProviderRequest;
import com.appworks.co.th.getserviceprovider.GetServiceProviderResponse;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.masterservice.ServiceProvider;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "cacheService")
public class CacheServiceImpl implements CacheService {

    @Autowired
    private MasterClient masterClient;

    @Autowired
    private MasterServiceProviderClient serviceProviderClient;

    @Autowired
    private CustomerInfoClient customerInfoClient;

    @Autowired
    private InvoiceMasterJdbcRepository invoiceMasterDao;

    @Override
    @Cacheable(cacheNames="master_provider", key = "#code", condition="#code != null")
    public ServiceProvider getMasterProvider(String code) {
        HqResponse hqResponse = masterClient.getHq(HqRequest.newBuilder().setHqCode(code).build());
        return hqResponse.getServiceProvider();
    }

    @Override
    @Cacheable(cacheNames="master_color", key = "#codes", condition="#codes != null")
    public ColorNameDTO getColorNameByCodes(List<String> codes) {
        return invoiceMasterDao.getColorNameByCodes(codes);
    }

    @Override
    @Cacheable(cacheNames="master_office", key = "#code", condition="#code != null")
    public OfficeNameDTO getOfficeName(String code) {
        return invoiceMasterDao.getOfficeNameByCode(code);
    }

    @Override
    @Cacheable(cacheNames="master_hq", key = "#code", condition="#code != null")
    public HQNameDTO getHQName(String code) {
        return invoiceMasterDao.getHQNameByCode(code);
    }

    @Override
    @Cacheable(cacheNames="master_plaza", key = "#code", condition="#code != null")
    public PlazaNameDTO getPlazaName(String code) {
        return invoiceMasterDao.getPlazaNameByCode(code);
    }

    @Override
    @Cacheable(cacheNames="master_brand", key = "#code", condition="#code != null")
    public BrandNameDTO getBrandName(String code) {
        return invoiceMasterDao.getBrandNameByCode(code);
    }

    @Override
    @Cacheable(cacheNames="master_lane", key = "#code", condition="#code != null")
    public LaneNameDTO getLaneName(String code) {
        return invoiceMasterDao.getLaneNameByCode(code);
    }

    @Override
    @Cacheable(cacheNames="customer_identity", key = "#customerId", condition="#customerId != null")
    public String getIdentityLast4Digit(String customerId) {

        MasterCustomerDataResponse response = customerInfoClient.getCustomerInfo(customerId);

        if (JURISTIC.equals(response.getCustomerType())) {
            String registrationId = response.getRegistrationId();
            if (StringUtils.isEmpty(registrationId)) {
                throw new NullPointerException("customer registrationId is null or empty");
            }
            return registrationId.substring(registrationId.length() - 4);
        }

        if (THAI.equals(response.getNationality())) {
            String citizenId = response.getCitizenId();
            if (StringUtils.isEmpty(citizenId)) {
                throw new NullPointerException("customer citizenId is null or empty");
            }
            return citizenId.substring(citizenId.length() - 4);
        }

        if (!THAI.equals(response.getNationality())) {
            String passportId = response.getPassportId();
            if (StringUtils.isEmpty(passportId)) {
                throw new NullPointerException("customer passportId is null or empty");
            }
            return passportId.substring(passportId.length() - 4);
        }

        throw new NullPointerException("customer identity is null or empty");
    }

    @Override
    @Cacheable(cacheNames="customer_phone", key = "#customerId", condition="#customerId != null")
    public String getPhone(String customerId) {
        MasterCustomerDataResponse response = customerInfoClient.getCustomerInfo(customerId);
        if(StringUtils.isNotEmpty(response.getMobile())) {
            return response.getMobile();
        }
        throw new NullPointerException("customer mobile is null or empty");
    }

    @Override
    public GetServiceProviderResponse getProvider(String code, String lang) {
        GetServiceProviderRequest request = GetServiceProviderRequest.newBuilder()
                .setServ(code)
                .setLanguage(lang)
                .build();
        return serviceProviderClient.getServiceProviderService(request);
    }

}
