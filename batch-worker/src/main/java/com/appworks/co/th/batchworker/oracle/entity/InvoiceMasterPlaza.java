package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_INVOICE_MASTER_PLAZA",
        indexes = {@Index(columnList="CODE"),@Index(columnList="CODE,HQ_CODE")},
        uniqueConstraints={@UniqueConstraint(columnNames = {"HQ_CODE","CODE"})})
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceMasterPlaza extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceMasterPLAZA", sequenceName = "SEQ_MF_INVOICE_MASTER_PLAZA", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceMasterPLAZA")
    public Long id;

    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String hqCode;

    @Column(name = "CODE", columnDefinition = "VARCHAR2(25) NOT NULL")
    private String code;

    @Column(name = "NAME", columnDefinition = "VARCHAR2(255) NOT NULL")
    private String name;

    @Column(name = "NAME_EN", columnDefinition = "VARCHAR2(255) NOT NULL")
    private String nameEn;

    @Column(name = "DESCRIPTION", columnDefinition = "VARCHAR2(255) NOT NULL")
    private String description;

    @Column(name = "DESCRIPTION_EN", columnDefinition = "VARCHAR2(255) NOT NULL")
    private String descriptionEn;
}
