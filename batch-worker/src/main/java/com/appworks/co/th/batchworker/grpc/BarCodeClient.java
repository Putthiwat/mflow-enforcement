package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeRequest;
import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeResponse;
import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeServiceGrpc;
import com.appworks.co.th.mflow.fileservice.FileRequest;
import com.appworks.co.th.mflow.fileservice.FileResponse;
import com.appworks.co.th.mflow.fileservice.FileServiceGrpcGrpc;
import com.appworks.co.th.qrcodeallservice.QrCodeAllServiceGrpcGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class BarCodeClient {

    @Value("${grpc.invoiceservice.host}")
    private String host;

    @Value("${grpc.invoiceservice.port}")
    private int port;

    private InvoiceBarCodeServiceGrpc.InvoiceBarCodeServiceBlockingStub blockingStub;

//    @PostConstruct
//    public void startConsumer() {
//        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
//        this.blockingStub = InvoiceBarCodeServiceGrpc.newBlockingStub(channel);
//    }

    @Autowired
    public void startConsumer() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        InvoiceBarCodeServiceGrpc.InvoiceBarCodeServiceBlockingStub stub = InvoiceBarCodeServiceGrpc.newBlockingStub(channel);
        this.blockingStub = stub;
    }

    public InvoiceBarCodeResponse getBarCodeMember(InvoiceBarCodeRequest request){
        InvoiceBarCodeResponse response = this.blockingStub.getBarCodeMember(request);
        return response;
    }
//
//    public InvoiceBarCodeResponse getBarCodeMember(InvoiceBarCodeRequest request){
//        return blockingStub.getBarCodeMember(request);
//    }

//    public InvoiceBarCodeResponse getBarCodeNonMember(InvoiceBarCodeRequest request){
//        return blockingStub.getBarCodeNonMember(request);
//    }

    public InvoiceBarCodeResponse getBarCodeNonMember(InvoiceBarCodeRequest request){
        InvoiceBarCodeResponse response = this.blockingStub.getBarCodeNonMember(request);
        return response;
    }

}
