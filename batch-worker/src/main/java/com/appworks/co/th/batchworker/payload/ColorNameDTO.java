package com.appworks.co.th.batchworker.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class ColorNameDTO implements Serializable  {

    private String invoiceNo;
    private String descriptionTh;
    private String descriptionEn;

}
