package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class Hq {
    private String code;
    private String name;
}
