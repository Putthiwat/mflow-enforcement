package com.appworks.co.th.batchworker.service;

public interface GrpcSendSmsAndEmailService {
    void sendSmsVerifyToCustomer(String action, String type, String mobile, String refId,String refId2,String refId3,String language);

    void sendEmailVerifyToCustomer(String action, String type, String email, String refId,String refId2,String language);
}
