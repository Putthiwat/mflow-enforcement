package com.appworks.co.th.batchworker.config;

import com.appworks.co.th.batchworker.config.prop.AsyncCustomerProperty;
import com.appworks.co.th.batchworker.config.prop.AsyncLogProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.boot.task.TaskExecutorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Slf4j
@EnableAsync
@Configuration
@RequiredArgsConstructor
public class AsyncConfig implements AsyncConfigurer {

    private final AsyncLogProperty prop;
    private final AsyncCustomerProperty asyncCustomerProperty;

    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor =
                new TaskExecutorBuilder()
                        .corePoolSize(prop.getCorePoolSize())
                        .queueCapacity(prop.getQueueCapacity())
                        .maxPoolSize(prop.getMaxPoolSize())
                        .threadNamePrefix(prop.getThreadNamePrefix())
                        .allowCoreThreadTimeOut(true)
                        .build();
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.initialize();
        return executor;
    }

    @Bean
    public ThreadPoolTaskExecutor loggingTaskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setWaitForTasksToCompleteOnShutdown(true);
        pool.setThreadNamePrefix("logging-");
        pool.setCorePoolSize(prop.getCorePoolSize());
        pool.setMaxPoolSize(prop.getMaxPoolSize());
        pool.setQueueCapacity(prop.getQueueCapacity());
        pool.initialize();
        return pool;
    }

    @Bean
    public ThreadPoolTaskExecutor customerTaskExecutor() {
        ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
        pool.setThreadNamePrefix("batch-");
        pool.setCorePoolSize(asyncCustomerProperty.getCorePoolSize());
        pool.setMaxPoolSize(asyncCustomerProperty.getMaxPoolSize());
        pool.setQueueCapacity(asyncCustomerProperty.getQueueCapacity());
        pool.setWaitForTasksToCompleteOnShutdown(true);
        pool.initialize();
        return pool;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) -> {
            log.error("AsyncMethod::{}({})", method.getName(), params);
            log.error("Exception::{}", ex.getMessage());
        };
    }
}