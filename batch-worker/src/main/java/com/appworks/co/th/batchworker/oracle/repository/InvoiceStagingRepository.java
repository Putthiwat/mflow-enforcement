package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceStaging;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Repository
@Transactional
public interface InvoiceStagingRepository extends JpaRepository<InvoiceStaging, Long> {


    @Query("UPDATE InvoiceStaging o SET  o.feeAmount = :feeAmount,o.fineAmount = :fineAmount,o.collectionAmount = :collectionAmount,o.totalAmount = :totalAmount WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateAmountByInvoiceNo(@Param("feeAmount") BigDecimal feeAmount, @Param("fineAmount") BigDecimal fineAmount, @Param("collectionAmount") BigDecimal collectionAmount, @Param("totalAmount") BigDecimal totalAmount, @Param("invoiceNo") String invoiceNo);

    @Query("UPDATE InvoiceStaging o SET  o.status = :status WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateStatusByInvoiceNo(@Param("status") String status, @Param("invoiceNo") String invoiceNo);

    @Query("UPDATE InvoiceStaging o SET o.status = :status, o.feeAmount = :feeAmount,o.fineAmount = :fineAmount,o.collectionAmount = :collectionAmount,o.totalAmount = :totalAmount WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateAmountAndStatusByInvoiceNo(@Param("status") String status, @Param("feeAmount") BigDecimal feeAmount, @Param("fineAmount") BigDecimal fineAmount, @Param("collectionAmount") BigDecimal collectionAmount, @Param("totalAmount") BigDecimal totalAmount, @Param("invoiceNo") String invoiceNo);

    @Query("select o from InvoiceStaging o WHERE o.customerId = :customerId and o.hqCode = :hqCode and o.status = :status and o.vehicleCode = :vehicleCode")
    Optional<InvoiceStaging> findByCustomerIdAndHqCodeAndStatusAndVehicleCode(@Param("customerId") String customerId, @Param("hqCode") String hqCode, @Param("status") String status, @Param("vehicleCode") String vehicleCode);
}
