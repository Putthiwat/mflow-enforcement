package com.appworks.co.th.batchworker.commons;

public class Constants {
    public static String SYSTEM = "batch-service";

    public static class Status {
        public static final String MOVED = "MOVED";
        public static final String WAITING_TO_MOVE = "WAITING_TO_MOVE";
        public static final String PAYMENT_WAITING = "PAYMENT_WAITING";
        public static final String PAYMENT_FAILED = "PAYMENT_FAILED";
        public static final String PAYMENT_SUCCESS = "PAYMENT_SUCCESS";
        public static final String PAYMENT_CANCEL = "PAYMENT_CANCEL";
        public static final String PAYMENT_NOCHARGE = "PAYMENT_NOCHARGE";
        public static final String PAYMENT_INPROGRESS = "PAYMENT_INPROGRESS";
        public static final String BATCH_PROCESSING = "BATCH_PROCESSING";
    }

    public static class CustomerType {
        public static final String INDIVIDUAL = "Individual";
        public static final String JURISTIC = "Juristic";
    }

    public static class NationalityCode {
        public static final String THAI = "NCODE00066";
    }

    public static class PaymentMethod {
        public static final String CRD = "CRD";
        public static final String RTP = "RTP";
        public static final String CREDIT_CARD = "CREDIT_CARD";
        public static final String DEBIT = "DEBIT";
        public static final String ACCOUNT="ACCOUNT";
        public static final String QR="QR";
        public static final String MPASS="MPASS";
        public static final String EASYPASS="EASYPASS";
    }

    public static class Invoice {

        public static final String EBIL_FORMAT = "ebill_%s_%s";

    }

    public static class BillCycle{
        public static final String BILL_CYCLE = "BILL_CYCLE";
        public static final String BILL_TIME = "BILL_TIME";
    }

    public static class generateKeyInvoiceNonmember {
        public static final String CREATE_INVOICE_NONMEMBER = "CREATE_INVOICE_NONMEMBER";
        public static final String CREATE_INVOICE_MEMBER = "CREATE_INVOICE_MEMBER";
    }

}
