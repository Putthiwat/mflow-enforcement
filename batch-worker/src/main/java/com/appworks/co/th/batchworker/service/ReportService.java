package com.appworks.co.th.batchworker.service;

import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;

import java.io.IOException;

public interface ReportService {

    JasperPrint generateInvoiceMember(Invoice invoice) throws JRException, IOException;

    JasperPrint generateInvoiceNonMember(InvoiceNonMember invoiceNonMember)  throws JRException, IOException;

}
