package com.appworks.co.th.batchworker.job.resetBuildInvoiceProceedList;

import com.appworks.co.th.batchworker.dto.InvoiceProceedListDTO;
import com.appworks.co.th.batchworker.service.invoice.InvoiceNonMemberService;
import com.appworks.co.th.batchworker.service.invoice.InvoiceStageService;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceDetail;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceHeader;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
public class ResetBuildInvoiceProceedListJobItemWriter implements ItemWriter<InvoiceProceedListDTO> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MessagePublisher messagePublisher;
    @Autowired
    private InvoiceStageService invoiceStageService;

    @Autowired
    private InvoiceNonMemberService invoiceNonMemberService;

    @Override
    public void write(List<? extends InvoiceProceedListDTO> items) throws Exception {
        for (InvoiceProceedListDTO invoiceProceedMemberDTO : items) {
            if(ObjectUtils.isNotEmpty(invoiceProceedMemberDTO.getCustomerId())) {
                this.writerInvoiceStaging(invoiceProceedMemberDTO);
            }else {
                this.writerInvoiceNonMember(invoiceProceedMemberDTO);
            }
            log.info("Data member : {}", invoiceProceedMemberDTO);
        }
    }

    private void writerInvoiceStaging(InvoiceProceedListDTO invoiceProceedMemberDTO) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
        Long id = null;
        try {
            Map<String, Object> transaction = objectMapper.readValue(invoiceProceedMemberDTO.getDetail(), new TypeReference<Map<String, Object>>() {
            });
            Map<String, Object> header = (Map<String, Object>) transaction.get("header");
            Map<String, Object> detail = (Map<String, Object>) transaction.get("detail");
            CreateInvoiceDetail createInvoiceDetail = objectMapper.convertValue(detail, CreateInvoiceDetail.class);
            CreateInvoiceHeader createInvoiceHeader = objectMapper.convertValue(header, CreateInvoiceHeader.class);
            String jobId = Objects.toString(transaction.get("id"), null);
            if (StringUtils.isNotEmpty(jobId)) {
                id = Long.valueOf(jobId);
            }
            resultInvoiceDetail = invoiceStageService.createMemberStagingInvoice(invoiceProceedMemberDTO.getInvoiceNo(), createInvoiceHeader, createInvoiceDetail, resultInvoiceDetail);
            log.info("Data resultInvoiceDetail : {}, invoice : {}",resultInvoiceDetail.getTransactionIds(),invoiceProceedMemberDTO.getInvoiceNo());
        } catch (IOException e) {
            log.info("Error IOException writerInvoiceStaging : {}",e.getStackTrace());
        } catch (Exception e) {
            log.info("Error Exception writerInvoiceStaging : {}",e.getMessage());
        }
        if (CollectionUtils.isNotEmpty(resultInvoiceDetail.getTransactionIds())) {
            try {
                resultInvoiceDetail.setStatus(true);
                resultInvoiceDetail.setMessage("Success");

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("MEMBER");

                CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.APPROVED, resultInvoiceHeader, resultInvoiceDetail);
                createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
            } catch (Exception e) {
                resultInvoiceDetail.setStatus(false);
                resultInvoiceDetail.setMessage(ExceptionUtils.getStackTrace(e));

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("MEMBER");

                CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
                createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
            }
        }
    }


    private void writerInvoiceNonMember(InvoiceProceedListDTO invoiceProceedMemberDTO) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
        Long id = null;
        try {
            Map<String, Object> transaction = objectMapper.readValue(invoiceProceedMemberDTO.getDetail(), new TypeReference<Map<String, Object>>() {
            });
            Map<String, Object> header = (Map<String, Object>) transaction.get("header");
            Map<String, Object> detail = (Map<String, Object>) transaction.get("detail");
            CreateInvoiceDetail createInvoiceDetail = objectMapper.convertValue(detail, CreateInvoiceDetail.class);
            CreateInvoiceHeader createInvoiceHeader = objectMapper.convertValue(header, CreateInvoiceHeader.class);
            String jobId = Objects.toString(transaction.get("id"), null);
            if (StringUtils.isNotEmpty(jobId)) {
                id = Long.valueOf(jobId);
            }
            resultInvoiceDetail = invoiceNonMemberService.createNonMemberInvoice(invoiceProceedMemberDTO.getInvoiceNo(), createInvoiceHeader, createInvoiceDetail);

            if(resultInvoiceDetail!=null && CollectionUtils.isNotEmpty(resultInvoiceDetail.getTransactionIds())){
                resultInvoiceDetail.setStatus(true);
                resultInvoiceDetail.setMessage("Success");

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("NONMEMBER");

                CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceNonMemberCommand.class, CommonState.APPROVED, resultInvoiceHeader, resultInvoiceDetail);
                createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
            }else{
                resultInvoiceDetail = new ResultInvoiceDetail();
                resultInvoiceDetail.setStatus(false);
                resultInvoiceDetail.setMessage("Cannot insert invoice nonmember");

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("NONMEMBER");

                CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceNonMemberCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
                createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
            }
            log.info("Data resultInvoiceDetail : {}, invoice : {}",resultInvoiceDetail.getTransactionIds(),invoiceProceedMemberDTO.getInvoiceNo());
        } catch (Exception e) {
            log.info("Error Exception writerInvoiceNonMember : {}",e.getMessage());
            resultInvoiceDetail = new ResultInvoiceDetail();
            resultInvoiceDetail.setStatus(false);
            resultInvoiceDetail.setMessage(ExceptionUtils.getStackTrace(e));

            ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
            resultInvoiceHeader.setType("NONMEMBER");

            CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceNonMemberCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
            createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
            createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
            messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
        }
    }


}
