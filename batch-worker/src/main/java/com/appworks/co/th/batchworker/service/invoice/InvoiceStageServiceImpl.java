package com.appworks.co.th.batchworker.service.invoice;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceStagingDetailRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceStagingRepository;
import com.appworks.co.th.batchworker.service.customerService.CustomerVehicleInfoService;
import com.appworks.co.th.sagaappworks.batch.InvoiceHeader;
import com.appworks.co.th.sagaappworks.batch.InvoiceInfo;
import com.appworks.co.th.sagaappworks.batch.InvoiceTransaction;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.OptimisticLockingFailureException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
@Service(value = "invoiceStageService")
public class InvoiceStageServiceImpl implements InvoiceStageService {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceStageServiceImpl.class);
    final DateFormat df = new SimpleDateFormat("yyMMdd");
    @Autowired
    InvoiceStagingRepository invoiceStagingRepository;
    @Autowired
    InvoiceStagingDetailRepository invoiceStagingDetailRepository;

    public static final FastDateFormat YYYYMMDD_HHMMSS_SSS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("Asia/Bangkok"));

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    CustomerVehicleInfoService customerVehicleInfoService;

    @Value("${fine.invoice.member.Type.zero}")
    private Long invoiceMemberTypeZero;
    @Value("${fine.invoice.member.Type.one}")
    private Long invoiceMemberTypeOne;
    @Value("${fine.invoice.member.Type.three}")
    private Long invoiceMemberTypethree;
    @Value("${fine.time.multiply}")
    private Long fineTimeMultiply;
    @Value("${fine.fineAmount.member.multiply}")
    private Long fineAmount;
    @Value("${fine.collectionAmount.member.multiply}")
    private Long collectionAmount;
    @Value("${operation.fee.member.multiply}")
    private Long operationFeeAmount;


    @Override
    public ResultInvoiceDetail createMemberStagingInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, ResultInvoiceDetail resultInvoiceDetail) {
        List<InvoiceInfo> res = new ArrayList<>();
        InvoiceStaging invoiceStaging = this.InvoiceStagingDetail(invoiceNo, res, createInvoiceHeader, createInvoiceDetail);
        if (ObjectUtils.isNotEmpty(invoiceStaging)) {
            mapToInvoiceData(invoiceStaging);
            resultInvoiceDetail.setTransactionIds(res);
            resultInvoiceDetail.setServiceProvider(createInvoiceHeader.getServiceProvider());
        }
//        resultInvoiceDetail.setTransactionIds(res);
        return resultInvoiceDetail;
    }


    private InvoiceStaging InvoiceStagingDetail(String invoiceNo, List<InvoiceInfo> res, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail) {
        InvoiceStaging invoice = invoiceStagingRepository.findByCustomerIdAndHqCodeAndStatusAndVehicleCode(
                createInvoiceHeader.getCustomerId(),
                createInvoiceHeader.getHqCode(),
                Constants.Status.WAITING_TO_MOVE,
                createInvoiceHeader.getVehicleCode()
        ).orElse(null);
        logger.info("InvoiceStaging : {} ,invoiceNo : {}", invoice, invoiceNo);
        try {
            if (ObjectUtils.isNotEmpty(invoice)) {
                List<InvoiceStagingDetail> invoiceDetails = createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                    InvoiceStagingDetail invoiceDetail = new InvoiceStagingDetail();
                    invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
                    invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
                    invoiceDetail.setInvoice(invoice);
                    invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
                    invoiceDetail.setTransactionDate(transactionDate(createInvoiceTransaction.getTransactionDate()));
                    invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
                    invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
                    invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
                    invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
                    invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
                    invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());
                    invoiceDetail.setDestHqCode(createInvoiceTransaction.getDestinationHqCode());
                    invoiceDetail.setDestPlazaCode(createInvoiceTransaction.getDestinationPlazaCode());
                    invoiceDetail.setDestLaneCode(createInvoiceTransaction.getDestinationLaneCode());
                    BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());

                    invoiceDetail.setRawFee(rawFee);
                    invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
                    invoiceDetail.setFeeAmount(rawFee);
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
                    invoiceDetail.setFineAmount(BigDecimal.valueOf(createInvoiceTransaction.getFineAmount()));
                    invoiceDetail.setCollectionAmount(BigDecimal.valueOf(createInvoiceTransaction.getCollectionAmount()));
                    BigDecimal totalAmount = invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount());
                    invoiceDetail.setTotalAmount(totalAmount);
                    invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
                    invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
                    invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                        InvoiceStagingEvidence evidence = new InvoiceStagingEvidence();
                        evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                        evidence.setCreateChannel(createInvoiceHeader.getChannel());
                        evidence.setDetail(invoiceDetail);
                        evidence.setTransactionId(invoiceDetail.getTransactionId());
                        evidence.setFile(evd.getFile());
                        evidence.setType(evd.getType());
                        return evidence;
                    }).collect(Collectors.toList()));
                    InvoiceInfo invoiceInfo = new InvoiceInfo();
                    invoiceInfo.setId(createInvoiceTransaction.getId());
                    invoiceInfo.setInvoiceNo(invoice.getInvoiceNo());
                    res.add(invoiceInfo);
                    return invoiceDetail;
                }).collect(Collectors.toList());
                logger.info("createInvoiceHeader Test Details : {}", invoiceDetails);
                invoiceStagingDetailRepository.saveAll(invoiceDetails);
                return invoice;
            }
        } catch (Exception e) {
            throw new OptimisticLockingFailureException(e.getMessage());
        }
        return invoice;
    }

    private ResultInvoiceDetail mapToInvoiceData(InvoiceStaging invoice) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();

        InvoiceHeader header = new InvoiceHeader();

        header.setInvoiceNo(invoice.getInvoiceNo());
        header.setInvoiceNoRef(invoice.getInvoiceNoRef());
        header.setInvoiceType(String.valueOf(invoice.getInvoiceType()));
        // ==== customer
        header.setCustomerId(invoice.getCustomerId());
        header.setCustomerType("MEMBER");
        header.setFullName(invoice.getFullName());
        header.setAddress(invoice.getAddress());

        // ==== date time
        header.setIssueDate(invoice.getIssueDate());
        header.setDueDate(invoice.getDueDate());

        header.setHqCode(invoice.getHqCode());
        // ====
        header.setFeeAmount(invoice.getFeeAmount().doubleValue());
        header.setFineAmount(invoice.getFineAmount().doubleValue());
        header.setCollectionAmount(invoice.getCollectionAmount().doubleValue());
        header.setTotalAmount(invoice.getTotalAmount().doubleValue());
        header.setVat(invoice.getVat().doubleValue());
        if (!ObjectUtils.isEmpty(invoice.getDiscount()))
            header.setDiscount(invoice.getDiscount().doubleValue());
        header.setPrintFlag(invoice.getPrintFlag());
        header.setStatus(invoice.getStatus());

        resultInvoiceDetail.setInvoiceHeader(header);

        resultInvoiceDetail.setInvoiceTransactions(invoice.getDetails().stream().map(detail -> {

            InvoiceTransaction tran = new InvoiceTransaction();

            tran.setId(detail.getTransactionId());
            tran.setTransactionDate(detail.getTransactionDate().toString());
            tran.setPlate1(detail.getPlate1());
            tran.setPlate2(detail.getPlate2());
            tran.setProvince(detail.getProvince());
            tran.setInvoiceNo(invoice.getInvoiceNo());

            tran.setHqCode(detail.getHqCode());
            tran.setPlazaCode(detail.getPlazaCode());
            tran.setLaneCode(detail.getLaneCode());
            tran.setDestinationHqCode(detail.getDestHqCode());
            tran.setDestinationPlazaCode(detail.getDestPlazaCode());
            tran.setDestinationLaneCode(detail.getDestLaneCode());
            tran.setFeeAmount(detail.getFeeAmount().doubleValue());
            tran.setFineAmount(detail.getFineAmount().doubleValue());
            tran.setCollectionAmount(detail.getCollectionAmount().doubleValue());
            tran.setTotalAmount(detail.getTotalAmount().doubleValue());
            tran.setDiscount(detail.getDiscount().doubleValue());
            tran.setVat(detail.getVat().doubleValue());
            tran.setEvidences(detail.getEvidences().stream().map(invoiceEvidence -> {
                com.appworks.co.th.sagaappworks.batch.InvoiceEvidence createEnforcementEvidence = new com.appworks.co.th.sagaappworks.batch.InvoiceEvidence();
                createEnforcementEvidence.setTransactionId(invoiceEvidence.getTransactionId());
                createEnforcementEvidence.setType(invoiceEvidence.getType());
                createEnforcementEvidence.setFile(invoiceEvidence.getFile());
                return createEnforcementEvidence;
            }).collect(Collectors.toList()));


            return tran;
        }).collect(Collectors.toList()));

        return resultInvoiceDetail;
    }

    public static Instant transactionDate(String data) {
        Instant anotherInstant = null;
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        isoFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        try {
            Date dateFormat = isoFormat.parse(data);
            anotherInstant = dateFormat.toInstant();
        } catch (ParseException e) {
            e.printStackTrace();
            anotherInstant = Instant.now();
        } catch (Exception e) {
            log.error("Error Date : {} ", e);
            anotherInstant = Instant.now();
        }
        return anotherInstant;
    }

    public ResultInvoiceDetail insertInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, String invoiceChannel) {
        List<InvoiceInfo> res = new ArrayList<>();
        if (StringUtils.isNotEmpty(createInvoiceHeader.getInvoiceRefNo())) {
            invoiceRepository.updateInvoiceTypeByInvoiceNo(3, createInvoiceHeader.getCreateBy(), createInvoiceHeader.getInvoiceRefNo());
        }
        Invoice invoice = new Invoice();
        Instant dueDate = null;
        Date issueDate = null;
        invoice.setFeeAmount(BigDecimal.ZERO);
        invoice.setFineAmount(BigDecimal.ZERO);
        invoice.setCollectionAmount(BigDecimal.ZERO);
        invoice.setOperationFee(BigDecimal.ZERO);
        invoice.setTotalAmount(BigDecimal.ZERO);
        invoice.setDiscount(BigDecimal.ZERO);
        if (Constants.BillCycle.BILL_CYCLE.equals(invoiceChannel)) {
            if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 1) {
                issueDate = this.issueDate();
                dueDate = issueDate.toInstant().plus(invoiceMemberTypeOne, ChronoUnit.DAYS);
            } else if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 2) {
                issueDate = this.issueDate();
                invoice.setOperationFee(BigDecimal.valueOf(operationFeeAmount));
                invoice.setTotalAmount(invoice.getOperationFee());
            }
        } else {
            if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 1) {
                issueDate = this.issueDate();
                dueDate = issueDate.toInstant().plus(invoiceMemberTypeOne, ChronoUnit.DAYS);
            } else if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 2) {
                issueDate = this.issueDate();
                invoice.setOperationFee(BigDecimal.valueOf(operationFeeAmount));
                invoice.setTotalAmount(invoice.getOperationFee());
            }
        }
        invoice.setIssueDate(issueDate);
        if (dueDate != null) {
            invoice.setDueDate(Date.from(dueDate));
        }
        invoice.setInvoiceNo(invoiceNo);
        invoice.setInvoiceNoRef(createInvoiceHeader.getInvoiceRefNo());
        invoice.setCreateBy(createInvoiceHeader.getCreateBy());
        invoice.setCreateChannel(createInvoiceHeader.getChannel());
        invoice.setHqCode(createInvoiceHeader.getHqCode());
        invoice.setInvoiceNo(invoiceNo);
        invoice.setTransactionType("Member");
        invoice.setInvoiceType(Integer.valueOf(createInvoiceHeader.getInvoiceType()));
        invoice.setCustomerId(createInvoiceHeader.getCustomerId());
        invoice.setFullName(createInvoiceHeader.getFullName());
        invoice.setAddress(createInvoiceHeader.getAddress());
        invoice.setCreateChannel(createInvoiceHeader.getChannel());
        invoice.setCreateBy(createInvoiceHeader.getCreateBy());
        invoice.setStatus(Constants.Status.PAYMENT_WAITING);
        invoice.setVat(BigDecimal.ZERO);
        invoice.setPlate1(createInvoiceHeader.getPlate1());
        invoice.setPlate2(createInvoiceHeader.getPlate2());
        invoice.setProvince(createInvoiceHeader.getPlateProvince());
        invoice.setBrand(customerVehicleInfoService.getBrand(createInvoiceHeader.getVehicleCode()));
        invoice.setVehicleCode(createInvoiceHeader.getVehicleCode());
        invoice.setVehicleType(createInvoiceHeader.getVehicleTypeCode());
        invoice.setInvoiceChannel(invoiceChannel);

        if (!createInvoiceHeader.getColor().isEmpty()) {
            List<InvoiceColor> colorList = new ArrayList<>();
            for (String colorString : createInvoiceHeader.getColor()) {
                if (StringUtils.isNotEmpty(colorString)) {
                    InvoiceColor color = new InvoiceColor();
                    color.setInvoice(invoice);
                    color.setCode(colorString);
                    colorList.add(color);
                }
            }
            if (!colorList.isEmpty()) {
                invoice.setColors(colorList);
            }
        }

        List<InvoiceDetail> invoiceDetails = createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
//            BigDecimal totalAmountVat = new BigDecimal("0");
            InvoiceDetail invoiceDetail = new InvoiceDetail();
            invoiceDetail.setCollectionAmount(new BigDecimal(0));
            invoiceDetail.setOperationFee(new BigDecimal(0));
            invoiceDetail.setFineAmount(new BigDecimal(0));
            BigDecimal sumFineAmount = new BigDecimal(0);
            BigDecimal sumCollectionAmount = new BigDecimal(0);
            BigDecimal operationFee = new BigDecimal(0);
            invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
            invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
            invoiceDetail.setInvoice(invoice);
            invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
            invoiceDetail.setTransactionDate(transactionDate(createInvoiceTransaction.getTransactionDate()));
            invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
            invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
            invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
            invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
            invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
            invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());
            invoiceDetail.setDestHqCode(createInvoiceTransaction.getDestinationHqCode());
            invoiceDetail.setDestPlazaCode(createInvoiceTransaction.getDestinationPlazaCode());
            invoiceDetail.setDestLaneCode(createInvoiceTransaction.getDestinationLaneCode());

            BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());
            invoiceDetail.setRawFee(rawFee);
            invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
            invoiceDetail.setFeeAmount(rawFee);
            if (StringUtils.equals("0", createInvoiceHeader.getInvoiceType())) {
                invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceHeader.getVat()).setScale(2, RoundingMode.DOWN));
            } else {
                invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
            }
            if (createInvoiceTransaction.getFeeAmountOld() != null) {
                invoiceDetail.setFeeAmountOld(BigDecimal.valueOf(createInvoiceTransaction.getFeeAmountOld()));
            } else {
                invoiceDetail.setFeeAmountOld(BigDecimal.ZERO);
            }
            switch (Integer.valueOf(createInvoiceHeader.getInvoiceType())) {
                case 1:
                    if (invoiceDetail.getFeeAmount() != null && invoiceDetail.getFeeAmount().compareTo(BigDecimal.ZERO) == 1) {
                        sumCollectionAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumCollectionAmount = sumCollectionAmount.multiply(BigDecimal.valueOf(fineAmount));
                    }
                    invoiceDetail.setCollectionAmount(sumCollectionAmount);
                    break;
                case 2:
                    if (invoiceDetail.getFeeAmount() != null && invoiceDetail.getFeeAmount().compareTo(BigDecimal.ZERO) == 1) {
                        sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumCollectionAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(collectionAmount));
                        sumCollectionAmount = sumCollectionAmount.multiply(BigDecimal.valueOf(fineAmount));
                        operationFee = operationFee.add(BigDecimal.ZERO);
                    }
                    invoiceDetail.setCollectionAmount(sumCollectionAmount);
                    invoiceDetail.setOperationFee(operationFee);
                    invoiceDetail.setFineAmount(sumFineAmount);
                    break;
            }
            // invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getOperationFee()).add(invoiceDetail.getCollectionAmount()));
            invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getOperationFee()).add(invoiceDetail.getCollectionAmount()));
            invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
            invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
            invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                InvoiceEvidence evidence = new InvoiceEvidence();
                evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                evidence.setCreateChannel(createInvoiceHeader.getChannel());
                evidence.setDetail(invoiceDetail);
                evidence.setTransactionId(invoiceDetail.getTransactionId());
                evidence.setFile(evd.getFile());
                evidence.setType(evd.getType());
                return evidence;
            }).collect(Collectors.toList()));

            //    invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
            invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
            invoice.setFineAmount(invoice.getFineAmount().add(invoiceDetail.getFineAmount()));
            invoice.setCollectionAmount(invoice.getCollectionAmount().add(invoiceDetail.getCollectionAmount()));
            invoice.setTotalAmount(invoice.getTotalAmount().add(invoiceDetail.getTotalAmount()));
            invoice.setOperationFee(invoice.getOperationFee().add(invoiceDetail.getOperationFee()));
            invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
            InvoiceInfo invoiceInfo = new InvoiceInfo();
            invoiceInfo.setId(createInvoiceTransaction.getId());
            invoiceInfo.setInvoiceNo(invoice.getInvoiceNo());
            res.add(invoiceInfo);

            return invoiceDetail;
        }).collect(Collectors.toList());

        invoice.setDetails(invoiceDetails);
        log.info("sendPaymentHistory member : {} , invoice : {}", invoice.getTotalAmount(), invoice);
        if (invoice.getTotalAmount().compareTo(BigDecimal.ZERO) == 0) {
            log.debug("Opend sendPaymentHistory member : {} , invoice : {}", invoice.getFeeAmount(), invoice.getInvoiceNo());
            invoice.setStatus(Constants.Status.PAYMENT_SUCCESS);
        }
        Invoice invoiceAfterSave = invoiceRepository.saveAndFlush(invoice);

        if(invoiceAfterSave!=null && invoiceAfterSave.getId()!=null){
            ResultInvoiceDetail invoiceData = mapToInvoiceData(invoiceAfterSave, invoiceChannel);
            invoiceData.setTransactionIds(res);
            invoiceData.setServiceProvider(createInvoiceHeader.getServiceProvider());
            return invoiceData;
        }
        return null;
    }

    private Date issueAddDate(CreateInvoiceDetail createInvoiceDetail, Long invoiceMemberTypeZero) {
        Date dateTo = null;
        try {
            if (ObjectUtils.isNotEmpty(createInvoiceDetail.getDetails())) {
                dateTo = DateUtils.parseDateStrictly(createInvoiceDetail.getDetails().get(0).getTransactionDate(),
                        new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});
                int newDate = 1;

                Calendar currentDate = Calendar.getInstance();
                currentDate.setLenient(false);
                currentDate.set(Calendar.HOUR_OF_DAY, 0);
                currentDate.set(Calendar.MINUTE, 0);
                currentDate.set(Calendar.SECOND, 0);
                currentDate.set(Calendar.MILLISECOND, 0);

                Calendar issueDate = Calendar.getInstance();
                issueDate.setLenient(false);
                issueDate.setTime(dateTo);
                if (invoiceMemberTypeZero != null) {
                    issueDate.add(Calendar.DATE, Integer.valueOf(String.valueOf(invoiceMemberTypeZero)) + newDate);
                }
                issueDate.set(Calendar.HOUR_OF_DAY, 0);
                issueDate.set(Calendar.MINUTE, 0);
                issueDate.set(Calendar.SECOND, 0);
                issueDate.set(Calendar.MILLISECOND, 0);

                if (issueDate.getTime().before(currentDate.getTime()))
                    return currentDate.getTime();
                return issueDate.getTime();
            } else {
                return this.issueDate();
            }
        } catch (Exception e) {
            log.error("Error issueAddDate : {}", e.getMessage());
        }
        return this.issueDate();
    }

    private Date issueAddDateOfBillCycle(String issue, Long invoiceMemberTypeZero) {
        Date dateTo = null;
        try {
            dateTo = DateUtils.parseDateStrictly(issue,
                    new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});
            int newDate = 1;
            Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(dateTo);
            if (invoiceMemberTypeZero != null) {
                c.add(Calendar.DATE, Integer.valueOf(String.valueOf(invoiceMemberTypeZero)) + newDate);
            }
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            dateTo = c.getTime();
        } catch (Exception e) {
            log.error("Error issueAddDateOfBillCycle : {}", e.getMessage());
        }
        return dateTo;
    }

//    private Date issueDateMember(CreateInvoiceDetail createInvoiceDetail) {
//        Date dateTo = null;
//        try {
//            if (ObjectUtils.isNotEmpty(createInvoiceDetail.getDetails())) {
//                try {
//                    dateTo = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(createInvoiceDetail.getDetails().get(0).getTransactionDate());
//                } catch (Exception e) {
//                    dateTo = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(createInvoiceDetail.getDetails().get(0).getTransactionDate());
//                }
//                Calendar c = Calendar.getInstance();
//                c.setLenient(false);
//                c.setTime(dateTo);
//                c.set(Calendar.HOUR_OF_DAY, 0);
//                c.set(Calendar.MINUTE, 0);
//                c.set(Calendar.SECOND, 0);
//                c.set(Calendar.MILLISECOND, 0);
//                return c.getTime();
//            } else {
//                dateTo = this.issueDate();
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//            dateTo = this.issueDate();
//        }
//        return dateTo;
//    }

    private Date issueDate() {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    private ResultInvoiceDetail mapToInvoiceData(Invoice invoice, String invoiceChannel) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();

        InvoiceHeader header = new InvoiceHeader();

        header.setInvoiceNo(invoice.getInvoiceNo());
        header.setInvoiceNoRef(invoice.getInvoiceNoRef());
        header.setInvoiceType(String.valueOf(invoice.getInvoiceType()));
        header.setInvoiceChannel(invoiceChannel);
        // ==== customer
        header.setCustomerId(invoice.getCustomerId());
        header.setCustomerType("MEMBER");
        header.setFullName(invoice.getFullName());
        header.setAddress(invoice.getAddress());

        // ==== date time
        header.setIssueDate(invoice.getIssueDate());
        header.setDueDate(invoice.getDueDate());

        header.setHqCode(invoice.getHqCode());
        // ====
        header.setFeeAmount(invoice.getFeeAmount().doubleValue());
        header.setFineAmount(invoice.getFineAmount().doubleValue());
        header.setCollectionAmount(invoice.getCollectionAmount().doubleValue());
        header.setTotalAmount(invoice.getTotalAmount().doubleValue());
        header.setOperationFee(invoice.getOperationFee().doubleValue());
        header.setVat(invoice.getVat().doubleValue());
        header.setPrintFlag(invoice.getPrintFlag());
        header.setStatus(invoice.getStatus());

        resultInvoiceDetail.setInvoiceHeader(header);

        resultInvoiceDetail.setInvoiceTransactions(invoice.getDetails().stream().map(detail -> {

            InvoiceTransaction tran = new InvoiceTransaction();

            tran.setId(detail.getTransactionId());
            tran.setTransactionDate(detail.getTransactionDate().toString());
            tran.setPlate1(detail.getPlate1());
            tran.setPlate2(detail.getPlate2());
            tran.setProvince(detail.getProvince());
            tran.setInvoiceNo(invoice.getInvoiceNo());

            tran.setHqCode(detail.getHqCode());
            tran.setPlazaCode(detail.getPlazaCode());
            tran.setLaneCode(detail.getLaneCode());
            tran.setDestinationHqCode(detail.getDestHqCode());
            tran.setDestinationPlazaCode(detail.getDestPlazaCode());
            tran.setDestinationLaneCode(detail.getDestLaneCode());
            tran.setFeeAmount(detail.getFeeAmount().doubleValue());
            tran.setOperationFee(detail.getOperationFee().doubleValue());
            tran.setFineAmount(detail.getFineAmount().doubleValue());
            tran.setVat(detail.getVat().doubleValue());
            tran.setCollectionAmount(detail.getCollectionAmount().doubleValue());
            tran.setTotalAmount(detail.getTotalAmount().doubleValue());
            tran.setEvidences(detail.getEvidences().stream().map(invoiceEvidence -> {
                com.appworks.co.th.sagaappworks.batch.InvoiceEvidence createEnforcementEvidence = new com.appworks.co.th.sagaappworks.batch.InvoiceEvidence();
                createEnforcementEvidence.setTransactionId(invoiceEvidence.getTransactionId());
                createEnforcementEvidence.setType(invoiceEvidence.getType());
                createEnforcementEvidence.setFile(invoiceEvidence.getFile());
                return createEnforcementEvidence;
            }).collect(Collectors.toList()));


            return tran;
        }).collect(Collectors.toList()));

        return resultInvoiceDetail;
    }

}
