package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Entity
@ToString(exclude = {"colors","details"})
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_STAGING"
        , uniqueConstraints = {
        @UniqueConstraint(columnNames = {"CUSTOMER_ID", "ISSUE_DATE", "VEHICLE_CODE", "HQ_CODE","INVOICE_CHANNEL"}, name = "MF_INVOICE_STAGING_UN")
})
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceStaging extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceStaging", sequenceName = "SEQ_MF_INVOICE_STAGING", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "seqInvoiceStaging")
    public Long id;

    @NaturalId
    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(20)")
    private String invoiceNo;

    @Column(name = "INVOICE_NO_REF", columnDefinition = "VARCHAR2(50)")
    private String invoiceNoRef;
    @Column(name="INVOICE_TYPE", columnDefinition = "SMALLINT")
    @NotNull
    private Integer invoiceType;
    @Column(name="TRANSACTION_TYPE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String transactionType;
    @Column(name="CUSTOMER_ID", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String customerId;
    @Column(name = "PLATE1", columnDefinition = "VARCHAR2(255)")
    private String plate1;
    @Column(name = "PLATE2", columnDefinition = "VARCHAR2(255)")
    private String plate2;
    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(25)")
    private String province;
    @Column(name="FULL_NAME", columnDefinition = "VARCHAR2(500)")
    @NotNull
    private String fullName;
    @Column(name="ADDRESS", columnDefinition = "VARCHAR2(1500)")
    @NotNull
    private String address;
    @Column(name="ISSUE_DATE", columnDefinition = "DATE")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date issueDate;
    @Column(name="DUE_DATE", columnDefinition = "DATE")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date dueDate;
    @Column(name="PAYMENT_DATE", columnDefinition = "DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate ;
    @Column(name="FEE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    @NotNull
    private BigDecimal feeAmount;
    @Column(name="FINE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal fineAmount;
    @Column(name="PRINT_FLAG", columnDefinition = "SMALLINT")
    private Integer printFlag;
    @Column(name="STATUS", columnDefinition = "VARCHAR2(25)")
    private String status;
    @Column(name="ERROR_MESSAGE", columnDefinition = "VARCHAR2(4000)")
    private String errorMessage;

    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String hqCode;

    @Column(name = "COLLECTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal collectionAmount;

    @Column(name = "TOTAL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalAmount;

    @Column(name="VAT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal vat;

    @Column(name = "DISCOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal discount;

    @Column(name = "BRAND", columnDefinition = "VARCHAR2(25)")
    private String brand;

    @Column(name = "VEHICLE_CODE", columnDefinition = "VARCHAR2(25)")
    private String vehicleCode;

    @Column(name = "VEHICLE_TYPE", columnDefinition = "VARCHAR2(25)")
    private String vehicleType;

    @Column(name = "INVOICE_CHANNEL", columnDefinition = "VARCHAR2(50)")
    private String invoiceChannel;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceStagingDetail> details;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceStagingColor> colors;

}
