package com.appworks.co.th.batchworker.service.audit;

import com.appworks.co.th.batchworker.dto.audit.AuditBillingReq;
import com.appworks.co.th.batchworker.dto.audit.AuditTransResponseBody;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceAuditDoh;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMemberAuditDoh;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceAuditDohRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberAuditDohRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.StringJoiner;


@RequiredArgsConstructor
@Slf4j
@Service
public class AuditServiceImp {

    //private final RestTemplate restTemplate;

    @Value("${api.audit.billing.url}")
    private String url;

    @Value("${api.audit.billing.user}")
    private String user;

    @Value("${api.audit.billing.pass}")
    private String pass;

    private final InvoiceRepository invoiceRepository;

    private final InvoiceNonMemberRepository invoiceNonMemberRepository;

    private final InvoiceAuditDohRepository invoiceAuditDohRepository;

    private final InvoiceNonMemberAuditDohRepository invoiceNonMemberAuditDohRepository;



    @Async
    public void auditBillingWithId(Long id,String transType,AuditBillingReq auditTransReq) {
        boolean isSuccess = callRest(auditTransReq);
        InvoiceAuditDoh invoiceAuditDoh = new InvoiceAuditDoh();
        InvoiceNonMemberAuditDoh invoiceNonMemberAuditDoh = new InvoiceNonMemberAuditDoh();
        if(isSuccess){
            if(transType.equalsIgnoreCase("MEMBER")){
                invoiceAuditDohRepository.updateAuditFlagByInvoiceNo("Y", auditTransReq.getInvoiceNo());
                //invoiceRepository.updateAuditFlagById("Y","SYSTEM", id);
            }else if(transType.equalsIgnoreCase("NONMEMBER")){
                invoiceNonMemberAuditDohRepository.updateAuditFlagByInvoiceNo("Y", auditTransReq.getInvoiceNo());
                //invoiceNonMemberRepository.updateAuditFlagById("Y","SYSTEM", id);
            }
        }else {
            log.info("Submit to Audit is 'false'");
        }
    }

    @Async
    public void auditBilling(AuditBillingReq auditTransReq) {
        boolean isSuccess = callRest(auditTransReq);
        //String invoice = auditTransReq.getInvoiceNo();
        InvoiceAuditDoh invoiceAuditDoh = new InvoiceAuditDoh();
        if(isSuccess){
            invoiceAuditDoh.setAuditFlag("Y");
            invoiceAuditDoh.setInvoiceNo(auditTransReq.getInvoiceNo());
            invoiceAuditDoh.setCreateChannel("doh-system");
            invoiceAuditDohRepository.save(invoiceAuditDoh);
            //invoiceRepository.updateAuditFlagByInvoiceNo("Y","SYSTEM",invoice);
        }else {
            invoiceAuditDoh.setAuditFlag("N");
            invoiceAuditDoh.setInvoiceNo(auditTransReq.getInvoiceNo());
            invoiceAuditDoh.setCreateChannel("doh-system");
            invoiceAuditDohRepository.save(invoiceAuditDoh);
            log.info("Submit to Audit is 'false'");
        }
    }

    @Async
    public void auditBillingNonmember(AuditBillingReq auditTransReq) {
        boolean isSuccess = callRest(auditTransReq);
        InvoiceNonMemberAuditDoh invoiceNonMemberAuditDoh = new InvoiceNonMemberAuditDoh();
        if(isSuccess){
            invoiceNonMemberAuditDoh.setAuditFlag("Y");
            invoiceNonMemberAuditDoh.setInvoiceNo(auditTransReq.getInvoiceNo());
            invoiceNonMemberAuditDoh.setCreateChannel("doh-system");
            invoiceNonMemberAuditDohRepository.save(invoiceNonMemberAuditDoh);
        }else {
            invoiceNonMemberAuditDoh.setAuditFlag("N");
            invoiceNonMemberAuditDoh.setInvoiceNo(auditTransReq.getInvoiceNo());
            invoiceNonMemberAuditDoh.setCreateChannel("doh-system");
            invoiceNonMemberAuditDohRepository.save(invoiceNonMemberAuditDoh);
            log.info("Submit to Audit is 'false'");
        }
    }

    private boolean callRest(AuditBillingReq auditTransReq) {
        try {
            Date date = new Date();
            DateFormat dateFormat1 = new SimpleDateFormat("yyyymmddHHMMsssss");
            String currentDate = dateFormat1.format(date);
            DateFormat dateFormat2 = new SimpleDateFormat("yyyy-mm-dd'T'HH:MM:ss.sss");
            String requestDate = dateFormat2.format(date);
            int random= (int) (Math.random() * 999999);
            String idSeq = "T" + currentDate + random;

            String auth = user + ":" + pass;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic " + new String(encodedAuth);

            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_TYPE, "application/json; charset=UTF-8");
            headers.add("Authorization", authHeader);
            headers.set("TransactionId", idSeq);
            headers.set("RequestDate", requestDate);

            HttpEntity<AuditBillingReq> requestMap = new HttpEntity<>(auditTransReq,
                    headers);
            ObjectMapper mapper = new ObjectMapper();
            byte[] header = mapper.writeValueAsString(requestMap.getHeaders()).getBytes();
            byte[] body = mapper.writeValueAsString(requestMap.getBody()).getBytes();
            //String requestString = mapper.writeValueAsString(requestMap);
            StringJoiner request = new StringJoiner("\n");
            request.add("").add("===========================request begin================================================")
                    .add("URI         : "+ url)
                    .add("Method      : POST")
                    .add("Headers     : "+new String(header,"UTF-8"))
                    .add("Request body: "+new String(body,"UTF-8"))
                    .add("=============================request end================================================");
            log.info(request.toString());

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestMap,
                    String.class);

            AuditTransResponseBody responseBody = mapper.readValue(responseEntity.getBody(), AuditTransResponseBody.class);

            StringJoiner res = new StringJoiner("\n");
            res.add("").add("===========================response begin================================================")
                    .add("Status code  : "+ responseEntity.getStatusCodeValue())
                    .add("Status text  : "+ responseEntity.getStatusCode())
                    .add("Headers      : "+ responseEntity.getHeaders())
                    .add("Response body: "+ responseEntity.getBody())
                    .add("==========================response end================================================");
            log.info(res.toString());
            return responseBody.isStatus();
        } catch (HttpStatusCodeException e){
            StringJoiner res = new StringJoiner("\n");
            res.add("").add("===========================response begin================================================")
                    .add("Status code  : "+ e.getStatusCode())
                    .add("Status text  : "+ e.getStatusText())
                    .add("Headers      : "+ e.getResponseHeaders())
                    .add("Response body: "+ e.getResponseBodyAsString())
                    .add("==========================response end================================================");
            log.info(res.toString());
            return false;
        } catch (Exception e) {
            log.error("Exception : AuditTransaction ERROR : ", e);
            return false;
        }
    }
}