package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.getserviceprovider.GetMasterServiceProviderServiceGrpc;
import com.appworks.co.th.getserviceprovider.GetServiceProviderRequest;
import com.appworks.co.th.getserviceprovider.GetServiceProviderResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MasterServiceProviderClient {
    private static final Logger logger = LoggerFactory.getLogger(MasterServiceProviderClient.class);

    @Value("${grpc.masterservice.host}")
    private String host;
    @Value("${grpc.masterservice.port}")
    private String port;

    private GetMasterServiceProviderServiceGrpc.GetMasterServiceProviderServiceBlockingStub getMasterServiceProviderServiceBlockingStub;

    @Autowired
    public void setPaymentServiceStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        GetMasterServiceProviderServiceGrpc.GetMasterServiceProviderServiceBlockingStub stub = GetMasterServiceProviderServiceGrpc.newBlockingStub(channel);
        this.getMasterServiceProviderServiceBlockingStub = stub;
    }

    public GetServiceProviderResponse getServiceProviderService(GetServiceProviderRequest request){
        GetServiceProviderResponse collect = getMasterServiceProviderServiceBlockingStub.getServiceProviderService(request);
        return collect;
    }

}
