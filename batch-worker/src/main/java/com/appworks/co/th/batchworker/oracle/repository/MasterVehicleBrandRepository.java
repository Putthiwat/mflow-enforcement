package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceMasterVehicleBrand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterVehicleBrandRepository extends JpaRepository<InvoiceMasterVehicleBrand, Long> {

}
