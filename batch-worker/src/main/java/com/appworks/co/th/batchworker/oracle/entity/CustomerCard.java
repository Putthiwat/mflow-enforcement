package com.appworks.co.th.batchworker.oracle.entity;

import com.appworks.co.th.batchworker.commons.AESConverter;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_CUST_VISA_MASTER_CARD")
@Where(clause = "DELETE_FLAG = 0")
public class CustomerCard extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 6411992580148208199L;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqCustomerCard", sequenceName = "SEQ_MF_CUSTOMER_VISA_MASTER_CARD", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqCustomerCard")
    public Long id;

    @Column(name = "CUSTOMER_ID", columnDefinition = "VARCHAR2(25) NOT NULL")
    private String customerId;

    @Column(name = "CODE", columnDefinition = "VARCHAR2(25) NOT NULL", unique = true)
    private String code;

    @Column(name = "PAYMENT_CHANNEL", columnDefinition = "VARCHAR2(25) NOT NULL")
    private String paymentChannel;

    @Convert(converter = AESConverter.class)
    @Column(name = "CARD_NO", columnDefinition = "VARCHAR2(250) NOT NULL")
    private String cardNo;

    @Column(name = "CARD_TYPE", columnDefinition = "VARCHAR2(25) NOT NULL")
    private String cardType;

    @Convert(converter = AESConverter.class)
    @Column(name = "EXP_CARD_DATE", columnDefinition = "VARCHAR2(100)")
    private String expCardDate;

    @Column(name = "CARD_NAME", columnDefinition = "VARCHAR2(150)")
    private String cardName;

    @Column(name = "MEMBER_ID", columnDefinition = "VARCHAR2(255)")
    private String memberId;

    @Column(name = "TOKEN", columnDefinition = "VARCHAR2(3000)")
    private String token;

    @Column(name = "TOKEN_ORIGIN", columnDefinition = "VARCHAR2(3000)")
    private String tokenOrigin;

    @Column(name = "RETRY", columnDefinition = "SMALLINT default 0")
    private Integer retry;

    @Column(name = "PAYMENT_TYPE", columnDefinition = "SMALLINT default 1")
    private int paymentType;

    @Column(name = "DRAFT_FLAG", columnDefinition = "SMALLINT default 1")
    private Integer draftFlag = 1;

}
