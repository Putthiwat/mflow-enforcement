package com.appworks.co.th.batchworker.service.customerService;

import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.customerservice.GetBrandRequest;
import com.appworks.co.th.customerservice.GetBrandResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerVehicleInfoServiceImpl implements CustomerVehicleInfoService {
    @Autowired
    CustomerClient customerInfoClient;

    @Override
    public String getBrand(String vehicleCode) {
        GetBrandRequest.Builder getBrandRequest = GetBrandRequest.newBuilder();
        if(vehicleCode!=null){
            getBrandRequest.setVehicleCode(vehicleCode);
            GetBrandResponse grpcResponse = customerInfoClient.getBrand(getBrandRequest.build());
            if(grpcResponse.getStatus()==true&& StringUtils.isNotBlank(grpcResponse.getBrandCode())){
                return grpcResponse.getBrandCode();
            }else {
                return null;
            }
        }else {
            return null;
        }
    }
}
