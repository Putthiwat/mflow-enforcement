package com.appworks.co.th.batchworker.job.audit;

import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class AuditInvoiceMemberJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("auditInvoiceMemberJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("auditInvoiceMemberJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    private ApplicationContext applicationContext;

    public AuditInvoiceMemberJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> AuditInvoiceMemberItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}", minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o, InvoiceAuditDoh a "+"WHERE  a.invoiceNo = o.invoiceNo and a.auditFlag = 'N' AND a.deleteFlag = 0 AND a.createDate < TO_DATE(TO_CHAR (SYSDATE, 'YYYY-MM-DD'),'YYYY-MM-DD')" +" and o.id >= " + minValue + " and o.id <= " + maxValue;
        //String jpqlQuery = "SELECT o from Invoice o " + where + " and id >= " + minValue + " and id <= " + maxValue;
        log.debug("auditInvoiceMemberJob SQL {}", jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }

    @Bean("auditInvoiceMemberItemProcessor")
    public ItemProcessor<Invoice, Invoice> auditInvoiceMemberItemProcessor() {
        return item -> item;
    }

    @Bean
    public Step auditInvoiceMemberStep() throws Exception {
        return this.workerStepBuilderFactory.get("auditInvoiceMemberStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, Invoice>chunk(1000)
                .reader(AuditInvoiceMemberItemReader(null, null, null))
                .processor(auditInvoiceMemberItemProcessor())
                .writer(new AuditInvoiceMemberItemWriter(
                        (InvoiceRepository) applicationContext.getBean("invoiceRepository"),
                        (InvoiceListValueRepository) applicationContext.getBean("invoiceListValueRepository"),
                        (MasterVehicleOfficeRepository) applicationContext.getBean("masterVehicleOfficeRepository"),
                        (InvoiceMasterHqRepository) applicationContext.getBean("invoiceMasterHqRepository"),
                        (MasterPlazaRepository) applicationContext.getBean("masterPlazaRepository"),
                        (InvoiceMasterLaneRepository) applicationContext.getBean("invoiceMasterLaneRepository"),
                        (AuditServiceImp) applicationContext.getBean("auditServiceImp")
                ))
                .build();
    }
}
