package com.appworks.co.th.batchworker.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service(value = "redisService")
public class RedisServiceImpl implements RedisService {
    @Value("${mflow.accesskey.timout:14400}")
    private long timeout;


    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value, timeout, TimeUnit.SECONDS);
    }

    @Override
    public Object get(String key) {
        try {
            return redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            log.error("Redis get : {}", e.getMessage());
            return null;
        }
    }


    @Override
    public Long getOpsForList(String key) {
        try {
            return redisTemplate.opsForList().size(key);
        } catch (Exception e) {
            log.error("Redis getOpsForList : {}", e.getMessage());
            return null;
        }
    }

    @Override
    public void setOpsForList(String key, Object invoiceObject) {
        try {
            redisTemplate.opsForList().rightPush(key, invoiceObject);
        } catch (Exception e) {
            log.error("Redis setOpsForList : {}", e.getMessage());
        }
    }

    @Override
    public boolean delete(String key) {
        try {
            return redisTemplate.delete(key);
        } catch (Exception e) {
            log.error("Redis get : {}", e.getMessage());
            return false;
        }
    }
}
