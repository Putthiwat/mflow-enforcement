package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceListValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface InvoiceListValueRepository extends JpaRepository<InvoiceListValue, Long>{

    //@Query(value = "SELECT * FROM  MF_INVOICE_LIST_VALUE WHERE  CODE = ? AND  TYPE = ?", nativeQuery = true)
    InvoiceListValue findByCodeAndType(String code, String type);
}
