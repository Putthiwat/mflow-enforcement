package com.appworks.co.th.batchworker.job.memberwarningduedate;

import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.batchworker.job.memberwarning1.MemberWarning1ItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceEvidence;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceTransaction;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;

@Configuration
public class MemberWarningDueDateJob {
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("memberWarningDueDateJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("memberWarningDueDateJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ApplicationContext applicationContext;

    public MemberWarningDueDateJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> memberWarningDueDateItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue
                + " and due_date = TO_DATE(\'"+currentDate()+"\', \'YYYY/MM/DD\') ";
        log.info(jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }

    @Bean("memberWarningDueDateItemProcessor")
    public ItemProcessor<Invoice, Invoice> memberWarningDueDateItemProcessor() {
        return new ItemProcessor<Invoice, Invoice>() {
            @Override
            public Invoice process(Invoice item) throws Exception {

                return item;
            }
        };
    }


    @Bean
    public Step memberWarningDueDateStep() throws Exception {
        return this.workerStepBuilderFactory.get("memberWarningDueDateStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, Invoice>chunk(1000)
                .reader(memberWarningDueDateItemReader(null,null, null))
                .processor(memberWarningDueDateItemProcessor())
                .writer(new MemberWarningDueDateItemWriter(
                        (NotificationManagementClient) applicationContext.getBean("notificationManagementClient"),
                        (CustomerClient) applicationContext.getBean("customerClient")
                ))
                .build();
    }

    private String currentDate(){
        Date date = DateUtils.addDays(new Date(),0);
        String currentDate = df.format(date);
        log.info("currentDate : {}", currentDate);
        return currentDate;
    }

}

