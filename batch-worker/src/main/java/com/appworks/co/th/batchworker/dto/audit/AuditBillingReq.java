package com.appworks.co.th.batchworker.dto.audit;


import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class AuditBillingReq {
    private String invoiceNo;
    private String invoiceRefNo;
    private InvoiceType invoiceType;
    private String transactionType;
    private String customerId;
    private String fullName;
    private String address;
    private String issueDate;
    private String dueDate;
    private BigDecimal feeAmount;
    private BigDecimal fineAmount;
    private BigDecimal collectionAmount;
    private BigDecimal totalAmount;
    private InvoiceStatus invoiceStatus;
    private String errorMessage;
    private List<InvoiceDetail> invoiceDetails;


}
