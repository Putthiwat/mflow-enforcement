package com.appworks.co.th.batchworker.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class OfficeNameDTO implements Serializable {

    private String code;
    private String descriptionTh;
    private String descriptionEn;

}
