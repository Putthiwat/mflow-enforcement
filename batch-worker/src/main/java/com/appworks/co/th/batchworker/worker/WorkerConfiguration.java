/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appworks.co.th.batchworker.worker;

import com.appworks.co.th.batchworker.config.BrokerConfiguration;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class WorkerConfiguration {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;


    public WorkerConfiguration(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
//    @Bean
//    public DirectChannel requests() {
//        return new DirectChannel();
//    }
//
//    @Bean
//    public IntegrationFlow inboundFlow(ActiveMQConnectionFactory connectionFactory) {
//        return IntegrationFlows
//                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("requests"))
//                .channel(requests())
//                .get();
//    }
//
//    /*
//     * Configure outbound flow (replies going to the master)
//     */
//    @Bean
//    public DirectChannel replies() {
//        return new DirectChannel();
//    }
//
//    @Bean
//    public IntegrationFlow outboundFlow(ActiveMQConnectionFactory connectionFactory) {
//        return IntegrationFlows
//                .from(replies())
//                .handle(Jms.outboundAdapter(connectionFactory).destination("replies"))
//                .get();
//    }
//

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "invoiceNonMemStagingJobRequests")
    public DirectChannel invoiceNonMemStagingJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceNonMemStagingJobRequestsInboundFlow")
    public IntegrationFlow invoiceNonMemStagingJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceNonMemStagingJobRequests"))
                .channel(invoiceNonMemStagingJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "invoiceNonMemStagingJobReplies")
    public DirectChannel invoiceNonMemStagingJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceNonMemStagingJobRepliesOutboundFlow")
    public IntegrationFlow invoiceNonMemStagingJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceNonMemStagingJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceNonMemStagingJobReplies"))
                .get();
    }



    @Bean(name = "invoiceStagingJobRequests")
    public DirectChannel invoiceStagingJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceStagingJobRequestsInboundFlow")
    public IntegrationFlow invoiceStagingJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceStagingJobRequests"))
                .channel(invoiceStagingJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "invoiceStagingJobReplies")
    public DirectChannel invoiceStagingJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceStagingJobRepliesOutboundFlow")
    public IntegrationFlow invoiceStagingJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceStagingJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceStagingJobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberWarning1JobRequests")
    public DirectChannel memberWarning1JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning1JobRequestsInboundFlow")
    public IntegrationFlow memberWarning1JobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberWarning1JobRequests"))
                .channel(memberWarning1JobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberWarning1JobReplies")
    public DirectChannel memberWarning1JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning1JobRepliesOutboundFlow")
    public IntegrationFlow memberWarning1JobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberWarning1JobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberWarning1JobReplies"))
                .get();
    }


    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberWarning2JobRequests")
    public DirectChannel memberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning2JobRequestsInboundFlow")
    public IntegrationFlow memberWarning2JobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberWarning2JobRequests"))
                .channel(memberWarning2JobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberWarning2JobReplies")
    public DirectChannel memberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning2JobRepliesOutboundFlow")
    public IntegrationFlow memberWarning2JobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberWarning2JobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberWarning2JobReplies"))
                .get();
    }


    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemberWarning1JobRequests")
    public DirectChannel nonMemberWarning1JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning1JobRequestsInboundFlow")
    public IntegrationFlow nonMemberWarning1JobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemberWarning1JobRequests"))
                .channel(nonMemberWarning1JobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemberWarning1JobReplies")
    public DirectChannel nonMemberWarning1JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning1JobRepliesOutboundFlow")
    public IntegrationFlow nonMemberWarning1JobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemberWarning1JobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemberWarning1JobReplies"))
                .get();
    }


    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemberWarning2JobRequests")
    public DirectChannel nonMemberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning2JobRequestsInboundFlow")
    public IntegrationFlow nonMemberWarning2JobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemberWarning2JobRequests"))
                .channel(nonMemberWarning2JobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemberWarning2JobReplies")
    public DirectChannel nonMemberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning2JobRepliesOutboundFlow")
    public IntegrationFlow nonMemberWarning2JobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemberWarning2JobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemberWarning2JobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberWarningDueDateJobRequests")
    public DirectChannel memberWarningDueDateJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarningDueDateJobRequestsInboundFlow")
    public IntegrationFlow memberWarningDueDateJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberWarningDueDateJobRequests"))
                .channel(memberWarningDueDateJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberWarningDueDateJobReplies")
    public DirectChannel memberWarningDueDateJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarningDueDateJobRepliesOutboundFlow")
    public IntegrationFlow memberWarningDueDateJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberWarningDueDateJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberWarningDueDateJobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceGenerateEBillRequests")
    public DirectChannel memberInvoiceGenerateEBillRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRequestsInboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRequests"))
                .channel(memberInvoiceGenerateEBillRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceGenerateEBillReplies")
    public DirectChannel memberInvoiceGenerateEBillReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRepliesOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateEBillReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateEBillReplies"))
                .get();
    }


    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceGenerateGroupRequests")
    public DirectChannel memberInvoiceGenerateGroupRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupRequestsInboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateGroupRequests"))
                .channel(memberInvoiceGenerateGroupRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceGenerateGroupReplies")
    public DirectChannel memberInvoiceGenerateGroupReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupRepliesOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateGroupReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateGroupReplies"))
                .get();
    }





    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "invoiceMemberWriterFileGenerateRequests")
    public DirectChannel invoiceMemberWriterFileGenerateRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceMemberWriterFileGenerateRequestsInboundFlow")
    public IntegrationFlow invoiceMemberWriterFileGenerateRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceMemberWriterFileGenerateRequests"))
                .channel(invoiceMemberWriterFileGenerateRequests())
                .get();
    }


    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "invoiceMemberWriterFileGenerateReplies")
    public DirectChannel invoiceMemberWriterFileGenerateReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceMemberWriterFileGenerateRepliesOutboundFlow")
    public IntegrationFlow invoiceMemberWriterFileGenerateRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceMemberWriterFileGenerateReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceMemberWriterFileGenerateReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "invoiceProceedListJobRequests")
    public DirectChannel invoiceProceedListJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedListJobRequestsInboundFlow")
    public IntegrationFlow invoiceProceedListJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceProceedListJobRequests"))
                .channel(invoiceProceedListJobRequests())
                .get();
    }


    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "invoiceProceedListJobReplies")
    public DirectChannel invoiceProceedListJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedListJobRepliesOutboundFlow")
    public IntegrationFlow invoiceProceedListJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceProceedListJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceProceedListJobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "invoiceProceedNonMemberListJobRequests")
    public DirectChannel invoiceProceedNonMemberListJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedNonMemberListJobRequestsInboundFlow")
    public IntegrationFlow invoiceProceedNonMemberListJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceProceedNonMemberListJobRequests"))
                .channel(invoiceProceedNonMemberListJobRequests())
                .get();
    }



    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "invoiceProceedNonMemberListJobReplies")
    public DirectChannel invoiceProceedNonMemberListJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedNonMemberListOutboundFlow")
    public IntegrationFlow invoiceProceedNonMemberListOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceProceedNonMemberListJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceProceedNonMemberListJobReplies"))
                .get();
    }


    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemGenerateEBillRequestsWarning1")
    public DirectChannel nonMemGenerateEBillRequestsWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRequestsWarning1InboundFlow")
    public IntegrationFlow nonMemGenerateEBillRequestsWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateEBillRequestsWarning1"))
                .channel(nonMemGenerateEBillRequestsWarning1())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemGenerateEBillRepliesWarning1")
    public DirectChannel nonMemGenerateEBillRepliesWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRepliesWarning1OutboundFlow")
    public IntegrationFlow nonMemGenerateEBillRepliesWarning1OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateEBillRepliesWarning1())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateEBillRepliesWarning1"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemGenerateEBillRequestsWarning2")
    public DirectChannel nonMemGenerateEBillRequestsWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRequestsWarning2InboundFlow")
    public IntegrationFlow nonMemGenerateEBillRequestsWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateEBillRequestsWarning2"))
                .channel(nonMemGenerateEBillRequestsWarning2())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemGenerateEBillRepliesWarning2")
    public DirectChannel nonMemGenerateEBillRepliesWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRepliesWarning2OutboundFlow")
    public IntegrationFlow nonMemGenerateEBillRepliesWarning2OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateEBillRepliesWarning2())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateEBillRepliesWarning2"))
                .get();
    }






    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning1")
    public DirectChannel memberInvoiceGenerateEBillRequestsWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning1InboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRequestsWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRequestsWarning1"))
                .channel(memberInvoiceGenerateEBillRequestsWarning1())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceGenerateEBillRepliesWarning1")
    public DirectChannel memberInvoiceGenerateEBillRepliesWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillWarning1RepliesOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillWarning1RepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateEBillRepliesWarning1())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRepliesWarning1"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning2")
    public DirectChannel memberInvoiceGenerateEBillRequestsWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning2InboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRequestsWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRequestsWarning2"))
                .channel(memberInvoiceGenerateEBillRequestsWarning2())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceGenerateEBillRepliesWarning2")
    public DirectChannel memberInvoiceGenerateEBillRepliesWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillWarning2RepliesOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillWarning2RepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateEBillRepliesWarning2())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRepliesWarning2"))
                .get();
    }






    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemGenerateGroupRequestsWarning2")
    public DirectChannel nonMemGenerateGroupRequestsWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRequestsWarning2InboundFlow")
    public IntegrationFlow nonMemGenerateGroupRequestsWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateGroupRequestsWarning2"))
                .channel(nonMemGenerateGroupRequestsWarning2())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemGenerateGroupRepliesWarning2")
    public DirectChannel nonMemGenerateGroupRepliesWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRepliesWarning2OutboundFlow")
    public IntegrationFlow nonMemGenerateGroupRepliesWarning2OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateGroupRepliesWarning2())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateGroupRepliesWarning2"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemGenerateGroupRequestsWarning1")
    public DirectChannel nonMemGenerateGroupRequestsWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRequestsWarning1InboundFlow")
    public IntegrationFlow nonMemGenerateGroupRequestsWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateGroupRequestsWarning1"))
                .channel(nonMemGenerateGroupRequestsWarning1())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemGenerateGroupRepliesWarning1")
    public DirectChannel nonMemGenerateGroupRepliesWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRepliesWarning1OutboundFlow")
    public IntegrationFlow nonMemGenerateGroupRepliesWarning1OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateGroupRepliesWarning1())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateGroupRepliesWarning1"))
                .get();
    }





    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceGenerateGroupWarning2Requests")
    public DirectChannel memberInvoiceGenerateGroupWarning2Requests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning2RequestsInboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning2RequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning2Requests"))
                .channel(memberInvoiceGenerateGroupWarning2Requests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceGenerateGroupWarning2Replies")
    public DirectChannel memberInvoiceGenerateGroupWarning2Replies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning2RepliesOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning2RepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateGroupWarning2Replies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning2Replies"))
                .get();
    }




    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceGenerateGroupWarning1Requests")
    public DirectChannel memberInvoiceGenerateGroupWarning1Requests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning1RequestsInboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning1RequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning1Requests"))
                .channel(memberInvoiceGenerateGroupWarning1Requests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceGenerateGroupWarning1Replies")
    public DirectChannel memberInvoiceGenerateGroupWarning1Replies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning1RepliesOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning1RepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateGroupWarning1Replies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning1Replies"))
                .get();
    }




    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "memberInvoiceProceedListBillTimeRequests")
    public DirectChannel memberInvoiceProceedListBillTimeRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceProceedListBillTimeRequestsInboundFlow")
    public IntegrationFlow memberInvoiceProceedListBillTimeRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceProceedListBillTimeRequests"))
                .channel(memberInvoiceProceedListBillTimeRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "memberInvoiceProceedListBillTimeReplies")
    public DirectChannel memberInvoiceProceedListBillTimeReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceProceedListBillTimeRepliesOutboundFlow")
    public IntegrationFlow memberInvoiceProceedListBillTimeRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceProceedListBillTimeReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceProceedListBillTimeReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "groupInvoiceNonmemberWarning2JobRequests")
    public DirectChannel groupInvoiceNonmemberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceNonmemberWarning2JobRequestsInboundFlow")
    public IntegrationFlow groupInvoiceNonmemberWarning2JobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("groupInvoiceNonmemberWarning2JobRequests"))
                .channel(groupInvoiceNonmemberWarning2JobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "groupInvoiceNonmemberWarning2JobReplies")
    public DirectChannel groupInvoiceNonmemberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceNonmemberWarning2JobRepliesOutboundFlow")
    public IntegrationFlow groupInvoiceNonmemberWarning2JobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(groupInvoiceNonmemberWarning2JobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("groupInvoiceNonmemberWarning2JobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "groupInvoiceMemberWarning2JobRequests")
    public DirectChannel groupInvoiceMemberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceMemberWarning2JobRequestsInboundFlow")
    public IntegrationFlow groupInvoiceMemberWarning2JobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("groupInvoiceMemberWarning2JobRequests"))
                .channel(groupInvoiceMemberWarning2JobRequests())
                .get();
    }


    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "groupInvoiceMemberWarning2JobReplies")
    public DirectChannel groupInvoiceMemberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceMemberWarning2JobRepliesOutboundFlow")
    public IntegrationFlow groupInvoiceMemberWarning2JobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(groupInvoiceMemberWarning2JobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("groupInvoiceMemberWarning2JobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "regenerateInvoiceMemberJobRequests")
    public DirectChannel regenerateInvoiceMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceMemberJobRequestsInboundFlow")
    public IntegrationFlow regenerateInvoiceMemberJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateInvoiceMemberJobRequests"))
                .channel(regenerateInvoiceMemberJobRequests())
                .get();
    }


    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "regenerateInvoiceMemberJobReplies")
    public DirectChannel regenerateInvoiceMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceMemberJobRepliesOutboundFlow")
    public IntegrationFlow regenerateInvoiceMemberJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateInvoiceMemberJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateInvoiceMemberJobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "regenerateInvoiceNonMemberJobRequests")
    public DirectChannel regenerateInvoiceNonMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceNonMemberJobRequestsInboundFlow")
    public IntegrationFlow regenerateInvoiceNonMemberJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateInvoiceNonMemberJobRequests"))
                .channel(regenerateInvoiceNonMemberJobRequests())
                .get();
    }


    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "regenerateInvoiceNonMemberJobReplies")
    public DirectChannel regenerateInvoiceNonMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceNonMemberJobRepliesOutboundFlow")
    public IntegrationFlow regenerateInvoiceNonMemberJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateInvoiceNonMemberJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateInvoiceNonMemberJobReplies"))
                .get();
    }






    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemGenerateEBillRequests")
    public DirectChannel nonMemGenerateEBillRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRequestsInboundFlow")
    public IntegrationFlow nonMemGenerateEBillRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateEBillRequests"))
                .channel(nonMemGenerateEBillRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemGenerateEBillReplies")
    public DirectChannel nonMemGenerateEBillReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRepliesOutboundFlow")
    public IntegrationFlow nonMemGenerateEBillRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateEBillReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateEBillReplies"))
                .get();
    }


    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "nonMemGenerateGroupRequests")
    public DirectChannel nonMemGenerateGroupRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRequestsInboundFlow")
    public IntegrationFlow nonMemGenerateGroupRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateGroupRequests"))
                .channel(nonMemGenerateGroupRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "nonMemGenerateGroupReplies")
    public DirectChannel nonMemGenerateGroupReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRepliesOutboundFlow")
    public IntegrationFlow nonMemGenerateGroupRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateGroupReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateGroupReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "mPassFTPFileJobRequests")
    public DirectChannel mPassFTPFileJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "mPassFTPFileJobRequestsInboundFlow")
    public IntegrationFlow mPassFTPFileJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("mPassFTPFileJobRequests"))
                .channel(mPassFTPFileJobRequests())
                .get();
    }


    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "mPassFTPFileJobReplies")
    public DirectChannel mPassFTPFileJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "mPassFTPFileJobRepliesOutboundFlow")
    public IntegrationFlow mPassFTPFileJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(mPassFTPFileJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("mPassFTPFileJobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "mPassRepayJobRequests")
    public DirectChannel mPassRepayJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "mPassRepayJobRequestsInboundFlow")
    public IntegrationFlow mPassRepayJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("mPassRepayJobRequests"))
                .channel(mPassRepayJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "mPassRepayJobReplies")
    public DirectChannel mPassRepayJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "mPassRepayJobRepliesOutboundFlow")
    public IntegrationFlow mPassRepayJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(mPassRepayJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("mPassRepayJobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "easyPassFTPFileJobRequests")
    public DirectChannel easyPassFTPFileJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "easyPassFTPFileJobRequestsInboundFlow")
    public IntegrationFlow easyPassFTPFileJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("easyPassFTPFileJobRequests"))
                .channel(easyPassFTPFileJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "easyPassFTPFileJobReplies")
    public DirectChannel easyPassFTPFileJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "easyPassFTPFileJobRepliesOutboundFlow")
    public IntegrationFlow easyPassFTPFileJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(easyPassFTPFileJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("easyPassFTPFileJobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "easyPassRepayJobRequests")
    public DirectChannel easyPassRepayJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "easyPassRepayJobRequestsInboundFlow")
    public IntegrationFlow easyPassRepayJobRequestsInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("easyPassRepayJobRequests"))
                .channel(easyPassRepayJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "easyPassRepayJobReplies")
    public DirectChannel easyPassRepayJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "easyPassRepayJobRepliesOutboundFlow")
    public IntegrationFlow easyPassRepayJobRepliesOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(easyPassRepayJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("easyPassRepayJobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "auditInvoiceMemberJobRequests")
    public DirectChannel auditInvoiceMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "auditInvoiceMemberJobRepliesInboundFlow")
    public IntegrationFlow auditInvoiceMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("auditInvoiceMemberJobRequests"))
                .channel(auditInvoiceMemberJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "auditInvoiceMemberJobReplies")
    public DirectChannel auditInvoiceMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "auditInvoiceMemberJobRequestsOutboundFlow")
    public IntegrationFlow auditInvoiceMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(auditInvoiceMemberJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("auditInvoiceMemberJobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "auditInvoiceNonMemberJobRequests")
    public DirectChannel auditInvoiceNonMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "auditInvoiceNonMemberJobRepliesInboundFlow")
    public IntegrationFlow auditInvoiceNonMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("auditInvoiceNonMemberJobRequests"))
                .channel(auditInvoiceNonMemberJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "auditInvoiceNonMemberJobReplies")
    public DirectChannel auditInvoiceNonMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "auditInvoiceNonMemberJobRequestsOutboundFlow")
    public IntegrationFlow auditInvoiceNonMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(auditInvoiceNonMemberJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("auditInvoiceNonMemberJobReplies"))
                .get();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "regenerateEBillFileMemberJobRequests")
    public DirectChannel regenerateEBillFileMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateEBillFileMemberJobRequestsOutboundFlow")
    public IntegrationFlow regenerateEBillFileMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateEBillFileMemberJobRequests"))
                .channel(regenerateEBillFileMemberJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "regenerateEBillFileMemberJobReplies")
    public DirectChannel regenerateEBillFileMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateEBillFileMemberJobRepliesInboundFlow")
    public IntegrationFlow regenerateEBillFileMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateEBillFileMemberJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateEBillFileMemberJobReplies"))
                .get();
    }



    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean(name = "regenerateEBillFileNonMemberJobRequests")
    public DirectChannel regenerateEBillFileNonMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateEBillFileNonMemberJobRequestsOutboundFlow")
    public IntegrationFlow regenerateEBillFileNonMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateEBillFileNonMemberJobRequests"))
                .channel(regenerateEBillFileMemberJobRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean(name = "regenerateEBillFileNonMemberJobReplies")
    public DirectChannel regenerateEBillFileNonMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateEBillFileNonMemberJobRepliesInboundFlow")
    public IntegrationFlow regenerateEBillFileNonMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateEBillFileMemberJobReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateEBillFileNonMemberJobReplies"))
                .get();
    }
}
