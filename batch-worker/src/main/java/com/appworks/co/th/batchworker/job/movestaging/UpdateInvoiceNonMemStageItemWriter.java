package com.appworks.co.th.batchworker.job.movestaging;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.commons.DomainEvent;
import com.appworks.co.th.batchworker.config.BillingMessagePublisher;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.payload.PaymentObj;
import com.appworks.co.th.customerservice.PaymentTypeRequest;
import com.appworks.co.th.customerservice.PaymentTypeResponse;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.InvoiceUpdateDomainEvent;
import com.appworks.co.th.sagaappworks.invoice.InvoiceUpdateHeader;
import com.appworks.co.th.sagaappworks.invoice.UpdateStatusInvoiceCommand;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
public class UpdateInvoiceNonMemStageItemWriter implements ItemWriter<InvoiceNonMember> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private MessagePublisher messagePublisher;

    private BillingMessagePublisher billingMessagePublisher;

    private MasterClient masterClient;

    private CustomerClient customerClient;

    private InvoiceNonMemberRepository invoiceNonMemberRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void write(List<? extends InvoiceNonMember> items) throws Exception {

        items.stream().forEach(invoice -> {
            InvoiceNonMember invoiceNonMemberAfterSave = null;
            try {
                invoiceNonMemberAfterSave = invoiceNonMemberRepository.saveAndFlush(invoice);
            }catch (Exception e){
                log.error("Error write UpdateInvoiceNonMemStageItemWriter : {} ", e);
            }finally {
                try{
                    if(invoiceNonMemberAfterSave!=null && invoiceNonMemberAfterSave.getId()!=null)
                        callBackToInvoice(invoice);
                }catch (Exception e){
                    log.error("Error send updateInvoiceNonmemberStagingService: {} ", e);
                }
            }
        });

    }

    private void callBackToInvoice(InvoiceNonMember invoice) {
        InvoiceUpdateHeader updateHeader = new InvoiceUpdateHeader();

        updateHeader.setStatus(Constants.Status.MOVED);
        updateHeader.setChannel("Batch-service");
        updateHeader.setUpdateBy("Batch-service");
        updateHeader.setInvoiceNo(invoice.getInvoiceNo());
        updateHeader.setType("NON MEMBER");
        InvoiceUpdateDomainEvent domainEvent = new InvoiceUpdateDomainEvent<>(invoice.getId(), UpdateStatusInvoiceCommand.class, CommonState.APPROVED, updateHeader);
        log.info("callBackToInvoice {}", domainEvent);
        messagePublisher.sendDomainEvent("updateInvoiceStagingService", domainEvent);

    }
}