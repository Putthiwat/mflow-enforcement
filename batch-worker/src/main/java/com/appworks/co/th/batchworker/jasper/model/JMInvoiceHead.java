package com.appworks.co.th.batchworker.jasper.model;

import lombok.Data;

@Data
public class JMInvoiceHead {
    private String plate;
    private String provinceName;
    private String feeAmount;
    private String fineAmount;
    private String collectionAmount;
    private String totalAmount;
    private String operationAmount;
    private String vat;
}
