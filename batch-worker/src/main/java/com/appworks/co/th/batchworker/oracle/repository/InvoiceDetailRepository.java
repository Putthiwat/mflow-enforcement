package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface InvoiceDetailRepository extends JpaRepository<InvoiceDetail, Long> {

    @Query(value = "SELECT * FROM MF_INVOICE_DETAIL WHERE INVOICE_NO = ?1 AND DELETE_FLAG = 0",nativeQuery = true)
    List<InvoiceDetail> findAllByInvoiceNo(String invoiceNo);

    List<InvoiceDetail> findAllByInvoice(Invoice invoice);
}
