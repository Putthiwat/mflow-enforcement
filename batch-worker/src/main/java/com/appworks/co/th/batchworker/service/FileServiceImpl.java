package com.appworks.co.th.batchworker.service;

import com.appworks.co.th.batchworker.grpc.FileClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.mflow.fileservice.FileRequest;
import com.appworks.co.th.mflow.fileservice.FileResponse;
import com.google.protobuf.ByteString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service(value = "fileService")
public class FileServiceImpl implements FileService {

    @Autowired
    FileClient fileClient;

    @Override
    public String uploadFile(byte[] bytes, String extension, String fileId) {
        FileRequest request = FileRequest.newBuilder()
            .setSystem("batch-service")
            .setFileId(fileId)
            .setExtension(extension)
            .setFile(ByteString.copyFrom(bytes))
            .build();
        FileResponse response = fileClient.uploadFile(request);
        return response.getFileId();
    }
}
