package com.appworks.co.th.batchworker.service;

import com.appworks.co.th.batchworker.payload.*;
import com.appworks.co.th.getserviceprovider.GetServiceProviderResponse;
import com.appworks.co.th.masterservice.ServiceProvider;

import java.util.List;
import java.util.WeakHashMap;

public interface CacheService {

    ServiceProvider getMasterProvider(String code);

    OfficeNameDTO getOfficeName(String code);

    ColorNameDTO getColorNameByCodes(List<String> codes);

    HQNameDTO getHQName(String code);

    PlazaNameDTO getPlazaName(String code);

    BrandNameDTO getBrandName(String code);

    LaneNameDTO getLaneName(String code);

    String getIdentityLast4Digit(String customerId);

    String getPhone(String customerId);

    GetServiceProviderResponse getProvider(String code, String lang);

}
