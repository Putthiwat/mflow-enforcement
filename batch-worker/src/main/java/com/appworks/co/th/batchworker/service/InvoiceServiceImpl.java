package com.appworks.co.th.batchworker.service;

import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeRequest;
import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeResponse;
import com.appworks.co.th.batchworker.grpc.BarCodeClient;
import com.appworks.co.th.batchworker.grpc.QrCodeClient;
import com.appworks.co.th.qrcodeallservice.MasterQrDataAllResponse;
import com.appworks.co.th.qrcodeallservice.QrCodeMemberRequest;
import com.appworks.co.th.qrcodeallservice.QrCodeNonMemberRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    private QrCodeClient qrClient;

    @Autowired
    private BarCodeClient barClient;

    @Override
    public String getQrCodeMemberBase64(String invoiceNo) {
        QrCodeMemberRequest request = QrCodeMemberRequest.newBuilder().setInvoiceNo(invoiceNo).build();
        MasterQrDataAllResponse response = qrClient.getQrCodeBotMember(request);
        return response.getQr();
    }

    @Override
    public String getQrCodeNonMemberBase64(String invoiceNo) {
        QrCodeNonMemberRequest request = QrCodeNonMemberRequest.newBuilder().setInvoiceNo(invoiceNo).build();
        MasterQrDataAllResponse response = qrClient.getQrCodeBotNonMember(request);
        return response.getQr();
    }

    @Override
    public String getBarCodeMemberBase64(String invoiceNo) {
        InvoiceBarCodeRequest request = InvoiceBarCodeRequest.newBuilder().setInvoiceNo(invoiceNo).build();
        InvoiceBarCodeResponse response = barClient.getBarCodeMember(request);
        return response.getBarcode();
    }

    @Override
    public String getBarCodeNonMemberBase64(String invoiceNo) {
        InvoiceBarCodeRequest request = InvoiceBarCodeRequest.newBuilder().setInvoiceNo(invoiceNo).build();
        InvoiceBarCodeResponse response = barClient.getBarCodeNonMember(request);
        return response.getBarcode();
    }

}
