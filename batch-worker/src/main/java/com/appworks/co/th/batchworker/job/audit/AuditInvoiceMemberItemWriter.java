package com.appworks.co.th.batchworker.job.audit;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.audit.*;
import com.appworks.co.th.batchworker.dto.audit.InvoiceDetail;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@AllArgsConstructor
public class AuditInvoiceMemberItemWriter implements ItemWriter<Invoice> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private InvoiceRepository invoiceRepository;

    private InvoiceListValueRepository invoiceListValueRepository;

    private MasterVehicleOfficeRepository masterVehicleOfficeRepository;

    private InvoiceMasterHqRepository invoiceMasterHqRepository;

    private MasterPlazaRepository masterPlazaRepository;

    private InvoiceMasterLaneRepository invoiceMasterLaneRepository;


    private AuditServiceImp auditServiceImp;

    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        items.stream().forEach(a -> {
            try {
                Long id = a.getId();
                log.info("Start submit audit billing with id : {}",id);
                auditServiceImp.auditBillingWithId(id, "MEMBER",setAuditBilling(a));

            } catch (Exception e) {
                log.error("AuditInvoiceMemberItemWriter error : {}", e);
            }
        });
    }
    private AuditBillingReq setAuditBilling(Invoice invoice){
        /*Optional<Invoice> invoiceById = invoiceRepository.findById(id);*/
        AuditBillingReq auditBillingReq = new AuditBillingReq();
        //if(invoiceById.isPresent()){
            /*Invoice invoice = invoiceById.get();*/
            if (invoice != null){
                auditBillingReq.setInvoiceNo(invoice.getInvoiceNo());
                auditBillingReq.setInvoiceRefNo(invoice.getInvoiceNoRef()!=null?invoice.getInvoiceNoRef():null);
                InvoiceType invoiceType = new InvoiceType();
                if(invoice.getInvoiceType()!=null){
                    invoiceType.setCode(invoice.getInvoiceType().toString());
                    log.debug("invoice.getInvoiceType ----> :{}",invoice.getInvoiceType().toString());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoice.getInvoiceType().toString(), "T000012");
                    if (invoiceListValue != null) {
                        invoiceType.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                    }
                }
                auditBillingReq.setInvoiceType(invoiceType);
                auditBillingReq.setTransactionType("MEMBER");
                auditBillingReq.setCustomerId(invoice.getCustomerId()!=null?invoice.getCustomerId():null);
                auditBillingReq.setFullName(invoice.getFullName()!=null?invoice.getFullName():null);
                auditBillingReq.setAddress(invoice.getAddress()!=null?invoice.getAddress():null);

                if(invoice.getIssueDate()!=null)
                    auditBillingReq.setIssueDate(DateUtils.issueDateDueDateAudit(invoice.getIssueDate()));
                if(invoice.getDueDate()!=null)
                auditBillingReq.setDueDate(DateUtils.issueDateDueDateAudit(invoice.getDueDate()));

                auditBillingReq.setFeeAmount(invoice.getFeeAmount()!=null?invoice.getFeeAmount():null);
                auditBillingReq.setFineAmount(invoice.getFineAmount()!=null?invoice.getFineAmount():null);
                auditBillingReq.setCollectionAmount(invoice.getCollectionAmount()!=null?invoice.getCollectionAmount():null);
                auditBillingReq.setTotalAmount(invoice.getTotalAmount()!=null?invoice.getTotalAmount():null);
                InvoiceStatus invoiceStatus = new InvoiceStatus();
                if(invoice.getStatus()!=null){
                    invoiceStatus.setCode(invoice.getStatus());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoice.getStatus(), "T000013");
                    if (invoiceListValue != null) {
                        invoiceStatus.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                    }
                }
                auditBillingReq.setInvoiceStatus(invoiceStatus);
                auditBillingReq.setErrorMessage(invoice.getErrorMessage()!=null?invoice.getErrorMessage():null);
                auditBillingReq.setInvoiceDetails(invoice.getDetails().stream().map(item ->{
                    com.appworks.co.th.batchworker.dto.audit.InvoiceDetail invoiceDetail = new com.appworks.co.th.batchworker.dto.audit.InvoiceDetail();
                    invoiceDetail.setInvoiceNo(item.getInvoice().getInvoiceNo());
                    invoiceDetail.setTransactionId(item.getTransactionId());
                    invoiceDetail.setTransactionDate(DateUtils.transactionDate(item.getTransactionDate()));
                    invoiceDetail.setPlate1(item.getPlate1());
                    invoiceDetail.setPlate2(item.getPlate2());
                    Province province = new Province();
                    if(item.getProvince()!=null){
                        province.setCode(item.getProvince());
                        InvoiceMasterVehicleOffice invoiceMasterVehicleOffice = masterVehicleOfficeRepository.findByCode(item.getProvince());
                        if(invoiceMasterVehicleOffice!=null){
                            province.setDescription(invoiceMasterVehicleOffice.getDescription());
                        }
                    }
                    invoiceDetail.setProvice(province);
                    Origin origin = new Origin();
                    Hq hqOrigin = new Hq();
                    if(item.getHqCode()!=null){
                        hqOrigin.setCode(item.getHqCode());
                        InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getHqCode());
                        if(invoiceMasterHq!=null){
                            hqOrigin.setName(invoiceMasterHq.getDescription());
                        }
                    }
                    Plaza plazaOrigin = new Plaza();
                    if(item.getPlazaCode()!=null){
                        plazaOrigin.setCode(item.getPlazaCode());
                        InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getPlazaCode());
                        if(invoiceMasterPlaza!=null){
                            plazaOrigin.setName(invoiceMasterPlaza.getDescription());
                        }
                    }
                    Lane laneOrigin = new Lane();
                    if(item.getLaneCode()!=null){
                        laneOrigin.setCode(item.getLaneCode());
                        InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getLaneCode());
                        if(invoiceMasterLane!=null){
                            laneOrigin.setName(invoiceMasterLane.getDescription());
                        }
                    }
                    origin.setHq(hqOrigin);
                    origin.setLane(laneOrigin);
                    origin.setPlaza(plazaOrigin);
                    invoiceDetail.setOrigin(origin);
                    Destination destination = new Destination();
                    Hq hqDes = new Hq();
                    if(item.getDestHqCode()!=null){
                        hqDes.setCode(item.getDestHqCode());
                        InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getDestHqCode());
                        if(invoiceMasterHq!=null){
                            hqDes.setName(invoiceMasterHq.getDescription());
                        }
                    }
                    Plaza plazaDes = new Plaza();
                    if(item.getDestPlazaCode()!=null){
                        plazaDes.setCode(item.getDestPlazaCode());
                        InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getDestPlazaCode());
                        if(invoiceMasterPlaza!=null){
                            plazaDes.setName(invoiceMasterPlaza.getDescription());
                        }
                    }
                    Lane laneDes = new Lane();
                    if(item.getDestLaneCode()!=null){
                        laneDes.setCode(item.getDestLaneCode());
                        InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getDestLaneCode());
                        if(invoiceMasterLane!=null){
                            laneDes.setName(invoiceMasterLane.getDescription());
                        }
                    }
                    destination.setHq(hqDes);
                    destination.setLane(laneDes);
                    destination.setPlaza(plazaDes);
                    invoiceDetail.setDestination(destination);
                    invoiceDetail.setFeeAmount(item.getFeeAmount());
                    invoiceDetail.setFineAmount(item.getFineAmount());
                    invoiceDetail.setCollectionAmount(item.getCollectionAmount());
                    invoiceDetail.setTotalAmount(item.getTotalAmount());
                    OriginTranType originTranType = new OriginTranType();
                    if(item.getOriginTranType()!=null){
                        originTranType.setCode(item.getOriginTranType());
                        InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(item.getOriginTranType(), "T000018");
                        if (invoiceListValue != null) {
                            originTranType.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                        }
                    }
                    invoiceDetail.setOriginTranType(originTranType);
                    invoiceDetail.setEvidences(item.getEvidences().stream().map(e ->{
                        Evidences evidences = new Evidences();
                        evidences.setType(e.getType());
                        evidences.setFileId(e.getFile());
                        return evidences;
                    }).collect(Collectors.toList()));
                    return invoiceDetail;
                }).collect(Collectors.toList()));
            }
        //}
        return auditBillingReq;
    }
}
