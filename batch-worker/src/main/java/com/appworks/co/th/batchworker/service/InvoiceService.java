package com.appworks.co.th.batchworker.service;

public interface InvoiceService {

    String getQrCodeMemberBase64(String invoiceNo);

    String getQrCodeNonMemberBase64(String invoiceNo);

    String getBarCodeMemberBase64(String invoiceNo);

    String getBarCodeNonMemberBase64(String invoiceNo);

}
