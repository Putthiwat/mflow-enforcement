package com.appworks.co.th.batchworker.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
public class BillingMessagePublisher {

    private static final Logger log = LoggerFactory.getLogger(BillingMessagePublisher.class);

    @Autowired
    @Qualifier("billKafkaTemplate")
    private KafkaTemplate<String, String> billKafkaTemplate;

    public ListenableFuture<SendResult<String, String>> sendDomainEvent(String channel, String domainEventMessage) {
        ListenableFuture<SendResult<String, String>> future = billKafkaTemplate.send(channel, domainEventMessage);
        return future;

    }

}