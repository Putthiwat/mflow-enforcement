package com.appworks.co.th.batchworker.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerIdentityDTO implements Serializable  {

    private String citizenId;
    private String passportId;
    private String registrationId;

}
