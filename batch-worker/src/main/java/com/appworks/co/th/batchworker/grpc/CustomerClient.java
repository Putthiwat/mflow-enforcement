package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.customerservice.*;
import com.appworks.co.th.customerwriterfilepayment.CustomerWriterFilePaymentGrpc;
import com.appworks.co.th.customerwriterfilepayment.PaymentMethodForWriterFileRequest;
import com.appworks.co.th.customerwriterfilepayment.PaymentMethodForWriterFileResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

@Service
public class CustomerClient {

    private static final Logger logger = LoggerFactory.getLogger(CustomerClient.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;

    private static String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);


    @Value("${grpc.customerservice.host}")
    private String host;
    @Value("${grpc.customerservice.port}")
    private String port;

    private CustomerServiceGrpc.CustomerServiceBlockingStub customerServiceStub;

    private CustomerVehicleInfoGrpc.CustomerVehicleInfoBlockingStub customerVehicleInfoBlockingStub;

    private CustomerWriterFilePaymentGrpc.CustomerWriterFilePaymentBlockingStub customerWriterFilePaymentBlockingStub;
    @Autowired
    public void setCustomerServiceStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        CustomerServiceGrpc.CustomerServiceBlockingStub stub = CustomerServiceGrpc.newBlockingStub(channel);
        this.customerServiceStub = stub;
    }


    @Autowired
    public void setCustomerVehicleInfoBlockingStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port))
                .defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        CustomerVehicleInfoGrpc.CustomerVehicleInfoBlockingStub stub = CustomerVehicleInfoGrpc.newBlockingStub(channel);
        this.customerVehicleInfoBlockingStub = stub;
    }

    @Autowired
    public void customerWriterFilePaymentStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        CustomerWriterFilePaymentGrpc.CustomerWriterFilePaymentBlockingStub stub = CustomerWriterFilePaymentGrpc.newBlockingStub(channel);
        this.customerWriterFilePaymentBlockingStub = stub;
    }

    public PaymentTypeResponse getPaymentMethod(PaymentTypeRequest request){
        PaymentTypeResponse response = customerServiceStub.getPaymentMethod(request);
        return response;
    }

    public PaymentTypeResponse getPaymentMethodByVehicleCode(PaymentTypeByVehicleCodeRequest request){
        PaymentTypeResponse response = customerServiceStub.getPaymentMethodByVehicleCode(request);
        return response;
    }

    public String getCustomerNotificationInfo(String customerId){
        CustomerNotificationInfoRequest.Builder req =CustomerNotificationInfoRequest.newBuilder();
        req.setCustomerId(customerId);
        CustomerNotificationInfoResponse response = customerServiceStub.getCustomerNotificationInfo(req.build());
        return response.getLanguage();
    }

    public GetBrandResponse getBrand(GetBrandRequest request) {
        GetBrandResponse response = this.customerVehicleInfoBlockingStub.getBrand(request);
        return response;
    }


    public PaymentMethodForWriterFileResponse getPaymentMethodForWriterFile(PaymentMethodForWriterFileRequest request) {
        PaymentMethodForWriterFileResponse response = this.customerWriterFilePaymentBlockingStub.getPaymentMethodForWriterFile(request);
        return response;
    }


}
