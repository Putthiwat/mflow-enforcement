package com.appworks.co.th.batchworker.config.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@Validated
@ConfigurationProperties("mflow.async.log")
public class AsyncLogProperty {

    private static final Integer DEFAULT_CORE_POOL_SIZE = 4;
    private static final Integer DEFAULT_MAX_POOL_SIZE = 8;
    private static final Integer DEFAULT_QUEUE = Integer.MAX_VALUE;
    private static final String PREFIX_NAME = "AsyncBatchLog--";
    /**
     * Set the core number of threads.
     */
    @NotNull
    @Min(1)
    @Max(16)
    private Integer corePoolSize = DEFAULT_CORE_POOL_SIZE;

    /**
     * Set the capacity of the queue.
     */
    private Integer queueCapacity = DEFAULT_QUEUE;
    /**
     * Set the maximum allowed number of threads.
     */
    private Integer maxPoolSize = DEFAULT_MAX_POOL_SIZE;
    /**
     * Set the prefix to use for the names of newly created threads.
     */
    private String threadNamePrefix = PREFIX_NAME;
}
