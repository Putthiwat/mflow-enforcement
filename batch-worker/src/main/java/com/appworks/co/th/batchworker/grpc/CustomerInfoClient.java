package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeServiceGrpc;
import com.appworks.co.th.customerinfoservice.CustomerInfoRequest;
import com.appworks.co.th.customerinfoservice.CustomerInfoServiceGrpcGrpc;
import com.appworks.co.th.customerinfoservice.MasterCustomerDataResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

@Service
public class CustomerInfoClient {
    
    private static final Logger logger = LoggerFactory.getLogger(CustomerInfoClient.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;

    private static String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);


    @Value("${grpc.customerservice.host}")
    private String host;
    @Value("${grpc.customerservice.port}")
    private int port;

    private CustomerInfoServiceGrpcGrpc.CustomerInfoServiceGrpcBlockingStub grpcBlockingStub;


    @Autowired
    public void startConsumer() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        CustomerInfoServiceGrpcGrpc.CustomerInfoServiceGrpcBlockingStub stub = CustomerInfoServiceGrpcGrpc.newBlockingStub(channel);
        this.grpcBlockingStub = stub;
    }


    public MasterCustomerDataResponse getCustomerInfo(String customerId){
        CustomerInfoRequest.Builder request = CustomerInfoRequest.newBuilder();
        request.setLanguage("EN");
        request.setCustomerId(customerId);
        MasterCustomerDataResponse response = grpcBlockingStub.getCustomerInfo(request.build());
        return response;
    }


}
