package com.appworks.co.th.batchworker.job.generatememebillwarning2;

import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.batchworker.job.invoicegenerateebill.InvoiceGenerateEBillItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.service.CacheService;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.GrpcSendSmsAndEmailService;
import com.appworks.co.th.batchworker.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;

@Configuration
public class InvoiceGenerateEBillItemWaring2Writer {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    @Value("${job.generate-invoice.max-retry:3}")
    private int maxRetry;

    @Value("${job.generate-invoice.retry-delay:500}")
    private int retryDelay;

    @Value("${send.notification.ebill.flag}")
    private boolean sendNotificationFlag;

    @Value("${ebill.billcycle.url}")
    private String EbillLink;

    @Value("${AccrualList.billcycle.url}")
    private String AccrualListLink;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;


    @Autowired
    @Qualifier("memberInvoiceGenerateEBillRequestsWarning2")
    private DirectChannel requests;

    @Autowired
    @Qualifier("memberInvoiceGenerateEBillRepliesWarning2")
    private DirectChannel replies;

    public InvoiceGenerateEBillItemWaring2Writer(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> invoiceUpdateEBillWaring2ItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue+ " and invoiceType=2";
        log.info(jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("invoiceUpdateEBillWaring2ItemProcessor")
    public ItemProcessor<Invoice, Invoice> invoiceUpdateEBillWaring2ItemProcessor() {
        return item -> item;
    }


    @Bean
    public Step updateInvoiceEBillWarning2Step() throws Exception {
        return this.workerStepBuilderFactory.get("updateInvoiceEBillWarning2Step")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, Invoice>chunk(1000)
                .reader(invoiceUpdateEBillWaring2ItemReader(null,null, null))
                .processor(invoiceUpdateEBillWaring2ItemProcessor())
                .writer(new InvoiceGenerateEBillItemWriter(
                        EbillLink,sendNotificationFlag,AccrualListLink,maxRetry,
                        (CacheService) applicationContext.getBean("cacheService"),
                        (FileService) applicationContext.getBean("fileService"),
                        (ReportService) applicationContext.getBean("reportService"),
                        (InvoiceRepository) applicationContext.getBean("invoiceRepository"),
                        (NotificationManagementClient) applicationContext.getBean("notificationManagementClient"),
                        (GrpcSendSmsAndEmailService) applicationContext.getBean("grpcSendSmsAndEmailService"),
                        (CustomerInfoClient) applicationContext.getBean("customerInfoClient"),
                        (CustomerClient) applicationContext.getBean("customerClient")

                ))
                .build();
    }
}
