package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class AuditTransResponseHeader {
    private String TransactionId;
    private String RequestDate;
    private String ResponseDate;
}
