package com.appworks.co.th.batchworker.payload;

import lombok.Data;

@Data
public class MapIssueDateDTO {
    private boolean status=false;
    private String invoiceChannel;
    private String paymentMethod;
}
