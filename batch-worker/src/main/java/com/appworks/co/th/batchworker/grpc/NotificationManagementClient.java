package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.notificationmanagement.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class NotificationManagementClient {
    @Value("${grpc.notification-management-command.host}")
    private String host;
    @Value("${grpc.notification-management-command.port}")
    private String port;

    private NotificationManagementGrpc.NotificationManagementBlockingStub notificationManagementBlockingStub;

    @Autowired
    public void setNotificationManagementBlockingStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        NotificationManagementGrpc.NotificationManagementBlockingStub stub = NotificationManagementGrpc.newBlockingStub(channel);
        this.notificationManagementBlockingStub = stub;
    }

    public NotificationManagementResponse invoiceWarningDueDateNotification(NotificationManagementRequest request){
        NotificationManagementResponse response = notificationManagementBlockingStub.invoiceWarningDueDateNotification(request);
        return response;
    }


    public SmsReminderPaymentNotificationResponse smsReminderPaymentNotification(SmsReminderPaymentNotificationRequest request){
        SmsReminderPaymentNotificationResponse response = notificationManagementBlockingStub.smsReminderPaymentNotification(request);
        return response;
    }

    public SmsOTPNotificationResponse smsOTPNotification(SmsOTPNotificationRequest request){
        SmsOTPNotificationResponse response = notificationManagementBlockingStub.smsOTPNotification(request);
        return response;
    }
    public EmailVerifyNotificationResponse emailVerifyNotification(EmailVerifyNotificationRequest request){
        EmailVerifyNotificationResponse response = notificationManagementBlockingStub.emailVerifyNotification(request);
        return response;
    }

}
