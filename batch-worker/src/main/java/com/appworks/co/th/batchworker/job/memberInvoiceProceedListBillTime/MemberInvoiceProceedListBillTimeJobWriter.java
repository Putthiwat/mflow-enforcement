package com.appworks.co.th.batchworker.job.memberInvoiceProceedListBillTime;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.dto.InvoiceProceedListDTO;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.batchworker.payload.MapIssueDateDTO;
import com.appworks.co.th.batchworker.service.invoice.InvoiceNonMemberService;
import com.appworks.co.th.batchworker.service.invoice.InvoiceStageService;
import com.appworks.co.th.batchworker.service.issueday.InvoiceCommonService;
import com.appworks.co.th.customerservice.Payment;
import com.appworks.co.th.customerservice.PaymentTypeRequest;
import com.appworks.co.th.customerservice.PaymentTypeResponse;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceDetail;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceHeader;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import com.appworks.co.th.sagaappworks.invoice.CreatedInvoiceStagingDomainEvent;
import com.appworks.co.th.sagaappworks.invoice.InvoiceMemberCommand;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
public class MemberInvoiceProceedListBillTimeJobWriter implements ItemWriter<InvoiceProceedListDTO> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MessagePublisher messagePublisher;
    @Autowired
    private InvoiceStageService invoiceStageService;

    private CustomerClient customerClient;

    private InvoiceCommonService invoiceCommonService;

    @Override
    public void write(List<? extends InvoiceProceedListDTO> items) throws Exception {
        items.stream().forEach(invoiceObject -> {
            this.writerInvoiceStaging(invoiceObject);


            log.info("Data MemberInvoiceProceedListBillTimeJobWriter : {}", invoiceObject);
        });
    }

    private MapIssueDateDTO mapIssueDate(CreateInvoiceDetail createInvoiceDetail, CreateInvoiceHeader createInvoiceHeader) {
        PaymentTypeResponse customerInfo = null;
        try {
            customerInfo = customerClient.getPaymentMethod(PaymentTypeRequest.newBuilder().setPlate1(createInvoiceHeader.getPlate1()).setPlate2(createInvoiceHeader.getPlate2()).setProvince(createInvoiceHeader.getPlateProvince()).build());
            log.info("customerClient res {}", customerInfo);
        } catch (Exception e) {
            customerInfo = PaymentTypeResponse.newBuilder().setPayment(Payment.newBuilder().build()).build();
        }
        MapIssueDateDTO mapIssueDateDTO = new MapIssueDateDTO();
        if (customerInfo.getStatus() && customerInfo.getPayment() != null) {
            mapIssueDateDTO.setInvoiceChannel(customerInfo.getPayment().getInvoiceChannel());
            mapIssueDateDTO.setPaymentMethod(customerInfo.getPayment().getType());
        }
        if (Constants.BillCycle.BILL_TIME.equals(mapIssueDateDTO.getInvoiceChannel()) && !"1".equals(mapIssueDateDTO.getPaymentMethod())) {
            createInvoiceHeader.setIssueDate(invoiceCommonService.issueDateBillTime(createInvoiceDetail));
            mapIssueDateDTO.setStatus(true);
            return mapIssueDateDTO;
        } else {
            return mapIssueDateDTO;
        }
    }

    private void writerInvoiceStaging(InvoiceProceedListDTO invoiceProceedMemberDTO) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
        Long id = null;
        MapIssueDateDTO mapIssueDate = null;
        CreateInvoiceDetail createInvoiceDetail = null;
        CreateInvoiceHeader createInvoiceHeader = null;
        try {
            Map<String, Object> transaction = objectMapper.readValue(invoiceProceedMemberDTO.getDetail(), new TypeReference<Map<String, Object>>() {
            });
            Map<String, Object> header = (Map<String, Object>) transaction.get("header");
            Map<String, Object> detail = (Map<String, Object>) transaction.get("detail");
            createInvoiceDetail = objectMapper.convertValue(detail, CreateInvoiceDetail.class);
            createInvoiceHeader = objectMapper.convertValue(header, CreateInvoiceHeader.class);
            String jobId = Objects.toString(transaction.get("id"), null);
            if (StringUtils.isNotEmpty(jobId)) {
                id = Long.valueOf(jobId);
            }
            mapIssueDate = mapIssueDate(createInvoiceDetail, createInvoiceHeader);
        } catch (IOException e) {
            log.info("Error IOException writerInvoiceStaging : {}", e.getStackTrace());
        } catch (Exception e) {
            log.info("Error Exception writerInvoiceStaging : {}", e.getMessage());
        }
        if (mapIssueDate.isStatus()) {
            try {
                resultInvoiceDetail = invoiceStageService.insertInvoice(invoiceProceedMemberDTO.getInvoiceNo(), createInvoiceHeader, createInvoiceDetail, mapIssueDate.getInvoiceChannel());

                if(resultInvoiceDetail!=null){
                    resultInvoiceDetail.setStatus(true);
                    resultInvoiceDetail.setMessage("Success");

                    ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                    resultInvoiceHeader.setType("MEMBER");

                    CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.APPROVED, resultInvoiceHeader, resultInvoiceDetail);
                    createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                    createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                    messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
                }else{
                    resultInvoiceDetail = new ResultInvoiceDetail();
                    resultInvoiceDetail.setStatus(false);
                    resultInvoiceDetail.setMessage("Cannot insert invoice member.");

                    ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                    resultInvoiceHeader.setType("MEMBER");

                    CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
                    createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                    createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                    messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
                }


            } catch (Exception e) {
                resultInvoiceDetail = new ResultInvoiceDetail();
                resultInvoiceDetail.setStatus(false);
                resultInvoiceDetail.setMessage(ExceptionUtils.getStackTrace(e));

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("MEMBER");

                CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
                createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedMemberDTO.getInvoiceProceedID());
                createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedMemberDTO.getId());
                messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
            }
        }
    }

}
