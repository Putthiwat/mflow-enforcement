package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeRequest;
import com.appworks.co.th.barcodeinvoiceservice.InvoiceBarCodeResponse;
import com.appworks.co.th.masterservice.MasterServiceGrpc;
import com.appworks.co.th.mflow.fileservice.FileRequest;
import com.appworks.co.th.mflow.fileservice.FileResponse;
import com.appworks.co.th.mflow.fileservice.FileServiceGrpcGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class FileClient {

    @Value("${grpc.fileservice.host}")
    private String host;

    @Value("${grpc.fileservice.port}")
    private String port;

    private FileServiceGrpcGrpc.FileServiceGrpcBlockingStub blockingStub;

    @Autowired
    public void StartFileServiceConsumer() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        FileServiceGrpcGrpc.FileServiceGrpcBlockingStub stub = FileServiceGrpcGrpc.newBlockingStub(channel);
        this.blockingStub = stub;
    }

//    public FileResponse uploadFile(FileRequest request) {
//        return blockingStub.uploadFile(request);
//    }
    public FileResponse uploadFile(FileRequest request){
        FileResponse response = this.blockingStub.uploadFile(request);
        return response;
    }

}
