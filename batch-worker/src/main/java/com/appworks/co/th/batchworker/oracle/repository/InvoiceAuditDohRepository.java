package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceAuditDoh;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface InvoiceAuditDohRepository extends JpaRepository<InvoiceAuditDoh, Long> {

    @Transactional
    @Query(value = "UPDATE MF_INVOICE_AUDIT_DOH SET AUDIT_FLAG = :auditFlag WHERE INVOICE_NO = :invoiceNo", nativeQuery = true)
    @Modifying
    int updateAuditFlagByInvoiceNo(@Param("auditFlag") String auditFlag, @Param("invoiceNo") String invoiceNo);

}
