package com.appworks.co.th.batchworker.job.nonmemberwarning2;

import com.appworks.co.th.batchworker.dto.InvoiceWarningDTO;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.job.nonmemberwarning1.NonMemberWarning1ItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.payload.AdditionalObject;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import com.appworks.co.th.batchworker.service.invoice.InvoiceNonMemberService;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceEvidence;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceTransaction;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.OraclePagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Configuration
public class NonMemberWarning2Job {
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    private final FastDateFormat YYYYMMDD_HHMMSS_SSS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("Asia/Bangkok"));
    @Autowired
    @Qualifier("nonMemberWarning2JobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("nonMemberWarning2JobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Value("${memberwarning2.extend.day}")
    public int extendDay;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MasterClient masterClient;

    @Autowired
    private InvoiceNonMemberRepository invoiceNonMemberRepository;

    @Autowired
    private InvoiceNonMemberService invoiceNonMemberService;

    @Autowired
    private InvoiceJdbcRepository invoiceJdbcRepository;

    public NonMemberWarning2Job(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    private String fromClause() {
        return new StringBuilder()
                .append(" FROM ( ")
                .append("   SELECT ")
                .append("       ID,")
                .append("       INVOICE_NO,")
                .append("       STATUS,")
                .append("       TOTAL_AMOUNT,")
                .append("       INVOICE_TYPE,")
                .append("       VEHICLE_TYPE,")
                .append("       DELETE_FLAG,")
                .append("       DUE_DATE,")
                .append("       DUE_DATE + INTERVAL '" + extendDay + "' DAY AS EXTEND_DUE_DATE,")
                .append("       CREATE_DATE")
                .append("   FROM MF_INVOICE_NONMEMBER ")
                .append(") A ")
                .toString();
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<InvoiceWarningDTO> nonMemberWarning2ItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        HashMap<String, Order> sortKeys = new HashMap<>(2);
        sortKeys.put("ID", Order.ASCENDING);

        OraclePagingQueryProvider queryProvider = new OraclePagingQueryProvider();
        queryProvider.setSelectClause("*");
        queryProvider.setFromClause(this.fromClause());
     //   queryProvider.setWhereClause(String.format("%s AND ID >= %s AND ID <= %s AND EXTEND_DUE_DATE <= TO_DATE('%s','YYYY/MM/DD')", where, minValue, maxValue, this.currentDate()));
        queryProvider.setWhereClause(String.format("%s AND ID >= %s AND ID <= %s AND DUE_DATE < TO_DATE('%s','YYYY/MM/DD')", where, minValue, maxValue, this.currentDate()));
        queryProvider.setSortKeys(sortKeys);

        JdbcPagingItemReader<InvoiceWarningDTO> pagingItemReader = new JdbcPagingItemReader<>();
        pagingItemReader.setPageSize(1000);
        pagingItemReader.setDataSource(this.dataSource);
        pagingItemReader.setRowMapper(BeanPropertyRowMapper.newInstance(InvoiceWarningDTO.class));
        pagingItemReader.setQueryProvider(queryProvider);
        pagingItemReader.afterPropertiesSet();
        return pagingItemReader;
    }

//    @Bean
//    @StepScope
//    public JpaPagingItemReader<InvoiceNonMember> nonMemberWarning2ItemReader(
//            @Value("#{stepExecutionContext['minValue']}") Long minValue,
//            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
//            @Value("#{stepExecutionContext['where']}") String where
//    ) throws Exception {
//        log.info(" reading {} to {}" , minValue, maxValue);
////        String jpqlQuery = "SELECT o from InvoiceStaging o "+where+" and id >= "+ minValue +" and id <= "+ maxValue + " AND issueDate = TO_DATE(\'2020/04/15\', \'YYYY/MM/DD\')";
//        String jpqlQuery = "SELECT o from InvoiceNonMember o "+where+" and id >= "+ minValue +" and id <= "+ maxValue
//                + " and due_date <= TO_DATE(\'"+currentDatePlusDay(extendDay)+"\', \'YYYY/MM/DD\') ";
//        log.info(jpqlQuery);
//        JpaPagingItemReader<InvoiceNonMember> reader = new JpaPagingItemReader<>();
//        reader.setQueryString(jpqlQuery);
//        reader.setEntityManagerFactory(entityManagerFactory);
//        reader.setPageSize(1000);
//        reader.afterPropertiesSet();
//        reader.setSaveState(false);
//
//        return reader;
//    }

    @Bean("nonMemberWarning2ItemProcessor")
    public ItemProcessor<InvoiceWarningDTO, InvoiceObject> nonMemberWarning2ItemProcessor() {
        return new ItemProcessor<InvoiceWarningDTO, InvoiceObject>() {
            @Override
            public InvoiceObject process(InvoiceWarningDTO warningDTO) throws Exception {
                InvoiceNonMember item = invoiceNonMemberRepository.getOne(warningDTO.getId());
                HqResponse hqResponse = masterClient.getHq(HqRequest.newBuilder().setHqCode(item.getHqCode()).build());
                InvoiceObject invoiceObject = new InvoiceObject();
                CreateInvoiceHeader header = new CreateInvoiceHeader();
//                header.setCustomerId(item.getCustomerId());
                header.setInvoiceType("2");
                header.setInvoiceRefNo(item.getInvoiceNo());
//                header.setCustomerId(item.getCustomerId());
                header.setPlate1(item.getPlate1());
                header.setPlate2(item.getPlate2());
                header.setPlateProvince(item.getProvince());
                header.setFullName(item.getFullName());
                header.setAddress(item.getAddress());
                header.setIssueDate(item.getIssueDate());
                header.setChannel("Batch-service");
                header.setHqCode(item.getHqCode());
                header.setCreateBy(item.getCreateBy());
                header.setVat(0.0);
                header.setServiceProvider(hqResponse.getServiceProvider().getCode());
                header.setBrand(item.getBrand());
                header.setDocType(item.getDocType());
                header.setDocNo(item.getDocNo());
                header.setVehicleCode(item.getVehicleCode());
                header.setVehicleTypeCode(item.getVehicleType());
                header.setColor(item.getColors().stream().map(invoiceColor -> invoiceColor.getCode()).collect(Collectors.toList()));

                AdditionalObject additionalObject = new AdditionalObject();
                additionalObject.setDueDate(item.getDueDate());
                additionalObject.setFeeAmount(item.getFeeAmount());
                additionalObject.setFineAmount(item.getFineAmount());
                additionalObject.setCollectionAmount(item.getCollectionAmount());
                additionalObject.setTotalAmount(item.getTotalAmount());
                additionalObject.setStatus(item.getStatus());
                additionalObject.setErrorMessage(item.getErrorMessage());
                additionalObject.setDetailsNonMem(item.getDetails());

                CreateInvoiceDetail detail = new CreateInvoiceDetail();
                detail.setDetails(item.getDetails().stream().map(invoiceDetail -> {
                    CreateInvoiceTransaction transaction = new CreateInvoiceTransaction();
                    transaction.setId(invoiceDetail.getTransactionId());
                    transaction.setTransactionDate(String.valueOf(transactionInstantToDate(invoiceDetail.getTransactionDate())));
 //                   transaction.setTransactionDate(transactionDate());
                    transaction.setPlate1(invoiceDetail.getPlate1());
                    transaction.setPlate2(invoiceDetail.getPlate2());
                    transaction.setProvince(invoiceDetail.getProvince());
                    transaction.setHqCode(invoiceDetail.getHqCode());
                    transaction.setPlazaCode(invoiceDetail.getPlazaCode());
                    transaction.setLaneCode(invoiceDetail.getLaneCode());
                    transaction.setFeeAmountOld(invoiceDetail.getFeeAmountOld().doubleValue());
                    transaction.setDestinationHqCode(invoiceDetail.getDestHqCode());
                    transaction.setDestinationPlazaCode(invoiceDetail.getDestPlazaCode());
                    transaction.setDestinationLaneCode(invoiceDetail.getDestLaneCode());
                    //    transaction.setFeeAmount(invoiceDetail.getRawFee().doubleValue());
                    transaction.setFeeAmount(invoiceDetail.getRawFee().doubleValue());
                    transaction.setDiscount(invoiceDetail.getDiscount().doubleValue());
                    transaction.setFineAmount(invoiceDetail.getFineAmount().doubleValue());
                    transaction.setOperationFee(invoiceDetail.getOperationFee().doubleValue());
                    transaction.setCollectionAmount(invoiceDetail.getCollectionAmount().doubleValue());
                    transaction.setVehicleWheel(invoiceDetail.getVehicleWheel());
                    transaction.setVat(invoiceDetail.getVat().doubleValue());
                    transaction.setOriginTranType(invoiceDetail.getOriginTranType());
                    transaction.setEvidences(invoiceDetail.getEvidences().stream().map(invoiceEvidence -> {
                        CreateInvoiceEvidence evidence = new CreateInvoiceEvidence();
                        evidence.setType(invoiceEvidence.getType());
                        evidence.setFile(invoiceEvidence.getFile());
                        return evidence;
                    }).collect(Collectors.toList()));
                    return transaction;
                }).collect(Collectors.toList()));

                invoiceObject.setCreateInvoiceHeader(header);
                invoiceObject.setCreateInvoiceDetail(detail);
                return invoiceObject;
            }
        };
    }

    @Bean
    public Step nonMemberWarning2Step() throws Exception {
        return this.workerStepBuilderFactory.get("nonMemberWarning2Step")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceWarningDTO, InvoiceObject>chunk(1000)
                .reader(nonMemberWarning2ItemReader(null,null, null))
                .processor(nonMemberWarning2ItemProcessor())
                .writer(new NonMemberWarning1ItemWriter(
                        (MessagePublisher) applicationContext.getBean("messagePublisher"),
                        (InvoiceJdbcRepository) applicationContext.getBean("invoiceJdbcRepository"),
                        (InvoiceNonMemberService) applicationContext.getBean("invoiceNonMemberService"),
                        (InvoiceListValueRepository) applicationContext.getBean("invoiceListValueRepository"),
                        (MasterVehicleOfficeRepository) applicationContext.getBean("masterVehicleOfficeRepository"),
                        (InvoiceMasterHqRepository) applicationContext.getBean("invoiceMasterHqRepository"),
                        (MasterPlazaRepository) applicationContext.getBean("masterPlazaRepository"),
                        (InvoiceMasterLaneRepository) applicationContext.getBean("invoiceMasterLaneRepository"),
                        (AuditServiceImp) applicationContext.getBean("auditServiceImp")
                ))
                .build();
    }

    private String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }
    private String currentDatePlusDay(int day){
        Date date = DateUtils.addDays(new Date(),day - 1 );
        String currentDate = df.format(date);
        log.info("currentDate : {}", currentDate);
        return currentDate;
    }

    private String transactionDate() {
        return YYYYMMDD_HHMMSS_SSS.format(new Date());
    }
    private String transactionInstantToDate(Instant transactionDate) {
        Date date=Date.from(transactionDate);
        return YYYYMMDD_HHMMSS_SSS.format(date);
    }
}

