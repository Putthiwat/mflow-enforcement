package com.appworks.co.th.batchworker.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class BrandNameDTO implements Serializable {

    private String code;
    private String descriptionTh;
    private String descriptionEn;

}
