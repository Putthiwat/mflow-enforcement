package com.appworks.co.th.batchworker.oracle.jdbc;

import com.appworks.co.th.batchworker.oracle.entity.PaymentReconcileReport;
import com.appworks.co.th.batchworker.oracle.entity.ReconcileBankAccountReport;

import java.util.List;

public interface ReconcileJdbcRepository {

    int[] batchInsertAccountReconcile(String schema, List<ReconcileBankAccountReport> accountReconciles);

    int[] batchInsertPaymentReconcile(String schema, List<PaymentReconcileReport> paymentReconciles);

}
