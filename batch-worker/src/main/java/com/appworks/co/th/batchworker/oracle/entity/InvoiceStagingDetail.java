package com.appworks.co.th.batchworker.oracle.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
@Entity
@ToString(exclude = {"evidences"})
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_DETAIL_STAGING", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"TRANSACTION_ID", "INVOICE_NO"}, name = "MF_INVOICE_DETAIL_STAGING_UN")
})
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceStagingDetail extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceStageDetail", sequenceName = "SEQ_MF_INVOICE_STAGE_DETAIL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceStageDetail")
    public Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(
            name = "INVOICE_NO",
            referencedColumnName = "INVOICE_NO",
            foreignKey = @ForeignKey(name = "MF_INVOICE_STAGE_DETAIL_FK1"),
            nullable = false)
    private InvoiceStaging invoice;

    @Column(name = "TRANSACTION_ID", columnDefinition = "VARCHAR2(50)")
    private String transactionId;
    @Column(name = "TRANSACTION_DATE", columnDefinition = "TIMESTAMP")
    private Instant transactionDate;
    @Column(name = "PLATE1", columnDefinition = "VARCHAR2(255)")
    private String plate1;
    @Column(name = "PLATE2", columnDefinition = "VARCHAR2(255)")
    private String plate2;
    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(25)")
    private String province;
    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String hqCode;
    @Column(name = "PLAZA_CODE", columnDefinition = "VARCHAR2(25)")
    private String plazaCode;
    @Column(name = "LANE_CODE", columnDefinition = "VARCHAR2(25)")
    private String laneCode;
    @Column(name = "DEST_HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String destHqCode;
    @Column(name = "DEST_PLAZA_CODE", columnDefinition = "VARCHAR2(25)")
    private String destPlazaCode;
    @Column(name = "DEST_LANE_CODE", columnDefinition = "VARCHAR2(25)")
    private String destLaneCode;
    @Column(name = "FEE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal feeAmount;
    @Column(name = "FEE_AMOUNT_OLD", columnDefinition = "DECIMAL(5,2)")
    private BigDecimal feeAmountOld;
    @Column(name = "RAW_FEE", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal rawFee;

    @Column(name = "FINE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal fineAmount;

    @Column(name = "COLLECTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal collectionAmount;

    @Column(name = "TOTAL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalAmount;

    @Column(name="VAT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal vat;

    @Column(name = "DISCOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal discount;

    @Column(name="VEHICLE_WHEEL", columnDefinition = "VARCHAR2(25)")
//    @NotNull
    private String vehicleWheel;

    @Column(name = "ORIGIN_TRAN_TYPE", columnDefinition = "VARCHAR2(25)")
    private String originTranType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detail", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceStagingEvidence> evidences;

}
