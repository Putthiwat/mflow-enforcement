package com.appworks.co.th.batchworker.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

@Data
@ToString
public class InvoiceWarningDTO implements Serializable {

    private Long id;
    private String invoiceNo;
    private String status;
    private String invoiceType;
    private String vehicleType;
    private Integer deleteFlag;
    private Date dueDate;
    private Date extendDueDate;
    private Timestamp createDate;

}
