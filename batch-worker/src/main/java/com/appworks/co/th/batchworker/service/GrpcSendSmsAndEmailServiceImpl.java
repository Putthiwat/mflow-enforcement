package com.appworks.co.th.batchworker.service;

import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.notificationmanagement.EmailVerifyNotificationRequest;
import com.appworks.co.th.notificationmanagement.SmsOTPNotificationRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service(value = "grpcSendSmsAndEmailService")
public class GrpcSendSmsAndEmailServiceImpl implements GrpcSendSmsAndEmailService{
    private static final Logger logger = LoggerFactory.getLogger(GrpcSendSmsAndEmailServiceImpl.class);
    @Autowired
    private NotificationManagementClient notificationManagementClient;
    @Override
    public void sendSmsVerifyToCustomer(String action, String type, String mobile, String refId, String refId2, String refId3,String language) {
        try {
            if (StringUtils.isEmpty(mobile)) return;

            SmsOTPNotificationRequest request = SmsOTPNotificationRequest.newBuilder()
                    .setAction(action)
                    .setType(type)
                    .setMobile(mobile)
                    .setLanguage(language)
                    .setReferenceId(refId)
                    .setReferenceId2(refId2)
                    .setReferenceId3(refId3)
                    .build();
            notificationManagementClient.smsOTPNotification(request);
        } catch (Exception e) {
            logger.error("send sms verify customer error : ", e);
        }
    }

    @Override
    public void sendEmailVerifyToCustomer(String action, String type, String email, String refId, String refId2, String language) {
        try {
            if (StringUtils.isEmpty(email)) return;

            EmailVerifyNotificationRequest request = EmailVerifyNotificationRequest.newBuilder()
                    .setAction(action)
                    .setType(type)
                    .setEmail(email)
                    .setLanguage(language)
                    .setReferenceId(refId)
                    .setReferenceId2(refId2)
                    .build();
            notificationManagementClient.emailVerifyNotification(request);
        } catch (Exception e) {
            logger.error("send sms verify customer error : ", e);
        }
    }
}
