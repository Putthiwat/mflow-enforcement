package com.appworks.co.th.batchworker.dto;

import com.appworks.co.th.batchworker.oracle.entity.PaymentReconcileReport;
import com.appworks.co.th.batchworker.oracle.entity.ReconcileBankAccountReport;
import lombok.Data;

import java.io.Serializable;

@Data
public class CompositeNonmemberReconcileDTO implements Serializable {

    private PaymentReconcileReport paymentReconcile;
    private ReconcileBankAccountReport accountReconcile;

}
