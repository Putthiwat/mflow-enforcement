package com.appworks.co.th.batchworker.oracle.jdbc;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.oracle.entity.PaymentReconcileReport;
import com.appworks.co.th.batchworker.oracle.entity.ReconcileBankAccountReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.List;

@Repository
public class ReconcileJdbcRepositoryImpl implements ReconcileJdbcRepository {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("oracleJdbcTemplate")
    public JdbcTemplate jdbcTemplate;

    @Override
    public int[] batchInsertAccountReconcile(String schema, List<ReconcileBankAccountReport> accountReconciles) {
        String statement = "Insert into {0}.MF_RECONCILE_MFLOW_BANK_ACCOUNT_REPORT (ID,CREATE_BY,CREATE_BY_ID,CREATE_CHANNEL,CREATE_DATE,DELETE_FLAG,UPDATE_BY,UPDATE_BY_ID,UPDATE_CHANNEL,UPDATE_DATE,VERSION,BANK_REF_NO,BANK_TRANSACTION_NO,CUSTOMER_ID,CUSTOMER_NAME,CUSTOMER_REF,DIFF_AMOUNT,EFFECTIVE_DATE,PAYMENT_CHANNEL,PAYMENT_DATE,RECEIPT_DATE_TIME,RECEIPT_NO,TOTAL_BANK_TRANSACTION_AMOUNT,TOTAL_RECEIPT_AMOUNT,TRANSACTION_CODE,TRANSACTION_REF,CUSTOMER_TYPE,DEST_PLAZA_CODE,DEST_PLAZA_NAME,HQ_CODE,HQ_NAME,INVOICE_STATUS,MEMBER_TYPE,NATIONALITY_CODE,NATIONALITY_NAME,PLAZA_CODE,PLAZA_NAME,VEHICLE_TYPE_CODE,VEHICLE_TYPE_NAME,INVOICE_NO,TRANSACTION_DATE,TRANSACTION_ID) values ({0}.MF_RECONCILE_MFLOW_BANK_ACCOUNT_REPORT_SEQ.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args = new Object[] { schema };
        String statementWithSchema = new MessageFormat(statement).format(args);
        try{
            return jdbcTemplate.batchUpdate(statementWithSchema, this.accountReconcileBatchPreparedStatement(accountReconciles));
        }catch(Exception e){
            log.error("batchInsertAccountReconcile error {}",e);
        }
        return null;
    }

    @Override
    public int[] batchInsertPaymentReconcile(String schema, List<PaymentReconcileReport> paymentReconciles) {
        String statement = "Insert into {0}.MF_PAYMENT_RECONCILE_REPORT (ID,CREATE_BY,CREATE_BY_ID,CREATE_CHANNEL,CREATE_DATE,DELETE_FLAG,UPDATE_BY,UPDATE_BY_ID,UPDATE_CHANNEL,UPDATE_DATE,VERSION,BANK_CHANNEL,BANK_TRANSACTION_AMOUNT,BRAND,COLOR,CUSTOMER_ID,CUSTOMER_REF,DIFF_CHANNEL_AMOUNT,DIFF_MFLOW_AMOUNT,DUE_DATE,FEE_AMOUNT,FINE_AMOUNT,HQ_NAME,INVOICE_CREATE_DATE,INVOICE_NO,LANE_NAME,PAYMENT_CHANNEL,PLATE,PLAZA_NAME,POST_DATE,PROVINCE,RECEIPT_DATE_TIME,RECEIPT_NO,TOTAL_AMOUNT,TRANSACTION_AMOUNT,HQ_CODE,LANE_CODE,PLAZA_CODE,CUSTOMER_TYPE,NATIONALITY_CODE,NATIONALITY_NAME,VEHICLE_TYPE_CODE,VEHICLE_TYPE_NAME,MEMBER_TYPE,OPERATION_FEE,TRANSACTION_DATE,TRANSACTION_ID) values ({0}.SEQ_MF_PAYMENT_RECONCILE_REPORT.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args = new Object[] { schema };
        String statementWithSchema = new MessageFormat(statement).format(args);
        try{
            return jdbcTemplate.batchUpdate(statementWithSchema, this.paymentReconcileBatchPreparedStatement(paymentReconciles));
        }catch(Exception e){
            log.error("batchInsertPaymentReconcile error {}",e);
        }
        return null;
    }

    private BatchPreparedStatementSetter paymentReconcileBatchPreparedStatement(List<PaymentReconcileReport> paymentReconciles) {
        return new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                PaymentReconcileReport paymentReconcile = paymentReconciles.get(i);
                ps.setString(1, paymentReconcile.getCreateBy());
                ps.setString(2, paymentReconcile.getCreateById());
                ps.setString(3, paymentReconcile.getCreateChannel());
                ps.setTimestamp(4, paymentReconcile.getCreateDate()!=null ?
                        DateUtils.toTimestamp(paymentReconcile.getCreateDate()):null);
                ps.setInt(5, paymentReconcile.getDeleteFlag());
                ps.setString(6, paymentReconcile.getUpdateBy());
                ps.setString(7, paymentReconcile.getUpdateById());
                ps.setString(8, paymentReconcile.getUpdateChannel());
                ps.setTimestamp(9, paymentReconcile.getUpdateDate()!=null ?
                        DateUtils.toTimestamp(paymentReconcile.getUpdateDate()):null);
                ps.setInt(10, paymentReconcile.getVersion());
                ps.setString(11, paymentReconcile.getBankChannel());
                ps.setBigDecimal(12, paymentReconcile.getBankTransactionAmount());
                ps.setString(13, paymentReconcile.getBrand());
                ps.setString(14, paymentReconcile.getColor());
                ps.setString(15, paymentReconcile.getCustomerId());
                ps.setString(16, paymentReconcile.getCustomerRef());
                ps.setBigDecimal(17, paymentReconcile.getDiffChannelAmount());
                ps.setBigDecimal(18, paymentReconcile.getDiffMflowAmount());
                ps.setTimestamp(19, paymentReconcile.getDueDate()!=null ?
                        Timestamp.from(paymentReconcile.getDueDate()):null);
                ps.setBigDecimal(20, paymentReconcile.getFeeAmount());
                ps.setBigDecimal(21, paymentReconcile.getFineAmount());
                ps.setString(22, paymentReconcile.getHqName());
                ps.setTimestamp(23, paymentReconcile.getInvoiceCreateDate()!=null ?
                        DateUtils.toTimestamp(paymentReconcile.getInvoiceCreateDate()):null);
                ps.setString(24, paymentReconcile.getInvoiceNo());
                ps.setString(25, paymentReconcile.getLaneName());
                ps.setString(26, paymentReconcile.getPaymentChannel());
                ps.setString(27, paymentReconcile.getPlate());
                ps.setString(28, paymentReconcile.getPlazaName());
                ps.setTimestamp(29, paymentReconcile.getPostDate()!=null ?
                        DateUtils.toTimestamp(paymentReconcile.getPostDate()):null);
                ps.setString(30, paymentReconcile.getProvince());
                ps.setTimestamp(31, paymentReconcile.getReceiptDateTime()!=null ?
                        DateUtils.toTimestamp(paymentReconcile.getReceiptDateTime()):null);
                ps.setString(32, paymentReconcile.getReceiptNo());
                ps.setBigDecimal(33, paymentReconcile.getTotalAmount());
                ps.setBigDecimal(34, paymentReconcile.getTransactionAmount());
                ps.setString(35, paymentReconcile.getHqCode());
                ps.setString(36, paymentReconcile.getLaneCode());
                ps.setString(37, paymentReconcile.getPlazaCode());
                ps.setString(38, paymentReconcile.getCustomerType());
                ps.setString(39, paymentReconcile.getNationalityCode());
                ps.setString(40, paymentReconcile.getNationalityName());
                ps.setString(41, paymentReconcile.getVehicleTypeCode());
                ps.setString(42, paymentReconcile.getVehicleTypeName());
                ps.setString(43, paymentReconcile.getMemberType());
                ps.setBigDecimal(44, paymentReconcile.getOperationFee() == null?
                        BigDecimal.ZERO: paymentReconcile.getOperationFee());
                ps.setTimestamp(45, paymentReconcile.getTransactionDate()!=null ?
                        DateUtils.toTimestamp(paymentReconcile.getTransactionDate()):null);
                ps.setString(46, paymentReconcile.getTrasactionId());
            }

            @Override
            public int getBatchSize() {
                return paymentReconciles.size();
            }
        };
    }

    private BatchPreparedStatementSetter accountReconcileBatchPreparedStatement(List<ReconcileBankAccountReport> accountReconciles) {
        return new BatchPreparedStatementSetter() {

            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                ReconcileBankAccountReport accountReconcile = accountReconciles.get(i);
                ps.setString(1, accountReconcile.getCreateBy());
                ps.setString(2, accountReconcile.getCreateById());
                ps.setString(3, accountReconcile.getCreateChannel());
                ps.setTimestamp(4, accountReconcile.getCreateDate()!=null ?
                        DateUtils.toTimestamp(accountReconcile.getCreateDate()):null);
                ps.setInt(5, accountReconcile.getDeleteFlag());
                ps.setString(6, accountReconcile.getUpdateBy());
                ps.setString(7, accountReconcile.getUpdateById());
                ps.setString(8, accountReconcile.getUpdateChannel());
                ps.setTimestamp(9, accountReconcile.getUpdateDate()!=null ?
                        DateUtils.toTimestamp(accountReconcile.getUpdateDate()):null);
                ps.setInt(10, accountReconcile.getVersion());
                ps.setString(11, accountReconcile.getBankRefNo());
                ps.setString(12, accountReconcile.getBankTransactionNo());
                ps.setString(13, accountReconcile.getCustomerId());
                ps.setString(14, accountReconcile.getCustomerName());
                ps.setString(15, accountReconcile.getCustomerRef());
                ps.setBigDecimal(16, accountReconcile.getDiffAmount());
                ps.setTimestamp(17, accountReconcile.getEffectiveDate()!=null ?
                         DateUtils.toTimestamp(accountReconcile.getEffectiveDate()):null);
                ps.setString(18, accountReconcile.getPaymentChannel());
                ps.setTimestamp(19, accountReconcile.getPaymentDate()!=null ?
                        DateUtils.toTimestamp(accountReconcile.getPaymentDate()):null);
                ps.setTimestamp(20, accountReconcile.getReceiptDateTime()!=null ?
                        DateUtils.toTimestamp(accountReconcile.getReceiptDateTime()):null);
                ps.setString(21, accountReconcile.getReceiptNo());
                ps.setBigDecimal(22, accountReconcile.getTotalBankTransactionAmount());
                ps.setBigDecimal(23, accountReconcile.getTotalReceiptAmount());
                ps.setString(24, accountReconcile.getTransactionCode());
                ps.setString(25, accountReconcile.getTransactionRef());
                ps.setString(26, accountReconcile.getCustomerType());
                ps.setString(27, accountReconcile.getDestPlazaCode());
                ps.setString(28, accountReconcile.getDestPlazaName());
                ps.setString(29, accountReconcile.getHqCode());
                ps.setString(30, accountReconcile.getHqName());
                ps.setString(31, accountReconcile.getInvoiceStatus());
                ps.setString(32, accountReconcile.getMemberType());
                ps.setString(33, accountReconcile.getNationalityCode());
                ps.setString(34, accountReconcile.getNationalityName());
                ps.setString(35, accountReconcile.getPlazaCode());
                ps.setString(36, accountReconcile.getPlazaName());
                ps.setString(37, accountReconcile.getVehicleTypeCode());
                ps.setString(38, accountReconcile.getVehicleTypeName());
                ps.setString(39, accountReconcile.getInvoiceNo());
                ps.setTimestamp(40, accountReconcile.getTransactionDate()!=null ?
                        DateUtils.toTimestamp(accountReconcile.getTransactionDate()):null);
                ps.setString(41, accountReconcile.getTrasactionId());
            }

            @Override
            public int getBatchSize() {
                return accountReconciles.size();
            }

        };
    }

}
