package com.appworks.co.th.batchworker.job.easypassrepay;

import com.appworks.co.th.batchworker.grpc.EasyPassBatchClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.easypassbatch.EasyPassBatchRequest;
import com.appworks.co.th.easypassbatch.EasyPassBatchResponse;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import java.util.function.Function;

@Configuration
public class EasyPassRepayJob {
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("easyPassRepayJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("easyPassRepayJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private EasyPassBatchClient easyPassBatchClient;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    public EasyPassRepayJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> easyPassRepayItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        EasyPassBatchRequest request = EasyPassBatchRequest.newBuilder()
                .setMethod("repay")
                .build();
        easyPassBatchClient.callEasyPassService(request);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue;
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("easyPassRepayItemProcessor")
    public Function<? super Invoice, ? extends InvoiceObject> easyPassRepayItemProcessor() {
        EasyPassBatchRequest request = EasyPassBatchRequest.newBuilder()
                .setMethod("repay")
                .build();
        EasyPassBatchResponse response = easyPassBatchClient.callEasyPassService(request);
        return null;
    }

    @Bean
    public Step easyPassRepayStep() throws Exception {
        return this.workerStepBuilderFactory.get("easyPassRepayStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, InvoiceObject>chunk(1000)
                .reader(easyPassRepayItemReader(null,null, null))
                .processor(easyPassRepayItemProcessor())
                .writer(new EasyPassRepayItemWriter((MessagePublisher) applicationContext.getBean("messagePublisher")))
                .build();
    }
}
