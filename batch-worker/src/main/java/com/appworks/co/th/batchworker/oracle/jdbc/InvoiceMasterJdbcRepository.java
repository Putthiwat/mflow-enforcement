package com.appworks.co.th.batchworker.oracle.jdbc;

import com.appworks.co.th.batchworker.payload.*;

import java.util.List;

public interface InvoiceMasterJdbcRepository {

    ColorNameDTO getColorNameByCodes(List<String> codes);

    ColorNameDTO getColorNameByInvoiceNo(String invoiceNo);

    OfficeNameDTO getOfficeNameByCode(String code);

    HQNameDTO getHQNameByCode(String code);

    PlazaNameDTO getPlazaNameByCode(String code);

    BrandNameDTO getBrandNameByCode(String code);

    LaneNameDTO getLaneNameByCode(String code);

}
