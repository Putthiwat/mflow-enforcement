package com.appworks.co.th.batchworker.oracle.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;

@Data
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"createdDate", "updateDate", "deleteFlag"},
        allowGetters = true, allowSetters = true)
@MappedSuperclass
public class BaseEntity {

    @CreatedDate
    @Column(name = "CREATE_DATE", columnDefinition = "TIMESTAMP NOT NULL")
    private Instant createDate;

    @Column(name = "CREATE_BY", columnDefinition = "VARCHAR2(255) NOT NULL")
    private String createBy;

    @Column(name = "CREATE_BY_ID", columnDefinition = "VARCHAR2(255) NOT NULL")
    private String createById;

    @Column(name = "CREATE_CHANNEL", columnDefinition = "VARCHAR2(50) NOT NULL")
    private String createChannel;

    @LastModifiedDate
//    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern = "dd/MM/yyy HH:mm:ss")
    @Column(name = "UPDATE_DATE")
    private Instant updateDate;

    @Column(name = "UPDATE_BY", columnDefinition = "VARCHAR2(255)")
    private String updateBy;

    @Column(name = "UPDATE_BY_ID", columnDefinition = "VARCHAR2(255)")
    private String updateById;

    @Column(name = "UPDATE_CHANNEL", columnDefinition = "VARCHAR2(50)")
    private String updateChannel;

    @Column(name = "DELETE_FLAG", columnDefinition = "SMALLINT default 0 NOT NULL")
    private int deleteFlag;

    @Version
    @Column(name = "VERSION", columnDefinition = "SMALLINT default 1 NOT NULL")
    private int version;

    @PrePersist
    public void prePersist() {
        if(createBy == null)
            createBy = getFullName();
        if(createById == null)
            createById = getAccountId();
        if (createChannel == null)
            createChannel = "SYSTEM";
    }

    @PreUpdate
    public void preUpdate() {
        if(updateBy == null)
            updateBy = getFullName();
        if(updateById == null)
            updateById = getAccountId();
        if (updateChannel == null)
            updateChannel = "SYSTEM";
    }

    private String getAccountId() {
        return "SYSTEM";
    }

    private String getFullName() {
        return "Batch-service";
    }
}
