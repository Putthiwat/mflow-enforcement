package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_INVOICE_MASTER_HQ")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceMasterHq {
    @Id
    @Column(name = "ID")
    public Long id;

    @Column(name = "CODE")
    public String code;

    @Column(name = "DESCRIPTION")
    public String description;

    @Column(name = "DELETE_FLAG")
    public Long deleteFlag;
}
