package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMemberVehicle;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceVehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface InvoiceNonMemberVehicleRepository extends JpaRepository<InvoiceNonMemberVehicle, Long>{

    Optional<InvoiceNonMemberVehicle> findByPlate1AndPlate2AndProvince(String plate1, String plate2, String province);

    Optional<InvoiceNonMemberVehicle> findByPlate1AndPlate2AndProvinceAndVehicleType(String plate1, String plate2, String province,String vehicleType);

}
