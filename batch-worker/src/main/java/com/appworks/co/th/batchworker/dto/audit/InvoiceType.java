package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class InvoiceType {
    private String code;
    private String description;

}
