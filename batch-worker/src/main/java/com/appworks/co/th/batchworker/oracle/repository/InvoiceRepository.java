package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    /*@Query(value = "SELECT * FROM MF_INVOICE WHERE INVOICE_NO =?1", nativeQuery = true)
    Invoice findByInvoiceNo(String invoiceNo);*/

    Optional<Invoice> findByInvoiceNo(String invoiceNo);

    @Query(value = "SELECT * FROM MF_INVOICE WHERE INVOICE_NO =?1", nativeQuery = true)
    Invoice findByInvoiceNo2(String invoiceNo);

    @Query("UPDATE Invoice o SET o.ref1 = :refGroup , o.updateBy = :updateBy WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateRefGroupByInvoiceNo(@Param("refGroup") String refGroup, @Param("updateBy") String updateBy, @Param("invoiceNo") String invoiceNo);

    @Query("UPDATE Invoice o SET o.auditFlag = :audiFlag , o.updateBy = :updateBy WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateAuditFlagByInvoiceNo(@Param("audiFlag") String refGroup, @Param("updateBy") String updateBy, @Param("invoiceNo") String invoiceNo);

    @Query("UPDATE Invoice o SET o.auditFlag = :audiFlag , o.updateBy = :updateBy WHERE o.id = :id")
    @Modifying
    int updateAuditFlagById(@Param("audiFlag") String refGroup, @Param("updateBy") String updateBy, @Param("id") Long id);

    @Query("UPDATE Invoice o SET o.eBillFileId = :eBillFileId , o.updateBy = :updateBy WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateEBillFileIdByInvoiceNo(@Param("eBillFileId") String eBillFileId, @Param("updateBy") String updateBy, @Param("invoiceNo") String invoiceNo);

    @Query("UPDATE Invoice o SET o.invoiceType = :invoiceType , o.updateBy = :updateBy WHERE o.invoiceNo = :invoiceNo")
    @Modifying
    int updateInvoiceTypeByInvoiceNo(@Param("invoiceType") int invoiceType, @Param("updateBy") String updateBy, @Param("invoiceNo") String invoiceNo);

}
