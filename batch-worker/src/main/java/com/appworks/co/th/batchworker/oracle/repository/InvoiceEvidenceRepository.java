package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceDetail;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceEvidence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface InvoiceEvidenceRepository extends JpaRepository<InvoiceEvidence, Long> {
    @Query(value = "SELECT * FROM MF_INVOICE_EVIDENCE WHERE INVOICE_DETAIL_ID = ?1 AND DELETE_FLAG = 0",nativeQuery = true)
    List<InvoiceEvidence> findAllByInvoiceDetail(Long invoiceDetailId);
}
