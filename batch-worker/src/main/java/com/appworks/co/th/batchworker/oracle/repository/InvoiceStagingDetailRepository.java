package com.appworks.co.th.batchworker.oracle.repository;


import com.appworks.co.th.batchworker.oracle.entity.InvoiceStagingDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface InvoiceStagingDetailRepository extends JpaRepository<InvoiceStagingDetail, Long> {

}
