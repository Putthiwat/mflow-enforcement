package com.appworks.co.th.batchworker.job.mpassftpfile;


import com.appworks.co.th.batchworker.grpc.FileClient;
import com.appworks.co.th.batchworker.grpc.MPassBatchClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.mflow.fileservice.FileRequest;
import com.appworks.co.th.mflow.fileservice.FileResponse;
import com.appworks.co.th.mpassbatch.MPassBatchRequest;
import com.appworks.co.th.mpassbatch.MPassBatchResponse;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.google.protobuf.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import java.util.function.Function;

@Configuration
public class MPassFTPFileJob {
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("mPassFTPFileJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("mPassFTPFileJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    MPassBatchClient mPassBatchClient;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    public MPassFTPFileJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> mPassFTPFileItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        MPassBatchRequest request = MPassBatchRequest.newBuilder()
                .setMethod("file")
                .build();
        mPassBatchClient.callMPassService(request);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue
                + " and payment_channel = \'MPASS\' ";reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("mPassFTPFileItemProcessor")
    public Function<? super Invoice, ? extends InvoiceObject> mPassFTPFileItemProcessor() {
        MPassBatchRequest request = MPassBatchRequest.newBuilder()
                .setMethod("file")
                .build();
        MPassBatchResponse response = mPassBatchClient.callMPassService(request);
        return null;
    }

    @Bean
    public Step mPassFTPFileStep() throws Exception {
        return this.workerStepBuilderFactory.get("mPassFTPFileStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, InvoiceObject>chunk(1000)
                .reader(mPassFTPFileItemReader(null,null, null))
                .processor(mPassFTPFileItemProcessor())
                .writer(new MPassFTPFileItemWriter())
                .build();
    }
}
