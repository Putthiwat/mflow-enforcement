package com.appworks.co.th.batchworker.job.mpassrepay;


import com.appworks.co.th.batchworker.grpc.MPassBatchClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.mpassbatch.MPassBatchRequest;
import com.appworks.co.th.mpassbatch.MPassBatchResponse;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import java.util.function.Function;

@Configuration
public class MPassRepayJob {
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("mPassRepayJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("mPassRepayJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    MPassBatchClient mPassBatchClient;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    public MPassRepayJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> mPassRepayItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        MPassBatchRequest request = MPassBatchRequest.newBuilder()
                .setMethod("repay")
                .build();
        MPassBatchResponse response = mPassBatchClient.callMPassService(request);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue;
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("mPassRepayItemProcessor")
    public Function<? super Invoice, ? extends InvoiceObject> mPassRepayItemProcessor() {
        MPassBatchRequest request = MPassBatchRequest.newBuilder()
                .setMethod("repay")
                .build();
        MPassBatchResponse response = mPassBatchClient.callMPassService(request);
        return null;
    }

    @Bean
    public Step mPassRepayStep() throws Exception {
        return this.workerStepBuilderFactory.get("mPassRepayStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, InvoiceObject>chunk(1000)
                .reader(mPassRepayItemReader(null,null, null))
                .processor(mPassRepayItemProcessor())
                .writer(new MPassRepayItemWriter((MessagePublisher) applicationContext.getBean("messagePublisher")))
                .build();
    }

}
