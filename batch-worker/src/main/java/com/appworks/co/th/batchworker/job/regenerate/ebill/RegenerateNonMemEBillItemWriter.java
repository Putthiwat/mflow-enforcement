package com.appworks.co.th.batchworker.job.regenerate.ebill;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.ReportService;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.scheduling.annotation.Async;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.appworks.co.th.batchworker.commons.Constants.Invoice.EBIL_FORMAT;

@AllArgsConstructor
public class RegenerateNonMemEBillItemWriter implements ItemWriter<InvoiceNonMember> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    final SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmssSSS", Locale.US);

    private int maxRetry;

    private int retryDelay;

    private FileService fileService;

    private ReportService reportService;

    private InvoiceNonMemberRepository invoiceNonMemberRepository;

    @Override
    public void write(List<? extends InvoiceNonMember> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoiceNonMember -> {
                try {
                    log.info("Start Generate E-Bill for Non-Member with invoice no. : {}", invoiceNonMember != null ? invoiceNonMember.getInvoiceNo() : "-");
                    generateEbill(invoiceNonMember);
                } catch (Exception e) {
                    log.error("InvoiceGenerateNonMemEBillItemWriter error : {}", e);
                }
            });
        }
    }

    @Async
    public void generateEbill(InvoiceNonMember invoice) {
        try {
            JasperPrint jasperPrint = reportService.generateInvoiceNonMember(invoice);

            byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);

            String fileId = String.format(EBIL_FORMAT, invoice.getInvoiceNo(),df.format(Calendar.getInstance().getTime()));

            fileService.uploadFile(bytes, "pdf", fileId);

            invoiceNonMemberRepository.updateEBillFileIdByInvoiceNo(fileId, Constants.SYSTEM, invoice.getInvoiceNo());

        } catch (Exception e) {
            log.error("InvoiceGenerateNonMemEBillItemWriter: invoice no -> {}, error -> {}", invoice != null ? invoice.getInvoiceNo() : "-", e);
        }
    }

}