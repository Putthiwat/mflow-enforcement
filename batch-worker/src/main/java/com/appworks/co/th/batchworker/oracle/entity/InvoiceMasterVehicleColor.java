package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_INVOICE_MASTER_V_COLOR")
public class InvoiceMasterVehicleColor extends BaseEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "seqInvoiceMasterVehicleColor", sequenceName = "SEQ_MF_INVOICE_MASTER_V_COLOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceMasterVehicleColor")
	private Long id;

	@Column(name = "DLT_CODE", columnDefinition = "VARCHAR2(25) NOT NULL")
	private String dltCode;
	
	@Column(name = "CODE", columnDefinition = "VARCHAR2(25) NOT NULL")
	private String code;
	
	@Column(name = "DESCRIPTION", columnDefinition = "VARCHAR2(1500) NOT NULL")
	private String description;
	
	@Column(name = "DESCRIPTION_EN", columnDefinition = "VARCHAR2(1500) NOT NULL")
	private String descriptionEn;

	@Column(name = "STATUS", columnDefinition = "VARCHAR2(25) default 'ACTIVE'")
	private String status;


}
