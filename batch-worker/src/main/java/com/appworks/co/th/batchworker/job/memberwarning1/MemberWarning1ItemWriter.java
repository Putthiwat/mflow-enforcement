package com.appworks.co.th.batchworker.job.memberwarning1;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.audit.*;
import com.appworks.co.th.batchworker.dto.audit.InvoiceDetail;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import com.appworks.co.th.batchworker.service.invoice.InvoiceNonMemberService;
import com.appworks.co.th.batchworker.service.invoice.InvoiceStageService;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceDetail;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceHeader;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.leftPad;

@AllArgsConstructor
public class MemberWarning1ItemWriter implements ItemWriter<InvoiceObject> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private MessagePublisher messagePublisher;

    private final ObjectMapper mapper = new ObjectMapper();

    private InvoiceJdbcRepository invoiceJdbcRepository;
    private InvoiceStageService invoiceStageService;
    private InvoiceListValueRepository invoiceListValueRepository;
    private MasterVehicleOfficeRepository masterVehicleOfficeRepository;
    private InvoiceMasterHqRepository invoiceMasterHqRepository;
    private MasterPlazaRepository masterPlazaRepository;
    private InvoiceMasterLaneRepository invoiceMasterLaneRepository;
    private AuditServiceImp auditServiceImp;

    final DateFormat df = new SimpleDateFormat("yyMMdd");
    @Override
    public void write(List<? extends InvoiceObject> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoiceObject -> {
                boolean result = this.upDateInvoiceType(invoiceObject);
                if(result){
                    try{
                        this.auditServiceImp.auditBilling(setAuditBilling(invoiceObject));
                    }catch (Exception e){
                        log.error("Send audit system error: ",e);
                    }
                }
            });
        }
    }
    private InvoiceMemberDomainEvent<CreateInvoiceDetail, CreateInvoiceHeader> mapToAggregate(InvoiceObject obj){
        InvoiceMemberDomainEvent<CreateInvoiceDetail, CreateInvoiceHeader> invoiceDomainEvent = new InvoiceMemberDomainEvent<>();
        invoiceDomainEvent.setHeader(obj.getCreateInvoiceHeader());
        invoiceDomainEvent.setDetail(obj.getCreateInvoiceDetail());
        return invoiceDomainEvent;
    }

    private AuditBillingReq setAuditBilling(InvoiceObject invoice) {
        log.debug("invoice no." + invoice.getCreateInvoiceHeader().getInvoiceRefNo());
        //Invoice invoice = invoiceRepository.findByInvoiceNo(invoice.getInvoiceNo()).orElse(null);
        AuditBillingReq auditBillingReq = new AuditBillingReq();

        if (invoice != null && invoice.getAdditionalObject()!=null) {
            auditBillingReq.setInvoiceNo(invoice.getCreateInvoiceHeader().getInvoiceRefNo());
            auditBillingReq.setInvoiceRefNo(invoice.getCreateInvoiceHeader().getInvoiceRefNo() != null ? invoice.getCreateInvoiceHeader().getInvoiceRefNo() : null);
            InvoiceType invoiceType = new InvoiceType();
            if (invoice.getCreateInvoiceHeader().getInvoiceType() != null) {
                invoiceType.setCode(invoice.getCreateInvoiceHeader().getInvoiceType());
                log.debug("invoice.getInvoiceType ----> :{}", invoice.getCreateInvoiceHeader().getInvoiceType());
                InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoice.getCreateInvoiceHeader().getInvoiceType(), "T000012");
                if (invoiceListValue != null) {
                    invoiceType.setDescription(invoiceListValue.getDescription() != null ? invoiceListValue.getDescription() : null);
                }
            }
            auditBillingReq.setInvoiceType(invoiceType);
            auditBillingReq.setTransactionType("MEMBER");
            auditBillingReq.setCustomerId(invoice.getCreateInvoiceHeader().getCustomerId() != null ? invoice.getCreateInvoiceHeader().getCustomerId() : null);
            auditBillingReq.setFullName(invoice.getCreateInvoiceHeader().getFullName() != null ? invoice.getCreateInvoiceHeader().getFullName() : null);
            auditBillingReq.setAddress(invoice.getCreateInvoiceHeader().getAddress() != null ? invoice.getCreateInvoiceHeader().getAddress() : null);
            auditBillingReq.setIssueDate(DateUtils.issueDateDueDateAudit(invoice.getCreateInvoiceHeader().getIssueDate() != null ? invoice.getCreateInvoiceHeader().getIssueDate() : null));
            auditBillingReq.setDueDate(DateUtils.issueDateDueDateAudit(invoice.getAdditionalObject().getDueDate() != null ? invoice.getAdditionalObject().getDueDate() : null));
            auditBillingReq.setFeeAmount(invoice.getAdditionalObject().getFeeAmount() != null ? invoice.getAdditionalObject().getFeeAmount() : null);
            auditBillingReq.setFineAmount(invoice.getAdditionalObject().getFineAmount() != null ? invoice.getAdditionalObject().getFineAmount() : null);
            auditBillingReq.setCollectionAmount(invoice.getAdditionalObject().getCollectionAmount() != null ? invoice.getAdditionalObject().getCollectionAmount() : null);
            auditBillingReq.setTotalAmount(invoice.getAdditionalObject().getTotalAmount() != null ? invoice.getAdditionalObject().getTotalAmount() : null);
            InvoiceStatus invoiceStatus = new InvoiceStatus();
            if (invoice.getAdditionalObject().getStatus() != null) {
                invoiceStatus.setCode(invoice.getAdditionalObject().getStatus());
                InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoice.getAdditionalObject().getStatus(), "T000013");
                if (invoiceListValue != null) {
                    invoiceStatus.setDescription(invoiceListValue.getDescription() != null ? invoiceListValue.getDescription() : null);
                }
            }
            auditBillingReq.setInvoiceStatus(invoiceStatus);
            auditBillingReq.setErrorMessage(invoice.getAdditionalObject().getErrorMessage() != null ? invoice.getAdditionalObject().getErrorMessage() : null);
            //List<com.appworks.co.th.batchworker.dto.audit.InvoiceDetail> invoiceDetailList = new ArrayList<>();
            //List<InvoiceDetail> invoiceDetails = invoiceDetailRepository.findAllByInvoice(invoice);
            auditBillingReq.setInvoiceDetails(invoice.getAdditionalObject().getDetails().stream().map(item -> {
                com.appworks.co.th.batchworker.dto.audit.InvoiceDetail invoiceDetail = new com.appworks.co.th.batchworker.dto.audit.InvoiceDetail();
                invoiceDetail.setInvoiceNo(item.getInvoice().getInvoiceNo());
                invoiceDetail.setTransactionId(item.getTransactionId());
                invoiceDetail.setTransactionDate(DateUtils.transactionDate(item.getTransactionDate()));
                invoiceDetail.setPlate1(item.getPlate1());
                invoiceDetail.setPlate2(item.getPlate2());
                Province province = new Province();
                if (item.getProvince() != null) {
                    province.setCode(item.getProvince());
                    InvoiceMasterVehicleOffice invoiceMasterVehicleOffice = masterVehicleOfficeRepository.findByCode(item.getProvince());
                    if (invoiceMasterVehicleOffice != null) {
                        province.setDescription(invoiceMasterVehicleOffice.getDescription());
                    }
                }
                invoiceDetail.setProvice(province);
                Origin origin = new Origin();
                Hq hqOrigin = new Hq();
                if (item.getHqCode() != null) {
                    hqOrigin.setCode(item.getHqCode());
                    InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getHqCode());
                    if (invoiceMasterHq != null) {
                        hqOrigin.setName(invoiceMasterHq.getDescription());
                    }
                }
                Plaza plazaOrigin = new Plaza();
                if (item.getPlazaCode() != null) {
                    plazaOrigin.setCode(item.getPlazaCode());
                    InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getPlazaCode());
                    if (invoiceMasterPlaza != null) {
                        plazaOrigin.setName(invoiceMasterPlaza.getDescription());
                    }
                }
                Lane laneOrigin = new Lane();
                if (item.getLaneCode() != null) {
                    laneOrigin.setCode(item.getLaneCode());
                    InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getLaneCode());
                    if (invoiceMasterLane != null) {
                        laneOrigin.setName(invoiceMasterLane.getDescription());
                    }
                }
                origin.setHq(hqOrigin);
                origin.setLane(laneOrigin);
                origin.setPlaza(plazaOrigin);
                invoiceDetail.setOrigin(origin);
                Destination destination = new Destination();
                Hq hqDes = new Hq();
                if (item.getDestHqCode() != null) {
                    hqDes.setCode(item.getDestHqCode());
                    InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getDestHqCode());
                    if (invoiceMasterHq != null) {
                        hqDes.setName(invoiceMasterHq.getDescription());
                    }
                }
                Plaza plazaDes = new Plaza();
                if (item.getDestPlazaCode() != null) {
                    plazaDes.setCode(item.getDestPlazaCode());
                    InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getDestPlazaCode());
                    if (invoiceMasterPlaza != null) {
                        plazaDes.setName(invoiceMasterPlaza.getDescription());
                    }
                }
                Lane laneDes = new Lane();
                if (item.getDestLaneCode() != null) {
                    laneDes.setCode(item.getDestLaneCode());
                    InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getDestLaneCode());
                    if (invoiceMasterLane != null) {
                        laneDes.setName(invoiceMasterLane.getDescription());
                    }
                }
                destination.setHq(hqDes);
                destination.setLane(laneDes);
                destination.setPlaza(plazaDes);
                invoiceDetail.setDestination(destination);
                invoiceDetail.setFeeAmount(item.getFeeAmount());
                invoiceDetail.setFineAmount(item.getFineAmount());
                invoiceDetail.setCollectionAmount(item.getCollectionAmount());
                invoiceDetail.setTotalAmount(item.getTotalAmount());
                OriginTranType originTranType = new OriginTranType();
                if (item.getOriginTranType() != null) {
                    originTranType.setCode(item.getOriginTranType());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(item.getOriginTranType(), "T000018");
                    if (invoiceListValue != null) {
                        originTranType.setDescription(invoiceListValue.getDescription() != null ? invoiceListValue.getDescription() : null);
                    }
                }
                invoiceDetail.setOriginTranType(originTranType);
                //List<Evidences> evidencesList = new ArrayList<>();
                //List<InvoiceEvidence> invoiceEvidences = invoiceEvidenceRepository.findAllByInvoiceDetail(item.getId());
                //if(!invoiceEvidences.isEmpty()){
                invoiceDetail.setEvidences(item.getEvidences().stream().map(e -> {
                    Evidences evidences = new Evidences();
                    evidences.setType(e.getType());
                    evidences.setFileId(e.getFile());
                    //evidencesList.add(evidences);
                    return evidences;
                }).collect(Collectors.toList()));
                //}
                //invoiceDetail.setEvidences(evidencesList);
                //invoiceDetailList.add(invoiceDetail);
                return invoiceDetail;
            }).collect(Collectors.toList()));
            //auditBillingReq.setInvoiceDetails(invoiceDetailList);
        }

        return auditBillingReq;
    }

    private boolean upDateInvoiceType(InvoiceObject invoiceObject){
        try {
            InvoiceMemberDomainEvent<CreateInvoiceDetail, CreateInvoiceHeader> domainEvent = mapToAggregate(invoiceObject);
            String invoiceNo=createInvoice(invoiceObject);
            if(StringUtils.isNotEmpty(invoiceNo)) {
                //invoiceStageService.insertInvoice(invoiceNo, invoiceObject.getCreateInvoiceHeader(), invoiceObject.getCreateInvoiceDetail(),invoiceObject.getCreateInvoiceHeader().getInvoiceChannel());
                ResultInvoiceDetail resultInvoiceDetail=invoiceStageService.insertInvoice(invoiceNo, invoiceObject.getCreateInvoiceHeader(), invoiceObject.getCreateInvoiceDetail(),invoiceObject.getCreateInvoiceHeader().getInvoiceChannel());

                if(resultInvoiceDetail!=null){
                    resultInvoiceDetail.setStatus(true);
                    resultInvoiceDetail.setMessage("Success");

                    ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                    resultInvoiceHeader.setType("MEMBER");

                    CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(null, InvoiceMemberCommand.class, CommonState.APPROVED, resultInvoiceHeader, resultInvoiceDetail);
                    createdInvoiceStagingDomainEvent.setInvoiceProceedListId(null);
                    createdInvoiceStagingDomainEvent.setInvoiceProceedId(null);
                    messagePublisher.sendDomainEvent("sentToNotificationEnforcement", createdInvoiceStagingDomainEvent);
                    return true;
                }
            }
        }catch (Exception e){
            log.error("Update invoice warning 1 error: ",e);
        }
        return false;
    }

    private String createInvoice(InvoiceObject obj) {
        try {
            Long seq = invoiceJdbcRepository.getSeqInvoice();
            if (seq != null) {
                String number = leftPad(String.valueOf(seq), 9, "0");
                String memberType = StringUtils.isEmpty(obj.getCreateInvoiceHeader().getCustomerId()) ? "2" : "1";
                return "0" + memberType + obj.getCreateInvoiceHeader().getInvoiceType() + df.format(new Date()) + number;
            }
        } catch (Exception e) {
            log.error("Error createInvoiceNonmember : {}",e);
        }
        return null;
    }
}
