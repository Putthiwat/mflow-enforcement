package com.appworks.co.th.batchworker.job.invoicegenerategroup1;

import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.job.invoicegenerategroup.InvoiceGenerateGroupItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceCustomerRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceVehicleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;

@Configuration
public class InvoiceGenerateGroup1Job {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ApplicationContext applicationContext;


    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;



    @Autowired
    @Qualifier("memberInvoiceGenerateGroupWarning1Requests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("memberInvoiceGenerateGroupWarning1Replies")
    private DirectChannel replies;

    public InvoiceGenerateGroup1Job(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> invoiceUpdateGroup1ItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue + " and invoiceType=1";
        log.info(jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }


    @Bean("invoiceUpdateGroup1ItemProcessor")
    public ItemProcessor<Invoice, Invoice> invoiceUpdateGroup1ItemProcessor() {
        return item -> item;
    }


    @Bean
    public Step updateInvoiceGroupWarning1Step() throws Exception {
        return this.workerStepBuilderFactory.get("updateInvoiceGroupWarning1Step")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, Invoice>chunk(1000)
                .reader(invoiceUpdateGroup1ItemReader(null,null, null))
                .processor(invoiceUpdateGroup1ItemProcessor())
                .writer(new InvoiceGenerateGroupItemWriter(
                        (CustomerInfoClient) applicationContext.getBean("customerInfoClient"),
                        (InvoiceCustomerRepository) applicationContext.getBean("invoiceCustomerRepository"),
                        (InvoiceVehicleRepository) applicationContext.getBean("invoiceVehicleRepository"),
                        (InvoiceJdbcRepository) applicationContext.getBean("invoiceJdbcRepository"),
                        (InvoiceRepository) applicationContext.getBean("invoiceRepository")
                ))
                .build();
    }
}
