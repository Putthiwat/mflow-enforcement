package com.appworks.co.th.batchworker.job.invoiceMemberWriterFile;

import com.appworks.co.th.batchworker.config.BillingMessagePublisher;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
public class InvoiceMemberWriterFileGenerateJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("invoiceMemberWriterFileGenerateRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("invoiceMemberWriterFileGenerateReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("#{'${day.bill.cycle}'.split(',')}")
    private List<Integer> dayOfBillCycle;

    public InvoiceMemberWriterFileGenerateJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> invoiceMemberWriterFileItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading invoice item reader : {} to {}", minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o " + where + " and id >= " + minValue + " and id <= " + maxValue  +
                " AND ISSUE_DATE <= TO_DATE(\'"+currentDate()+"\', 'YYYY/MM/DD') " +
                " AND TO_DATE(\'"+currentDate()+"\', 'YYYY/MM/DD') <= DUE_DATE ";
        log.debug(jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("invoiceFailedItemProcessor")
    public ItemProcessor<Invoice, Invoice> invoiceMemberWriterFileItemProcessor() {
        return item -> item;
    }

    @Bean
    public Step moveFileInvoiceFailedStep() throws Exception {
        return this.workerStepBuilderFactory.get("moveFileInvoiceFailedStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, Invoice>chunk(1000)
                .reader(invoiceMemberWriterFileItemReader(null, null, null))
                .processor(invoiceMemberWriterFileItemProcessor())
                .writer(new InvoiceMemberWriterFileGenerateItemWriter(
                        (CustomerClient) applicationContext.getBean("customerClient"),
                        (BillingMessagePublisher) applicationContext.getBean("billingMessagePublisher"),
                        dayOfBillCycle,
                        (MasterClient) applicationContext.getBean("masterClient"),
                        (CustomerInfoClient) applicationContext.getBean("customerInfoClient"),
                        (InvoiceRepository) applicationContext.getBean("invoiceRepository")
                ))
                .build();
    }

    private String issueDate(){
        Calendar currentDate = Calendar.getInstance();
        currentDate.setTime(new Date());
        Date date = currentDate.getTime();
        String issueDate = df.format(date);
        return issueDate;
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        log.info("Data currentDate : {}",currentDate.getTime());
        return currentDateString;
    }

    private String previousIssueDate(){
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE,-1);
        Date date = currentDate.getTime();
        String issueDate = df.format(date);
        return issueDate;
    }

}
