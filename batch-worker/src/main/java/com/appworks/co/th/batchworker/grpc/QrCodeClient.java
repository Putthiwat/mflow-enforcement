package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.masterservice.MasterServiceGrpc;
import com.appworks.co.th.mpassbatch.MPassBatchGrpc;
import com.appworks.co.th.mpassbatch.MPassBatchRequest;
import com.appworks.co.th.mpassbatch.MPassBatchResponse;
import com.appworks.co.th.qrcodeallservice.MasterQrDataAllResponse;
import com.appworks.co.th.qrcodeallservice.QrCodeAllServiceGrpcGrpc;
import com.appworks.co.th.qrcodeallservice.QrCodeMemberRequest;
import com.appworks.co.th.qrcodeallservice.QrCodeNonMemberRequest;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class QrCodeClient {

    @Value("${grpc.invoiceservice.host}")
    private String host;

    @Value("${grpc.invoiceservice.port}")
    private String port;

    private QrCodeAllServiceGrpcGrpc.QrCodeAllServiceGrpcBlockingStub grpcBlockingStub;

//    @PostConstruct
//    public void startConsumer() {
//        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
//        this.grpcBlockingStub = QrCodeAllServiceGrpcGrpc.newBlockingStub(channel);
//    }

    @Autowired
    public void startConsumer() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        QrCodeAllServiceGrpcGrpc.QrCodeAllServiceGrpcBlockingStub stub = QrCodeAllServiceGrpcGrpc.newBlockingStub(channel);
        this.grpcBlockingStub = stub;
    }

    public MasterQrDataAllResponse getQrCodeMember(QrCodeMemberRequest request){
        return grpcBlockingStub.getQrCodeMember(request);
    }
    public MasterQrDataAllResponse getQrCodeBotMember(QrCodeMemberRequest request){
        MasterQrDataAllResponse response = this.grpcBlockingStub.getQrCodeBotMember(request);
        return response;
    }
//    public MasterQrDataAllResponse getQrCodeBotMember(QrCodeMemberRequest request){
//        return grpcBlockingStub.getQrCodeBotMember(request);
//    }

    public MasterQrDataAllResponse getQrCodeNonMember(QrCodeNonMemberRequest request){
        return grpcBlockingStub.getQrCodeNonMember(request);
    }

    public MasterQrDataAllResponse getQrCodeBotNonMember(QrCodeNonMemberRequest request){
        MasterQrDataAllResponse response = this.grpcBlockingStub.getQrCodeBotNonMember(request);
        return response;
    }

//    public MasterQrDataAllResponse getQrCodeBotNonMember(QrCodeNonMemberRequest request){
//        return grpcBlockingStub.getQrCodeBotNonMember(request);
//    }

}
