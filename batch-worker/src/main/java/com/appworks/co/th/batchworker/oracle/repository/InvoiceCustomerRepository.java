package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceCustomer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface InvoiceCustomerRepository extends JpaRepository<InvoiceCustomer, Long>{
    Optional<InvoiceCustomer> findByCustomerId(String customerId);
}
