package com.appworks.co.th.batchworker.dto.audit;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Plaza {
    private String code;
    private String name;
    private String description;
}
