package com.appworks.co.th.batchworker;

import com.appworks.co.th.batchworker.config.BrokerConfiguration;
import com.appworks.co.th.batchworker.config.prop.AsyncCustomerProperty;
import com.appworks.co.th.batchworker.config.prop.AsyncLogProperty;
import com.appworks.co.th.batchworker.grpc.QrCodeClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceMasterJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.InvoiceService;
import com.appworks.co.th.batchworker.service.ReportService;
import com.appworks.co.th.batchworker.service.ReportServiceImpl;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableBatchProcessing
@SpringBootApplication(scanBasePackages = "com.appworks.co.th")
@EnableConfigurationProperties({AsyncLogProperty.class, AsyncCustomerProperty.class})
@Import({BrokerConfiguration.class})
public class BatchWorkerApplication implements CommandLineRunner  {

	@Autowired
	ApplicationContext context;

	public static void main(String[] args) {
		SpringApplication.run(BatchWorkerApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		ReportService reportService = context.getBean(ReportServiceImpl.class);
//		InvoiceRepository invoiceRepository = context.getBean(InvoiceRepository.class);
//		InvoiceNonMemberRepository invoiceNonMemberRepository = context.getBean(InvoiceNonMemberRepository.class);
//		InvoiceMasterJdbcRepository invoiceMasterColorDao = context.getBean(InvoiceMasterJdbcRepository.class);
//		InvoiceService paymentService =  context.getBean(InvoiceService.class);
//		QrCodeClient qrClient = context.getBean(QrCodeClient.class);
//		FileService fileService = context.getBean(FileService.class);



//		List<String> strings = new ArrayList<>();
//		strings.add("VCOLOR0001");
//		strings.add("VCOLOR0002");
//		ColorNameDTO colorDescriptionDTO = invoiceMasterColorDao.getColorNameByCodes(null);
//		if (colorDescriptionDTO != null){
//			System.out.println(colorDescriptionDTO.getDescriptionTh());
//		}

//		String ddd = paymentService.getQrCodeMemberBase64("IM020201026055719511");
//		System.out.println(ddd);
//		byte[] bytes = Base64.decodeBase64(ddd);
//
//		OutputStream os = new FileOutputStream(new File("D:\\member.png"));
//		os.write(bytes);
//		os.flush();
//		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
//		javax.imageio.ImageIO.read(byteArrayInputStream);


//		for (int i = 2; i <=10; i++){
//			Invoice invoice = invoiceRepository.findByInvoiceNo("010201209000005992");
//			JasperPrint jasperMember = reportService.generateInvoiceMember(invoice);
//			JasperExportManager.exportReportToPdfFile(jasperMember, "./tarn"+i+".pdf");
//		}



//		JasperPrint jasperPrint = reportService.generateInvoiceMember(invoice);
//		byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);
//		String fileId = fileService.uploadFile(bytes,"pdf");
//		System.out.println(fileId);

//		InvoiceNonMember invoiceNonMember = invoiceNonMemberRepository.findByInvoiceNo("021201105000000158");
//		JasperPrint jasperNonMember = reportService.generateInvoiceNonMember(invoiceNonMember);
//		JasperExportManager.exportReportToPdfFile(jasperNonMember, "D:\\nonmember.pdf");

	}
}
