package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceMasterVehicleColor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterVehicleColorRepository extends JpaRepository<InvoiceMasterVehicleColor, Long> {
}
