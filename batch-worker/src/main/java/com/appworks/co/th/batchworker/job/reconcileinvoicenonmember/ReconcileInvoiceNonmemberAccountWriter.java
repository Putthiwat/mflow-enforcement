package com.appworks.co.th.batchworker.job.reconcileinvoicenonmember;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.CompositeMemberReconcileDTO;
import com.appworks.co.th.batchworker.dto.CompositeNonmemberReconcileDTO;
import com.appworks.co.th.batchworker.job.reconcileinvoicememner.ReconcileInvoiceMemberAccountWriter;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.entity.ReconcileBankAccountReport;
import com.appworks.co.th.batchworker.oracle.jdbc.ReconcileJdbcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReconcileInvoiceNonmemberAccountWriter implements ItemWriter<CompositeNonmemberReconcileDTO> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${service-schema.invoice}")
    private String invoiceSchema;

    @Autowired
    public ReconcileJdbcRepository reconcileJdbcRepository;

    @Override
    public void write(List<? extends CompositeNonmemberReconcileDTO> items) throws Exception {

        List<ReconcileBankAccountReport> accountReconciles = new ArrayList<>();
        for (CompositeNonmemberReconcileDTO item : items) {
            accountReconciles.add(item.getAccountReconcile());
        }

        int[] effects = reconcileJdbcRepository.batchInsertAccountReconcile(invoiceSchema, accountReconciles);
        log.info("reconcile-account-member write size {} updated rows {}", accountReconciles.size(), effects!=null ? effects.length:0);

    }

}
