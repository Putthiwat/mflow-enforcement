package com.appworks.co.th.batchworker.job.invoicegenerateebill;

import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.service.CacheService;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.GrpcSendSmsAndEmailService;
import com.appworks.co.th.batchworker.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
public class InvoiceGenerateEBillJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("memberInvoiceGenerateEBillRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("memberInvoiceGenerateEBillReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    @Value("${job.generate-invoice.max-retry:3}")
    private int maxRetry;

    @Value("${job.generate-invoice.retry-delay:500}")
    private int retryDelay;

    @Value("${send.notification.ebill.flag}")
    private boolean sendNotificationFlag;

    @Value("${ebill.billcycle.url}")
    private String EbillLink;

    @Value("${AccrualList.billcycle.url}")
    private String AccrualListLink;

    public InvoiceGenerateEBillJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> invoiceUpdateEBillItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
            ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue;
        log.info(jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("invoiceUpdateEBillItemProcessor")
    public ItemProcessor<Invoice, Invoice> invoiceUpdateEBillItemProcessor() {
        return item -> item;
    }

    @Bean
    public Step updateInvoiceEBillStep() throws Exception {
        return this.workerStepBuilderFactory.get("updateInvoiceEBillStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<Invoice, Invoice>chunk(1000)
                .reader(invoiceUpdateEBillItemReader(null,null, null))
                .processor(invoiceUpdateEBillItemProcessor())
                .writer(new InvoiceGenerateEBillItemWriter(
                        EbillLink,sendNotificationFlag,AccrualListLink,maxRetry,
                        (CacheService) applicationContext.getBean("cacheService"),
                        (FileService) applicationContext.getBean("fileService"),
                        (ReportService) applicationContext.getBean("reportService"),
                        (InvoiceRepository) applicationContext.getBean("invoiceRepository"),
                        (NotificationManagementClient) applicationContext.getBean("notificationManagementClient"),
                        (GrpcSendSmsAndEmailService) applicationContext.getBean("grpcSendSmsAndEmailService"),
                        (CustomerInfoClient) applicationContext.getBean("customerInfoClient"),
                        (CustomerClient) applicationContext.getBean("customerClient")

                ))
                .build();
    }

}
