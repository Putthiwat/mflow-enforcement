package com.appworks.co.th.batchworker.exception;

public class JobException extends RuntimeException {

    public JobException(){}

    public JobException(String message){
        super(message);
    }

}
