package com.appworks.co.th.batchworker.oracle.entity;


import lombok.Data;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@Table(name = "MF_INVOICE_RECONCILE_BATCH_JOB")
@Where(clause = "DELETE_FLAG = 0")
public class ReconcileBatchJob {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "BATCH_NAME", columnDefinition = "VARCHAR2(50)")
    @NotNull
    private String batchName;

    @Column(name = "FILE_ID", columnDefinition = "VARCHAR2(255)")
    @NotNull
    private String fileId;

    @Column(name = "FILE_NAME", columnDefinition = "VARCHAR2(255)")
    @NotNull
    private String fileName;

    @Column(name = "STATUS", columnDefinition = "VARCHAR2(50)")
    @NotNull
    private String status;

    @Column(name = "FAIL_REASON", columnDefinition = "VARCHAR2(255)")
    private String fileReason;

    @Column(name = "CREATE_CHANNEL", columnDefinition = "VARCHAR2(50) default 'Batch Job'")
    @NotNull
    private String createChannel;

    @Column(name = "CREATE_BY", columnDefinition = "VARCHAR2(255) default 'Batch Job'")
    @NotNull
    private String createBy;

    @Column(name = "CREATE_DATE")
    @NotNull
    private Instant createDate;

    @Column(name = "UPDATE_CHANNEL", columnDefinition = "VARCHAR2(25) default 'Batch Job'")
    private String updateChannel;

    @Column(name = "UPDATE_BY", columnDefinition = "VARCHAR2(255) default 'Batch Job'")
    private String updateBy;

    @Column(name = "UPDATE_DATE")
    private Instant updateDate;

    @Column(name = "VERSION", columnDefinition = "SMALLINT")
    @NotNull
    private Integer version;
}
