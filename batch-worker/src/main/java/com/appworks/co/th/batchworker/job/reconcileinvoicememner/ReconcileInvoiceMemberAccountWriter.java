package com.appworks.co.th.batchworker.job.reconcileinvoicememner;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.CompositeMemberReconcileDTO;
import com.appworks.co.th.batchworker.oracle.entity.ReconcileBankAccountReport;
import com.appworks.co.th.batchworker.oracle.jdbc.ReconcileJdbcRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Component
public class ReconcileInvoiceMemberAccountWriter implements ItemWriter<CompositeMemberReconcileDTO> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${service-schema.invoice}")
    private String invoiceSchema;

    @Autowired
    public ReconcileJdbcRepository reconcileJdbcRepository;

    @Override
    public void write(List<? extends CompositeMemberReconcileDTO> items) throws Exception {

        List<ReconcileBankAccountReport> accountReconciles = new ArrayList<>();
        for (CompositeMemberReconcileDTO item : items) {
            accountReconciles.add(item.getAccountReconcile());
        }

        int[] effects = reconcileJdbcRepository.batchInsertAccountReconcile(invoiceSchema, accountReconciles);
        log.info("reconcile-account-member write size {} updated rows {}", accountReconciles.size(), effects!=null ? effects.length: 0);

    }

}
