package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class InvoiceStatus {
    private String code;
    private String description;
}
