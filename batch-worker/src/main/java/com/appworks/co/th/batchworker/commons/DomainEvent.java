package com.appworks.co.th.batchworker.commons;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class DomainEvent<K,T> implements Serializable {
    private String hqCode;
    private String receiveInfo;
    private String paymentMethod;
    private String status;
    private K header;
    private T detail;

    public DomainEvent(String hqCode, String receiveInfo, String paymentMethod, String status, K header, T detail) {
        this.hqCode = hqCode;
        this.receiveInfo = receiveInfo;
        this.paymentMethod = paymentMethod;
        this.status = status;
        this.header = header;
        this.detail = detail;
    }
}
