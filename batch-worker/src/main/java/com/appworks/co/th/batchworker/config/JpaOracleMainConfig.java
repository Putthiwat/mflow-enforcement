package com.appworks.co.th.batchworker.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "com.appworks.co.th.batchworker.oracle.repository",
        entityManagerFactoryRef = "oracleMainEntityManagerFactory",
        transactionManagerRef = "oracleMainTransactionManager"
)
public class JpaOracleMainConfig {

    @Primary
    @Bean(name = "oracleMainDataSource")
    @ConfigurationProperties("spring.datasource.oracle-main.hikari")
    public DataSource oracleMainDataSource() {
        return new HikariDataSource();
    }

    @Bean(name = "oracleMainProperties")
    @ConfigurationProperties("spring.jpa.properties.oracle-main")
    public Properties oracleMainProperties() {
        return new Properties();
    }

    @Bean(name = "oracleMainEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean oracleMainEntityManagerFactory(
            @Qualifier("oracleMainDataSource") DataSource oracleMainDataSource,
            @Qualifier("oracleMainProperties") Properties oracleMainProperties) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(oracleMainDataSource);
        factory.setPackagesToScan("com.appworks.co.th.batchworker.oracle.entity");
        factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factory.setJpaProperties(oracleMainProperties);
        return factory;
    }

    @Bean(name = "oracleMainTransactionManager")
    public PlatformTransactionManager oracleMainTransactionManager(
            @Qualifier("oracleMainEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory){
        return new JpaTransactionManager(entityManagerFactory.getObject());
    }

    @Bean(name = "oracleMainJdbcTemplate")
    public JdbcTemplate oracleJdbcTemplate(@Qualifier("oracleMainDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean(name = "oracleMainNamedJdbcTemplate")
    public NamedParameterJdbcTemplate oracleNamedJdbcTemplate(@Qualifier("oracleMainDataSource") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }
}
