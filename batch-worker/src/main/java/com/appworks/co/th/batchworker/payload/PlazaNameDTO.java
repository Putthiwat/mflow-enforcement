package com.appworks.co.th.batchworker.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class PlazaNameDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private String nameTh;
    private String nameEn;

}
