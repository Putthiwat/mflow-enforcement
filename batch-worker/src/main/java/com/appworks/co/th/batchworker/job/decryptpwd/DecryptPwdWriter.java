package com.appworks.co.th.batchworker.job.decryptpwd;

import com.appworks.co.th.batchworker.oracle.entity.CustomerCard;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceStaging;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Component
public class DecryptPwdWriter implements ItemWriter<CustomerCard> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${service-schema.customer}")
    private String customerSchema;

    private static final String UPDATE_STATEMENT = "UPDATE %s.MF_CUST_VISA_MASTER_CARD SET RETRY = ?, TOKEN = ? WHERE ID = ?";

    @Autowired
    @Qualifier("oracleJdbcTemplate")
    public JdbcTemplate jdbcTemplate;

    @Override
    public void write(List<? extends CustomerCard> items) throws Exception {
        String statement = String.format(UPDATE_STATEMENT, customerSchema);
        int[] effects = jdbcTemplate.batchUpdate(statement, new CustomerCardBatchPreparedStatement(items));
        log.info("decrypt token job updated rows {}", effects.length);
    }

    class CustomerCardBatchPreparedStatement implements BatchPreparedStatementSetter {

        private List<? extends CustomerCard> customerCards;

        public CustomerCardBatchPreparedStatement(List<? extends CustomerCard> customerCards){
            this.customerCards = customerCards;
        }

        @Override
        public void setValues(PreparedStatement ps, int i) throws SQLException {
            CustomerCard customerCard = customerCards.get(i);
            ps.setInt(1, customerCard.getRetry());
            ps.setString(2, customerCard.getToken());
            ps.setLong(3, customerCard.getId());
        }

        @Override
        public int getBatchSize() {
            return this.customerCards.size();
        }

    }

}
