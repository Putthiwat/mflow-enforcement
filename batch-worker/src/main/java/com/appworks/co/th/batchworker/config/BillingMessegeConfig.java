package com.appworks.co.th.batchworker.config;

import com.appworks.co.th.batchworker.commons.DomainEvent;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class BillingMessegeConfig {

    @Value("${saga.kafka.bootstrap.servers}")
    private String kafka;


    @Bean
    public ProducerFactory<String, String> billProducerFactory() {
        return new DefaultKafkaProducerFactory<>(billProducerConfig());
    }

    @Bean
    public Map<String, Object> billProducerConfig() {
        Map<String, Object> props = new HashMap<>();
//        props.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafka);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        return props;
    }

    @Bean
    public KafkaTemplate<String, String> billKafkaTemplate() {
        return new KafkaTemplate<>(billProducerFactory());
    }

    @Bean
    public BillingMessagePublisher billMessagePublisher() {
        return new BillingMessagePublisher();
    }





}