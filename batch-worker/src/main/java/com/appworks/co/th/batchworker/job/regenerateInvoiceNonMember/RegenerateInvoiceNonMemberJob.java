package com.appworks.co.th.batchworker.job.regenerateInvoiceNonMember;

import com.appworks.co.th.batchworker.dto.InvoiceProceedDTO;
import com.appworks.co.th.batchworker.job.regenerateInvoiceMember.RegenerateInvoiceMemberAndNonJobWrite;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.OraclePagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Configuration
public class RegenerateInvoiceNonMemberJob {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    private EntityManagerFactory entityManagerFactory;

    @Value("${confing.day.invoiceNonMember}")
    private String dayInvoiceNonMember;

    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    @Qualifier("regenerateInvoiceNonMemberJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("regenerateInvoiceNonMemberJobReplies")
    private DirectChannel replies;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("#{'${master.invoice.day}'.split(',')}")
    private List<Integer> invoiceDay;

    public RegenerateInvoiceNonMemberJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    private String select1Clause() {
        StringBuilder select = new StringBuilder()
                .append("SELECT invoiceProceed.id AS id,\n" +
                        "\t\tinvoiceProceed.AGGREGATE_ID AS aggregateId ,\n" +
                        "\t\tinvoiceProceed.CUSTOMER_ID AS customerId,\n" +
                        "\t\tinvoiceProceed.INVOICE_NO AS invoiceNo,\n" +
                        "\t\tinvoiceProceed.CREATE_DATETIME AS createDateTime,\n" +
                        "\t\tinvoiceProceedList.ID AS invoiceProceedListID,\n" +
                        "\t\tinvoiceProceedList.COMMAND AS command,\n" +
                        "\t\tinvoiceProceedList.DETAIL AS detail,\n" +
                        "\t\tinvoiceProceedList.STATE AS   state\n");
        return select.toString();
    }

    private String from1Clause() {
        StringBuilder from = new StringBuilder()
                .append("\t\tFROM MF_INVOICE_PROCEED_LIST invoiceProceedList\n" +
                        "LEFT JOIN MF_INVOICE_PROCEED invoiceProceed ON  invoiceProceedList.INVOICE_PROCEED_ID =  invoiceProceed.ID " +
                        "AND invoiceProceedList.COMMAND = invoiceProceed.COMMAND ");
        return from.toString();
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<InvoiceProceedDTO> regenerateInvoiceNonMemberReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {

        String startOfDay = this.currentDate();
        String endOfDay = this.currentEndDate();
        if (!StringUtils.equals(dayInvoiceNonMember, "ALL")) {
            startOfDay = dayInvoiceNonMember + " 00:00:00";
            endOfDay = dayInvoiceNonMember + " 23:59:59";
        }

        HashMap<String, Order> sortKeys = new HashMap<>(2);
        sortKeys.put("ID", Order.ASCENDING);
        OraclePagingQueryProvider queryProvider = new OraclePagingQueryProvider();
        queryProvider.setSelectClause(this.select1Clause());
        queryProvider.setFromClause(this.from1Clause());

        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.HOUR, -1);

        queryProvider.setWhereClause(String.format("%s AND invoiceProceed.ID >= %s AND invoiceProceed.ID <= %s " +
                " and invoiceProceed.CREATE_DATETIME <= TO_TIMESTAMP(\'" + dateFormat.format(currentDate.getTime()) + "\'  , 'dd-mm-yyyy hh24:mi:ss')", where, minValue, maxValue));
        queryProvider.setSortKeys(sortKeys);
        JdbcPagingItemReader<InvoiceProceedDTO> pagingItemReader = new JdbcPagingItemReader<>();
        pagingItemReader.setPageSize(1000);
        pagingItemReader.setDataSource(this.dataSource);
        pagingItemReader.setRowMapper(BeanPropertyRowMapper.newInstance(InvoiceProceedDTO.class));
        pagingItemReader.setQueryProvider(queryProvider);
        pagingItemReader.afterPropertiesSet();
        return pagingItemReader;
    }

    @Bean("regenerateInvoiceNonMemberItemProcessor")
    public ItemProcessor<InvoiceProceedDTO, InvoiceProceedDTO> regenerateInvoiceNonMemberItemProcessor() {
        return item -> item;
    }


    @Bean
    public Step regenerateInvoiceNonMemberStep() throws Exception {
        return this.workerStepBuilderFactory.get("regenerateInvoiceNonMemberStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceProceedDTO, InvoiceProceedDTO>chunk(1000)
                .reader(regenerateInvoiceNonMemberReader(null, null, null))
                .processor(regenerateInvoiceNonMemberItemProcessor())
                .writer(new RegenerateInvoiceMemberAndNonJobWrite(
                        (MessagePublisher) applicationContext.getBean("messagePublisher"),
                        (InvoiceJdbcRepository) applicationContext.getBean("invoiceJdbcRepository")
                        , invoiceDay
                ))
                .build();
    }

    private String currentEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, -1);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return df.format(calendar.getTime());
    }

    private String currentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, -1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return df.format(calendar.getTime());
    }

}
