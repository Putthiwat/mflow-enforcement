package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMemberDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface InvoiceNonMemberDetailRepository extends JpaRepository<InvoiceNonMemberDetail, Long> {
}
