package com.appworks.co.th.batchworker.job.groupInvoiceNonmemberWarning2;

import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.payload.AdditionalObject;
import com.appworks.co.th.batchworker.service.RedisService;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.sagaappworks.batch.generateInvoice.InvoiceObject;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceEvidence;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceTransaction;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;

import javax.persistence.EntityManagerFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Configuration
public class GroupInvoiceNonmemberWarning2Job {


    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;
    private final FastDateFormat YYYYMMDD_HHMMSS_SSS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("Asia/Bangkok"));
    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("groupInvoiceNonmemberWarning2JobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("groupInvoiceNonmemberWarning2JobReplies")
    private DirectChannel replies;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MasterClient masterClient;

    @Autowired
    private InvoiceNonMemberRepository invoiceNonMemberRepository;


    public GroupInvoiceNonmemberWarning2Job(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<InvoiceNonMember> groupInvoiceNonmemberWarning2ItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info("invoiceUpdateNonMemGroupItemReader reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from InvoiceNonMember o "+where+" and id >= "+ minValue +" and id <= "+ maxValue+" AND DUE_DATE < TO_DATE(\'" + currentDate() + "\', 'YYYY/MM/DD')";
        JpaPagingItemReader<InvoiceNonMember> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }



    @Bean("groupInvoiceNonmemberWarning2ItemProcessor")
    public ItemProcessor<InvoiceNonMember, InvoiceObject> groupInvoiceNonmemberWarning2ItemProcessor() {
        return new ItemProcessor<InvoiceNonMember, InvoiceObject>() {
            @Override
            public InvoiceObject process(InvoiceNonMember item) throws Exception {
                HqResponse hqResponse = masterClient.getHq(HqRequest.newBuilder().setHqCode(item.getHqCode()).build());
                InvoiceObject invoiceObject = new InvoiceObject();
                CreateInvoiceHeader header = new CreateInvoiceHeader();
                header.setInvoiceType("2");
                header.setInvoiceRefNo(item.getInvoiceNo());
                header.setPlate1(item.getPlate1());
                header.setPlate2(item.getPlate2());
                header.setPlateProvince(item.getProvince());
                header.setFullName(item.getFullName());
                header.setAddress(item.getAddress());
                header.setIssueDate(item.getIssueDate());
                header.setDueDate(item.getDueDate());
                header.setChannel("Batch-service");
                header.setHqCode(item.getHqCode());
                header.setCreateBy(item.getCreateBy());
                header.setVat(0.0);
                header.setServiceProvider(hqResponse.getServiceProvider().getCode());
                header.setBrand(item.getBrand());
                header.setDocType(item.getDocType());
                header.setDocNo(item.getDocNo());
                header.setVehicleCode(item.getVehicleCode());
                header.setVehicleTypeCode(item.getVehicleType());
                header.setColor(item.getColors().stream().map(invoiceColor -> invoiceColor.getCode()).collect(Collectors.toList()));

                AdditionalObject additionalObject = new AdditionalObject();
                additionalObject.setDueDate(item.getDueDate());
                additionalObject.setFeeAmount(item.getFeeAmount());
                additionalObject.setFineAmount(item.getFineAmount());
                additionalObject.setCollectionAmount(item.getCollectionAmount());
                additionalObject.setTotalAmount(item.getTotalAmount());
                additionalObject.setStatus(item.getStatus());
                additionalObject.setErrorMessage(item.getErrorMessage());
                additionalObject.setDetailsNonMem(item.getDetails());

                CreateInvoiceDetail detail = new CreateInvoiceDetail();
                detail.setDetails(item.getDetails().stream().map(invoiceDetail -> {
                    CreateInvoiceTransaction transaction = new CreateInvoiceTransaction();
                    transaction.setId(invoiceDetail.getTransactionId());
                    transaction.setTransactionDate(String.valueOf(transactionInstantToDate(invoiceDetail.getTransactionDate())));
                    transaction.setPlate1(invoiceDetail.getPlate1());
                    transaction.setPlate2(invoiceDetail.getPlate2());
                    transaction.setProvince(invoiceDetail.getProvince());
                    transaction.setHqCode(invoiceDetail.getHqCode());
                    transaction.setPlazaCode(invoiceDetail.getPlazaCode());
                    transaction.setLaneCode(invoiceDetail.getLaneCode());
                    transaction.setFeeAmountOld(invoiceDetail.getFeeAmountOld().doubleValue());
                    transaction.setDestinationHqCode(invoiceDetail.getDestHqCode());
                    transaction.setDestinationPlazaCode(invoiceDetail.getDestPlazaCode());
                    transaction.setDestinationLaneCode(invoiceDetail.getDestLaneCode());
                    transaction.setFeeAmount(invoiceDetail.getRawFee().doubleValue());
                    transaction.setDiscount(invoiceDetail.getDiscount().doubleValue());
                    transaction.setFineAmount(invoiceDetail.getFineAmount().doubleValue());
                    transaction.setOperationFee(invoiceDetail.getOperationFee().doubleValue());
                    transaction.setCollectionAmount(invoiceDetail.getCollectionAmount().doubleValue());
                    transaction.setVehicleWheel(invoiceDetail.getVehicleWheel());
                    transaction.setVat(invoiceDetail.getVat().doubleValue());
                    transaction.setOriginTranType(invoiceDetail.getOriginTranType());
                    transaction.setEvidences(invoiceDetail.getEvidences().stream().map(invoiceEvidence -> {
                        CreateInvoiceEvidence evidence = new CreateInvoiceEvidence();
                        evidence.setType(invoiceEvidence.getType());
                        evidence.setFile(invoiceEvidence.getFile());
                        return evidence;
                    }).collect(Collectors.toList()));
                    return transaction;
                }).collect(Collectors.toList()));

                invoiceObject.setCreateInvoiceHeader(header);
                invoiceObject.setCreateInvoiceDetail(detail);
                return invoiceObject;
            }
        };
    }

    private String transactionInstantToDate(Instant transactionDate) {
        Date date=Date.from(transactionDate);
        return YYYYMMDD_HHMMSS_SSS.format(date);
    }

    @Bean
    public Step groupInvoiceNonmemberWarning2() throws Exception {
        return this.workerStepBuilderFactory.get("groupInvoiceNonmemberWarning2")
                .inputChannel(requests)
                .outputChannel(replies)
                .<InvoiceNonMember, InvoiceObject>chunk(1000)
                .reader(groupInvoiceNonmemberWarning2ItemReader(null,null, null))
                .processor(groupInvoiceNonmemberWarning2ItemProcessor())
                .writer(new GroupInvoiceNonmemberWarning2Writer(
                        (RedisService) applicationContext.getBean("redisService")
                ))
                .build();
    }


    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }

}
