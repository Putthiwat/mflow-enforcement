package com.appworks.co.th.batchworker.payload;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceDetail;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMemberDetail;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class AdditionalObject {
    private Date dueDate;
    private BigDecimal feeAmount;
    private BigDecimal fineAmount;
    private BigDecimal collectionAmount;
    private BigDecimal totalAmount;
    private String status;
    private String errorMessage;
    private List<InvoiceDetail> details;
    private List<InvoiceNonMemberDetail> detailsNonMem;
}
