package com.appworks.co.th.batchworker.service.invoice;

import com.appworks.co.th.sagaappworks.batch.ResultInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import org.springframework.stereotype.Service;

@Service
public interface InvoiceStageService {

    ResultInvoiceDetail createMemberStagingInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail,ResultInvoiceDetail resultInvoiceDetail);

    ResultInvoiceDetail insertInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, String invoiceChannel);


}
