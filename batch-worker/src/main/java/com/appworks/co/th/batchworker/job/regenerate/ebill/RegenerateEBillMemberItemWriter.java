package com.appworks.co.th.batchworker.job.regenerate.ebill;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.commons.PDFUtils;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.service.CacheService;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.GrpcSendSmsAndEmailService;
import com.appworks.co.th.batchworker.service.ReportService;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.appworks.co.th.batchworker.commons.Constants.Invoice.EBIL_FORMAT;

@AllArgsConstructor
public class RegenerateEBillMemberItemWriter implements ItemWriter<Invoice> {

    final SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmssSSS", Locale.US);

    @Value("${ebill.billcycle.url}")
    private String ebillLink;

    @Value("${send.notification.ebill.flag}")
    private boolean sendNotificationFlag;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private int maxRetry;

    private CacheService cacheService;

    private FileService fileService;

    private ReportService reportService;

    private InvoiceRepository invoiceRepository;


    private NotificationManagementClient notificationManagementClient;


    private GrpcSendSmsAndEmailService grpcSendSmsAndEmailService;


    private CustomerInfoClient customerInfoClient;

    private CustomerClient customerClient;

    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoice -> {
                try {
                    log.info("Start Generate E-Bill for Member with invoice no. : {}", invoice != null ? invoice.getInvoiceNo() : "-");
                    generateEbill(invoice);
                } catch (Exception e) {
                    log.error("InvoiceGenerateEBillItemWriter error : {}", e);
                }
            });
        }
    }

    @Async
    public void generateEbill(Invoice invoice) {
        try {
            JasperPrint jasperPrint = reportService.generateInvoiceMember(invoice);
            log.debug("JasperPrint : {}", jasperPrint);
            byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);

            String pdfPassword = cacheService.getPhone(invoice.getCustomerId());
            log.debug("pdfPassword : {}", pdfPassword);

            byte[] bytesEncrypt = PDFUtils.encryptWithPassword(bytes, pdfPassword);

            String fileId = String.format(EBIL_FORMAT, invoice.getInvoiceNo(), df.format(Calendar.getInstance().getTime()));

            fileService.uploadFile(bytesEncrypt, "pdf", fileId);

            invoiceRepository.updateEBillFileIdByInvoiceNo(fileId, Constants.SYSTEM, invoice.getInvoiceNo());

        } catch (Exception e) {
            log.error("Regenerate E-Bill Member invoice no -> {}, error -> {}", invoice != null ? invoice.getInvoiceNo() : "-", e);
        }
    }

    private String getCustomerLang(String customerId) {
        return customerClient.getCustomerNotificationInfo(customerId);
    }

    private String generateKey(String fileId) {

        Map<String, Object> claims = new HashMap<>();
        claims.put("fileId", fileId);
        return fileId;
    }

}