package com.appworks.co.th.batchworker.job.movestaging;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.config.BillingMessagePublisher;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.RedisService;
import com.appworks.co.th.batchworker.service.ReportService;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
public class UpdateInvoiceStageJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("invoiceStagingJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("invoiceStagingJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    @Value("#{'${day.bill.cycle}'.split(',')}")
    private List<Integer> dayOfBillCycle;

    @Value("${fine.invoice.member.Type.zero}")
    private Long invoiceMemberTypeZero;

    @Autowired
    RedisService redisService;

    public UpdateInvoiceStageJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    @Bean
    @StepScope
    public JpaPagingItemReader<InvoiceStaging> invoiceStagingUpdateItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}", minValue, maxValue);
        String jpqlQuery = "SELECT o from InvoiceStaging o " + where + " and id >= " + minValue + " and id <= " + maxValue;

        if (!isDoAll) {
            jpqlQuery += " AND (issueDate <= TO_DATE(\'" + previousIssueDate() + "\', \'YYYY/MM/DD\') " +
                    " OR (issueDate = TO_DATE(\'" + issueDate() + "\', \'YYYY/MM/DD\') AND invoiceChannel='BILL_CYCLE')) ";
        }

        log.debug("UpdateInvoiceStageJob SQL {}", jpqlQuery);

        JpaPagingItemReader<InvoiceStaging> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }

    @Bean("invoiceStageUpdateItemProcessor")
    public ItemProcessor<InvoiceStaging, Invoice> invoiceStageUpdateItemProcessor() {
        return new ItemProcessor<InvoiceStaging, Invoice>() {
            @Override
            public Invoice process(InvoiceStaging item) throws Exception {
                Invoice invoice = new Invoice();
                invoice.setHqCode(item.getHqCode());
                invoice.setInvoiceNo(item.getInvoiceNo());
                invoice.setInvoiceNoRef(item.getInvoiceNoRef());
                invoice.setInvoiceType(item.getInvoiceType());
                invoice.setTransactionType(item.getTransactionType());
                invoice.setCustomerId(item.getCustomerId());
                invoice.setPlate1(item.getPlate1());
                invoice.setPlate2(item.getPlate2());
                invoice.setProvince(item.getProvince());
                invoice.setFullName(item.getFullName());
                invoice.setAddress(item.getAddress());
                invoice.setIssueDate(item.getIssueDate());
                invoice.setDueDate(item.getDueDate());

                //MFAPP-2384: Repair issue date and due date,in case staging job is error
                if (item.getDueDate() != null) {
                    Calendar dueDate = Calendar.getInstance();
                    dueDate.setLenient(false);
                    dueDate.setTime(item.getDueDate());
                    dueDate.set(Calendar.HOUR_OF_DAY, 0);
                    dueDate.set(Calendar.MINUTE, 0);
                    dueDate.set(Calendar.SECOND, 0);
                    dueDate.set(Calendar.MILLISECOND, 0);

                    Calendar currentDate = Calendar.getInstance();
                    currentDate.setLenient(false);
                    currentDate.set(Calendar.HOUR_OF_DAY, 0);
                    currentDate.set(Calendar.MINUTE, 0);
                    currentDate.set(Calendar.SECOND, 0);
                    currentDate.set(Calendar.MILLISECOND, 0);

                    if (dueDate.getTime().before(currentDate.getTime()) || dueDate.getTime().equals(currentDate.getTime())) {
                        invoice.setIssueDate(currentDate.getTime());
                        currentDate.add(Calendar.DATE, invoiceMemberTypeZero.intValue());
                        invoice.setDueDate(currentDate.getTime());
                    }
                }

                invoice.setPaymentDate(item.getPaymentDate());
                invoice.setFeeAmount(item.getFeeAmount());
                invoice.setDiscount(item.getDiscount());
                invoice.setFineAmount(item.getFineAmount());
                invoice.setCollectionAmount(item.getCollectionAmount());
                invoice.setTotalAmount(item.getTotalAmount());
                invoice.setPrintFlag(item.getPrintFlag());
                invoice.setStatus(Constants.Status.PAYMENT_WAITING);
                invoice.setErrorMessage(item.getErrorMessage());
                invoice.setVat(item.getVat());
                invoice.setCreateBy("Batch-service");
                invoice.setCreateChannel("Batch-service");
                invoice.setBrand(item.getBrand());
                invoice.setVehicleCode(item.getVehicleCode());
                invoice.setVehicleType(item.getVehicleType());
                invoice.setInvoiceChannel(item.getInvoiceChannel());
                invoice.setColors(item.getColors().stream().map(colorStage -> {
                    InvoiceColor color = new InvoiceColor();
                    color.setInvoice(invoice);
                    color.setCode(colorStage.getCode());
                    return color;
                }).collect(Collectors.toList()));
                invoice.setDetails(item.getDetails().stream().map(detailStage -> {
                    InvoiceDetail detail = new InvoiceDetail();
                    detail.setInvoice(invoice);
                    detail.setTransactionId(detailStage.getTransactionId());
                    detail.setTransactionDate(detailStage.getTransactionDate());
                    detail.setPlate1(detailStage.getPlate1());
                    detail.setPlate2(detailStage.getPlate2());
                    detail.setProvince(detailStage.getProvince());
                    detail.setHqCode(detailStage.getHqCode());
                    detail.setPlazaCode(detailStage.getPlazaCode());
                    detail.setLaneCode(detailStage.getLaneCode());
                    detail.setDestHqCode(detailStage.getDestHqCode());
                    detail.setDestPlazaCode(detailStage.getDestPlazaCode());
                    detail.setDestLaneCode(detailStage.getDestLaneCode());
                    detail.setFeeAmount(detailStage.getFeeAmount());
                    detail.setFeeAmountOld(BigDecimal.ZERO);
                    if (detailStage.getFeeAmountOld() != null) {
                        detail.setFeeAmountOld(detailStage.getFeeAmountOld());
                    }
                    detail.setRawFee(detailStage.getRawFee());
                    detail.setDiscount(detailStage.getDiscount());
                    detail.setFineAmount(detailStage.getFineAmount());
                    detail.setCollectionAmount(detailStage.getCollectionAmount());
                    detail.setOperationFee(BigDecimal.ZERO);
                    detail.setTotalAmount(detailStage.getTotalAmount());
                    detail.setVehicleWheel(detailStage.getVehicleWheel());
                    detail.setOriginTranType(detailStage.getOriginTranType());
                    detail.setCreateBy("Batch-service");
                    detail.setCreateChannel("Batch-service");
                    detail.setVat(detailStage.getVat().setScale(2, RoundingMode.DOWN));
                    detail.setEvidences(detailStage.getEvidences().stream().map(evidenceStage -> {
                        InvoiceEvidence evidence = new InvoiceEvidence();
                        evidence.setDetail(detail);
                        evidence.setTransactionId(evidenceStage.getTransactionId());
                        evidence.setType(evidenceStage.getType());
                        evidence.setFile(evidenceStage.getFile());
                        evidence.setCreateBy("Batch-service");
                        evidence.setCreateChannel("Batch-service");
                        return evidence;
                    }).collect(Collectors.toList()));
                    invoice.setFeeAmount(invoice.getFeeAmount().add(detail.getFeeAmount()));
                    invoice.setFineAmount(invoice.getFineAmount().add(detail.getFineAmount()));
                    invoice.setCollectionAmount(invoice.getCollectionAmount().add(detail.getCollectionAmount()));
                    invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
                    invoice.setTotalAmount(invoice.getTotalAmount().add(detail.getTotalAmount()));
                    return detail;
                }).collect(Collectors.toList()));
                log.info("Invoice: {}", item.getInvoiceNo());
                return invoice;
            }
        };
    }

    @Bean
    public Step updateInvoiceStageStep() throws Exception {
        return this.workerStepBuilderFactory.get("updateInvoiceStageStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceStaging, Invoice>chunk(1000)
                .reader(invoiceStagingUpdateItemReader(null, null, null))
                .processor(invoiceStageUpdateItemProcessor())
                .writer(new UpdateInvoiceStageItemWriter(
                        (MessagePublisher) applicationContext.getBean("messagePublisher"),
                        (BillingMessagePublisher) applicationContext.getBean("billingMessagePublisher"),
                        (MasterClient) applicationContext.getBean("masterClient"),
                        (CustomerClient) applicationContext.getBean("customerClient"),
                        (CustomerInfoClient) applicationContext.getBean("customerInfoClient"),
                        (InvoiceRepository) applicationContext.getBean("invoiceRepository"),
                        (InvoiceListValueRepository) applicationContext.getBean("invoiceListValueRepository"),
                        (InvoiceDetailRepository) applicationContext.getBean("invoiceDetailRepository"),
                        (MasterVehicleOfficeRepository) applicationContext.getBean("masterVehicleOfficeRepository"),
                        (InvoiceMasterHqRepository) applicationContext.getBean("invoiceMasterHqRepository"),
                        (MasterPlazaRepository) applicationContext.getBean("masterPlazaRepository"),
                        (InvoiceMasterLaneRepository) applicationContext.getBean("invoiceMasterLaneRepository"),
                        (InvoiceEvidenceRepository) applicationContext.getBean("invoiceEvidenceRepository"),
                        (AuditServiceImp) applicationContext.getBean("auditServiceImp"),
                        dayOfBillCycle,
                        (InvoiceStagingRepository) applicationContext.getBean("invoiceStagingRepository")
                ))
                .build();
    }


    private String issueDate() {
        Calendar currentDate = Calendar.getInstance();
        // currentDate.add(Calendar.DATE,-1);
        Date date = currentDate.getTime();
        String issueDate = df.format(date);
        return issueDate;
    }

    private String previousIssueDate() {
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, -1);
        Date date = currentDate.getTime();
        String issueDate = df.format(date);
        return issueDate;
    }


//    @Bean
//    @StepScope
//    public JdbcBatchItemWriter<InvoiceStaging> invoiceStageUpdateItemWriting()
//    {
//        log.info("Writer");
//        JdbcBatchItemWriter<InvoiceStaging> itemWriter = new JdbcBatchItemWriter<>();
//        itemWriter.setDataSource(dataSource);
//        itemWriter.setSql("UPDATE MF_INVOICE_STAGING SET STATUS = :status WHERE ID = :id");
//
//        itemWriter.setItemSqlParameterSourceProvider
//                (new BeanPropertyItemSqlParameterSourceProvider<>());
//        itemWriter.afterPropertiesSet();
//
//        return itemWriter;
//
//    }
}
