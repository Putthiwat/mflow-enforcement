package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class Errors {
    private String code;
    private String รายละเอียด;
}
