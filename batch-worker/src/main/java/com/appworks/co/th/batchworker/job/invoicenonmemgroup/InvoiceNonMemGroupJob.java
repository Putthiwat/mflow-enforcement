package com.appworks.co.th.batchworker.job.invoicenonmemgroup;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberVehicleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
public class InvoiceNonMemGroupJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("nonMemGenerateGroupRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("nonMemGenerateGroupReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    public InvoiceNonMemGroupJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    @Bean
    @StepScope
    public JpaPagingItemReader<InvoiceNonMember> invoiceUpdateNonMemGroupItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
            ) throws Exception {
        log.info("invoiceUpdateNonMemGroupItemReader reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from InvoiceNonMember o "+where+" and id >= "+ minValue +" and id <= "+ maxValue;
        log.info(jpqlQuery);
        JpaPagingItemReader<InvoiceNonMember> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("invoiceNonMemUpdateGroupItemProcessor")
    public ItemProcessor<InvoiceNonMember, InvoiceNonMember> invoiceNonMemUpdateGroupItemProcessor() {
        return item -> item;
    }

    @Bean
    public Step updateNonMemInvoiceGroupStep() throws Exception {
        return this.workerStepBuilderFactory.get("updateNonMemInvoiceGroupStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceNonMember, InvoiceNonMember>chunk(1000)
                .reader(invoiceUpdateNonMemGroupItemReader(null,null, null))
                .processor(invoiceNonMemUpdateGroupItemProcessor())
                .writer(new InvoiceNonMemGroupItemWriter(
                        (InvoiceNonMemberVehicleRepository) applicationContext.getBean("invoiceNonMemberVehicleRepository"),
                        (InvoiceJdbcRepository) applicationContext.getBean("invoiceJdbcRepository"),
                        (InvoiceNonMemberRepository) applicationContext.getBean("invoiceNonMemberRepository")
                ))
                .build();
    }

}
