package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.easypassbatch.EasyPassBatchGrpc;
import com.appworks.co.th.easypassbatch.EasyPassBatchRequest;
import com.appworks.co.th.easypassbatch.EasyPassBatchResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EasyPassBatchClient {
    @Value("${grpc.easypassbatch.host}")
    private String host;

    @Value("${grpc.easypassbatch.port}")
    private String port;

    private EasyPassBatchGrpc.EasyPassBatchBlockingStub blockingStub;

    @Autowired
    public void StartFileServiceConsumer() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        EasyPassBatchGrpc.EasyPassBatchBlockingStub stub = EasyPassBatchGrpc.newBlockingStub(channel);
        this.blockingStub = stub;
    }

    public EasyPassBatchResponse callEasyPassService(EasyPassBatchRequest request){
        EasyPassBatchResponse response = this.blockingStub.callEasyPassService(request);
        return response;
    }
}
