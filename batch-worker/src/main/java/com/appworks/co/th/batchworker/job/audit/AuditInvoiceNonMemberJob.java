package com.appworks.co.th.batchworker.job.audit;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
public class AuditInvoiceNonMemberJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("auditInvoiceNonMemberJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("auditInvoiceNonMemberJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    private ApplicationContext applicationContext;

    public AuditInvoiceNonMemberJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    @Bean
    @StepScope
    public JpaPagingItemReader<InvoiceNonMember> AuditInvoiceNonMemberItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}", minValue, maxValue);
        String jpqlQuery = "SELECT o from InvoiceNonMember o, InvoiceNonMemberAuditDoh a "+"WHERE  a.invoiceNo = o.invoiceNo and a.auditFlag = 'N' AND a.deleteFlag = 0 AND a.createDate < TO_DATE(TO_CHAR (SYSDATE, 'YYYY-MM-DD'),'YYYY-MM-DD')" +" and o.id >= " + minValue + " and o.id <= " + maxValue;
        //String jpqlQuery = "SELECT o from InvoiceNonMember o " + where + " and id >= " + minValue + " and id <= " + maxValue;
        log.debug("auditInvoiceNonMemberJob SQL {}", jpqlQuery);
        JpaPagingItemReader<InvoiceNonMember> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }

    @Bean("auditInvoiceNonMemberItemProcessor")
    public ItemProcessor<InvoiceNonMember, InvoiceNonMember> auditInvoiceNonMemberItemProcessor() {
        return item -> item;
    }

    @Bean
    public Step auditInvoiceNonMemberStep() throws Exception {
        return this.workerStepBuilderFactory.get("auditInvoiceNonMemberStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceNonMember, InvoiceNonMember>chunk(1000)
                .reader(AuditInvoiceNonMemberItemReader(null, null, null))
                .processor(auditInvoiceNonMemberItemProcessor())
                .writer(new AuditInvoiceNonMemberItemWriter(
                        (InvoiceListValueRepository) applicationContext.getBean("invoiceListValueRepository"),
                        (MasterVehicleOfficeRepository) applicationContext.getBean("masterVehicleOfficeRepository"),
                        (InvoiceMasterHqRepository) applicationContext.getBean("invoiceMasterHqRepository"),
                        (MasterPlazaRepository) applicationContext.getBean("masterPlazaRepository"),
                        (InvoiceMasterLaneRepository) applicationContext.getBean("invoiceMasterLaneRepository"),
                        (InvoiceNonMemberRepository) applicationContext.getBean("invoiceNonMemberRepository"),
                        (AuditServiceImp) applicationContext.getBean("auditServiceImp")
                ))
                .build();
    }
}
