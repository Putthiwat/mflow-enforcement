package com.appworks.co.th.batchworker.job.groupInvoiceNonmemberWarning2;

import com.appworks.co.th.batchworker.service.RedisService;
import com.appworks.co.th.sagaappworks.batch.generateInvoice.InvoiceObject;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import static com.appworks.co.th.batchworker.commons.Constants.generateKeyInvoiceNonmember.CREATE_INVOICE_NONMEMBER;

@AllArgsConstructor
public class GroupInvoiceNonmemberWarning2Writer implements ItemWriter<InvoiceObject> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private RedisService redisService;

    @Override
    public void write(List<? extends InvoiceObject> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoiceObject -> {
                String key = CREATE_INVOICE_NONMEMBER + ":" + invoiceObject.getCreateInvoiceHeader().getVehicleCode() + ":" + dateFormatter(invoiceObject.getCreateInvoiceHeader().getDueDate());
                redisService.setOpsForList(key, invoiceObject);
            });
        }
    }

    private String dateFormatter(Date dueDate) {
        final DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String seq = df.format(dueDate);
        return seq;
    }
}
