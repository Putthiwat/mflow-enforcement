package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_PAYMENT_RECONCILE_REPORT")
@Where(clause = "DELETE_FLAG = 0")
public class PaymentReconcileReport extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqPaymentReconcileReport", sequenceName = "SEQ_MF_PAYMENT_RECONCILE_REPORT", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqPaymentReconcileReport")
    public Long id;

    @Column(name = "RECEIPT_DATE_TIME", columnDefinition = "TIMESTAMP")
    private Instant receiptDateTime;

    @Column(name = "RECEIPT_NO", columnDefinition = "VARCHAR2(255)")
    private String receiptNo;

    @Column(name = "DUE_DATE", columnDefinition = "TIMESTAMP")
    private Instant dueDate;

    @Column(name = "POST_DATE", columnDefinition = "TIMESTAMP")
    private Instant postDate;

    @Column(name = "INVOICE_CREATE_DATE", columnDefinition = "TIMESTAMP")
    private Instant invoiceCreateDate;

    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(255)")
    private String invoiceNo;

    @Column(name = "PLATE", columnDefinition = "VARCHAR2(255)")
    private String plate;

    @Column(name = "BRAND", columnDefinition = "VARCHAR2(255)")
    private String brand;

    @Column(name = "COLOR", columnDefinition = "VARCHAR2(255)")
    private String color;

    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(255)")
    private String province;

    @Column(name = "CUSTOMER_REF", columnDefinition = "VARCHAR2(255)")
    private String customerRef;

    @Column(name = "CUSTOMER_ID", columnDefinition = "VARCHAR2(255)")
    private String customerId;

    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(255)")
    private String hqCode;

    @Column(name = "HQ_NAME", columnDefinition = "VARCHAR2(255)")
    private String hqName;

    @Column(name = "PLAZA_CODE", columnDefinition = "VARCHAR2(255)")
    private String plazaCode;

    @Column(name = "PLAZA_NAME", columnDefinition = "VARCHAR2(255)")
    private String plazaName;

    @Column(name = "LANE_CODE", columnDefinition = "VARCHAR2(255)")
    private String laneCode;

    @Column(name = "LANE_NAME", columnDefinition = "VARCHAR2(255)")
    private String laneName;

    @Column(name = "PAYMENT_CHANNEL", columnDefinition = "VARCHAR2(255)")
    private String paymentChannel;

    @Column(name = "BANK_CHANNEL", columnDefinition = "VARCHAR2(255)")
    private String bankChannel;

    @Column(name = "TRANSACTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal transactionAmount;

    @Column(name = "FEE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal feeAmount;

    @Column(name = "FINE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal fineAmount;

    @Column(name = "TOTAL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalAmount;

    @Column(name = "BANK_TRANSACTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal bankTransactionAmount;

    @Column(name = "DIFF_MFLOW_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal diffMflowAmount;

    @Column(name = "DIFF_CHANNEL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal diffChannelAmount;

    @Column(name = "VEHICLE_TYPE_CODE", columnDefinition = "VARCHAR2(255)")
    private String vehicleTypeCode;

    @Column(name = "VEHICLE_TYPE_NAME", columnDefinition = "VARCHAR2(255)")
    private String vehicleTypeName;

    @Column(name = "CUSTOMER_TYPE", columnDefinition = "VARCHAR2(255)")
    private String customerType;

    @Column(name = "NATIONALITY_CODE", columnDefinition = "VARCHAR2(255)")
    private String nationalityCode;

    @Column(name = "NATIONALITY_NAME", columnDefinition = "VARCHAR2(255)")
    private String nationalityName;

    @Column(name = "MEMBER_TYPE", columnDefinition = "VARCHAR2(255)")
    private String memberType;

    @ColumnDefault("0")
    @Column(name="OPERATION_FEE", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal operationFee;

    @NotNull
    @Column(name = "TRANSACTION_DATE", columnDefinition = "TIMESTAMP")
    private Instant transactionDate;

    @NotNull
    @Column(name = "TRANSACTION_ID", columnDefinition = "VARCHAR2(50)")
    private String trasactionId;

}
