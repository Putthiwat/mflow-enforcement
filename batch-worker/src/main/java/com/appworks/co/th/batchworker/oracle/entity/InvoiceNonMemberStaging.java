package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_NON_STAGING")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceNonMemberStaging extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvNonMemStaging", sequenceName = "SEQ_MF_INV_NONM_STAG", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "seqInvNonMemStaging")
    public Long id;

    @NaturalId
    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(20)")
    private String invoiceNo;

    @Column(name = "INVOICE_NO_REF", columnDefinition = "VARCHAR2(50)")
    private String invoiceNoRef;
    @Column(name = "INVOICE_TYPE", columnDefinition = "SMALLINT")
    @NotNull
    private Integer invoiceType;
    @Column(name = "TRANSACTION_TYPE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String transactionType;

    @Column(name = "PLATE1", columnDefinition = "VARCHAR2(255)")
    private String plate1;
    @Column(name = "PLATE2", columnDefinition = "VARCHAR2(255)")
    private String plate2;
    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(25)")
    private String province;

    @Column(name = "FULL_NAME", columnDefinition = "VARCHAR2(500)")
    private String fullName;
    @Column(name = "ADDRESS", columnDefinition = "VARCHAR2(1500)")
    private String address;
    @Column(name = "ISSUE_DATE")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date issueDate;
    @Column(name = "DUE_DATE")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date dueDate;
    @Column(name="PAYMENT_DATE", columnDefinition = "DATE")
    @Temporal(TemporalType.DATE)
    private Date paymentDate ;
    @Column(name = "FEE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    @NotNull
    private BigDecimal feeAmount;

    @Column(name = "FINE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal fineAmount;

    @Column(name = "PRINT_FLAG", columnDefinition = "SMALLINT")
    private Integer printFlag;


    @Column(name = "STATUS", columnDefinition = "VARCHAR2(25)")
    private String status;

    @Column(name = "ERROR_MESSAGE", columnDefinition = "VARCHAR2(4000)")
    private String errorMessage;

    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String hqCode;

    @Column(name = "COLLECTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal collectionAmount;

    @Column(name = "TOTAL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalAmount;

    @Column(name="VAT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal vat;

    @Column(name = "DISCOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal discount;

    @Column(name = "BRAND", columnDefinition = "VARCHAR2(25)")
    private String brand;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceNonMemberStagingDetail> details;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceNonMemberStagingColor> colors;

    @Column(name = "DOC_TYPE", columnDefinition = "VARCHAR2(5)")
    private String docType;

    @Column(name = "DOC_NO", columnDefinition = "VARCHAR2(50)")
    private String docNo;

    @Column(name = "VEHICLE_CODE", columnDefinition = "VARCHAR2(25)")
    private String vehicleCode;
}
