package com.appworks.co.th.batchworker.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class HQNameDTO implements Serializable {

    private String code;
    private String nameTh;
    private String nameEn;

}
