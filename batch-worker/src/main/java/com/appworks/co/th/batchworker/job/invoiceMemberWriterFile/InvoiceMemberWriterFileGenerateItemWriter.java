package com.appworks.co.th.batchworker.job.invoiceMemberWriterFile;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.commons.DomainEvent;
import com.appworks.co.th.batchworker.config.BillingMessagePublisher;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.payload.PaymentObj;
import com.appworks.co.th.customerwriterfilepayment.Payment;
import com.appworks.co.th.customerwriterfilepayment.PaymentMethodForWriterFileRequest;
import com.appworks.co.th.customerwriterfilepayment.PaymentMethodForWriterFileResponse;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.appworks.co.th.batchworker.commons.Constants.BillCycle.BILL_CYCLE;

@AllArgsConstructor
public class InvoiceMemberWriterFileGenerateItemWriter implements ItemWriter<Invoice> {
    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private CustomerClient customerClient;

    private BillingMessagePublisher billingMessagePublisher;

    private List<Integer> dayOfBillCycle;

    private MasterClient masterClient;

    private CustomerInfoClient customerInfoClient;

    private InvoiceRepository invoiceRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    private String currentDate(Date issueDate) {
        return df.format(issueDate);
    }

    private String currentDateOfBillCycle(Date issueDate) {
        return df.format(DateUtils.addDays(issueDate, -1));
    }

    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoice -> {
                log.info("Write data, invoice no  -> {}", invoice.getInvoiceNo());
                PaymentMethodForWriterFileResponse paymentMethod = null;
                PaymentMethodForWriterFileRequest request = null;
                try {
                    if (BILL_CYCLE.equals(invoice.getInvoiceChannel())){
                        request = PaymentMethodForWriterFileRequest.newBuilder().setVehicleCode(invoice.getVehicleCode())
                                .setIssueDate(this.currentDateOfBillCycle(invoice.getIssueDate())).build();
                    } else {
                        request = PaymentMethodForWriterFileRequest.newBuilder().setVehicleCode(invoice.getVehicleCode())
                                .setIssueDate(this.currentDate(invoice.getIssueDate())).build();
                    }

                    paymentMethod = customerClient.getPaymentMethodForWriterFile(request);
                } catch (Exception e) {
                    paymentMethod = PaymentMethodForWriterFileResponse.newBuilder().setPayment(Payment.newBuilder().build()).build();
                }

                log.info("Payment method of invoice no  -> {} is {}", invoice.getInvoiceNo(),
                        paymentMethod);

                if ("1".equals(paymentMethod.getPayment().getType())) {
                    DomainEvent<Invoice, PaymentObj> invoiceDomainEvent = mapToAggregate(invoice, paymentMethod);
                    Invoice invoiceUpdate = invoiceDomainEvent.getHeader();
                    if (Constants.Status.BATCH_PROCESSING.equals(invoiceUpdate.getStatus())
                            || Constants.PaymentMethod.MPASS.equals(invoiceDomainEvent.getPaymentMethod())
                            || Constants.PaymentMethod.EASYPASS.equals(invoiceDomainEvent.getPaymentMethod())) {
                        try {

                            invoiceRepository.save(invoiceUpdate);

                            ListenableFuture<SendResult<String, String>> future = sendToBillingService(invoiceDomainEvent, invoiceUpdate);
                            if (ObjectUtils.isNotEmpty(future)) {
                                future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
                                    @Override
                                    public void onSuccess(SendResult<String, String> result) {
                                        log.debug("sendDomainEvent To Billing : {} ", result.getRecordMetadata());
                                    }

                                    @Override
                                    public void onFailure(Throwable ex) {
                                        log.error("sendDomainEvent To Billing error : {}", ex);
                                    }
                                });
                            }
                        } catch (JsonProcessingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private DomainEvent<Invoice, PaymentObj> mapToAggregate(Invoice invoice, PaymentMethodForWriterFileResponse paymentMethod) {
        HqResponse hqResponse = masterClient.getHq(HqRequest.newBuilder().setHqCode(invoice.getHqCode()).build());
        String receiveInfo = "";
        if (paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.CRD) ||
                paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.CREDIT_CARD) ||
                paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.DEBIT))
            receiveInfo = hqResponse.getServiceProvider().getMerchantId();
        else if (paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.ACCOUNT))
            receiveInfo = hqResponse.getServiceProvider().getReceiveAccount();

        if (((paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.CRD) && StringUtils.isNotEmpty(paymentMethod.getPayment().getCardToken())) ||
                (paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.CREDIT_CARD) && StringUtils.isNotEmpty(paymentMethod.getPayment().getCardToken())) ||
                (paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.DEBIT) && StringUtils.isNotEmpty(paymentMethod.getPayment().getCardToken())) ||
                (paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.ACCOUNT) && StringUtils.isNotEmpty(paymentMethod.getPayment().getAccount())) ||
                paymentMethod.getPayment().getMethodType().equals(Constants.PaymentMethod.QR))
                && "1".equals(paymentMethod.getPayment().getType())
        ) {
            invoice.setStatus(Constants.Status.BATCH_PROCESSING);
        } else {
            invoice.setStatus(Constants.Status.PAYMENT_WAITING);
        }
        if (BigDecimal.ZERO.compareTo(invoice.getTotalAmount()) == 0) {
            invoice.setStatus(Constants.Status.PAYMENT_SUCCESS);
        }
        //MasterCustomerDataResponse customerInfo = customerInfoClient.getCustomerInfo(invoice.getCustomerId());
        PaymentObj paymentObj = new PaymentObj(paymentMethod.getPayment(), paymentMethod.getPayment().getInvoiceChannel(), hqResponse.getServiceProvider().getMerchantId(), hqResponse.getServiceProvider().getCode(), hqResponse.getServiceProvider().getCompanyId(), paymentMethod.getPayment().getMemberId());
        DomainEvent<Invoice, PaymentObj> domainEvent = new DomainEvent(hqResponse.getCode(), receiveInfo, paymentMethod.getPayment().getMethodType(), invoice.getStatus(), invoice, paymentObj);
        return domainEvent;
    }


    private ListenableFuture<SendResult<String, String>> sendToBillingService(DomainEvent<Invoice, PaymentObj> domainEvent, Invoice invoiceUpdate) throws JsonProcessingException {
        mapper.registerModule(new JavaTimeModule());
        if (domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.ACCOUNT)) {
            return billingMessagePublisher.sendDomainEvent("ACCOUNT_INVOICE", mapper.writeValueAsString(domainEvent));
        } else if (domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.RTP)) {
            return billingMessagePublisher.sendDomainEvent("RTP_INVOICE", mapper.writeValueAsString(domainEvent));
        } else if (domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.CRD) ||
                domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.CREDIT_CARD) ||
                domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.DEBIT)) {
            if (BILL_CYCLE.equalsIgnoreCase(domainEvent.getHeader().getInvoiceChannel())) {
                if (checkBillCycle(invoiceUpdate, dayOfBillCycle)) {
                    return billingMessagePublisher.sendDomainEvent("CARD_INVOICE_BILL_CYCLE", mapper.writeValueAsString(domainEvent));
                } else {
                    return billingMessagePublisher.sendDomainEvent("CARD_INVOICE", mapper.writeValueAsString(domainEvent));
                }
            } else {
                return billingMessagePublisher.sendDomainEvent("CARD_INVOICE", mapper.writeValueAsString(domainEvent));
            }
        } else if (domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.MPASS)) {
            return billingMessagePublisher.sendDomainEvent("MPASS_INVOICE", mapper.writeValueAsString(domainEvent));
        } else if (domainEvent.getPaymentMethod().equals(Constants.PaymentMethod.EASYPASS)) {
            return billingMessagePublisher.sendDomainEvent("EASYPASS_INVOICE", mapper.writeValueAsString(domainEvent));
        }

        return null;
    }

    public Boolean checkBillCycle(Invoice invoiceUpdate, List<Integer> list) {
        Calendar issueDate = Calendar.getInstance();
        issueDate.setLenient(false);
        issueDate.setTime(invoiceUpdate.getIssueDate());
        issueDate.set(Calendar.HOUR_OF_DAY, 0);
        issueDate.set(Calendar.MINUTE, 0);
        issueDate.set(Calendar.SECOND, 0);
        issueDate.set(Calendar.MILLISECOND, 0);

        Calendar current = Calendar.getInstance();
        current.set(Calendar.HOUR_OF_DAY, 0);
        current.set(Calendar.MINUTE, 0);
        current.set(Calendar.SECOND, 0);
        current.set(Calendar.MILLISECOND, 0);

        if (issueDate.getTime().before(current.getTime()))
            return false;

        if (current.get(Calendar.DAY_OF_MONTH) <= this.findMin(list)) {
            return true;
        } else {
            return false;
        }
    }

    public Integer findMin(List<Integer> list) {
        return list.stream()                        // Stream<Integer>
                .min(Comparator.naturalOrder()) // Optional<Integer>
                .get();                         // Integer
    }

}