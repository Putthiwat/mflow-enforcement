package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

import java.util.List;

@Data
public class AuditTransResponseBody {
    private boolean status;
    private String invoiceNo;
    private String transactionId;
    private String message;
    private List<Errors> errors;
}
