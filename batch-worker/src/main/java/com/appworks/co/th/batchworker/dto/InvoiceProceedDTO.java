package com.appworks.co.th.batchworker.dto;

import lombok.Data;
import lombok.ToString;
import java.util.Date;
import java.io.Serializable;

@Data
@ToString
public class InvoiceProceedDTO implements Serializable {
    private Long id;
    private String aggregateId;
    private Long invoiceProceedListID;
    private String customerId;
    private Date createDateTime;
    private String invoiceNo;
    private String command;
    private String detail;
    private String state;
}
