package com.appworks.co.th.batchworker.service.issueday;


import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static java.util.Calendar.DAY_OF_MONTH;

@Service(value = "invoiceCommonService")
public class InvoiceCommonServiceImpl implements InvoiceCommonService {

    @Value("#{'${master.invoice.day}'.split(',')}")
    private List<Integer> invoiceDay;

    @Override
    public Date issueDateBillCycle() {
        //  Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        //  c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        for (int day : invoiceDay) {
            if (day == 32) {
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                return c.getTime();
            } else if (c.get(DAY_OF_MONTH) < day) {
                //  c.add(DAY_OF_MONTH, day - c.get(DAY_OF_MONTH));
                c.set(DAY_OF_MONTH, day);
                return c.getTime();
            }
        }
        //  c.add(Calendar.DAY_OF_MONTH, (c.getActualMaximum(Calendar.DAY_OF_MONTH) - c.get(DAY_OF_MONTH)) + findMin(invoiceDay));
        c.add(Calendar.MONTH, 1);
        c.set(Calendar.DAY_OF_MONTH, findMin(invoiceDay));
        return c.getTime();
    }

    public Integer findMin(List<Integer> list) {
        return list.stream()                        // Stream<Integer>
                .min(Comparator.naturalOrder()) // Optional<Integer>
                .get();                         // Integer
    }

    public Integer findMax(List<Integer> list) {
        return list.stream()                        // Stream<Integer>
                .max(Comparator.naturalOrder()) // Optional<Integer>
                .get();                         // Integer
    }

    public Date newDate() {
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    @Override
    public Date issueDateBillTime(CreateInvoiceDetail createInvoiceDetail) {
        Date dateTo = null;
        try {
            if (ObjectUtils.isNotEmpty(createInvoiceDetail.getDetails())) {
                dateTo = DateUtils.parseDateStrictly(createInvoiceDetail.getDetails().get(0).getTransactionDate(),
                        new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});
                Calendar c = Calendar.getInstance();
                c.setLenient(false);
                c.setTime(dateTo);
                c.set(Calendar.HOUR_OF_DAY, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                return c.getTime();
            } else {
                dateTo = this.newDate();
            }
        } catch (ParseException e) {
            e.printStackTrace();
            dateTo = this.newDate();
        }
        return dateTo;
    }

}
