package com.appworks.co.th.batchworker.job.memberInvoiceProceedListBillTime;

import com.appworks.co.th.batchworker.dto.InvoiceProceedListDTO;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.service.invoice.InvoiceStageService;
import com.appworks.co.th.batchworker.service.issueday.InvoiceCommonService;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.OraclePagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

@Configuration
public class MemberInvoiceProceedListBillTimeJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("memberInvoiceProceedListBillTimeRequests")
    private DirectChannel requests;

    @Value("${confing.sql.InvoiceProceed}")
    private boolean confingSqlInvoiceProceed;

    @Autowired
    @Qualifier("memberInvoiceProceedListBillTimeReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    private ApplicationContext applicationContext;


    @Autowired
    private MessagePublisher messagePublisher;

    @Autowired
    private InvoiceStageService invoiceStageService;
    @Autowired
    private CustomerClient customerClient;
    @Autowired
    private InvoiceCommonService invoiceCommonService;

    public MemberInvoiceProceedListBillTimeJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    private String select1Clause() {
        StringBuilder select = new StringBuilder()
                .append("SELECT invoice.id AS id,\n" +
                        "\t\tinvoice.AGGREGATE_ID AS aggregateId ,\n" +
                        "\t\tinvoice.CUSTOMER_ID AS customerId,\n" +
                        "\t\tinvoice.INVOICE_NO AS invoiceNo,\n" +
                        "\t\tinvoiceProceed.ID AS invoiceProceedID,\n" +
                        "\t\tinvoiceProceed.invoiceType AS invoiceType,\n" +
                        "\t\tinvoiceProceed.COMMAND AS command,\n" +
                        "\t\tinvoiceProceed.DETAIL AS detail,\n" +
                        "\t\tinvoiceProceed.STATE AS   state,\n" +
                        "\t\tinvoiceProceed.transactionDate AS transactionDate");
        return select.toString();
    }

    private String from1Clause() {
        StringBuilder from = new StringBuilder()
                .append("FROM (SELECT invoiceProceed.*,invoiceType.*,jt.transactionDate AS transactionDate\n" +
                        "FROM MF_INVOICE_PROCEED_LIST invoiceProceed , \n" +
                        "JSON_TABLE ( invoiceProceed.DETAIL , '$.detail.details[*]'\n" +
                        "COLUMNS (transactionDate  TIMESTAMP PATH '$.transactionDate')) AS jt,\n" +
                        "JSON_TABLE ( invoiceProceed.DETAIL , '$.header'\n" +
                        "COLUMNS (invoiceType  VARCHAR2 PATH '$.invoiceType' )) AS invoiceType\n" +
                        ") invoiceProceed\n" +
                        "LEFT JOIN MF_INVOICE_PROCEED invoice ON  invoiceProceed.INVOICE_PROCEED_ID =  invoice.ID ");
        return from.toString();
    }


    private String select2Clause() {
        StringBuilder select = new StringBuilder()
                .append("SELECT invoice.id AS id,\n" +
                        "\t\tinvoice.AGGREGATE_ID AS aggregateId ,\n" +
                        "\t\tinvoice.CUSTOMER_ID AS customerId,\n" +
                        "\t\tinvoice.INVOICE_NO AS invoiceNo,\n" +
                        "\t\tinvoiceProceed.ID AS invoiceProceedID,\n" +
                        "\t\tinvoiceProceed.invoiceType AS invoiceType,\n" +
                        "\t\tinvoiceProceed.COMMAND AS command,\n" +
                        "\t\tinvoiceProceed.DETAIL AS detail,\n" +
                        "\t\tinvoiceProceed.STATE AS   state,\n" +
                        "\t\tinvoiceProceed.TRANSACTION_DATE AS transactionDate");
        return select.toString();
    }

    private String from2Clause() {
        StringBuilder from = new StringBuilder()
                .append("FROM (SELECT invoiceProceed.*,invoiceType.*\n" +
                        "FROM MF_INVOICE_PROCEED_LIST invoiceProceed ,\n" +
                        "JSON_TABLE ( invoiceProceed.DETAIL , '$.header'\n" +
                        "COLUMNS (invoiceType  VARCHAR2 PATH '$.invoiceType'\n" +
                        " ))\n" +
                        "AS invoiceType\n" +
                        ") invoiceProceed\n" +
                        "LEFT JOIN MF_INVOICE_PROCEED invoice ON  invoiceProceed.INVOICE_PROCEED_ID =  invoice.ID");
        return from.toString();
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<InvoiceProceedListDTO> memberInvoiceProceedListBillTimeReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        HashMap<String, Order> sortKeys = new HashMap<>(2);
        sortKeys.put("ID", Order.ASCENDING);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String yesterday = mapDateFormat(DateUtils.addDays(new Date(), -1), dateFormat);
        String startOfDay = yesterday + " 00:00:00";
        String endOfDay = yesterday + " 23:59:59";
        OraclePagingQueryProvider queryProvider = new OraclePagingQueryProvider();
        if (confingSqlInvoiceProceed) {
//            String whereSql = "%s AND invoiceProceed.ID >= %s AND invoiceProceed.ID <= %s AND invoiceProceed.invoiceType=0 and invoiceProceed.COMMAND ='InvoiceMemberCommand' and \n" +
//                    " invoiceProceed.transactionDate >= TO_TIMESTAMP( '" + startOfDay + "' , 'dd-mm-yyyy hh24:mi:ss') AND invoiceProceed.transactionDate <= TO_TIMESTAMP( '" + endOfDay + "', 'dd-mm-yyyy hh24:mi:ss')";
            queryProvider.setSelectClause(this.select1Clause());
            queryProvider.setFromClause(this.from1Clause());
            queryProvider.setWhereClause(String.format("%s AND invoiceProceed.ID >= %s AND invoiceProceed.ID <= %s AND invoiceProceed.invoiceType=0  and invoiceProceed.transactionDate >= TO_TIMESTAMP( \'%s\' , 'dd-mm-yyyy hh24:mi:ss') AND invoiceProceed.transactionDate <= TO_TIMESTAMP( \'%s\' , 'dd-mm-yyyy hh24:mi:ss')", where, minValue, maxValue,startOfDay,endOfDay));
            queryProvider.setSortKeys(sortKeys);
            log.info("confingSqlInvoiceProceed : {} , Data sql : {}",confingSqlInvoiceProceed, queryProvider);
        } else {
            String whereSql = where + " AND invoiceProceed.ID >= " + minValue + " AND invoiceProceed.ID <= " + maxValue + " AND invoiceProceed.invoiceType=0 and invoiceProceed.COMMAND ='InvoiceMemberCommand' and \n" +
                    " invoiceProceed.TRANSACTION_DATE >= TO_TIMESTAMP( '" + startOfDay + "' , 'dd-mm-yyyy hh24:mi:ss') AND invoiceProceed.TRANSACTION_DATE <= TO_TIMESTAMP( '" + endOfDay + "', 'dd-mm-yyyy hh24:mi:ss')";
            queryProvider.setSelectClause(this.select2Clause());
            queryProvider.setFromClause(this.from2Clause());
            queryProvider.setWhereClause(whereSql);
            queryProvider.setSortKeys(sortKeys);
        }
        JdbcPagingItemReader<InvoiceProceedListDTO> pagingItemReader = new JdbcPagingItemReader<>();
        pagingItemReader.setPageSize(1000);
        pagingItemReader.setDataSource(this.dataSource);
        pagingItemReader.setRowMapper(BeanPropertyRowMapper.newInstance(InvoiceProceedListDTO.class));
        pagingItemReader.setQueryProvider(queryProvider);
        pagingItemReader.afterPropertiesSet();
        return pagingItemReader;
    }

    @Bean("memberInvoiceProceedListBillTimeItemProcessor")
    public ItemProcessor<InvoiceProceedListDTO, InvoiceProceedListDTO> memberInvoiceProceedListBillTimeItemProcessor() {
        return item -> item;
    }


    @Bean
    public Step memberInvoiceProceedListBillTimeStep() throws Exception {
        return this.workerStepBuilderFactory.get("memberInvoiceProceedListBillTimeStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceProceedListDTO, InvoiceProceedListDTO>chunk(1000)
                .reader(this.memberInvoiceProceedListBillTimeReader(null, null, null))
                .processor(this.memberInvoiceProceedListBillTimeItemProcessor())
                .writer(new MemberInvoiceProceedListBillTimeJobWriter(
                        (MessagePublisher) applicationContext.getBean("messagePublisher"),
                        (InvoiceStageService) applicationContext.getBean("invoiceStageService"),
                        (CustomerClient) applicationContext.getBean("customerClient"),
                        (InvoiceCommonService) applicationContext.getBean("invoiceCommonService")

                ))
                .build();
    }

    public static String mapDateFormat(Date date, DateFormat format) {
        return date != null ? format.format(date) : "";
    }
}
