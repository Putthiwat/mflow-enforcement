package com.appworks.co.th.batchworker.payload;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.customerwriterfilepayment.Payment;
import lombok.Data;

import java.io.Serializable;

@Data
public class PaymentObj implements Serializable {
    private String methodType;
    private String cardToken;
    private String cardMemberId;
    private String account;
    private String rtp;
    private String type;
    private String ddaRef1;
    private String invoiceChannel;
    private String merchantID;
    private String serviceProvider;
    private String companyId;

    public PaymentObj(Payment payment, String invoiceChannel, String merchantID, String serviceProvider, String companyId, String ddaRef1) {
        this.methodType = payment.getMethodType();
        this.cardToken = payment.getCardToken();
        this.cardMemberId = payment.getMemberId();
        this.account = payment.getAccount();
        this.rtp = payment.getRtp();
        this.type = payment.getType();
        this.invoiceChannel = invoiceChannel;
        this.merchantID = merchantID;
        this.serviceProvider = serviceProvider;
        this.companyId = companyId;
        this.ddaRef1 = ddaRef1;
//        if (this.methodType.equals(Constants.PaymentMethod.EASYPASS)||this.methodType.equals(Constants.PaymentMethod.MPASS)){
//            this.cardMemberId = null;
//            this.account = payment.getMemberId();
//        }
    }
}
