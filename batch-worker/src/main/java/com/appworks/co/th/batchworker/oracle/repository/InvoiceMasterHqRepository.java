package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceMasterHq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface InvoiceMasterHqRepository extends JpaRepository<InvoiceMasterHq, Long> {
    InvoiceMasterHq findByCode(String code);
}
