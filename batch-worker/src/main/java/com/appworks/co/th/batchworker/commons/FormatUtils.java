package com.appworks.co.th.batchworker.commons;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatUtils {

    public static String toString(BigDecimal bigDecimal, String format){
        bigDecimal = bigDecimal != null ? bigDecimal : BigDecimal.ZERO;
        return new DecimalFormat(format).format(bigDecimal);
    }

    public static String toString(Date date, DateFormat format){
        return date != null ? format.format(date) : "";
    }

}
