package com.appworks.co.th.batchworker.job.movestaging;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.config.BillingMessagePublisher;
import com.appworks.co.th.batchworker.dto.audit.*;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.InvoiceUpdateDomainEvent;
import com.appworks.co.th.sagaappworks.invoice.InvoiceUpdateHeader;
import com.appworks.co.th.sagaappworks.invoice.UpdateStatusInvoiceCommand;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static com.appworks.co.th.batchworker.commons.Constants.Status.PAYMENT_SUCCESS;

//import com.appworks.co.th.batchworker.service.audit.AuditService;

@AllArgsConstructor
public class UpdateInvoiceStageItemWriter implements ItemWriter<Invoice> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private MessagePublisher messagePublisher;

    private BillingMessagePublisher billingMessagePublisher;

    private MasterClient masterClient;

    private CustomerClient customerClient;

    private CustomerInfoClient customerInfoClient;

    private InvoiceRepository invoiceRepository;

    private InvoiceListValueRepository invoiceListValueRepository;

    private InvoiceDetailRepository invoiceDetailRepository;

    private MasterVehicleOfficeRepository masterVehicleOfficeRepository;

    private InvoiceMasterHqRepository invoiceMasterHqRepository;

    private MasterPlazaRepository masterPlazaRepository;

    private InvoiceMasterLaneRepository invoiceMasterLaneRepository;

    private InvoiceEvidenceRepository invoiceEvidenceRepository;

    private AuditServiceImp auditServiceImp;

    private List<Integer> dayOfBillCycle;

    private InvoiceStagingRepository invoiceStagingRepository;

    private final ObjectMapper mapper = new ObjectMapper();


    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoice -> {
                try {
                    if (invoice.getTotalAmount().compareTo(BigDecimal.ZERO) == 0) {
                        invoice.setStatus(PAYMENT_SUCCESS);
                    }

                    int updateResult = invoiceStagingRepository.updateStatusByInvoiceNo("IN-PROGRESS", invoice.getInvoiceNo());

                    if (updateResult > 0){

                        invoiceRepository.save(invoice);

                        try {
                            this.callBackToInvoice(invoice);
                        } catch (Exception e) {
                            log.error("Error send updateInvoiceStagingService Invoice No. {}: {} ",(invoice!=null?invoice.getInvoiceNo():"-"), e);
                        }

                        try {
                            auditServiceImp.auditBilling(setAuditBilling(invoice));
                        } catch (Exception e) {
                            log.error("Error send audit system Invoice No. {} : {} ",(invoice!=null?invoice.getInvoiceNo():"-"), e);
                        }
                    }
                } catch (Exception e) {
                    log.error("Error write UpdateInvoiceStageItemWriter Invoice No. {}: {} ",(invoice!=null?invoice.getInvoiceNo():"-"), e);
                }
            });
        }
    }

    private AuditBillingReq setAuditBilling(Invoice invoice) {
        log.debug("invoice no." + invoice.getInvoiceNo());
        //Invoice invoice = invoiceRepository.findByInvoiceNo(invoice.getInvoiceNo()).orElse(null);
        AuditBillingReq auditBillingReq = new AuditBillingReq();

        if (invoice != null) {
            auditBillingReq.setInvoiceNo(invoice.getInvoiceNo());
            auditBillingReq.setInvoiceRefNo(invoice.getInvoiceNoRef() != null ? invoice.getInvoiceNoRef() : null);
            InvoiceType invoiceType = new InvoiceType();
            if (invoice.getInvoiceType() != null) {
                invoiceType.setCode(invoice.getInvoiceType().toString());
                log.debug("invoice.getInvoiceType ----> :{}", invoice.getInvoiceType().toString());
                InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoice.getInvoiceType().toString(), "T000012");
                if (invoiceListValue != null) {
                    invoiceType.setDescription(invoiceListValue.getDescription() != null ? invoiceListValue.getDescription() : null);
                }
            }
            auditBillingReq.setInvoiceType(invoiceType);
            auditBillingReq.setTransactionType("MEMBER");
            auditBillingReq.setCustomerId(invoice.getCustomerId() != null ? invoice.getCustomerId() : null);
            auditBillingReq.setFullName(invoice.getFullName() != null ? invoice.getFullName() : null);
            auditBillingReq.setAddress(invoice.getAddress() != null ? invoice.getAddress() : null);
            auditBillingReq.setIssueDate(DateUtils.issueDateDueDateAudit(invoice.getIssueDate() != null ? invoice.getIssueDate() : null));
            auditBillingReq.setDueDate(DateUtils.issueDateDueDateAudit(invoice.getDueDate() != null ? invoice.getDueDate() : null));
            auditBillingReq.setFeeAmount(invoice.getFeeAmount() != null ? invoice.getFeeAmount() : null);
            auditBillingReq.setFineAmount(invoice.getFineAmount() != null ? invoice.getFineAmount() : null);
            auditBillingReq.setCollectionAmount(invoice.getCollectionAmount() != null ? invoice.getCollectionAmount() : null);
            auditBillingReq.setTotalAmount(invoice.getTotalAmount() != null ? invoice.getTotalAmount() : null);
            InvoiceStatus invoiceStatus = new InvoiceStatus();
            if (invoice.getStatus() != null) {
                invoiceStatus.setCode(invoice.getStatus());
                InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoice.getStatus(), "T000013");
                if (invoiceListValue != null) {
                    invoiceStatus.setDescription(invoiceListValue.getDescription() != null ? invoiceListValue.getDescription() : null);
                }
            }
            auditBillingReq.setInvoiceStatus(invoiceStatus);
            auditBillingReq.setErrorMessage(invoice.getErrorMessage() != null ? invoice.getErrorMessage() : null);
            //List<com.appworks.co.th.batchworker.dto.audit.InvoiceDetail> invoiceDetailList = new ArrayList<>();
            //List<InvoiceDetail> invoiceDetails = invoiceDetailRepository.findAllByInvoice(invoice);
            auditBillingReq.setInvoiceDetails(invoice.getDetails().stream().map(item -> {
                com.appworks.co.th.batchworker.dto.audit.InvoiceDetail invoiceDetail = new com.appworks.co.th.batchworker.dto.audit.InvoiceDetail();
                invoiceDetail.setInvoiceNo(item.getInvoice().getInvoiceNo());
                invoiceDetail.setTransactionId(item.getTransactionId());
                invoiceDetail.setTransactionDate(DateUtils.transactionDate(item.getTransactionDate()));
                invoiceDetail.setPlate1(item.getPlate1());
                invoiceDetail.setPlate2(item.getPlate2());
                Province province = new Province();
                if (item.getProvince() != null) {
                    province.setCode(item.getProvince());
                    InvoiceMasterVehicleOffice invoiceMasterVehicleOffice = masterVehicleOfficeRepository.findByCode(item.getProvince());
                    if (invoiceMasterVehicleOffice != null) {
                        province.setDescription(invoiceMasterVehicleOffice.getDescription());
                    }
                }
                invoiceDetail.setProvice(province);
                Origin origin = new Origin();
                Hq hqOrigin = new Hq();
                if (item.getHqCode() != null) {
                    hqOrigin.setCode(item.getHqCode());
                    InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getHqCode());
                    if (invoiceMasterHq != null) {
                        hqOrigin.setName(invoiceMasterHq.getDescription());
                    }
                }
                Plaza plazaOrigin = new Plaza();
                if (item.getPlazaCode() != null) {
                    plazaOrigin.setCode(item.getPlazaCode());
                    InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getPlazaCode());
                    if (invoiceMasterPlaza != null) {
                        plazaOrigin.setName(invoiceMasterPlaza.getDescription());
                    }
                }
                Lane laneOrigin = new Lane();
                if (item.getLaneCode() != null) {
                    laneOrigin.setCode(item.getLaneCode());
                    InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getLaneCode());
                    if (invoiceMasterLane != null) {
                        laneOrigin.setName(invoiceMasterLane.getDescription());
                    }
                }
                origin.setHq(hqOrigin);
                origin.setLane(laneOrigin);
                origin.setPlaza(plazaOrigin);
                invoiceDetail.setOrigin(origin);
                Destination destination = new Destination();
                Hq hqDes = new Hq();
                if (item.getDestHqCode() != null) {
                    hqDes.setCode(item.getDestHqCode());
                    InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getDestHqCode());
                    if (invoiceMasterHq != null) {
                        hqDes.setName(invoiceMasterHq.getDescription());
                    }
                }
                Plaza plazaDes = new Plaza();
                if (item.getDestPlazaCode() != null) {
                    plazaDes.setCode(item.getDestPlazaCode());
                    InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getDestPlazaCode());
                    if (invoiceMasterPlaza != null) {
                        plazaDes.setName(invoiceMasterPlaza.getDescription());
                    }
                }
                Lane laneDes = new Lane();
                if (item.getDestLaneCode() != null) {
                    laneDes.setCode(item.getDestLaneCode());
                    InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getDestLaneCode());
                    if (invoiceMasterLane != null) {
                        laneDes.setName(invoiceMasterLane.getDescription());
                    }
                }
                destination.setHq(hqDes);
                destination.setLane(laneDes);
                destination.setPlaza(plazaDes);
                invoiceDetail.setDestination(destination);
                invoiceDetail.setFeeAmount(item.getFeeAmount());
                invoiceDetail.setFineAmount(item.getFineAmount());
                invoiceDetail.setCollectionAmount(item.getCollectionAmount());
                invoiceDetail.setTotalAmount(item.getTotalAmount());
                OriginTranType originTranType = new OriginTranType();
                if (item.getOriginTranType() != null) {
                    originTranType.setCode(item.getOriginTranType());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(item.getOriginTranType(), "T000018");
                    if (invoiceListValue != null) {
                        originTranType.setDescription(invoiceListValue.getDescription() != null ? invoiceListValue.getDescription() : null);
                    }
                }
                invoiceDetail.setOriginTranType(originTranType);
                //List<Evidences> evidencesList = new ArrayList<>();
                //List<InvoiceEvidence> invoiceEvidences = invoiceEvidenceRepository.findAllByInvoiceDetail(item.getId());
                //if(!invoiceEvidences.isEmpty()){
                invoiceDetail.setEvidences(item.getEvidences().stream().map(e -> {
                    Evidences evidences = new Evidences();
                    evidences.setType(e.getType());
                    evidences.setFileId(e.getFile());
                    //evidencesList.add(evidences);
                    return evidences;
                }).collect(Collectors.toList()));
                //}
                //invoiceDetail.setEvidences(evidencesList);
                //invoiceDetailList.add(invoiceDetail);
                return invoiceDetail;
            }).collect(Collectors.toList()));
            //auditBillingReq.setInvoiceDetails(invoiceDetailList);
        }

        return auditBillingReq;
    }

    private void callBackToInvoice(Invoice invoice) {
        InvoiceUpdateHeader updateHeader = new InvoiceUpdateHeader();
        updateHeader.setStatus(Constants.Status.MOVED);
        updateHeader.setChannel("Batch-service");
        updateHeader.setUpdateBy("Batch-service");
        updateHeader.setInvoiceNo(invoice.getInvoiceNo());
        updateHeader.setType("MEMBER");
        InvoiceUpdateDomainEvent domainEvent = new InvoiceUpdateDomainEvent<>(invoice.getId(), UpdateStatusInvoiceCommand.class, CommonState.APPROVED, updateHeader);
        log.info("callBackToInvoice {}", domainEvent);
        messagePublisher.sendDomainEvent("updateInvoiceStagingService", domainEvent);

    }


}