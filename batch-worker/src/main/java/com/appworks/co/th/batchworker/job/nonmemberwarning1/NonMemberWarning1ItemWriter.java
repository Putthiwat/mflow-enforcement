package com.appworks.co.th.batchworker.job.nonmemberwarning1;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.audit.*;
import com.appworks.co.th.batchworker.dto.audit.InvoiceDetail;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.*;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.batchworker.service.audit.AuditServiceImp;
import com.appworks.co.th.batchworker.service.invoice.InvoiceNonMemberService;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.leftPad;

@AllArgsConstructor
public class NonMemberWarning1ItemWriter implements ItemWriter<InvoiceObject> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    final DateFormat df = new SimpleDateFormat("yyMMdd");
    private MessagePublisher messagePublisher;

    private final ObjectMapper mapper = new ObjectMapper();

    private InvoiceJdbcRepository invoiceJdbcRepository;

    @Autowired
    private InvoiceNonMemberService invoiceNonMemberService;

    @Autowired
    private InvoiceListValueRepository invoiceListValueRepository;

    @Autowired
    private MasterVehicleOfficeRepository masterVehicleOfficeRepository;

    @Autowired
    private InvoiceMasterHqRepository invoiceMasterHqRepository;

    @Autowired
    private MasterPlazaRepository masterPlazaRepository;

    @Autowired
    private InvoiceMasterLaneRepository invoiceMasterLaneRepository;

    @Autowired
    private AuditServiceImp auditServiceImp;

    @Override
    public void write(List<? extends InvoiceObject> items) throws Exception {
        items.stream().forEach(invoiceObject -> {
            this.upDateInvoiceType(invoiceObject);
            log.info("InvoiceNonMemberDomainEvent : {}", invoiceObject.getCreateInvoiceHeader().toString());
            //this.auditServiceImp.auditBillingNonmember(setAuditBillingNonMember(invoiceObject));
        });
    }

    private void upDateInvoiceType(InvoiceObject invoiceObject){
        try {
            InvoiceNonMemberDomainEvent<CreateInvoiceDetail, CreateInvoiceHeader> domainEvent = mapToAggregate(invoiceObject);
            String invoiceNo=createInvoice(invoiceObject);
            if(StringUtils.isNotEmpty(invoiceNo)) {
                invoiceNonMemberService.createNonMemberInvoice(invoiceNo, invoiceObject.getCreateInvoiceHeader(), invoiceObject.getCreateInvoiceDetail());
            }
            // messagePublisher.sendDomainEvent("createNonMemberInvoiceService", domainEvent);
            log.info("InvoiceNonMemberDomainEvent : {}", domainEvent.getHeader().toString());
        }catch (Exception e){

        }
    }

    private InvoiceNonMemberDomainEvent<CreateInvoiceDetail, CreateInvoiceHeader> mapToAggregate(InvoiceObject obj) {
        InvoiceNonMemberDomainEvent<CreateInvoiceDetail, CreateInvoiceHeader> invoiceDomainEvent = new InvoiceNonMemberDomainEvent<>();
        invoiceDomainEvent.setHeader(obj.getCreateInvoiceHeader());
        invoiceDomainEvent.setDetail(obj.getCreateInvoiceDetail());
        return invoiceDomainEvent;
    }


    private String createInvoice(InvoiceObject obj) {
        try {
            Long seq = invoiceJdbcRepository.getSeqInvoice();
            if (seq != null) {
                String number = leftPad(String.valueOf(seq), 9, "0");
                String memberType = StringUtils.isEmpty(obj.getCreateInvoiceHeader().getCustomerId()) ? "2" : "1";
                return "0" + memberType + obj.getCreateInvoiceHeader().getInvoiceType() + df.format(new Date()) + number;
            }
        } catch (Exception e) {
            log.error("Error createInvoiceNonmember : {}",e);
        }
        return null;
    }

    public AuditBillingReq setAuditBillingNonMember(InvoiceObject invoiceNonMember){
        AuditBillingReq auditBillingReq = new AuditBillingReq();
            if (invoiceNonMember != null){
                auditBillingReq.setInvoiceNo(invoiceNonMember.getCreateInvoiceHeader().getInvoiceRefNo());
                auditBillingReq.setInvoiceRefNo(invoiceNonMember.getCreateInvoiceHeader().getInvoiceRefNo()!=null?invoiceNonMember.getCreateInvoiceHeader().getInvoiceRefNo():null);
                InvoiceType invoiceType = new InvoiceType();
                if(invoiceNonMember.getCreateInvoiceHeader().getInvoiceType()!=null){
                    invoiceType.setCode(invoiceNonMember.getCreateInvoiceHeader().getInvoiceType());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoiceNonMember.getCreateInvoiceHeader().getInvoiceType(), "T000012");
                    if (invoiceListValue != null) {
                        invoiceType.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                    }
                }
                auditBillingReq.setInvoiceType(invoiceType);
                auditBillingReq.setTransactionType("NONMEMBER");
                auditBillingReq.setFullName(invoiceNonMember.getCreateInvoiceHeader().getFullName()!=null?invoiceNonMember.getCreateInvoiceHeader().getFullName():null);
                auditBillingReq.setAddress(invoiceNonMember.getCreateInvoiceHeader().getAddress()!=null?invoiceNonMember.getCreateInvoiceHeader().getAddress():null);
                auditBillingReq.setIssueDate(DateUtils.issueDateDueDateAudit(invoiceNonMember.getCreateInvoiceHeader().getIssueDate()!=null?invoiceNonMember.getCreateInvoiceHeader().getIssueDate():null));
                if(invoiceNonMember.getAdditionalObject().getDueDate()!=null) {
                    auditBillingReq.setDueDate(DateUtils.issueDateDueDateAudit(invoiceNonMember.getAdditionalObject().getDueDate() != null ? invoiceNonMember.getAdditionalObject().getDueDate() : null));
                }
                auditBillingReq.setFeeAmount(invoiceNonMember.getAdditionalObject().getFeeAmount()!=null?invoiceNonMember.getAdditionalObject().getFeeAmount():null);
                auditBillingReq.setFineAmount(invoiceNonMember.getAdditionalObject().getFineAmount()!=null?invoiceNonMember.getAdditionalObject().getFineAmount():null);
                auditBillingReq.setCollectionAmount(invoiceNonMember.getAdditionalObject().getCollectionAmount()!=null?invoiceNonMember.getAdditionalObject().getCollectionAmount():null);
                auditBillingReq.setTotalAmount(invoiceNonMember.getAdditionalObject().getTotalAmount()!=null?invoiceNonMember.getAdditionalObject().getTotalAmount():null);
                InvoiceStatus invoiceStatus = new InvoiceStatus();
                if(invoiceNonMember.getAdditionalObject().getStatus()!=null){
                    invoiceStatus.setCode(invoiceNonMember.getAdditionalObject().getStatus());
                    InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(invoiceNonMember.getAdditionalObject().getStatus(), "T000013");
                    if (invoiceListValue != null) {
                        invoiceStatus.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                    }
                }
                auditBillingReq.setInvoiceStatus(invoiceStatus);
                auditBillingReq.setErrorMessage(invoiceNonMember.getAdditionalObject().getErrorMessage()!=null?invoiceNonMember.getAdditionalObject().getErrorMessage():null);
                auditBillingReq.setInvoiceDetails(invoiceNonMember.getAdditionalObject().getDetailsNonMem().stream().map(item ->{
                    InvoiceDetail invoiceDetail = new InvoiceDetail();
                    invoiceDetail.setInvoiceNo(item.getInvoice().getInvoiceNo());
                    invoiceDetail.setTransactionId(item.getTransactionId());
                    invoiceDetail.setTransactionDate(DateUtils.transactionDate(item.getTransactionDate()));
                    invoiceDetail.setPlate1(item.getPlate1());
                    invoiceDetail.setPlate2(item.getPlate2());
                    Province province = new Province();
                    if(item.getProvince()!=null){
                        province.setCode(item.getProvince());
                        InvoiceMasterVehicleOffice invoiceMasterVehicleOffice = masterVehicleOfficeRepository.findByCode(item.getProvince());
                        if(invoiceMasterVehicleOffice!=null){
                            province.setDescription(invoiceMasterVehicleOffice.getDescription());
                        }
                    }
                    invoiceDetail.setProvice(province);
                    Origin origin = new Origin();
                    Hq hqOrigin = new Hq();
                    if(item.getHqCode()!=null){
                        hqOrigin.setCode(item.getHqCode());
                        InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getHqCode());
                        if(invoiceMasterHq!=null){
                            hqOrigin.setName(invoiceMasterHq.getDescription());
                        }
                    }
                    Plaza plazaOrigin = new Plaza();
                    if(item.getPlazaCode()!=null){
                        plazaOrigin.setCode(item.getPlazaCode());
                        InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getPlazaCode());
                        if(invoiceMasterPlaza!=null){
                            plazaOrigin.setName(invoiceMasterPlaza.getDescription());
                        }
                    }
                    Lane laneOrigin = new Lane();
                    if(item.getLaneCode()!=null){
                        laneOrigin.setCode(item.getLaneCode());
                        InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getLaneCode());
                        if(invoiceMasterLane!=null){
                            laneOrigin.setName(invoiceMasterLane.getDescription());
                        }
                    }
                    origin.setHq(hqOrigin);
                    origin.setLane(laneOrigin);
                    origin.setPlaza(plazaOrigin);
                    invoiceDetail.setOrigin(origin);
                    Destination destination = new Destination();
                    Hq hqDes = new Hq();
                    if(item.getDestHqCode()!=null){
                        hqDes.setCode(item.getDestHqCode());
                        InvoiceMasterHq invoiceMasterHq = invoiceMasterHqRepository.findByCode(item.getDestHqCode());
                        if(invoiceMasterHq!=null){
                            hqDes.setName(invoiceMasterHq.getDescription());
                        }
                    }
                    Plaza plazaDes = new Plaza();
                    if(item.getDestPlazaCode()!=null){
                        plazaDes.setCode(item.getDestPlazaCode());
                        InvoiceMasterPlaza invoiceMasterPlaza = masterPlazaRepository.findByCode(item.getDestPlazaCode());
                        if(invoiceMasterPlaza!=null){
                            plazaDes.setName(invoiceMasterPlaza.getDescription());
                        }
                    }
                    Lane laneDes = new Lane();
                    if(item.getDestLaneCode()!=null){
                        laneDes.setCode(item.getDestLaneCode());
                        InvoiceMasterLane invoiceMasterLane = invoiceMasterLaneRepository.findByCode(item.getDestLaneCode());
                        if(invoiceMasterLane!=null){
                            laneDes.setName(invoiceMasterLane.getDescription());
                        }
                    }
                    destination.setHq(hqDes);
                    destination.setLane(laneDes);
                    destination.setPlaza(plazaDes);
                    invoiceDetail.setDestination(destination);
                    invoiceDetail.setFeeAmount(item.getFeeAmount());
                    invoiceDetail.setFineAmount(item.getFineAmount());
                    invoiceDetail.setCollectionAmount(item.getCollectionAmount());
                    invoiceDetail.setTotalAmount(item.getTotalAmount());
                    OriginTranType originTranType = new OriginTranType();
                    if(item.getOriginTranType()!=null){
                        originTranType.setCode(item.getOriginTranType());
                        InvoiceListValue invoiceListValue = invoiceListValueRepository.findByCodeAndType(item.getOriginTranType(), "T000018");
                        if (invoiceListValue != null) {
                            originTranType.setDescription(invoiceListValue.getDescription()!=null?invoiceListValue.getDescription():null);
                        }
                    }
                    invoiceDetail.setOriginTranType(originTranType);
                    invoiceDetail.setEvidences(item.getEvidences().stream().map(evi ->{
                        Evidences evidences = new Evidences();
                        evidences.setType(evi.getType());
                        evidences.setFileId(evi.getFile());
                        return evidences;
                    }).collect(Collectors.toList()));

                    return invoiceDetail;
                }).collect(Collectors.toList()));
            }
        //}
        return auditBillingReq;
    }
}
