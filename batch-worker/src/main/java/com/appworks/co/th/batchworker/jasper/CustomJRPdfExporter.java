package com.appworks.co.th.batchworker.jasper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.List;


public class CustomJRPdfExporter extends JRPdfExporter {

	public byte[] exportReportToPdf(List<JasperPrint> jasperPrint) throws JRException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		setExporterInput(SimpleExporterInput.getInstance(jasperPrint));
		setExporterOutput(new SimpleOutputStreamExporterOutput(baos));
		exportReport();
		return baos.toByteArray();
	}
	
	public byte[] exportReportToPdf(JasperPrint...jasperPrint) throws JRException {
		return exportReportToPdf(Arrays.asList(jasperPrint));
	}
	
}
