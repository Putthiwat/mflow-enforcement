package com.appworks.co.th.batchworker.job.decryptpwd;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.oracle.entity.CustomerCard;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceStaging;
import com.appworks.co.th.batchworker.service.CacheService;
import com.appworks.co.th.getserviceprovider.GetServiceProviderResponse;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.annotation.*;
import java.util.Collections;

@Component
public class DecryptPwdProcessor implements ItemProcessor<CustomerCard, CustomerCard> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${decryptcardtoken.url}")
    public String decryptTokenUrl;

    @Autowired
    private CacheService cacheService;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public CustomerCard process(CustomerCard item) throws Exception {
        log.info("decrypt job process -> {}", item.getCustomerId());
        try {

            GetServiceProviderResponse providerResponse = this.cacheService.getProvider("M00000", "TH");
            String staticTokenKey = providerResponse.getStaticTokenKey();
            String staticTokenSalt = providerResponse.getTokenSalt();

            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            httpHeaders.setAccept(Collections.singletonList(MediaType.TEXT_XML));

            MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
            form.add("action", "decrypt");
            form.add("staticTokenKey", staticTokenKey);
            form.add("staticTokenSalt", staticTokenSalt);
            form.add("encryptedToken", StringUtils.trimToEmpty(item.getTokenOrigin()));

            HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(form,httpHeaders);
            ResponseEntity<String> response = this.restTemplate.postForEntity(decryptTokenUrl, httpEntity, String.class);

            if(response.getStatusCode().is2xxSuccessful()) {
                String responseBody = response.getBody();
                String decryptedResult = this.extract(responseBody);
                item.setRetry(0);
                item.setToken(decryptedResult);
            }
            return item;

        } catch (Exception e) {
            log.info("decrypt token job process error {}", e);
            return null;
        }

    }

   private String extract(String text) {
        String value = "";
        text = text.replace("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>","");
        value = StringUtils.substringBetween(text, ">", "<");
        return value;
   }

}
