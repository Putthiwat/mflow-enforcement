package com.appworks.co.th.batchworker.service;


public interface RedisService {
    public void set(String key, Object value);

    public Object get(String key);

    Long getOpsForList(String key);

    void setOpsForList(String key, Object invoiceObject);

    public boolean delete(String key);
}
