package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_LIST_VALUE ")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceListValue {
    @Id
    @Column(name = "ID")
    public Long id;

    @Column(name = "TYPE")
    public String type;

    @Column(name = "TYPE_DESCRIPTION")
    public String typeDescription;

    @Column(name = "CODE")
    public String code;

    @Column(name = "DESCRIPTION")
    public String description;

    @Column(name = "DESCRIPTION_EN")
    public String descriptionEn;

    @Column(name = "STATUS")
    public String status;

    @Column(name = "DELETE_FLAG")
    public Long deleteFlag;

}
