package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class Province {
    private String code;
    private String description;
}
