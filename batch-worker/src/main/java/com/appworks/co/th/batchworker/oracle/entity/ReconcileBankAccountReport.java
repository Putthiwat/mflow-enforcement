package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_RECONCILE_MFLOW_BANK_ACCOUNT_REPORT")
@Where(clause = "DELETE_FLAG = 0")
public class ReconcileBankAccountReport extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "mfReconcileMflowBankAccountReportSeq", sequenceName = "MF_RECONCILE_MFLOW_BANK_ACCOUNT_REPORT_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mfReconcileMflowBankAccountReportSeq")
    public Long id;

    @Column(name = "RECEIPT_DATE_TIME", columnDefinition = "TIMESTAMP")
    private Instant receiptDateTime;

    @Column(name = "RECEIPT_NO", columnDefinition = "VARCHAR2(255)")
    private String receiptNo;

    @Column(name = "PAYMENT_DATE", columnDefinition = "TIMESTAMP")
    private Instant paymentDate;

    @Column(name = "EFFECTIVE_DATE", columnDefinition = "TIMESTAMP")
    private Instant effectiveDate;

    @Column(name = "BANK_TRANSACTION_NO", columnDefinition = "VARCHAR2(255)")
    private String bankTransactionNo;

    @Column(name = "BANK_REF_NO", columnDefinition = "VARCHAR2(255)")
    private String bankRefNo;

    @Column(name = "TRANSACTION_CODE", columnDefinition = "VARCHAR2(255)")
    private String transactionCode;

    @Column(name = "TRANSACTION_REF", columnDefinition = "VARCHAR2(255)")
    private String transactionRef;

    @Column(name = "CUSTOMER_NAME", columnDefinition = "VARCHAR2(255)")
    private String customerName;

    @Column(name = "CUSTOMER_REF", columnDefinition = "VARCHAR2(255)")
    private String customerRef;

    @Column(name = "CUSTOMER_ID", columnDefinition = "VARCHAR2(255)")
    private String customerId;

    @Column(name = "PAYMENT_CHANNEL", columnDefinition = "VARCHAR2(255)")
    private String paymentChannel;

    @Column(name = "TOTAL_RECEIPT_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalReceiptAmount;

    @Column(name = "TOTAL_BANK_TRANSACTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalBankTransactionAmount;

    @Column(name = "DIFF_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal diffAmount;

    @Column(name = "INVOICE_STATUS", columnDefinition = "VARCHAR2(255)")
    private String invoiceStatus;

    @Column(name = "VEHICLE_TYPE_CODE", columnDefinition = "VARCHAR2(255)")
    private String vehicleTypeCode;

    @Column(name = "VEHICLE_TYPE_NAME", columnDefinition = "VARCHAR2(255)")
    private String vehicleTypeName;

    @Column(name = "MEMBER_TYPE", columnDefinition = "VARCHAR2(255)")
    private String memberType;

    @Column(name = "CUSTOMER_TYPE", columnDefinition = "VARCHAR2(255)")
    private String customerType;

    @Column(name = "NATIONALITY_CODE", columnDefinition = "VARCHAR2(255)")
    private String nationalityCode;

    @Column(name = "NATIONALITY_NAME", columnDefinition = "VARCHAR2(255)")
    private String nationalityName;

    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(255)")
    private String hqCode;

    @Column(name = "HQ_NAME", columnDefinition = "VARCHAR2(255)")
    private String hqName;

    @Column(name = "PLAZA_CODE", columnDefinition = "VARCHAR2(255)")
    private String plazaCode;

    @Column(name = "PLAZA_NAME", columnDefinition = "VARCHAR2(255)")
    private String plazaName;

    @Column(name = "DEST_PLAZA_CODE", columnDefinition = "VARCHAR2(255)")
    private String destPlazaCode;

    @Column(name = "DEST_PLAZA_NAME", columnDefinition = "VARCHAR2(255)")
    private String destPlazaName;

    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(255)")
    private String invoiceNo;

    @NotNull
    @Column(name = "TRANSACTION_DATE", columnDefinition = "TIMESTAMP")
    private Instant transactionDate;

    @NotNull
    @Column(name = "TRANSACTION_ID", columnDefinition = "VARCHAR2(50)")
    private String trasactionId;

}
