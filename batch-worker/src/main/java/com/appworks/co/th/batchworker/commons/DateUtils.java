package com.appworks.co.th.batchworker.commons;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;


public class DateUtils {

    public static Instant toInstant(Timestamp timestamp){
        return timestamp != null ? timestamp.toInstant() : null;
    }

    public static Instant toInstant(Date date){
        return date != null ? date.toInstant() : null;
    }

    public static Timestamp toTimestamp(Instant instant){
        return instant != null ? Timestamp.from(instant) : null;
    }

    public static  String issueDateDueDateAudit(java.util.Date date){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String newDate = dateFormat.format(date);
        return newDate;
    }
    public static  String transactionDate(Instant date){
        String dateString = null;
        java.util.Date parsedDate = java.util.Date.from(date);
        SimpleDateFormat print = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        dateString = print.format(parsedDate);
        return  dateString;
    }

}
