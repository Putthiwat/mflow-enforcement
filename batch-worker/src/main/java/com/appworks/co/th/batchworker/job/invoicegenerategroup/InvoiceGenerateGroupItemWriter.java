package com.appworks.co.th.batchworker.job.invoicegenerategroup;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceCustomer;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceVehicle;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceCustomerRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceVehicleRepository;
import com.appworks.co.th.customerinfoservice.MasterCustomerDataResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.scheduling.annotation.Async;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class InvoiceGenerateGroupItemWriter implements ItemWriter<Invoice> {
    final DateFormat df = new SimpleDateFormat("yyMMdd");

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private CustomerInfoClient customerInfoClient;

    private InvoiceCustomerRepository invoiceCustomerRepository;

    private InvoiceVehicleRepository invoiceVehicleRepository;

    private InvoiceJdbcRepository invoiceJdbcRepository;

    private InvoiceRepository invoiceRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoice -> {
                this.updateGenerateGroup(invoice);
            });
        }
    }

    @Async
    public void updateGenerateGroup(Invoice invoice) {
        try {
            log.info("invoice no -> {}", invoice.getInvoiceNo());
            if (!invoiceCustomerRepository.findByCustomerId(invoice.getCustomerId()).isPresent()){
                InvoiceCustomer invoiceCustomer = new InvoiceCustomer();
                invoiceCustomer.setCustomerId(invoice.getCustomerId());

                try {
                    log.info("customerInfoClient.getCustomerInfo {}", invoice.getCustomerId());
                    MasterCustomerDataResponse customerInfo = customerInfoClient.getCustomerInfo(invoice.getCustomerId());
                    invoiceCustomer.setMobile(customerInfo.getMobile());
//                    String title = customerInfo.getTitlesList().stream().collect(Collectors.joining(" "));
                    String fullName = invoice.getFullName();
                    invoiceCustomer.setFullName(fullName);
                    invoiceCustomer.setAddress(invoice.getAddress());
                    invoiceCustomerRepository.save(invoiceCustomer);
                } catch (Exception e) {
                    log.error("customerInfoClient.getCustomerInfo {}", e);
                }

            }
            Optional<InvoiceVehicle> optInvoiceVehicle = invoiceVehicleRepository.findByCustomerIdAndPlate1AndPlate2AndProvinceAndVehicleType(invoice.getCustomerId(),
                    invoice.getPlate1(), invoice.getPlate2(), invoice.getProvince(),invoice.getVehicleType());
            if (!optInvoiceVehicle.isPresent()) {
                try {
                    InvoiceVehicle invoiceVehicle = new InvoiceVehicle();
                    invoiceVehicle.setCustomerId(invoice.getCustomerId());
                    invoiceVehicle.setPlate1(invoice.getPlate1());
                    invoiceVehicle.setPlate2(invoice.getPlate2());
                    invoiceVehicle.setProvince(invoice.getProvince());
                    invoiceVehicle.setVehicleType(invoice.getVehicleType());
                    invoiceVehicle.setRefGroup(generateInvoiceNo(String.valueOf(invoice.getInvoiceType())));
                    invoiceVehicleRepository.save(invoiceVehicle);
                    invoiceRepository.updateRefGroupByInvoiceNo(invoiceVehicle.getRefGroup(),Constants.SYSTEM,invoice.getInvoiceNo());
                } catch (Exception e) {
                    log.error("InvoiceVehicle invoiceVehicle {}", e);
                }
            }else{
                invoiceRepository.updateRefGroupByInvoiceNo(optInvoiceVehicle.get().getRefGroup(),Constants.SYSTEM,invoice.getInvoiceNo());
            }
        }catch (Exception e){
            log.info("Error updateGenerateGroup : {}",e.getMessage());
        }
    }

    private String generateInvoiceNo(String type) {
        String seq = invoiceJdbcRepository.getSeq();
        String text  = "00"+type+df.format(new Date())+seq;
        return text;
    }
}