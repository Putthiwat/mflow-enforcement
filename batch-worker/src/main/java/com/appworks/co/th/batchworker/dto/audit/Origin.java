package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class Origin {
    private Hq hq;
    private Plaza plaza;
    private Lane lane;
}
