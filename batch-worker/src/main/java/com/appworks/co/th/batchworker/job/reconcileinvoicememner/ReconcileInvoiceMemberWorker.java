package com.appworks.co.th.batchworker.job.reconcileinvoicememner;

import com.appworks.co.th.batchworker.commons.FormatUtils;
import com.appworks.co.th.batchworker.dto.CompositeMemberReconcileDTO;
import com.appworks.co.th.batchworker.dto.MemberReconcileDTO;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.OraclePagingQueryProvider;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

import javax.sql.DataSource;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Configuration
public class ReconcileInvoiceMemberWorker {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${service-schema.invoice}")
    private String invoiceSchema;

    @Value("${service-schema.customer}")
    private String customerSchema;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    @Value("${reconcile-invoice-member.date.frequency}")
    private String reconcileDate;

    @Autowired
    private ReconcileInvoiceMemberPaymentWriter reconcileMemberPaymentWriter;

    @Autowired
    private ReconcileInvoiceMemberAccountWriter reconcileMemberAccountWriter;

    @Autowired
    private ReconcileInvoiceMemberProcessor reconcileMemberProcessor;

    @Autowired
    private RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private String selectClause() {
        StringBuilder select = new StringBuilder()
            .append(" I.ID,  ")
            .append(" I.INVOICE_NO,  ")
            .append(" I.PAYMENT_DATE,  ")
            .append(" I.PAYMENT_CHANNEL, ")
            .append(" I.DUE_DATE,  ")
            .append(" I.CREATE_DATE,  ")
            .append(" I.PLATE1 || ' ' ||  I.PLATE2 AS PLATE, ")
            .append(" I.FULL_NAME, ")
            .append(" I.STATUS, ")
            .append(" I.COLLECTION_AMOUNT, ")
            .append(" I.FEE_AMOUNT, ")
            .append(" I.FINE_AMOUNT, ")
            .append(" I.TOTAL_AMOUNT, ")
            .append(" I.HQ_CODE, ")
            .append(" I.RECEIPT_NO, ")
            .append(" I.OPERATION_FEE, ")
            .append(" D.LANE_CODE, ")
            .append(" D.PLAZA_CODE, ")
            .append(" D.VEHICLE_WHEEL, ")
                .append(" D.TRANSACTION_DATE, ")
                .append(" D.TRANSACTION_ID, ")
            .append(" B.DESCRIPTION AS BRAND_TH, ")
            .append(" B.DESCRIPTION_EN AS BRAND_EN, ")
            .append(" P.DESCRIPTION AS PROVINCE_TH, ")
            .append(" P.DESCRIPTION_EN AS PROVINCE_EN, ")
            .append(" C.CUSTOMER_ID, ")
            .append(" C.CITIZEN_ID, ")
            .append(" C.CUSTOMER_TYPE, ")
            .append(" C.NATIONALITY_CODE, ")
            .append(" H.NAME AS HQ_NAME_TH, ")
            .append(" H.NAME_EN AS HQ_NAME_EN, ")
            .append(" PZ.NAME AS PLAZA_TH, ")
            .append(" PZ.NAME_EN AS PLAZA_EN, ")
            .append(" L.NAME AS LANE_TH, ")
            .append(" L.NAME_EN AS LANE_EN, ")
            .append(" N.DESCRIPTION AS NATIONALITY_TH, ")
            .append(" N.DESCRIPTION_EN AS NATIONALITY_EN, ")
            .append(" W.DESCRIPTION AS VEHICLE_WHEEL_TH, ")
            .append(" W.DESCRIPTION_EN AS VEHICLE_WHEEL_EN, ")
            .append(" VC.COLOR_TH, ")
            .append(" VC.COLOR_EN ");
        return select.toString();
    }

    private String groupClause() {
        StringBuilder group = new StringBuilder()
            .append(" GROUP BY  ")
            .append(" I.ID,  ")
            .append(" I.INVOICE_NO,  ")
            .append(" I.PAYMENT_DATE,  ")
            .append(" I.PAYMENT_CHANNEL, ")
            .append(" I.DUE_DATE,  ")
            .append(" I.CREATE_DATE,  ")
            .append(" I.PLATE1 || ' ' ||  I.PLATE2, ")
            .append(" I.FULL_NAME, ")
            .append(" I.STATUS, ")
            .append(" I.COLLECTION_AMOUNT, ")
            .append(" I.FEE_AMOUNT, ")
            .append(" I.FINE_AMOUNT, ")
            .append(" I.TOTAL_AMOUNT, ")
            .append(" I.HQ_CODE, ")
            .append(" I.RECEIPT_NO, ")
            .append(" I.OPERATION_FEE, ")
            .append(" D.LANE_CODE, ")
            .append(" D.PLAZA_CODE, ")
            .append(" D.VEHICLE_WHEEL, ")
                .append(" D.TRANSACTION_DATE, ")
                .append(" D.TRANSACTION_ID, ")
            .append(" B.DESCRIPTION, ")
            .append(" B.DESCRIPTION_EN, ")
            .append(" P.DESCRIPTION, ")
            .append(" P.DESCRIPTION_EN, ")
            .append(" C.CUSTOMER_ID, ")
            .append(" C.CITIZEN_ID, ")
            .append(" C.CUSTOMER_TYPE, ")
            .append(" C.NATIONALITY_CODE, ")
            .append(" H.NAME, ")
            .append(" H.NAME_EN, ")
            .append(" PZ.NAME, ")
            .append(" PZ.NAME_EN, ")
            .append(" L.NAME, ")
            .append(" L.NAME_EN, ")
            .append(" N.DESCRIPTION, ")
            .append(" N.DESCRIPTION_EN, ")
            .append(" W.DESCRIPTION, ")
            .append(" W.DESCRIPTION_EN, ")
            .append(" VC.COLOR_TH, ")
            .append(" VC.COLOR_EN ");
        return group.toString();
    }

    private String fromClause() {
        StringBuilder from = new StringBuilder()
            .append(" FROM {0}.MF_INVOICE I ")
            .append(" INNER JOIN {0}.MF_INVOICE_DETAIL D ")
            .append("   ON I.INVOICE_NO = D.INVOICE_NO AND D.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN {0}.MF_INVOICE_MASTER_V_BRAND B ")
            .append("   ON I.BRAND = B.CODE AND B.DELETE_FLAG = 0  ")
            .append(" LEFT JOIN {0}.MF_INVOICE_MASTER_V_OFFICE P ")
            .append("   ON I.PROVINCE = P.CODE AND P.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN {1}.MF_CUST_INFO C ")
            .append("   ON I.CUSTOMER_ID = C.CUSTOMER_ID AND C.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN {1}.MF_CUST_MASTER_NATIONALITY N ")
            .append("   ON C.NATIONALITY_CODE = N.CODE AND N.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN {0}.MF_INVOICE_MASTER_HQ H ")
            .append("   ON I.HQ_CODE = H.CODE AND H.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN {0}.MF_INVOICE_MASTER_PLAZA PZ ")
            .append("   ON D.PLAZA_CODE = PZ.CODE AND PZ.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN {0}.MF_INVOICE_MASTER_LANE L ")
            .append("   ON D.LANE_CODE = L.CODE AND L.DELETE_FLAG = 0  ")
            .append(" LEFT JOIN {0}.MF_INVOICE_MAS_FEE_WHEEL W ")
            .append("   ON D.VEHICLE_WHEEL = W.CODE AND W.DELETE_FLAG = 0 ")
            .append(" LEFT JOIN ( ")
            .append("   SELECT ")
            .append("       VHC.INVOICE_NO, ")
            .append("       LISTAGG(VC.DESCRIPTION, {2}) OVER (PARTITION BY VHC.INVOICE_NO) AS COLOR_TH, ")
            .append("       LISTAGG(VC.DESCRIPTION_EN, {2}) OVER (PARTITION BY VHC.INVOICE_NO) AS COLOR_EN ")
            .append("   FROM {0}.MF_INVOICE_VHC_COLOR VHC ")
            .append("   INNER JOIN {0}.MF_INVOICE_MASTER_V_COLOR VC ON VC.CODE = VHC.CODE AND VC.DELETE_FLAG = 0 ")
            .append("   WHERE VHC.DELETE_FLAG = 0 ")
            .append(" ) VC ON I.INVOICE_NO = VC.INVOICE_NO ");
        Object[] args = new Object[]{ invoiceSchema, customerSchema ,"','"};
        return new MessageFormat(from.toString()).format(args);
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<MemberReconcileDTO> reconcileMemberReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String yesterday = FormatUtils.toString(DateUtils.addDays(new Date(), -1) ,dateFormat);
        String startOfDay = yesterday + " 00:00:00";
        String endOfDay = yesterday + " 23:59:59";
        if(!"ALL".equals(reconcileDate)){
            startOfDay = reconcileDate + " 00:00:00";
            endOfDay = reconcileDate + " 23:59:59";
        }

        HashMap<String, Order> sortKeys = new HashMap<>(2);
        sortKeys.put("ID", Order.ASCENDING);

        OraclePagingQueryProvider queryProvider = new OraclePagingQueryProvider();
        queryProvider.setSelectClause(this.selectClause());
        queryProvider.setFromClause(this.fromClause());
        queryProvider.setWhereClause(String.format("%s AND I.ID >= %s AND I.ID <= %s AND I.CREATE_DATE >= TO_TIMESTAMP('" + startOfDay + "', 'dd-mm-yyyy hh24:mi:ss') AND I.CREATE_DATE <= TO_TIMESTAMP('" + endOfDay + "', 'dd-mm-yyyy hh24:mi:ss')", where, minValue, maxValue));
        queryProvider.setGroupClause(this.groupClause());
        queryProvider.setSortKeys(sortKeys);

        JdbcPagingItemReader<MemberReconcileDTO> pagingItemReader = new JdbcPagingItemReader<>();
        pagingItemReader.setPageSize(1000);
        pagingItemReader.setDataSource(this.dataSource);
        pagingItemReader.setRowMapper(BeanPropertyRowMapper.newInstance(MemberReconcileDTO.class));
        pagingItemReader.setQueryProvider(queryProvider);
        pagingItemReader.afterPropertiesSet();
        return pagingItemReader;
    }

    @Bean
    public CompositeItemWriter<CompositeMemberReconcileDTO> reconcileMemberCompositeWriter() {
        List<ItemWriter<CompositeMemberReconcileDTO>> itemWriters = new ArrayList<>();
        itemWriters.add(this.reconcileMemberPaymentWriter);
        itemWriters.add(this.reconcileMemberAccountWriter);
        CompositeItemWriter compositeItemWriter = new CompositeItemWriter<>();
        compositeItemWriter.setDelegates(itemWriters);
        compositeItemWriter.setIgnoreItemStream(true);
        return compositeItemWriter;
    }

    @Bean
    public Step reconcileMemberWorkerStep() throws Exception {
        return this.workerStepBuilderFactory.get("reconcileMemberWorkerStep")
                .inputChannel(this.reconcileMemberRequests())
                .outputChannel(this.reconcileMemberReplies())
                .<MemberReconcileDTO, CompositeMemberReconcileDTO>chunk(1000)
                .reader(this.reconcileMemberReader(null,null,null))
                .processor(this.reconcileMemberProcessor)
                .writer(this.reconcileMemberCompositeWriter())
                .build();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean
    public DirectChannel reconcileMemberRequests() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow reconcileMemberInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("reconcile.member.requests"))
                .channel(reconcileMemberRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean
    public DirectChannel reconcileMemberReplies() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow reconcileMemberOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(reconcileMemberReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("reconcile.member.replies"))
                .get();
    }

}
