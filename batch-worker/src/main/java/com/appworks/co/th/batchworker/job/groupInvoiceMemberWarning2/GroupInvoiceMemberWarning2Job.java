package com.appworks.co.th.batchworker.job.groupInvoiceMemberWarning2;

import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.job.memberwarning1.MemberWarning1ItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.payload.AdditionalObject;
import com.appworks.co.th.batchworker.service.RedisService;
import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.sagaappworks.batch.generateInvoice.InvoiceObject;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceEvidence;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceTransaction;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;

import javax.persistence.EntityManagerFactory;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Configuration
public class GroupInvoiceMemberWarning2Job {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;
    private final FastDateFormat YYYYMMDD_HHMMSS_SSS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("Asia/Bangkok"));
    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    @Autowired
    @Qualifier("groupInvoiceMemberWarning2JobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("groupInvoiceMemberWarning2JobReplies")
    private DirectChannel replies;

    @Autowired
    private MasterClient masterClient;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    public GroupInvoiceMemberWarning2Job(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<Invoice> groupInvoiceMemberWarning2ItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from Invoice o "+where+" and id >= "+ minValue +" and id <= "+ maxValue +" AND DUE_DATE < TO_DATE(\'" + currentDate() + "\', 'YYYY/MM/DD')";
        log.debug(jpqlQuery);
        JpaPagingItemReader<Invoice> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }


    @Bean("groupInvoiceMemberWarning2ItemProcessor")
    public ItemProcessor<Invoice, InvoiceObject> groupInvoiceMemberWarning2ItemProcessor() {
        return new ItemProcessor<Invoice, InvoiceObject>() {
            @Override
            public InvoiceObject process(Invoice item) throws Exception {
                HqResponse hqResponse = masterClient.getHq(HqRequest.newBuilder().setHqCode(item.getHqCode()).build());
                InvoiceObject invoiceObject = new InvoiceObject();
                CreateInvoiceHeader header = new CreateInvoiceHeader();
                header.setCustomerId(item.getCustomerId());
                header.setInvoiceType("2");
                header.setInvoiceRefNo(item.getInvoiceNo());
                header.setCustomerId(item.getCustomerId());
                header.setPlate1(item.getPlate1());
                header.setPlate2(item.getPlate2());
                header.setPlateProvince(item.getProvince());
                header.setFullName(item.getFullName());
                header.setAddress(item.getAddress());
                header.setIssueDate(item.getIssueDate());
                header.setDueDate(item.getDueDate());
                header.setInvoiceChannel(item.getInvoiceChannel());
                header.setChannel("Batch-service");
                header.setHqCode(item.getHqCode());
                header.setCreateBy(item.getCreateBy());
                header.setVat(0.0);
                header.setServiceProvider(hqResponse.getServiceProvider().getCode());
                header.setBrand(item.getBrand());
                header.setVehicleCode(item.getVehicleCode());
                header.setVehicleTypeCode(item.getVehicleType());
                header.setColor(item.getColors().stream().map(invoiceColor -> invoiceColor.getCode()).collect(Collectors.toList()));

                AdditionalObject additionalObject = new AdditionalObject();
                additionalObject.setDueDate(item.getDueDate());
                additionalObject.setFeeAmount(item.getFeeAmount());
                additionalObject.setFineAmount(item.getFineAmount());
                additionalObject.setCollectionAmount(item.getCollectionAmount());
                additionalObject.setTotalAmount(item.getTotalAmount());
                additionalObject.setStatus(item.getStatus());
                additionalObject.setErrorMessage(item.getErrorMessage());
                additionalObject.setDetails(item.getDetails());

                CreateInvoiceDetail detail = new CreateInvoiceDetail();
                detail.setDetails(item.getDetails().stream().map(invoiceDetail -> {
                    CreateInvoiceTransaction transaction = new CreateInvoiceTransaction();
                    transaction.setId(invoiceDetail.getTransactionId());
                    transaction.setTransactionDate(String.valueOf(transactionInstantToDate(invoiceDetail.getTransactionDate())));
                    //   transaction.setTransactionDate(transactionDate());
                    transaction.setPlate1(invoiceDetail.getPlate1());
                    transaction.setPlate2(invoiceDetail.getPlate2());
                    transaction.setProvince(invoiceDetail.getProvince());
                    transaction.setHqCode(invoiceDetail.getHqCode());
                    transaction.setPlazaCode(invoiceDetail.getPlazaCode());
                    transaction.setLaneCode(invoiceDetail.getLaneCode());
                    transaction.setFeeAmountOld(invoiceDetail.getFeeAmountOld().doubleValue());
                    transaction.setDestinationHqCode(invoiceDetail.getDestHqCode());
                    transaction.setDestinationPlazaCode(invoiceDetail.getDestPlazaCode());
                    transaction.setDestinationLaneCode(invoiceDetail.getDestLaneCode());
                    transaction.setFeeAmount(invoiceDetail.getRawFee().doubleValue());
                    transaction.setDiscount(invoiceDetail.getDiscount().doubleValue());
                    transaction.setFineAmount(invoiceDetail.getFineAmount().doubleValue());
                    transaction.setOperationFee(invoiceDetail.getOperationFee().doubleValue());
                    transaction.setCollectionAmount(invoiceDetail.getCollectionAmount().doubleValue());
                    transaction.setVehicleWheel(invoiceDetail.getVehicleWheel());
                    transaction.setVat(invoiceDetail.getVat().doubleValue());
                    transaction.setOriginTranType(invoiceDetail.getOriginTranType());
                    transaction.setEvidences(invoiceDetail.getEvidences().stream().map(invoiceEvidence -> {
                        CreateInvoiceEvidence evidence = new CreateInvoiceEvidence();
                        evidence.setType(invoiceEvidence.getType());
                        evidence.setFile(invoiceEvidence.getFile());
                        return evidence;
                    }).collect(Collectors.toList()));
                    return transaction;
                }).collect(Collectors.toList()));

                invoiceObject.setCreateInvoiceHeader(header);
                invoiceObject.setCreateInvoiceDetail(detail);
                return invoiceObject;
            }
        };
    }

    @Bean
    public Step groupInvoiceMemberWarning2() throws Exception {
        return this.workerStepBuilderFactory.get("groupInvoiceMemberWarning2")
                .inputChannel(requests)
                .outputChannel(replies)
                .<Invoice, InvoiceObject>chunk(1000)
                .reader(groupInvoiceMemberWarning2ItemReader(null,null, null))
                .processor(groupInvoiceMemberWarning2ItemProcessor())
                .writer(new GroupInvoiceMemberWarning2Writer(
                        (RedisService) applicationContext.getBean("redisService")
                ))
                .build();
    }

    private String transactionInstantToDate(Instant transactionDate) {
        Date date=Date.from(transactionDate);
        return YYYYMMDD_HHMMSS_SSS.format(date);
    }


    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }


}
