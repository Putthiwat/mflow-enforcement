package com.appworks.co.th.batchworker.jasper;

import java.util.HashMap;

public class JasperParameter extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public JasperParameter() {}

	public JasperParameter(String key, Object value) {
		put(key, value);
	}

}
