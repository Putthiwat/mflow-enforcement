package com.appworks.co.th.batchworker.job.regenerateInvoiceMember;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.dto.InvoiceProceedDTO;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.sagaappworks.batch.InvoiceInfo;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceCommand;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceDetail;
import com.appworks.co.th.sagaappworks.batch.ResultInvoiceHeader;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Calendar.DAY_OF_MONTH;

@AllArgsConstructor
public class RegenerateInvoiceMemberAndNonJobWrite implements ItemWriter<InvoiceProceedDTO> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MessagePublisher messagePublisher;

    private final InvoiceJdbcRepository invoiceJdbcRepository;

    private List<Integer> invoiceDay;

    @Override
    public void write(List<? extends InvoiceProceedDTO> items) throws Exception {
        for (InvoiceProceedDTO invoiceProceedDTO : items) {
            if (StringUtils.isNotEmpty(invoiceProceedDTO.getCustomerId())) {
                this.invoiceMember(invoiceProceedDTO);
            } else {
                this.invoiceNonMember(invoiceProceedDTO);
            }
        }
    }

    private void invoiceMember(InvoiceProceedDTO invoiceProceedDTO) {
        Long id = null;
        try {
            Map<String, Object> transaction = objectMapper.readValue(invoiceProceedDTO.getDetail(), new TypeReference<Map<String, Object>>() {
            });
            Map<String, Object> header = (Map<String, Object>) transaction.get("header");
            Map<String, Object> detail = (Map<String, Object>) transaction.get("detail");
            CreateInvoiceDetail createInvoiceDetail = objectMapper.convertValue(detail, CreateInvoiceDetail.class);
            CreateInvoiceHeader createInvoiceHeader = objectMapper.convertValue(header, CreateInvoiceHeader.class);
            String jobId = Objects.toString(transaction.get("id"), null);
            if (StringUtils.isNotEmpty(jobId)) {
                id = Long.valueOf(jobId);
                if (Constants.BillCycle.BILL_CYCLE.equals(createInvoiceHeader.getInvoiceChannel()))
                    createInvoiceHeader.setIssueDate(this.issueDateBillCycle());
                else
                    createInvoiceHeader.setIssueDate(this.issueDateBillTime(createInvoiceDetail));
                if (invoiceJdbcRepository.getInvoiceMember(createInvoiceDetail.getDetails().get(0).getId()) > 0) {
                    ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
                    List<InvoiceInfo> res = new ArrayList<>();
                    createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                        InvoiceInfo invoiceInfo = new InvoiceInfo();
                        invoiceInfo.setId(createInvoiceTransaction.getId());
                        invoiceInfo.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                        res.add(invoiceInfo);
                        return null;
                    }).collect(Collectors.toList());
                    resultInvoiceDetail.setTransactionIds(res);
                    resultInvoiceDetail.setStatus(true);
                    resultInvoiceDetail.setMessage("Success");
                    resultInvoiceDetail.setServiceProvider(createInvoiceHeader.getServiceProvider());
                    ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                    resultInvoiceHeader.setType("MEMBER");
                    CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.APPROVED, resultInvoiceHeader, resultInvoiceDetail);
                    createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedDTO.getInvoiceProceedListID());
                    createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedDTO.getId());
                    log.info("invoiceMember : {} ", createdInvoiceStagingDomainEvent);
                    messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
                } else {
                    CreatedInvoiceStagingDomainEvent invoiceMemberDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.APPROVED, createInvoiceHeader, createInvoiceDetail);
                    invoiceMemberDomainEvent.setInvoiceProceedListId(invoiceProceedDTO.getInvoiceProceedListID());
                    invoiceMemberDomainEvent.setInvoiceProceedId(invoiceProceedDTO.getId());
                    invoiceMemberDomainEvent.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                    log.info("Data RegenerateInvoiceMemberJobWrite : {}", invoiceMemberDomainEvent);
                    messagePublisher.sendDomainEvent(invoiceMemberDomainEvent.channel, invoiceMemberDomainEvent);
                }
            }else {
                ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
                resultInvoiceDetail.setStatus(false);
                resultInvoiceDetail.setMessage("id for createdInvoiceMemberCommand null");

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("MEMBER");

                CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceMemberCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
                createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedDTO.getInvoiceProceedListID());
                createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedDTO.getId());
                createdInvoiceStagingDomainEvent.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);
            }
        } catch (Exception e) {
            log.error("Error invoiceMember : {}",e.getMessage());
        }
    }

    private void invoiceNonMember(InvoiceProceedDTO invoiceProceedDTO) {
        Long id = null;
        try {
            Map<String, Object> transaction = objectMapper.readValue(invoiceProceedDTO.getDetail(), new TypeReference<Map<String, Object>>() {
            });
            Map<String, Object> header = (Map<String, Object>) transaction.get("header");
            Map<String, Object> detail = (Map<String, Object>) transaction.get("detail");
            CreateInvoiceDetail createInvoiceDetail = objectMapper.convertValue(detail, CreateInvoiceDetail.class);
            CreateInvoiceHeader createInvoiceHeader = objectMapper.convertValue(header, CreateInvoiceHeader.class);
            String jobId = Objects.toString(transaction.get("id"), null);
            if (StringUtils.isNotEmpty(jobId)) {
                id = Long.valueOf(jobId);
                createInvoiceHeader.setIssueDate(this.issueDateBillTime(createInvoiceDetail));
                if (invoiceJdbcRepository.getInvoiceNonMember(createInvoiceDetail.getDetails().get(0).getId()) > 0) {
                    ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
                    List<InvoiceInfo> res = new ArrayList<>();
                    createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                        InvoiceInfo invoiceInfo = new InvoiceInfo();
                        invoiceInfo.setId(createInvoiceTransaction.getId());
                        invoiceInfo.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                        res.add(invoiceInfo);
                        return null;
                    }).collect(Collectors.toList());
                    resultInvoiceDetail.setTransactionIds(res);
                    resultInvoiceDetail.setStatus(true);
                    resultInvoiceDetail.setMessage("Success");
                    resultInvoiceDetail.setServiceProvider(createInvoiceHeader.getServiceProvider());
                    ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                    resultInvoiceHeader.setType("NONMEMBER");
                    CreatedInvoiceStagingDomainEvent createdInvoiceStagingDomainEvent = new CreatedInvoiceStagingDomainEvent<>(id, InvoiceNonMemberCommand.class, CommonState.APPROVED, resultInvoiceHeader, resultInvoiceDetail);
                    createdInvoiceStagingDomainEvent.setInvoiceProceedListId(invoiceProceedDTO.getInvoiceProceedListID());
                    createdInvoiceStagingDomainEvent.setInvoiceProceedId(invoiceProceedDTO.getId());
                    createdInvoiceStagingDomainEvent.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                    log.info("invoiceNonMember : {} ", createdInvoiceStagingDomainEvent);
                    messagePublisher.sendDomainEvent("replyCreateInvoiceStagingCommand", createdInvoiceStagingDomainEvent);

                } else {
                    CreatedNonMemberInvoiceDomainEvent createdNonMemberInvoiceDomainEvent = new CreatedNonMemberInvoiceDomainEvent(id, InvoiceNonMemberCommand.class, CommonState.APPROVED, createInvoiceHeader, createInvoiceDetail);
                    createdNonMemberInvoiceDomainEvent.setInvoiceProceedId(invoiceProceedDTO.getId());
                    createdNonMemberInvoiceDomainEvent.setInvoiceProceedListId(invoiceProceedDTO.getInvoiceProceedListID());
                    createdNonMemberInvoiceDomainEvent.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                    log.info("Data RegenerateInvoiceNonMemberJobWrite : {}", createdNonMemberInvoiceDomainEvent);
                    messagePublisher.sendDomainEvent(createdNonMemberInvoiceDomainEvent.channel, createdNonMemberInvoiceDomainEvent);
                }
            }else {

                ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
                resultInvoiceDetail.setStatus(false);
                resultInvoiceDetail.setMessage("id for createdInvoiceMemberCommand null");

                ResultInvoiceHeader resultInvoiceHeader = new ResultInvoiceHeader();
                resultInvoiceHeader.setType("NONMEMBER");
                CreatedNonMemberInvoiceDomainEvent createdNonMemberInvoiceDomainEvent = new CreatedNonMemberInvoiceDomainEvent(id, ResultInvoiceCommand.class, CommonState.REJECTED, resultInvoiceHeader, resultInvoiceDetail);
                createdNonMemberInvoiceDomainEvent.setInvoiceProceedId(invoiceProceedDTO.getId());
                createdNonMemberInvoiceDomainEvent.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                createdNonMemberInvoiceDomainEvent.setInvoiceNo(invoiceProceedDTO.getInvoiceNo());
                messagePublisher.sendDomainEvent("replyCreatedNonMemberInvoiceCommand", createdNonMemberInvoiceDomainEvent);
            }
        } catch (Exception e) {
            log.info("Error invoiceNonMember : {}",e.getMessage());
        }
    }


    public Date issueDateBillCycle() {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        for (int day : invoiceDay) {
            if (day == 32) {
                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                return c.getTime();
            } else if (c.get(DAY_OF_MONTH) < day) {
                c.set(DAY_OF_MONTH, day);
                return c.getTime();
            }
        }
        c.add(Calendar.MONTH, 1);
        c.set(Calendar.DAY_OF_MONTH, findMin(invoiceDay));
        return c.getTime();
    }

    public Integer findMin(List<Integer> list) {
        return list.stream()                        // Stream<Integer>
                .min(Comparator.naturalOrder()) // Optional<Integer>
                .get();                         // Integer
    }



    public Date newDate() {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTime();
    }

    public Date issueDateBillTime(CreateInvoiceDetail createInvoiceDetail) {
        Date dateTo = null;
        try {
            if (ObjectUtils.isNotEmpty(createInvoiceDetail.getDetails())) {
                dateTo = DateUtils.parseDateStrictly(createInvoiceDetail.getDetails().get(0).getTransactionDate(),
                        new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});
                Calendar currentDate = Calendar.getInstance();
                currentDate.setLenient(false);
                currentDate.set(Calendar.HOUR_OF_DAY, 0);
                currentDate.set(Calendar.MINUTE, 0);
                currentDate.set(Calendar.SECOND, 0);
                currentDate.set(Calendar.MILLISECOND, 0);

                Calendar issueDate = Calendar.getInstance();
                issueDate.setLenient(false);
                issueDate.setTime(dateTo);
                issueDate.set(Calendar.HOUR_OF_DAY, 0);
                issueDate.set(Calendar.MINUTE, 0);
                issueDate.set(Calendar.SECOND, 0);
                issueDate.set(Calendar.MILLISECOND, 0);

                if(issueDate.before(currentDate))
                    return currentDate.getTime();

                return issueDate.getTime();
            } else {
                dateTo = this.newDate();
            }
        } catch (ParseException e) {
            e.printStackTrace();
            dateTo = this.newDate();
        }
        return dateTo;
    }


}
