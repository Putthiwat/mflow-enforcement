package com.appworks.co.th.batchworker.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Data
public class MemberReconcileDTO implements Serializable {

    private Long id;
    private String invoiceNo;
    private Date paymentDate;
    private String paymentChannel;
    private Date dueDate;
    private Timestamp createDate;
    private String plate;
    private String fullName;
    private String status;
    private BigDecimal collectionAmount;
    private BigDecimal feeAmount;
    private BigDecimal fineAmount;
    private BigDecimal totalAmount;
    private String hqCode;
    private BigDecimal operationFee;
    private String receiptNo;
    private String laneCode;
    private String plazaCode;
    private String vehicleWheel;
    private String brandTh;
    private String brandEn;
    private String provinceTh;
    private String provinceEn;
    private String customerId;
    private String citizenId;
    private String customerType;
    private String hqNameTh;
    private String hqNameEn;
    private String plazaTh;
    private String plazaEn;
    private String laneTh;
    private String laneEn;
    private String nationalityTh;
    private String nationalityEn;
    private String vehicleWheelTh;
    private String vehicleWheelEn;
    private String colorTh;
    private String colorEn;
    private String nationalityCode;
    private String transactionId;
    private Timestamp transactionDate;

}
