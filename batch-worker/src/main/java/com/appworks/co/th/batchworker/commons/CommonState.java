package com.appworks.co.th.batchworker.commons;

public final class CommonState {

    public static final String APPROVAL_PENDING = "APPROVAL_PENDING";
    public static final String APPROVED = "APPROVED";
    public static final String REJECTED = "REJECTED";
    public static final String CANCEL_PENDING = "CANCEL_PENDING";
    public static final String CANCELLED = "CANCELLED";
    public static final String RESUME_PENDING = "RESUME_PENDING";



}
