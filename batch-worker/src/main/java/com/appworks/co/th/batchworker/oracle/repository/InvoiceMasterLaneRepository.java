package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.InvoiceMasterLane;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface InvoiceMasterLaneRepository extends JpaRepository<InvoiceMasterLane,Long> {
    InvoiceMasterLane findByCode(String code);
}
