package com.appworks.co.th.batchworker.oracle.jdbc;

import com.appworks.co.th.batchworker.payload.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InvoiceMasterJdbcRepositoryImpl implements InvoiceMasterJdbcRepository {

    @Autowired
    @Qualifier("oracleMainJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("oracleMainNamedJdbcTemplate")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public ColorNameDTO getColorNameByCodes(List<String> codes) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" LISTAGG(C.DESCRIPTION, ',') WITHIN GROUP (ORDER BY C.ID) description_th , ")
                .append(" LISTAGG(C.DESCRIPTION_EN, ',') WITHIN GROUP (ORDER BY C.ID) description_en ")
                .append(" FROM MF_INVOICE_MASTER_V_COLOR C ")
                .append(" WHERE C.DELETE_FLAG = 0 ")
                .append(" AND C.CODE IN(:codes) ")
                .toString();
        SqlParameterSource paramSource = new MapSqlParameterSource("codes", codes);
        List<ColorNameDTO> result = namedParameterJdbcTemplate.query(statement, paramSource, BeanPropertyRowMapper.newInstance(ColorNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public ColorNameDTO getColorNameByInvoiceNo(String invoiceNo) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" v.invoice_no, ")
                .append(" LISTAGG(m.description, ',') OVER (PARTITION BY v.invoice_no) description_th, ")
                .append(" LISTAGG(m.description_en, ',') OVER (PARTITION BY v.invoice_no) description_en ")
                .append(" FROM MF_INVOICE_VHC_COLOR v ")
                .append(" INNER JOIN mf_invoice_master_v_color m  on v.code = m.code ")
                .append(" WHERE v.DELETE_FLAG = 0 ")
                .append(" AND m.DELETE_FLAG = 0 ")
                .append(" AND v.invoice_no = ? ")
                .toString();
        Object[] params = new Object[]{invoiceNo};
        List<ColorNameDTO> result = jdbcTemplate.query(statement, params, BeanPropertyRowMapper.newInstance(ColorNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public OfficeNameDTO getOfficeNameByCode(String code) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" P.CODE code, ")
                .append(" P.DESCRIPTION description_th, ")
                .append(" P.DESCRIPTION_EN description_en ")
                .append(" FROM MF_INVOICE_MASTER_V_OFFICE P ")
                .append(" WHERE P.DELETE_FLAG = 0 AND P.CODE = ? ")
                .toString();
        Object[] params = new Object[]{code};
        List<OfficeNameDTO> result = jdbcTemplate.query(statement, params, BeanPropertyRowMapper.newInstance(OfficeNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public HQNameDTO getHQNameByCode(String code) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" H.CODE code,")
                .append(" H.NAME name_th,")
                .append(" H.NAME_EN name_en")
                .append(" FROM MF_INVOICE_MASTER_HQ H ")
                .append(" WHERE H.DELETE_FLAG = 0 AND H.CODE = ? ")
                .toString();
        Object[] params = new Object[]{code};
        List<HQNameDTO> result = jdbcTemplate.query(statement, params, BeanPropertyRowMapper.newInstance(HQNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public PlazaNameDTO getPlazaNameByCode(String code) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" P.CODE code, ")
                .append(" P.NAME name_th, ")
                .append(" P.NAME_EN name_en")
                .append(" FROM MF_INVOICE_MASTER_PLAZA P ")
                .append(" WHERE P.DELETE_FLAG = 0 AND P.CODE = ? ")
                .toString();
        Object[] params = new Object[]{code};
        List<PlazaNameDTO> result = jdbcTemplate.query(statement, params, BeanPropertyRowMapper.newInstance(PlazaNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public BrandNameDTO getBrandNameByCode(String code) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" B.CODE code, ")
                .append(" B.DESCRIPTION description_th, ")
                .append(" B.DESCRIPTION_EN description_en")
                .append(" FROM MF_INVOICE_MASTER_V_BRAND B ")
                .append(" WHERE B.DELETE_FLAG = 0 AND B.CODE = ? ")
                .toString();
        Object[] params = new Object[]{code};
        List<BrandNameDTO> result = jdbcTemplate.query(statement, params, BeanPropertyRowMapper.newInstance(BrandNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    public LaneNameDTO getLaneNameByCode(String code) {
        String statement = new StringBuilder()
                .append(" SELECT ")
                .append(" P.CODE code, ")
                .append(" P.NAME name_th, ")
                .append(" P.NAME_EN name_en")
                .append(" FROM mf_invoice_master_lane P ")
                .append(" WHERE P.DELETE_FLAG = 0 AND P.CODE = ? ")
                .toString();
        Object[] params = new Object[]{code};
        List<LaneNameDTO> result = jdbcTemplate.query(statement, params, BeanPropertyRowMapper.newInstance(LaneNameDTO.class));
        return result.isEmpty() ? null : result.get(0);
    }

}
