package com.appworks.co.th.batchworker.job.movestaging;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.config.BillingMessagePublisher;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.MasterClient;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;

@Configuration
public class UpdateInvoiceNonMemStageJob {

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Autowired
    @Qualifier("invoiceNonMemStagingJobRequests")
    private DirectChannel requests;

    @Autowired
    @Qualifier("invoiceNonMemStagingJobReplies")
    private DirectChannel replies;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    public UpdateInvoiceNonMemStageJob(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }


    @Bean
    @StepScope
    public JpaPagingItemReader<InvoiceNonMemberStaging> invoiceNonMemStagingUpdateItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
            ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);

//        String jpqlQuery = "SELECT o from InvoiceStaging o "+where+" and id >= "+ minValue +" and id <= "+ maxValue + " AND issueDate = TO_DATE(\'2020/04/15\', \'YYYY/MM/DD\')";
        String jpqlQuery = "SELECT o from InvoiceNonMemberStaging o "+where+" and id >= "+ minValue +" and id <= "+ maxValue
//                + " AND issueDate = TO_DATE(\'"+issueDate()+"\', \'YYYY/MM/DD\')"
        ;

        if (!isDoAll){
            jpqlQuery += " AND issueDate = TO_DATE(\'"+issueDate()+"\', \'YYYY/MM/DD\')";
        }

        log.info(jpqlQuery);
        JpaPagingItemReader<InvoiceNonMemberStaging> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }

    @Bean("invoiceNonMemStageUpdateItemProcessor")
    public ItemProcessor<InvoiceNonMemberStaging, InvoiceNonMember> invoiceNonMemStageUpdateItemProcessor() {
        return new ItemProcessor<InvoiceNonMemberStaging, InvoiceNonMember>() {
            @Override
            public InvoiceNonMember process(InvoiceNonMemberStaging item) throws Exception {
                InvoiceNonMember invoice = new InvoiceNonMember();
                invoice.setHqCode(item.getHqCode());
                invoice.setInvoiceNo(item.getInvoiceNo());
                invoice.setInvoiceNoRef(item.getInvoiceNoRef());
                invoice.setInvoiceType(item.getInvoiceType());
                invoice.setTransactionType(item.getTransactionType());
                invoice.setPlate1(item.getPlate1());
                invoice.setPlate2(item.getPlate2());
                invoice.setProvince(item.getProvince());
                invoice.setFullName(item.getFullName());
                invoice.setAddress(item.getAddress());
                invoice.setIssueDate(item.getIssueDate());
                invoice.setDueDate(item.getDueDate());
                invoice.setPaymentDate(item.getPaymentDate());
                invoice.setFeeAmount(item.getFeeAmount());
                invoice.setDiscount(item.getDiscount());
                invoice.setFineAmount(item.getFineAmount());
                invoice.setCollectionAmount(item.getCollectionAmount());
                invoice.setTotalAmount(item.getTotalAmount());
                invoice.setPrintFlag(item.getPrintFlag());
                invoice.setStatus(Constants.Status.PAYMENT_WAITING);
                invoice.setErrorMessage(item.getErrorMessage());
                invoice.setVat(item.getVat());
                invoice.setCreateBy("Batch-service");
                invoice.setCreateChannel("Batch-service");
                invoice.setBrand(item.getBrand());
                invoice.setDocNo(item.getDocNo());
                invoice.setDocType(item.getDocType());
                invoice.setVehicleCode(item.getVehicleCode());
                invoice.setColors(item.getColors().stream().map(colorStage -> {
                    InvoiceNonMemberColor color = new InvoiceNonMemberColor();
                    color.setInvoice(invoice);
                    color.setCode(colorStage.getCode());
                    return color;
                }).collect(Collectors.toList()));
                invoice.setDetails(item.getDetails().stream().map(detailStage -> {
                    InvoiceNonMemberDetail detail = new InvoiceNonMemberDetail();
                    detail.setInvoice(invoice);
                    detail.setTransactionId(detailStage.getTransactionId());
                    detail.setTransactionDate(detailStage.getTransactionDate());
                    detail.setPlate1(detailStage.getPlate1());
                    detail.setPlate2(detailStage.getPlate2());
                    detail.setProvince(detailStage.getProvince());
                    detail.setHqCode(detailStage.getHqCode());
                    detail.setPlazaCode(detailStage.getPlazaCode());
                    detail.setLaneCode(detailStage.getLaneCode());
                    detail.setDestHqCode(detailStage.getDestHqCode());
                    detail.setDestPlazaCode(detailStage.getDestPlazaCode());
                    detail.setDestLaneCode(detailStage.getDestLaneCode());
                    detail.setFeeAmount(detailStage.getFeeAmount());
                    detail.setRawFee(detailStage.getRawFee());
                    detail.setDiscount(detailStage.getDiscount());
                    detail.setFineAmount(detailStage.getFineAmount());
                    detail.setCollectionAmount(detailStage.getCollectionAmount());
                    detail.setTotalAmount(detailStage.getTotalAmount());
                    detail.setVehicleWheel(detailStage.getVehicleWheel());
                    detail.setOriginTranType(detailStage.getOriginTranType());
                    detail.setCreateBy("Batch-service");
                    detail.setCreateChannel("Batch-service");
                    detail.setVat(detailStage.getVat());
                    detail.setEvidences(detailStage.getEvidences().stream().map(evidenceStage -> {
                        InvoiceNonMemberEvidence evidence = new InvoiceNonMemberEvidence();
                        evidence.setDetail(detail);
                        evidence.setTransactionId(evidenceStage.getTransactionId());
                        evidence.setType(evidenceStage.getType());
                        evidence.setFile(evidenceStage.getFile());
                        evidence.setCreateBy("Batch-service");
                        evidence.setCreateChannel("Batch-service");
                        return evidence;
                    }).collect(Collectors.toList()));
                    return detail;

                }).collect(Collectors.toList()));
                log.info("Invoice NonMem {}",item.getInvoiceNo());
                return invoice;
            }
        };
    }

    @Bean
    public Step updateInvoiceNonMemStageStep() throws Exception {
        return this.workerStepBuilderFactory.get("updateInvoiceNonMemStageStep")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceNonMemberStaging, InvoiceNonMember>chunk(1000)
                .reader(invoiceNonMemStagingUpdateItemReader(null,null, null))
                .processor(invoiceNonMemStageUpdateItemProcessor())
                .writer(new UpdateInvoiceNonMemStageItemWriter(
                        (MessagePublisher) applicationContext.getBean("messagePublisher"),
                        (BillingMessagePublisher) applicationContext.getBean("billingMessagePublisher"),
                        (MasterClient) applicationContext.getBean("masterClient"),
                        (CustomerClient) applicationContext.getBean("customerClient"),
                        (InvoiceNonMemberRepository) applicationContext.getBean("invoiceNonMemberRepository")
                ))
                .build();
    }


    private String issueDate(){
        Date date = new Date();
        date = DateUtils.addDays(new Date(),-1);
        String issueDate = df.format(date);
        log.info("IssueDate : {}", issueDate);
        return issueDate;
    }


//    @Bean
//    @StepScope
//    public JdbcBatchItemWriter<InvoiceStaging> invoiceStageUpdateItemWriting()
//    {
//        log.info("Writer");
//        JdbcBatchItemWriter<InvoiceStaging> itemWriter = new JdbcBatchItemWriter<>();
//        itemWriter.setDataSource(dataSource);
//        itemWriter.setSql("UPDATE MF_INVOICE_STAGING SET STATUS = :status WHERE ID = :id");
//
//        itemWriter.setItemSqlParameterSourceProvider
//                (new BeanPropertyItemSqlParameterSourceProvider<>());
//        itemWriter.afterPropertiesSet();
//
//        return itemWriter;
//
//    }
}
