package com.appworks.co.th.batchworker.service.issueday;

import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public interface InvoiceCommonService {
    Date issueDateBillCycle();

    Date issueDateBillTime(CreateInvoiceDetail createInvoiceDetail);
}
