package com.appworks.co.th.batchworker.service;

public interface FileService {

    String uploadFile(byte[] bytes, String extension, String fileId);

}
