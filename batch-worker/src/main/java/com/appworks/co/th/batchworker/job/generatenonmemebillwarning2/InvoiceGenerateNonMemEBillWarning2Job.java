package com.appworks.co.th.batchworker.job.generatenonmemebillwarning2;

import com.appworks.co.th.batchworker.job.invoicegeneratenonmemebill.InvoiceGenerateNonMemEBillItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;

@Configuration
public class InvoiceGenerateNonMemEBillWarning2Job {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${batch.member.do.all}")
    private boolean isDoAll;

    @Value("${job.generate-invoice.max-retry:3}")
    private int maxRetry;

    @Value("${job.generate-invoice.retry-delay:500}")
    private int retryDelay;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    @Qualifier("nonMemGenerateEBillRequestsWarning2")
    private DirectChannel requests;

    @Autowired
    @Qualifier("nonMemGenerateEBillRepliesWarning2")
    private DirectChannel replies;

    public InvoiceGenerateNonMemEBillWarning2Job(RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory) {
        this.workerStepBuilderFactory = workerStepBuilderFactory;
    }

    @Bean
    @StepScope
    public JpaPagingItemReader<InvoiceNonMember> invoiceUpdateNonMemEBillWarning2ItemReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from InvoiceNonMember o "+where+" and id >= "+ minValue +" and id <= "+ maxValue+ " and invoiceType=2";
        log.info(jpqlQuery);
        JpaPagingItemReader<InvoiceNonMember> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(1000);
        reader.afterPropertiesSet();
        reader.setSaveState(false);
        return reader;
    }

    @Bean("invoiceUpdateNonMemEBillWarning2ItemProcessor")
    public ItemProcessor<InvoiceNonMember, InvoiceNonMember> invoiceUpdateNonMemEBillWarning2ItemProcessor() {
        return item -> item;
    }

    @Bean
    public Step updateInvoiceNonMemEBillWarning2Step() throws Exception {
        return this.workerStepBuilderFactory.get("updateInvoiceNonMemEBillWarning2Step")
                .inputChannel(requests)
                .outputChannel(replies)
                .transactionManager(transactionManager)
                .<InvoiceNonMember, InvoiceNonMember>chunk(1000)
                .reader(invoiceUpdateNonMemEBillWarning2ItemReader(null,null, null))
                .processor(invoiceUpdateNonMemEBillWarning2ItemProcessor())
                .writer(new InvoiceGenerateNonMemEBillItemWriter(
                        maxRetry,
                        retryDelay,
                        (FileService) applicationContext.getBean("fileService"),
                        (ReportService) applicationContext.getBean("reportService"),
                        (InvoiceNonMemberRepository) applicationContext.getBean("invoiceNonMemberRepository")
                ))
                .build();
    }

}
