package com.appworks.co.th.batchworker.service.invoice;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.sagaappworks.batch.*;
import com.appworks.co.th.sagaappworks.batch.InvoiceEvidence;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service(value = "invoiceNonMemberService")
public class InvoiceNonMemberServicelmpl implements InvoiceNonMemberService {
    @Value("${fine.invoice.nonMember.Type.zero}")
    private Long invoiceMemberTypeZero;
    @Value("${fine.invoice.nonMember.Type.one}")
    private Long invoiceMemberTypeOne;
    @Value("${fine.invoice.nonMember.Type.three}")
    private Long invoiceMemberTypethree;
    @Value("${fine.time.multiply}")
    private Long fineTimeMultiply;
    @Value("${fine.fineAmount.nonMember.multiply}")
    private Long fineAmount;

    @Value("${operation.fee.nonMember.multiply}")
    private Long operationFeeAmount;


    @Autowired
    InvoiceNonMemberRepository nonMemberRepository;

    @Override
    public ResultInvoiceDetail createNonMemberInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();
        List<InvoiceInfo> res = new ArrayList<>();
        if (StringUtils.isNotEmpty(createInvoiceHeader.getInvoiceRefNo()))
            nonMemberRepository.updateInvoiceTypeByInvoiceNo(3, createInvoiceHeader.getCreateBy(), createInvoiceHeader.getInvoiceRefNo());
        InvoiceNonMember invoice = new InvoiceNonMember();
        Date issueDate = null;
        Instant dueDate = null;
        invoice.setTotalAmount(BigDecimal.ZERO);
        invoice.setFeeAmount(BigDecimal.ZERO);
        invoice.setFineAmount(BigDecimal.ZERO);
        invoice.setCollectionAmount(BigDecimal.ZERO);
        invoice.setOperationFee(BigDecimal.ZERO);
        invoice.setDiscount(BigDecimal.ZERO);
        if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 1) {
            issueDate = this.issueDate();
            dueDate = issueDate.toInstant().plus(invoiceMemberTypeOne, ChronoUnit.DAYS);
        } else if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 2) {
            issueDate = this.issueDate();
            invoice.setOperationFee(BigDecimal.valueOf(operationFeeAmount));
            invoice.setTotalAmount(invoice.getOperationFee());
        }
        invoice.setInvoiceNo(invoiceNo);
        invoice.setInvoiceNoRef(createInvoiceHeader.getInvoiceRefNo());
        invoice.setCreateBy(createInvoiceHeader.getCreateBy());
        invoice.setCreateChannel(createInvoiceHeader.getChannel());
        invoice.setHqCode(createInvoiceHeader.getHqCode());
        invoice.setInvoiceNo(invoiceNo);
        invoice.setTransactionType("Non Member");
        invoice.setInvoiceType(Integer.valueOf(createInvoiceHeader.getInvoiceType()));
        invoice.setFullName(createInvoiceHeader.getFullName());
        invoice.setDocNo(createInvoiceHeader.getDocNo());
        invoice.setDocType(createInvoiceHeader.getDocType());
        invoice.setAddress(createInvoiceHeader.getAddress());
        invoice.setIssueDate(issueDate);
        if (dueDate != null) {
            invoice.setDueDate(Date.from(dueDate));
        }
        invoice.setCreateChannel(createInvoiceHeader.getChannel());
        invoice.setCreateBy(createInvoiceHeader.getCreateBy());
        invoice.setStatus(Constants.Status.PAYMENT_WAITING);
        invoice.setPlate1(createInvoiceHeader.getPlate1());
        invoice.setPlate2(createInvoiceHeader.getPlate2());
        invoice.setProvince(createInvoiceHeader.getPlateProvince());

        invoice.setVat(BigDecimal.ZERO);
        invoice.setBrand(createInvoiceHeader.getBrand());
        invoice.setVehicleCode(createInvoiceHeader.getVehicleCode());
        invoice.setVehicleType(createInvoiceHeader.getVehicleTypeCode());

        if (!createInvoiceHeader.getColor().isEmpty()) {
            List<InvoiceNonMemberColor> colorList = new ArrayList<>();
            for (String colorString : createInvoiceHeader.getColor()) {
                if (colorString != null && StringUtils.isNotEmpty(colorString)) {
                    InvoiceNonMemberColor color = new InvoiceNonMemberColor();
                    color.setInvoice(invoice);
                    color.setCode(colorString);
                    colorList.add(color);
                }
            }
            if (!colorList.isEmpty()) {
                invoice.setColors(colorList);
            }
        }


        List<InvoiceNonMemberDetail> invoiceDetails = createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
            InvoiceNonMemberDetail invoiceDetail = new InvoiceNonMemberDetail();
            invoiceDetail.setCollectionAmount(BigDecimal.valueOf(0));
            invoiceDetail.setFineAmount(BigDecimal.valueOf(0));
            invoiceDetail.setOperationFee(new BigDecimal(0));
            BigDecimal sumFineAmount = new BigDecimal(0);
            BigDecimal sumCollectionAmount = new BigDecimal(0);
            BigDecimal operationFee = new BigDecimal(0);
            invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
            invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
            invoiceDetail.setInvoice(invoice);
            invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
            invoiceDetail.setTransactionDate(this.transactionDate(createInvoiceTransaction.getTransactionDate()));
            invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
            invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
            invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
            invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
            invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
            invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());


            BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());
            invoiceDetail.setRawFee(rawFee);
            invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
           // invoiceDetail.setFeeAmount(rawFee.multiply(BigDecimal.valueOf(100).subtract(invoiceDetail.getDiscount()).divide(BigDecimal.valueOf(100))));
            invoiceDetail.setFeeAmount(rawFee);
            if (StringUtils.equals("0", createInvoiceHeader.getInvoiceType())) {
                invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceHeader.getVat()).setScale(2, RoundingMode.DOWN));
            } else {
                invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
            }
            if(createInvoiceTransaction.getFeeAmountOld()!=null) {
                invoiceDetail.setFeeAmountOld(BigDecimal.valueOf(createInvoiceTransaction.getFeeAmountOld()));
            }else {
                invoiceDetail.setFeeAmountOld(BigDecimal.ZERO);
            }
            switch (Integer.valueOf(createInvoiceHeader.getInvoiceType())) {
                case 1:
                    sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                    sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(fineAmount));
                    invoiceDetail.setFineAmount(sumFineAmount);
                    break;
                case 2:
                    sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                    sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(fineAmount));
                    invoiceDetail.setFineAmount(sumFineAmount);
                    operationFee = operationFee.add(BigDecimal.ZERO);
                    invoiceDetail.setOperationFee(operationFee);
                    break;
            }

            log.info("feeAmount  :  {}  ,fineAmount : {} ,collectionAmount   : {}", invoiceDetail.getFeeAmount(), invoiceDetail.getFineAmount(), invoiceDetail.getCollectionAmount());
            //    invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount()).add(invoiceDetail.getOperationFee()));
            invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount()).add(invoiceDetail.getOperationFee()));
            invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
            invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
            invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                InvoiceNonMemberEvidence evidence = new InvoiceNonMemberEvidence();
                evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                evidence.setCreateChannel(createInvoiceHeader.getChannel());
                evidence.setDetail(invoiceDetail);
                evidence.setTransactionId(invoiceDetail.getTransactionId());
                evidence.setFile(evd.getFile());
                evidence.setType(evd.getType());
                return evidence;
            }).collect(Collectors.toList()));

            //    invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
            invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
            invoice.setFineAmount(invoice.getFineAmount().add(invoiceDetail.getFineAmount()));
            invoice.setCollectionAmount(invoice.getCollectionAmount().add(invoiceDetail.getCollectionAmount()));
            invoice.setTotalAmount(invoice.getTotalAmount().add(invoiceDetail.getTotalAmount()));
            invoice.setOperationFee(invoice.getOperationFee().add(invoiceDetail.getOperationFee()));
            invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
            InvoiceInfo invoiceInfo = new InvoiceInfo();
            invoiceInfo.setId(createInvoiceTransaction.getId());
            invoiceInfo.setInvoiceNo(invoice.getInvoiceNo());
            res.add(invoiceInfo);
            return invoiceDetail;
        }).collect(Collectors.toList());
        log.debug("sendPaymentHistory nonmember: " + invoice.getFeeAmount());
        if (invoice.getTotalAmount().compareTo(BigDecimal.ZERO) == 0) {
            log.debug("opend sendPaymentHistory nonmember: " + invoice.getFeeAmount());
            invoice.setStatus(Constants.Status.PAYMENT_SUCCESS);
        }
        invoice.setDetails(invoiceDetails);

        nonMemberRepository.save(invoice);

        ResultInvoiceDetail invoiceData = mapToInvoiceData(invoice);
        invoiceData.setTransactionIds(res);
        invoiceData.setServiceProvider(createInvoiceHeader.getServiceProvider());
        resultInvoiceDetail.setTransactionIds(res);
        return resultInvoiceDetail;

    }

//    private Date issueDateNonmember(CreateInvoiceDetail createInvoiceDetail) {
//        Date dateTo = null;
//        try {
//            if (ObjectUtils.isNotEmpty(createInvoiceDetail.getDetails())) {
//                try {
//                    dateTo = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(createInvoiceDetail.getDetails().get(0).getTransactionDate());
//                } catch (Exception e) {
//                    dateTo = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'").parse(createInvoiceDetail.getDetails().get(0).getTransactionDate());
//                }
//                Calendar c = Calendar.getInstance();
//                c.setLenient(false);
//                c.setTime(dateTo);
//                c.set(Calendar.HOUR_OF_DAY, 0);
//                c.set(Calendar.MINUTE, 0);
//                c.set(Calendar.SECOND, 0);
//                c.set(Calendar.MILLISECOND, 0);
//                return c.getTime();
//            } else {
//                dateTo = this.issueDate();
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//            dateTo = this.issueDate();
//        }
//        return dateTo;
//    }

    private Date issueDate() {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Instant transactionDate(String data) {
        Instant anotherInstant = null;
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        isoFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        try {
            Date dateFormat = isoFormat.parse(data);
            anotherInstant = dateFormat.toInstant();
        } catch (ParseException e) {
            e.printStackTrace();
            anotherInstant = Instant.now();
        } catch (Exception e) {
            log.error("Error Date : {} ", e);
            anotherInstant = Instant.now();
        }
        return anotherInstant;
    }

    private Date issueAddDate(CreateInvoiceDetail createInvoiceDetail, Long invoiceMemberTypeZero) {
        Date dateTo = null;
        try {
            if (ObjectUtils.isNotEmpty(createInvoiceDetail.getDetails())) {
                dateTo = DateUtils.parseDateStrictly(createInvoiceDetail.getDetails().get(0).getTransactionDate(),
                        new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});

                Calendar currentDate = Calendar.getInstance();
                currentDate.setLenient(false);
                currentDate.set(Calendar.HOUR_OF_DAY, 0);
                currentDate.set(Calendar.MINUTE, 0);
                currentDate.set(Calendar.SECOND, 0);
                currentDate.set(Calendar.MILLISECOND, 0);

                Calendar issueDate = Calendar.getInstance();
                issueDate.setLenient(false);
                issueDate.setTime(dateTo);
                if (invoiceMemberTypeZero != null) {
                    issueDate.add(Calendar.DATE, Integer.valueOf(String.valueOf(invoiceMemberTypeZero)) + 1);
                }
                issueDate.set(Calendar.HOUR_OF_DAY, 0);
                issueDate.set(Calendar.MINUTE, 0);
                issueDate.set(Calendar.SECOND, 0);
                issueDate.set(Calendar.MILLISECOND, 0);

                if(issueDate.getTime().before(currentDate.getTime()))
                    return currentDate.getTime();
                else
                    return issueDate.getTime();
            } else {
                return this.issueDate();
            }
        } catch (Exception e) {
            log.error("Error issueAddDate of InvoiceNonMemberStagingServiceImpl : {}", e.getMessage());
        }
        return this.issueDate();
    }

    private ResultInvoiceDetail mapToInvoiceData(InvoiceNonMember invoice) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();

        InvoiceHeader header = new InvoiceHeader();


        header.setInvoiceNo(invoice.getInvoiceNo());
        header.setInvoiceNoRef(invoice.getInvoiceNoRef());
        header.setInvoiceType(String.valueOf(invoice.getInvoiceType()));
        // ==== customer
//        header.setCustomerId(invoice.getCustomerId());
        header.setCustomerType("Non Member");
        header.setFullName(invoice.getFullName());
        header.setAddress(invoice.getAddress());

        // ==== date time
        header.setIssueDate(invoice.getIssueDate());
        header.setDueDate(invoice.getDueDate());

        header.setHqCode(invoice.getHqCode());
        // ====
        header.setFeeAmount(invoice.getFeeAmount().doubleValue());
        header.setFineAmount(invoice.getFineAmount().doubleValue());
        header.setCollectionAmount(invoice.getCollectionAmount().doubleValue());
        header.setTotalAmount(invoice.getTotalAmount().doubleValue());
        header.setVat(invoice.getVat().doubleValue());
        header.setOperationFee(invoice.getOperationFee().doubleValue());
        header.setPrintFlag(invoice.getPrintFlag());
        header.setStatus(invoice.getStatus());

        resultInvoiceDetail.setInvoiceHeader(header);

        resultInvoiceDetail.setInvoiceTransactions(invoice.getDetails().stream().map(detail -> {

            InvoiceTransaction tran = new InvoiceTransaction();

            tran.setId(detail.getTransactionId());
            tran.setTransactionDate(detail.getTransactionDate().toString());
            tran.setPlate1(detail.getPlate1());
            tran.setPlate2(detail.getPlate2());
            tran.setProvince(detail.getProvince());
            tran.setInvoiceNo(invoice.getInvoiceNo());

            tran.setHqCode(detail.getHqCode());
            tran.setPlazaCode(detail.getPlazaCode());
            tran.setLaneCode(detail.getLaneCode());
            tran.setDestinationHqCode(detail.getDestHqCode());
            tran.setDestinationPlazaCode(detail.getDestPlazaCode());
            tran.setDestinationLaneCode(detail.getDestLaneCode());
            tran.setFeeAmount(detail.getFeeAmount().doubleValue());
            tran.setFineAmount(detail.getFineAmount().doubleValue());
            tran.setCollectionAmount(detail.getCollectionAmount().doubleValue());
            tran.setOperationFee(detail.getOperationFee().doubleValue());
            tran.setTotalAmount(detail.getTotalAmount().doubleValue());
            tran.setVat(detail.getVat().doubleValue());
            tran.setEvidences(detail.getEvidences().stream().map(invoiceEvidence -> {
                InvoiceEvidence createEnforcementEvidence = new InvoiceEvidence();
                createEnforcementEvidence.setTransactionId(invoiceEvidence.getTransactionId());
                createEnforcementEvidence.setType(invoiceEvidence.getType());
                createEnforcementEvidence.setFile(invoiceEvidence.getFile());
                return createEnforcementEvidence;
            }).collect(Collectors.toList()));

            tran.setOriginTranType(detail.getOriginTranType());
            tran.setVehicleWheel(detail.getVehicleWheel());

            return tran;
        }).collect(Collectors.toList()));

        return resultInvoiceDetail;
    }

}
