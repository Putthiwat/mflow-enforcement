package com.appworks.co.th.batchworker.grpc;


import com.appworks.co.th.mflow.fileservice.FileRequest;
import com.appworks.co.th.mflow.fileservice.FileResponse;
import com.appworks.co.th.mpassbatch.MPassBatchGrpc;
import com.appworks.co.th.mpassbatch.MPassBatchRequest;
import com.appworks.co.th.mpassbatch.MPassBatchResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MPassBatchClient {
    @Value("${grpc.mpassbatch.host}")
    private String host;

    @Value("${grpc.mpassbatch.port}")
    private String port;

    private MPassBatchGrpc.MPassBatchBlockingStub blockingStub;

    @Autowired
    public void StartFileServiceConsumer() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        MPassBatchGrpc.MPassBatchBlockingStub stub = MPassBatchGrpc.newBlockingStub(channel);
        this.blockingStub = stub;
    }

//    public MPassBatchResponse callMPassService(MPassBatchRequest request) {
//        return blockingStub.callMPassService(request);
//    }

    public MPassBatchResponse callMPassService(MPassBatchRequest request){
        MPassBatchResponse response = this.blockingStub.callMPassService(request);
        return response;
    }
}
