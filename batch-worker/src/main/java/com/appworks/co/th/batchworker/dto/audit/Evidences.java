package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

@Data
public class Evidences {
    private String type;
    private String fileId;
}
