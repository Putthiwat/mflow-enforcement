package com.appworks.co.th.batchworker.grpc;

import com.appworks.co.th.masterservice.HqRequest;
import com.appworks.co.th.masterservice.HqResponse;
import com.appworks.co.th.masterservice.MasterServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MasterClient {
    @Value("${grpc.masterservice.host}")
    private String host;
    @Value("${grpc.masterservice.port}")
    private String port;

    private MasterServiceGrpc.MasterServiceBlockingStub masterServiceBlockingStub;

    @Autowired
    public void setMasterServiceBlockingStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port)).defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        MasterServiceGrpc.MasterServiceBlockingStub stub = MasterServiceGrpc.newBlockingStub(channel);
        this.masterServiceBlockingStub = stub;
    }

    public HqResponse getHq(HqRequest request){
        HqResponse response = masterServiceBlockingStub.getHq(request);
        return response;
    }

}
