package com.appworks.co.th.batchworker.oracle.jdbc;

import com.appworks.co.th.batchworker.oracle.repository.InvoiceEvidenceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

import static org.apache.commons.lang3.StringUtils.leftPad;

@Repository
public class InvoiceJdbcRepository {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceJdbcRepository.class);

    @Autowired
    @Qualifier("oracleMainJdbcTemplate")
    private JdbcTemplate jdbcTemplate;

    @Autowired
    @Qualifier("oracleMainNamedJdbcTemplate")
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private InvoiceEvidenceRepository invoiceEvidenceRepository;

    private static int batchSize = 1000;

    public Long getSeqInvoice() {
        try {
            String sql = "SELECT invoice_daily_seq.nextval FROM DUAL";
            return jdbcTemplate.queryForObject(sql, Long.class);
        } catch (Exception e) {
            logger.error("InvoiceJdbcRepository getSeq Exception : {}", e);
        }
        return null;
    }

    public String getSeq() {

        Long seq = 0l;
        try {
            String sql = "SELECT invoice_daily_seq.nextval FROM DUAL";

            seq = jdbcTemplate.queryForObject(sql, Long.class);

            String number = leftPad(String.valueOf(seq), 9, "0");

            return number;

        } catch (Exception e) {
            logger.error("InvoiceJdbcRepository getSeq Exception : {}", e);
        }

        return null;
    }

    public String getRefSeq() {

        Long seq = 0l;
        try {
            String sql = "SELECT invoice_daily_seq.nextval FROM DUAL";

            seq = jdbcTemplate.queryForObject(sql, Long.class);

            String number = leftPad(String.valueOf(seq), 9, "0");

            return number;

        } catch (Exception e) {
            logger.error("InvoiceJdbcRepository getSeq Exception : {}", e);
        }

        return null;
    }

    public int getInvoiceMember(String TransactionId){
        StringBuilder sql = new StringBuilder();
        ArrayList params = new ArrayList();
        sql.append(" SELECT staging.countstaging+invoice.countInvoice AS totalTransactionId\n" +
                "FROM (SELECT count(*) AS countInvoice FROM MF_INVOICE_DETAIL WHERE TRANSACTION_ID = ? AND DELETE_FLAG = 0) invoice,\n" +
                "(SELECT count(*) AS countstaging FROM MF_INVOICE_DETAIL_STAGING WHERE TRANSACTION_ID = ? AND DELETE_FLAG = 0 ) staging");
        params.add(TransactionId);
        params.add(TransactionId);
        return jdbcTemplate.queryForObject(sql.toString(), params.toArray(), Integer.class);
    }


    public int getInvoiceNonMember(String TransactionId){
        StringBuilder sql = new StringBuilder();
        ArrayList params = new ArrayList();
        sql.append(" SELECT count(*) AS totalTransactionId FROM MF_INVOICE_DETAIL_NONMEMBER WHERE TRANSACTION_ID = ? AND DELETE_FLAG = 0");
        params.add(TransactionId);
        return jdbcTemplate.queryForObject(sql.toString(), params.toArray(), Integer.class);
    }

}



