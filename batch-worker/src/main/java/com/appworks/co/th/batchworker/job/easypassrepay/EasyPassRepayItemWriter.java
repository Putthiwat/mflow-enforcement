package com.appworks.co.th.batchworker.job.easypassrepay;

import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

@AllArgsConstructor
public class EasyPassRepayItemWriter implements ItemWriter {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private MessagePublisher messagePublisher;


    @Override
    public void write(List items) throws Exception {

    }
}
