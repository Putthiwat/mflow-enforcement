package com.appworks.co.th.batchworker.dto;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class InvoiceProceedListDTO implements Serializable {
    private Long id;
    private String aggregateId;
    private Long invoiceProceedID;
    private String customerId;
    private String invoiceNo;
    private String command;
    private String detail;
    private String state;
}
