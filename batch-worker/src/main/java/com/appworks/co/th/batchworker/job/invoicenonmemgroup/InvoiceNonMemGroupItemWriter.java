package com.appworks.co.th.batchworker.job.invoicenonmemgroup;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceNonMemberVehicle;
import com.appworks.co.th.batchworker.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberRepository;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceNonMemberVehicleRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
public class InvoiceNonMemGroupItemWriter implements ItemWriter<InvoiceNonMember> {
    final DateFormat df = new SimpleDateFormat("yyMMdd");

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private InvoiceNonMemberVehicleRepository invoiceNonMemberVehicleRepository;

    private InvoiceJdbcRepository invoiceJdbcRepository;

    private InvoiceNonMemberRepository invoiceNonMemberRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void write(List<? extends InvoiceNonMember> items) throws Exception {
        if(items!=null && items.size()>0){
            items.stream().forEach(invoice -> {
                this.upInvoiceNonMemGroup(invoice);
            });
        }

    }
    private void upInvoiceNonMemGroup(InvoiceNonMember invoiceNonMember){
        try {
            log.info("InvoiceNonMember no -> {}", invoiceNonMember.getInvoiceNo());
            Optional<InvoiceNonMemberVehicle> optInvoiceNonMemVehicle = invoiceNonMemberVehicleRepository.findByPlate1AndPlate2AndProvinceAndVehicleType(
                    invoiceNonMember.getPlate1(),invoiceNonMember.getPlate2(),invoiceNonMember.getProvince(),invoiceNonMember.getVehicleType());
            if (!optInvoiceNonMemVehicle.isPresent()){
                InvoiceNonMemberVehicle vehicle = new InvoiceNonMemberVehicle();
                vehicle.setPlate1(invoiceNonMember.getPlate1());
                vehicle.setPlate2(invoiceNonMember.getPlate2());
                vehicle.setProvince(invoiceNonMember.getProvince());
                vehicle.setFullName(invoiceNonMember.getFullName());
                vehicle.setAddress(invoiceNonMember.getAddress());
                vehicle.setVehicleType(invoiceNonMember.getVehicleType());
                vehicle.setRefGroup(generateInvoiceNo(String.valueOf(invoiceNonMember.getInvoiceType())));
                invoiceNonMemberVehicleRepository.save(vehicle);
                invoiceNonMemberRepository.updateRefGroupByInvoiceNo(optInvoiceNonMemVehicle.get().getRefGroup(), Constants.SYSTEM,invoiceNonMember.getInvoiceNo());
                log.info("InvoiceNonMember generate ref group -> {}", vehicle.getRefGroup());
            }else{
                invoiceNonMemberRepository.updateRefGroupByInvoiceNo(optInvoiceNonMemVehicle.get().getRefGroup(), Constants.SYSTEM,invoiceNonMember.getInvoiceNo());
            }
        }catch (Exception e){
            log.error("Error upInvoiceNonMemGroup : {}",e);
        }
    }

    private String generateInvoiceNo(String type) {
        String seq = invoiceJdbcRepository.getSeq();
        String text  = "09"+type+df.format(new Date())+seq;
        return text;
    }
}