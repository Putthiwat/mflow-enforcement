package com.appworks.co.th.batchworker.service.customerService;

public interface CustomerVehicleInfoService {
    String getBrand (String vehicleCode);
}
