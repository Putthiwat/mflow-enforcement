package com.appworks.co.th.batchworker.service;

import com.appworks.co.th.batchworker.commons.FormatUtils;
import com.appworks.co.th.batchworker.jasper.JasperParameter;
import com.appworks.co.th.batchworker.jasper.model.JMInvoiceDTO;
import com.appworks.co.th.batchworker.jasper.model.JMInvoiceDetail;
import com.appworks.co.th.batchworker.jasper.model.JMInvoiceHead;
import com.appworks.co.th.batchworker.oracle.entity.*;
import com.appworks.co.th.batchworker.payload.*;
import com.appworks.co.th.masterservice.ServiceProvider;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
@Slf4j
@Service(value = "reportService")
public class ReportServiceImpl implements ReportService {

    public static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceImpl.class);

    public static final String CLASSPATH_IMAGE_PREFIX = "classpath:reports/images/";
    public static final String CLASSPATH_INVOICE_MEMBER = "classpath:reports/INVOICE_MEMBER.jrxml";
    public static final String CLASSPATH_INVOICE_NONMEMBER = "classpath:reports/INVOICE_NONMEMBER.jrxml";

    public static final String NUMBER_FORMAT = "#,##0.00";
    public static final Locale LOCALE_TH = new Locale("th", "TH");
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy", LOCALE_TH);
    public static final DateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", LOCALE_TH);

    @Autowired
    private CacheService cacheService;

    @Autowired
    private InvoiceService paymentService;

    @Autowired
    private ResourceLoader resourceLoader;

    @Override
    public JasperPrint generateInvoiceMember(Invoice invoice) throws JRException, IOException {
        Resource jrxml = resourceLoader.getResource(CLASSPATH_INVOICE_MEMBER);

        List<JMInvoiceDetail> invoiceDetails = new ArrayList<>();
        List<JMInvoiceDTO> jmInvoiceDTOS = new ArrayList<>();
        List<JMInvoiceHead> jmInvoiceHeads = null;
        List<InvoiceDetail> details = invoice.getDetails();
        LinkedHashMap<String, JMInvoiceDTO> linkedHashMapJMInvoiceDTO = new LinkedHashMap<>();
        for (InvoiceDetail detail : details) {
            HQNameDTO hqNameCache = cacheService.getHQName(detail.getHqCode());
            PlazaNameDTO plazaNameCache = cacheService.getPlazaName(detail.getPlazaCode());
            OfficeNameDTO officeNameCache = cacheService.getOfficeName(detail.getProvince());
            LaneNameDTO laneNameCache = cacheService.getLaneName(detail.getLaneCode());

            String hqName = hqNameCache != null ? hqNameCache.getNameTh() : "";
            String laneName = laneNameCache != null ? laneNameCache.getNameTh() : "";
            String plazaName = plazaNameCache != null ? plazaNameCache.getNameTh() : "";
            String officeName = officeNameCache != null ? officeNameCache.getDescriptionTh() : "";

            JMInvoiceDetail jmInvoiceDetail = new JMInvoiceDetail();
            jmInvoiceDetail.setHqName(hqName);
            jmInvoiceDetail.setLaneName(laneName);
            jmInvoiceDetail.setPlate(detail.getPlate1() + " " + detail.getPlate2());
            jmInvoiceDetail.setPlazaName(plazaName);
            jmInvoiceDetail.setProvinceName(officeName);
            jmInvoiceDetail.setTransactionDate(FormatUtils.toString(Date.from(detail.getTransactionDate()), DATE_TIME_FORMAT));
            jmInvoiceDetail.setTotalAmount(FormatUtils.toString(detail.getFeeAmount(), NUMBER_FORMAT));
            invoiceDetails.add(jmInvoiceDetail);
            jmInvoiceDTOS = this.mapHqCode(linkedHashMapJMInvoiceDTO, detail, plazaName);
        }
        log.info("Data Obj  jmInvoiceDTOS :{} ",jmInvoiceDTOS);
        jmInvoiceHeads = jmInvoiceDTOS.stream().map(attachment -> {
            JMInvoiceHead jmInvoiceHead = new JMInvoiceHead();
            jmInvoiceHead.setPlate(attachment.getPlate());
            jmInvoiceHead.setProvinceName(attachment.getProvinceName());
            jmInvoiceHead.setFeeAmount(FormatUtils.toString(attachment.getFeeAmount(), NUMBER_FORMAT));
            jmInvoiceHead.setFineAmount(FormatUtils.toString(attachment.getFineAmount(), NUMBER_FORMAT));
            jmInvoiceHead.setCollectionAmount(FormatUtils.toString(attachment.getCollectionAmount(), NUMBER_FORMAT));
            jmInvoiceHead.setTotalAmount(FormatUtils.toString(attachment.getTotalAmount(), NUMBER_FORMAT));
            jmInvoiceHead.setOperationAmount(FormatUtils.toString(attachment.getOperationAmount(), NUMBER_FORMAT));
            jmInvoiceHead.setVat(FormatUtils.toString(attachment.getVat(), NUMBER_FORMAT));
            return jmInvoiceHead;
        }).collect(Collectors.toList());

        log.info("Data Obj  jmInvoiceHeads :{} ",jmInvoiceHeads);

        List<String> colorCodes = null;
        if (CollectionUtils.isNotEmpty(invoice.getColors())) {
            colorCodes = new ArrayList<>();
            for (InvoiceColor color : invoice.getColors()) {
                colorCodes.add(color.getCode());
            }
        }
        ColorNameDTO colorNameCache = cacheService.getColorNameByCodes(colorCodes);
        OfficeNameDTO officeNameCache = cacheService.getOfficeName(invoice.getProvince());
        BrandNameDTO brandNameCache = cacheService.getBrandName(invoice.getBrand());
        ServiceProvider providerCache = cacheService.getMasterProvider(invoice.getHqCode());

        String provinceName = officeNameCache != null ? officeNameCache.getDescriptionTh() : "";
        String brandName = brandNameCache != null ? brandNameCache.getDescriptionTh() : "-";
        String colorName = colorNameCache != null ? colorNameCache.getDescriptionTh() : "-";
        String qrCodeBase64 = paymentService.getQrCodeMemberBase64(invoice.getInvoiceNo());
        String barCodeBase64 = paymentService.getBarCodeMemberBase64(invoice.getInvoiceNo());

        JRBeanCollectionDataSource jrCollectionDataSource = new JRBeanCollectionDataSource(invoiceDetails.stream()
                .sorted(Comparator.comparing(JMInvoiceDetail::getTransactionDate))
                .collect(Collectors.toList()));

        JRBeanCollectionDataSource jrCollectionDataSourceHead = new JRBeanCollectionDataSource(jmInvoiceHeads.stream()
                .sorted(Comparator.comparing(JMInvoiceHead::getPlate))
                .collect(Collectors.toList()));

        JasperParameter parameter = new JasperParameter();
        parameter.put("providerDescription", providerCache.getDescription());
        parameter.put("providerAddress", providerCache.getAddress());
        parameter.put("providerPhone", providerCache.getPhone());
        parameter.put("providerFax", providerCache.getFax());
        parameter.put("currentDate", FormatUtils.toString(new Date(), DATE_FORMAT));
        parameter.put("invoiceNo", invoice.getInvoiceNo());
        parameter.put("dueDate", FormatUtils.toString(invoice.getDueDate(), DATE_FORMAT));
        parameter.put("fullName", invoice.getFullName());
        parameter.put("address", invoice.getAddress());
        parameter.put("provinceName", provinceName);
        parameter.put("brandName", brandName);
        parameter.put("colorName", colorName);
        parameter.put("plate", invoice.getPlate1() + " " + invoice.getPlate2());
        parameter.put("feeAmount", FormatUtils.toString(invoice.getFeeAmount(), NUMBER_FORMAT));
        parameter.put("fineAmount", FormatUtils.toString(invoice.getFineAmount(), NUMBER_FORMAT));
        parameter.put("collectionAmount", FormatUtils.toString(invoice.getCollectionAmount(), NUMBER_FORMAT));
        parameter.put("totalAmount", FormatUtils.toString(invoice.getTotalAmount(), NUMBER_FORMAT));
        parameter.put("barCodeBase64", barCodeBase64 != null ? barCodeBase64 : "-");
        parameter.put("qrCodeBase64", qrCodeBase64 != null ? qrCodeBase64 : "-");
        parameter.put("detail", jrCollectionDataSource);
        parameter.put("header", jrCollectionDataSourceHead);
        parameter.put("customerId", invoice.getCustomerId());
        parameter.put("vehicleCode", invoice.getVehicleCode());
        parameter.put("operationAmount", FormatUtils.toString(invoice.getOperationFee(), NUMBER_FORMAT));

        JRDataSource jrDataSource = new JREmptyDataSource();
        JasperReport jasperReport = JasperCompileManager.compileReport(jrxml.getInputStream());
        return JasperFillManager.fillReport(jasperReport, parameter, jrDataSource);
    }

    @Override
    public JasperPrint generateInvoiceNonMember(InvoiceNonMember invoiceNonMember) throws JRException, IOException {
        Resource jrxml = resourceLoader.getResource(CLASSPATH_INVOICE_NONMEMBER);

        List<String> colorCodes = null;
        if (CollectionUtils.isNotEmpty(invoiceNonMember.getColors())) {
            colorCodes = new ArrayList<>();
            for (InvoiceNonMemberColor color : invoiceNonMember.getColors()) {
                colorCodes.add(color.getCode());
            }
        }

        ColorNameDTO colorNameCache = cacheService.getColorNameByCodes(colorCodes);
        OfficeNameDTO officeNameCache = cacheService.getOfficeName(invoiceNonMember.getProvince());
        BrandNameDTO brandNameCache = cacheService.getBrandName(invoiceNonMember.getBrand());
        ServiceProvider providerCache = cacheService.getMasterProvider(invoiceNonMember.getHqCode());

        String colorName = colorNameCache != null ? colorNameCache.getDescriptionTh() : "-";
        String provinceName = officeNameCache != null ? officeNameCache.getDescriptionTh() : "";
        String brandName = brandNameCache != null ? brandNameCache.getDescriptionTh() : "-";
        String qrCodeBase64 = paymentService.getQrCodeNonMemberBase64(invoiceNonMember.getInvoiceNo());
        String barCodeBase64 = paymentService.getBarCodeNonMemberBase64(invoiceNonMember.getInvoiceNo());

        JasperParameter parameter = new JasperParameter();
        parameter.put("providerDescription", providerCache.getDescription());
        parameter.put("providerAddress", providerCache.getAddress());
        parameter.put("providerPhone", providerCache.getPhone());
        parameter.put("providerFax", providerCache.getFax());
        parameter.put("plate", invoiceNonMember.getPlate1() + " " + invoiceNonMember.getPlate2());
        parameter.put("provinceName", provinceName);
        parameter.put("brandName", brandName);
        parameter.put("colorName", colorName);
        parameter.put("currentDate", FormatUtils.toString(new Date(), DATE_FORMAT));
        parameter.put("invoiceNo", invoiceNonMember.getInvoiceNo());
        parameter.put("dueDate", FormatUtils.toString(invoiceNonMember.getDueDate(), DATE_FORMAT));
        parameter.put("feeAmount", FormatUtils.toString(invoiceNonMember.getFeeAmount(), NUMBER_FORMAT));
        parameter.put("fineAmount", FormatUtils.toString(invoiceNonMember.getFineAmount(), NUMBER_FORMAT));
        parameter.put("collectionAmount", FormatUtils.toString(invoiceNonMember.getCollectionAmount(), NUMBER_FORMAT));
        parameter.put("totalAmount", FormatUtils.toString(invoiceNonMember.getTotalAmount(), NUMBER_FORMAT));
        parameter.put("barCodeBase64", barCodeBase64 != null ? barCodeBase64 : "-");
        parameter.put("qrCodeBase64", qrCodeBase64 != null ? qrCodeBase64 : "-");
        parameter.put("vehicleCode", invoiceNonMember.getVehicleCode());
        parameter.put("operationAmount", FormatUtils.toString(invoiceNonMember.getOperationFee(), NUMBER_FORMAT));
        JRDataSource jrDataSource = new JREmptyDataSource();
        JasperReport jasperReport = JasperCompileManager.compileReport(jrxml.getInputStream());
        return JasperFillManager.fillReport(jasperReport, parameter, jrDataSource);
    }

    private List<JMInvoiceDTO> mapHqCode(LinkedHashMap<String, JMInvoiceDTO> linkedHashMapJMInvoiceDTO, InvoiceDetail detail, String plazaName) {
        BigDecimal totalAmountVat = new BigDecimal("0");
        JMInvoiceDTO jmInvoiceDTO = linkedHashMapJMInvoiceDTO.get(detail.getHqCode());
        if (ObjectUtils.isEmpty(jmInvoiceDTO)) {
            JMInvoiceDTO jmInvoiceHead = new JMInvoiceDTO();
            jmInvoiceHead.setPlate(detail.getPlate1() + " " + detail.getPlate2());
            jmInvoiceHead.setProvinceName(plazaName);
            jmInvoiceHead.setFeeAmount(detail.getFeeAmount());
            jmInvoiceHead.setFineAmount(detail.getFineAmount());
            jmInvoiceHead.setCollectionAmount(detail.getCollectionAmount());
            jmInvoiceHead.setTotalAmount(detail.getTotalAmount());
            jmInvoiceHead.setOperationAmount(detail.getOperationFee());

            jmInvoiceHead.setVat(detail.getVat());
            if (detail.getVat().compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal bn2 = new BigDecimal("100");
                BigDecimal bn3 = detail.getVat().divide(bn2);
                BigDecimal total = detail.getFeeAmount().multiply(bn3);
                totalAmountVat=totalAmountVat.add(total.setScale(2, RoundingMode.HALF_UP));
                jmInvoiceHead.setVat(totalAmountVat);
            }

            jmInvoiceDTO = jmInvoiceHead;
            linkedHashMapJMInvoiceDTO.put(detail.getHqCode(), jmInvoiceDTO);
        } else {
            if (detail.getFeeAmount().compareTo(BigDecimal.ZERO) > 0) {
                jmInvoiceDTO.getFeeAmount().add(detail.getFeeAmount());
            }
            if (detail.getFineAmount().compareTo(BigDecimal.ZERO) > 0) {
                jmInvoiceDTO.getFineAmount().add(detail.getFineAmount());
            }
            if (detail.getCollectionAmount().compareTo(BigDecimal.ZERO) > 0) {
                jmInvoiceDTO.getCollectionAmount().add(detail.getCollectionAmount());
            }
            if (detail.getTotalAmount().compareTo(BigDecimal.ZERO) > 0) {
                jmInvoiceDTO.getTotalAmount().add(detail.getTotalAmount());
            }
            if (detail.getOperationFee().compareTo(BigDecimal.ZERO) > 0) {
                jmInvoiceDTO.getOperationAmount().add(detail.getOperationFee());
            }
            if (detail.getVat().compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal bn2 = new BigDecimal("100");
                BigDecimal bn3 = detail.getVat().divide(bn2);
                BigDecimal total = detail.getFeeAmount().multiply(bn3);
                totalAmountVat=totalAmountVat.add(total.setScale(2, RoundingMode.HALF_UP));
                jmInvoiceDTO.getVat().add(totalAmountVat);
            }
        }
        return new ArrayList<>(linkedHashMapJMInvoiceDTO.values());
    }

}
