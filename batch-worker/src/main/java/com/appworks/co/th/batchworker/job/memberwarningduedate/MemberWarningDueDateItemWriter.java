package com.appworks.co.th.batchworker.job.memberwarningduedate;

import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import com.appworks.co.th.customerservice.PaymentTypeByVehicleCodeRequest;
import com.appworks.co.th.customerservice.PaymentTypeRequest;
import com.appworks.co.th.customerservice.PaymentTypeResponse;
import com.appworks.co.th.notificationmanagement.NotificationManagementRequest;
import com.appworks.co.th.notificationmanagement.SmsReminderPaymentNotificationRequest;
import com.appworks.co.th.sagaappworks.common.CommonState;
import com.appworks.co.th.sagaappworks.config.MessagePublisher;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import com.appworks.co.th.sagaappworks.invoice.InvoiceMemberCommand;
import com.appworks.co.th.sagaappworks.invoice.InvoiceMemberDomainEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

@AllArgsConstructor
public class MemberWarningDueDateItemWriter implements ItemWriter<Invoice> {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private NotificationManagementClient notificationManagementClient;

    private CustomerClient customerClient;

    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        items.stream().forEach(invoiceObject -> {
            PaymentTypeResponse paymentMethod;
            paymentMethod = customerClient.getPaymentMethodByVehicleCode(PaymentTypeByVehicleCodeRequest.newBuilder().setVehicleCode(invoiceObject.getVehicleCode())
                    .setIsGenerateFilePayment("false").build());
            if("2".equals(paymentMethod.getPayment().getType())){
                NotificationManagementRequest.Builder req = NotificationManagementRequest.newBuilder();
                req.setInvoiceNo(invoiceObject.getInvoiceNo());
                req.setType("WARNING");
                notificationManagementClient.invoiceWarningDueDateNotification(req.build());

                SmsReminderPaymentNotificationRequest.Builder request = SmsReminderPaymentNotificationRequest.newBuilder();
                request.setReferenceId8(invoiceObject.getInvoiceNo());
                request.setReferenceId(invoiceObject.getFullName());
                notificationManagementClient.smsReminderPaymentNotification(request.build());
            }
        });
    }
}
