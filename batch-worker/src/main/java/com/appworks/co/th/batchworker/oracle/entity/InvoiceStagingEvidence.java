package com.appworks.co.th.batchworker.oracle.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_EVIDENCE_STAGING")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceStagingEvidence extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceStageEvi", sequenceName = "SEQ_MF_INVOICE_STAGE_EVI", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceStageEvi")
    public Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(
            name = "INVOICE_DETAIL_ID",
            referencedColumnName = "ID",
            foreignKey = @ForeignKey(name = "MF_INV_STAGE_EVI_FK1"),
            nullable = false)
    private InvoiceStagingDetail detail;

    @Column(name = "TRANSACTION_ID", columnDefinition = "VARCHAR2(50)")
    @NotNull
    private String transactionId;

    @Column(name = "TYPE", columnDefinition = "VARCHAR2(20)")
    private String type;

    @Column(name = "FILE_ID", columnDefinition = "VARCHAR2(40)")
    private String file;

}
