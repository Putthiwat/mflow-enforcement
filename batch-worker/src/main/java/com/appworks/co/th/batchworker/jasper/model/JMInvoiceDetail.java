package com.appworks.co.th.batchworker.jasper.model;


import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class JMInvoiceDetail {

    private String transactionDate;
    private String plate;
    private String provinceName;
    private String hqName;
    private String laneName;
    private String plazaName;
    private String totalAmount;

}
