package com.appworks.co.th.batchworker.oracle.repository;

import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceVehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional
public interface InvoiceVehicleRepository extends JpaRepository<InvoiceVehicle, Long>{

    Optional<InvoiceVehicle> findByCustomerIdAndPlate1AndPlate2AndProvince(String customerId, String plate1, String plate2, String province);

    Optional<InvoiceVehicle> findByCustomerIdAndPlate1AndPlate2AndProvinceAndVehicleType(String customerId, String plate1, String plate2, String province,String vehicleType);
}
