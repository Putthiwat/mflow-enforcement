package com.appworks.co.th.batchworker.job.reconcileinvoicememner;

import com.appworks.co.th.batchworker.commons.DateUtils;
import com.appworks.co.th.batchworker.dto.CompositeMemberReconcileDTO;
import com.appworks.co.th.batchworker.dto.MemberReconcileDTO;
import com.appworks.co.th.batchworker.oracle.entity.PaymentReconcileReport;
import com.appworks.co.th.batchworker.oracle.entity.ReconcileBankAccountReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Instant;

@Component
public class ReconcileInvoiceMemberProcessor implements ItemProcessor<MemberReconcileDTO, CompositeMemberReconcileDTO> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final String BANK_CHANNEL = "D";
    private static final String TRANSACTION_CODE = "D";
    private static final String MEMBER_TYPE = "MEMBER";
    private static final String CREATE_BY = "System";
    private static final String CREATE_BY_CHANNEL = "Batch-service";

    //    @Value("#{stepExecution}")
    //    private StepExecution stepExecution;

    @Override
    public CompositeMemberReconcileDTO process(MemberReconcileDTO item) throws Exception {
        CompositeMemberReconcileDTO compositeReconcileDTO = new CompositeMemberReconcileDTO();
        compositeReconcileDTO.setPaymentReconcile(this.getPaymentReconcile(item));
        compositeReconcileDTO.setAccountReconcile(this.getAccountReconcile(item));
        return compositeReconcileDTO;
    }

    private ReconcileBankAccountReport getAccountReconcile(MemberReconcileDTO item) {
        ReconcileBankAccountReport accountReconcile = new ReconcileBankAccountReport();
        accountReconcile.setCreateBy(CREATE_BY);
        accountReconcile.setCreateById(CREATE_BY);
        accountReconcile.setCreateChannel(CREATE_BY_CHANNEL);
        accountReconcile.setCreateDate(Instant.now());
        accountReconcile.setDeleteFlag(0);
        accountReconcile.setUpdateBy(null);
        accountReconcile.setUpdateById(null);
        accountReconcile.setUpdateChannel(null);
        accountReconcile.setUpdateDate(Instant.now());
        accountReconcile.setVersion(1);
        accountReconcile.setBankRefNo(null);
        accountReconcile.setBankTransactionNo(null);
        accountReconcile.setCustomerId(item.getCustomerId());
        accountReconcile.setCustomerName(item.getFullName());
        accountReconcile.setCustomerRef(item.getCitizenId());

        accountReconcile.setEffectiveDate(Instant.now());
        accountReconcile.setPaymentChannel(item.getPaymentChannel());
        accountReconcile.setPaymentDate(DateUtils.toInstant(item.getPaymentDate()));
        accountReconcile.setReceiptDateTime(DateUtils.toInstant(item.getPaymentDate()));
        accountReconcile.setReceiptNo(item.getReceiptNo());
        accountReconcile.setTotalBankTransactionAmount(BigDecimal.valueOf(0));

        BigDecimal diffAmount = item.getTotalAmount().subtract(accountReconcile.getTotalBankTransactionAmount()!=null ?
                accountReconcile.getTotalBankTransactionAmount():BigDecimal.valueOf(0));
        accountReconcile.setDiffAmount(diffAmount);

        accountReconcile.setTotalReceiptAmount(item.getTotalAmount());
        accountReconcile.setTransactionCode(TRANSACTION_CODE);
        accountReconcile.setTransactionRef(null);
        accountReconcile.setCustomerType(item.getCustomerType());
        accountReconcile.setDestPlazaCode(item.getLaneCode());
        accountReconcile.setDestPlazaName(item.getLaneTh());
        accountReconcile.setHqCode(item.getHqCode());
        accountReconcile.setHqName(item.getHqNameTh());
        accountReconcile.setInvoiceStatus(item.getStatus());
        accountReconcile.setMemberType(MEMBER_TYPE);
        accountReconcile.setNationalityCode(item.getNationalityCode());
        accountReconcile.setNationalityName(item.getNationalityTh());
        accountReconcile.setPlazaCode(item.getPlazaCode());
        accountReconcile.setPlazaName(item.getPlazaTh());
        accountReconcile.setVehicleTypeCode(item.getVehicleWheel());
        accountReconcile.setVehicleTypeName(item.getVehicleWheelTh());
        accountReconcile.setInvoiceNo(item.getInvoiceNo());
        accountReconcile.setTransactionDate(DateUtils.toInstant(item.getTransactionDate()));
        accountReconcile.setTrasactionId(item.getTransactionId());
        return accountReconcile;
    }

    private PaymentReconcileReport getPaymentReconcile(MemberReconcileDTO item) {
        PaymentReconcileReport paymentReconcile = new PaymentReconcileReport();
        paymentReconcile.setCreateBy(CREATE_BY);
        paymentReconcile.setCreateById(CREATE_BY);
        paymentReconcile.setCreateChannel(CREATE_BY_CHANNEL);
        paymentReconcile.setCreateDate(Instant.now());
        paymentReconcile.setDeleteFlag(0);
        paymentReconcile.setUpdateBy(null);
        paymentReconcile.setUpdateById(null);
        paymentReconcile.setUpdateChannel(null);
        paymentReconcile.setUpdateDate(Instant.now());
        paymentReconcile.setVersion(1);
        paymentReconcile.setBankChannel(BANK_CHANNEL);
        paymentReconcile.setBankTransactionAmount(item.getTotalAmount());
        paymentReconcile.setBrand(item.getBrandTh());
        paymentReconcile.setColor(item.getColorTh());
        paymentReconcile.setCustomerId(item.getCustomerId());
        paymentReconcile.setCustomerRef(item.getCitizenId());

        BigDecimal diffChannelAmount = paymentReconcile.getBankTransactionAmount().subtract(item.getTotalAmount()!=null ?
                item.getTotalAmount():BigDecimal.valueOf(0));
        paymentReconcile.setDiffChannelAmount(diffChannelAmount);

        BigDecimal diffMflowAmount = item.getTotalAmount().subtract(paymentReconcile.getBankTransactionAmount()!=null ?
                paymentReconcile.getBankTransactionAmount():BigDecimal.valueOf(0));
        paymentReconcile.setDiffMflowAmount(diffMflowAmount);

        paymentReconcile.setDueDate(item.getDueDate()!=null ? DateUtils.toInstant(item.getDueDate()):null);

        paymentReconcile.setFeeAmount(item.getFeeAmount());
        paymentReconcile.setFineAmount(item.getFineAmount());
        paymentReconcile.setHqName(item.getHqNameTh());
        paymentReconcile.setInvoiceCreateDate(DateUtils.toInstant(item.getCreateDate()));
        paymentReconcile.setInvoiceNo(item.getInvoiceNo());
        paymentReconcile.setLaneName(item.getLaneTh());
        paymentReconcile.setPaymentChannel(item.getPaymentChannel());
        paymentReconcile.setPlate(item.getPlate());
        paymentReconcile.setPlazaName(item.getPlazaTh());
        paymentReconcile.setPostDate(DateUtils.toInstant(item.getPaymentDate()));
        paymentReconcile.setProvince(item.getProvinceTh());
        paymentReconcile.setReceiptDateTime(DateUtils.toInstant(item.getPaymentDate()));
        paymentReconcile.setReceiptNo(item.getReceiptNo());
        paymentReconcile.setTotalAmount(item.getTotalAmount());
        paymentReconcile.setHqCode(item.getHqCode());
        paymentReconcile.setLaneCode(item.getLaneCode());
        paymentReconcile.setPlazaCode(item.getPlazaCode());
        paymentReconcile.setCustomerType(item.getCustomerType());
        paymentReconcile.setNationalityCode(item.getNationalityCode());
        paymentReconcile.setNationalityName(item.getNationalityTh());
        paymentReconcile.setVehicleTypeCode(item.getVehicleWheel());
        paymentReconcile.setVehicleTypeName(item.getVehicleWheelTh());
        paymentReconcile.setMemberType(MEMBER_TYPE);
        paymentReconcile.setOperationFee(item.getOperationFee());
        paymentReconcile.setTransactionDate(DateUtils.toInstant(item.getTransactionDate()));
        paymentReconcile.setTrasactionId(item.getTransactionId());
        return paymentReconcile;
    }

}
