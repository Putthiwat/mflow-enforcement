package com.appworks.co.th.batchworker.commons;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class PDFUtils {

    public static byte[] encryptWithPassword(byte[] bytes, String password) throws IOException {
        return encryptWithPassword(bytes, password, 128);
    }

    public static byte[] encryptWithPassword(byte[] bytes, String password, int length) throws IOException {
        try (PDDocument document = PDDocument.load(new ByteArrayInputStream(bytes))) {
            AccessPermission permission = new AccessPermission();
            StandardProtectionPolicy standardPP = new StandardProtectionPolicy(password, password, permission);
            standardPP.setEncryptionKeyLength(length);
            document.protect(standardPP);
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            document.save(outputStream);
            return outputStream.toByteArray();
        }
    }

}
