package com.appworks.co.th.batchworker.jasper.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class JMInvoiceDTO {
    private String plate;
    private String provinceName;
    private BigDecimal feeAmount;
    private BigDecimal fineAmount;
    private BigDecimal collectionAmount;
    private BigDecimal totalAmount;
    private BigDecimal operationAmount;
    private BigDecimal vat;
}
