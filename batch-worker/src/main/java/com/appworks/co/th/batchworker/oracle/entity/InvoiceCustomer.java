package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_INVOICE_CUSTOMER",indexes = {
        @Index(name="IDX_INVOICE_CUST",columnList="CUSTOMER_ID"),
        @Index(name="IDX_INVOICE_MOBILE",columnList="MOBILE")
        },
        uniqueConstraints=@UniqueConstraint(columnNames = {"MOBILE","CUSTOMER_ID"}))
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceCustomer extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceCustomer", sequenceName = "SEQ_MF_INVOICE_CUSTOMER", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceCustomer")
    public Long id;

    @Column(name = "CUSTOMER_ID", columnDefinition = "VARCHAR2(25) NOT NULL")
    private String customerId;

    @Column(name = "MOBILE", columnDefinition = "VARCHAR2(70) NOT NULL")
    private String mobile;

    @Column(name = "FULL_NAME", columnDefinition = "VARCHAR2(500)")
    private String fullName;

    @Column(name = "ADDRESS", columnDefinition = "VARCHAR2(1500)")
    private String address;

}
