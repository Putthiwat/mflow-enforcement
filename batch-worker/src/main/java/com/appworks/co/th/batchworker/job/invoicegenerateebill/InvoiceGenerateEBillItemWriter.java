package com.appworks.co.th.batchworker.job.invoicegenerateebill;

import com.appworks.co.th.batchworker.commons.Constants;
import com.appworks.co.th.batchworker.commons.PDFUtils;
import com.appworks.co.th.batchworker.grpc.CustomerClient;
import com.appworks.co.th.batchworker.grpc.CustomerInfoClient;
import com.appworks.co.th.batchworker.grpc.NotificationManagementClient;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.repository.InvoiceRepository;
import com.appworks.co.th.batchworker.service.CacheService;
import com.appworks.co.th.batchworker.service.FileService;
import com.appworks.co.th.batchworker.service.GrpcSendSmsAndEmailService;
import com.appworks.co.th.batchworker.service.ReportService;
import com.appworks.co.th.customerinfoservice.MasterCustomerDataResponse;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.appworks.co.th.batchworker.commons.Constants.Invoice.EBIL_FORMAT;

@AllArgsConstructor
public class InvoiceGenerateEBillItemWriter implements ItemWriter<Invoice> {

    final SimpleDateFormat df = new SimpleDateFormat("yyMMddHHmmssSSS", Locale.US);


    @Value("${ebill.billcycle.url}")
    private String EbillLink;

    @Value("${send.notification.ebill.flag}")
    private boolean sendNotificationFlag;

    @Value("${AccrualList.billcycle.url}")
    private String AccrualListLink;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private int maxRetry;

    private CacheService cacheService;

    private FileService fileService;

    private ReportService reportService;

    private InvoiceRepository invoiceRepository;


    private NotificationManagementClient notificationManagementClient;


    private GrpcSendSmsAndEmailService grpcSendSmsAndEmailService;


    private CustomerInfoClient customerInfoClient;

    private CustomerClient customerClient;

    @Override
    public void write(List<? extends Invoice> items) throws Exception {
        if (items != null && items.size() > 0) {
            items.stream().forEach(invoice -> {
                try {
                    log.info("Start Generate E-Bill for Member with invoice no. : {}",invoice!=null? invoice.getInvoiceNo():"-");
                    generateEbill(invoice);
                } catch (Exception e) {
                    log.error("InvoiceGenerateEBillItemWriter error : {}", e);
                }
            });
        }
    }

    @Async
    public void generateEbill(Invoice invoice) {
        try {
            JasperPrint jasperPrint = reportService.generateInvoiceMember(invoice);
            log.debug("JasperPrint : {}", jasperPrint);
            byte[] bytes = JasperExportManager.exportReportToPdf(jasperPrint);

            String pdfPassword = cacheService.getPhone(invoice.getCustomerId());
            log.debug("pdfPassword : {}", pdfPassword);

            byte[] bytesEncrypt = PDFUtils.encryptWithPassword(bytes, pdfPassword);

            String fileId = String.format(EBIL_FORMAT, invoice.getInvoiceNo(),df.format(Calendar.getInstance().getTime()));

            fileService.uploadFile(bytesEncrypt, "pdf", fileId);

            invoiceRepository.updateEBillFileIdByInvoiceNo(fileId, Constants.SYSTEM, invoice.getInvoiceNo());

            log.info("Generate E-Bill File Id : {}", fileId);
            if (StringUtils.isNotEmpty(fileId)) {
                if (invoice.getTotalAmount().compareTo(BigDecimal.ZERO) > 0) {
                    MasterCustomerDataResponse customerInfo = customerInfoClient.getCustomerInfo(invoice.getCustomerId());

                    String lang = getCustomerLang(invoice.getCustomerId());
                    log.debug("CustomerId: {}", lang);

                    String mobile = customerInfo.getMobile();
                    log.debug("Mobile: {}", mobile);

                    String email = customerInfo.getEmail();
                    log.debug("E-mail: {}", email);

                    String ebill = (EbillLink.replaceAll("\\{fileId\\}", generateKey(fileId)));
                    log.debug("E-bill: {}", ebill);
                    String invoices = invoice.getInvoiceNo();
                    String invoiceNo = (AccrualListLink.replaceAll("\\{invoiceNo\\}", generateKeyAccrualList(invoices)));
                    log.debug("AccrualListLink: {}", invoiceNo);
                    if (sendNotificationFlag) {
                        if ("Individual".equals(customerInfo.getCustomerType()) && "NCODE00066".equals(customerInfo.getNationality())) {
                            if (StringUtils.isNotEmpty(mobile)) {
                                if (StringUtils.isNotEmpty(invoiceNo)) {
                                    grpcSendSmsAndEmailService.sendSmsVerifyToCustomer("MEMBER", "EBILL", mobile, ebill, mobile, invoiceNo, lang);
                                }
                                if (StringUtils.isNotEmpty(email)) {
                                    grpcSendSmsAndEmailService.sendEmailVerifyToCustomer("MEMBER", "EBILL", email, ebill, mobile, lang);
                                }
                            }
                        } else if ("Juristic".equals(customerInfo.getCustomerType())) {
                            if (StringUtils.isNotEmpty(mobile)) {
                                if (StringUtils.isNotEmpty(invoiceNo)) {
                                    grpcSendSmsAndEmailService.sendSmsVerifyToCustomer("MEMBER", "EBILL", mobile, ebill, mobile, invoiceNo, lang);
                                }
                                if (StringUtils.isNotEmpty(email)) {
                                    grpcSendSmsAndEmailService.sendEmailVerifyToCustomer("MEMBER", "EBILL", email, ebill, mobile, lang);
                                }
                            }
                        } else {
                            if (StringUtils.isNotEmpty(mobile)) {
                                if (StringUtils.isNotEmpty(invoiceNo)) {
                                    grpcSendSmsAndEmailService.sendSmsVerifyToCustomer("MEMBER", "EBILL", mobile, ebill, mobile, invoiceNo, lang);
                                }
                                if (StringUtils.isNotEmpty(email)) {
                                    grpcSendSmsAndEmailService.sendEmailVerifyToCustomer("MEMBER", "EBILL", email, ebill, mobile, lang);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error("invoice no -> {}, error -> {}", invoice!=null ? invoice.getInvoiceNo(): "-", e);
        }

    }

    private String getCustomerLang(String customerId) {
        return customerClient.getCustomerNotificationInfo(customerId);
    }

    private String generateKey(String fileId) {

        Map<String, Object> claims = new HashMap<>();
        claims.put("fileId", fileId);
        return fileId;
    }

    private String generateKeyAccrualList(String invoiceNo) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("invoiceNo", invoiceNo);
        return invoiceNo;
    }

}