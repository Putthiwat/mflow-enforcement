package com.appworks.co.th.batchworker.job.decryptpwd;

import com.appworks.co.th.batchworker.job.mpassftpfile.MPassFTPFileItemWriter;
import com.appworks.co.th.batchworker.oracle.entity.CustomerCard;
import com.appworks.co.th.batchworker.oracle.entity.Invoice;
import com.appworks.co.th.batchworker.oracle.entity.InvoiceStaging;
import com.appworks.co.th.batchworker.payload.InvoiceObject;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.batch.integration.partition.RemotePartitioningWorkerStepBuilderFactory;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.JpaPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.OraclePagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration(value = "decryptPwdJobConfig")
public class DecryptPwdJob {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("oracleMainDataSource")
    private DataSource dataSource;

    @Autowired
    @Qualifier("oracleMainEntityManagerFactory")
    private EntityManagerFactory entityManagerFactory;

    @Autowired
    @Qualifier("oracleMainTransactionManager")
    private PlatformTransactionManager transactionManager;

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    public  DecryptPwdWriter decryptPwdWriter;

    @Autowired
    private RemotePartitioningWorkerStepBuilderFactory workerStepBuilderFactory;

    @Autowired
    private DecryptPwdProcessor decryptPwdProcessor;

//    public RowMapper<CustomerCard> rowMapper() {
//        return (rs, rowNum) -> {
//            CustomerCard account = new CustomerCard();
//            account.setId(rs.getLong("ID"));
//            account.setMemberId(rs.getString("MEMBER_ID"));
//            account.setToken(rs.getString("TOKEN"));
//            account.setTokenOrigin(rs.getString("TOKEN_ORIGIN"));
//            account.setRetry(rs.getInt("RETRY"));
//            log.info(String.format("CustomerCard reader %s ", account));
//            return account;
//        };
//    }

//    public JdbcPagingItemReader<CustomerCard> customerCardReader() throws Exception {
//
//        HashMap<String, Order> sortKeys = new HashMap<>(2);
//        sortKeys.put("ID", Order.ASCENDING);
//
//        OraclePagingQueryProvider queryProvider = new OraclePagingQueryProvider();
//        queryProvider.setSelectClause("ID,MEMBER_ID,TOKEN,TOKEN_ORIGIN,RETRY");
//        queryProvider.setFromClause("MF_CUST_VISA_MASTER_CARD");
//        queryProvider.setWhereClause("RETRY > 0 AND RETRY <= 3 AND DELETE_FLAG = 0");
//        queryProvider.setSortKeys(sortKeys);
//
//        JdbcPagingItemReader<CustomerCard> pagingItemReader = new JdbcPagingItemReader<>();
//        pagingItemReader.setPageSize(10);
//        pagingItemReader.setDataSource(this.dataSource);
//        pagingItemReader.setRowMapper(this.rowMapper());
//        pagingItemReader.setQueryProvider(queryProvider);
//        pagingItemReader.afterPropertiesSet();
//        return pagingItemReader;
//    }

    @Bean
    @StepScope
    public JpaPagingItemReader<CustomerCard> customerCardReader(
            @Value("#{stepExecutionContext['minValue']}") Long minValue,
            @Value("#{stepExecutionContext['maxValue']}") Long maxValue,
            @Value("#{stepExecutionContext['where']}") String where
    ) throws Exception {
        log.info(" reading {} to {}" , minValue, maxValue);
        String jpqlQuery = "SELECT o from CustomerCard o " + where + " and id >= "+ minValue +" and id <= "+ maxValue;

        JpaPagingItemReader<CustomerCard> reader = new JpaPagingItemReader<>();
        reader.setQueryString(jpqlQuery);
        reader.setEntityManagerFactory(entityManagerFactory);
        reader.setPageSize(100);
        reader.afterPropertiesSet();
        reader.setSaveState(false);

        return reader;
    }

    @Bean
    public Step decryptPwdStep() throws Exception {
        return this.workerStepBuilderFactory.get("decryptPwdStep")
                .inputChannel(decryptRequests())
                .outputChannel(decryptReplies())
                .<CustomerCard, CustomerCard>chunk(100)
                .reader(customerCardReader(null,null,null))
                .processor(this.decryptPwdProcessor)
                .writer(this.decryptPwdWriter)
//                .taskExecutor(new SimpleAsyncTaskExecutor())
                .build();
    }

    /*
     * Configure inbound flow (requests coming from the master)
     */
    @Bean
    public DirectChannel decryptRequests() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow decryptInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("decrypt.requests"))
                .channel(decryptRequests())
                .get();
    }

    /*
     * Configure outbound flow (replies going to the master)
     */
    @Bean
    public DirectChannel decryptReplies() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow decryptOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(decryptReplies())
                .handle(Jms.outboundAdapter(connectionFactory).destination("decrypt.replies"))
                .get();
    }

}
