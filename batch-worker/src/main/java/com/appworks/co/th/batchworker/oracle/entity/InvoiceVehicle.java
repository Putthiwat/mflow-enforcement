package com.appworks.co.th.batchworker.oracle.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper=false)
@Table(name = "MF_INVOICE_VEHICLE",indexes = {
        @Index(name="IDX_INVOICE_VEHICLE1",columnList="PLATE2,PROVINCE"),
        @Index(name="IDX_INVOICE_VEHICLE2",columnList="PLATE1,PLATE2,PROVINCE")
        },
        uniqueConstraints=@UniqueConstraint(columnNames = {"CUSTOMER_ID","PLATE2","PROVINCE","REF_GROUP"}))
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceVehicle extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceVehicle", sequenceName = "SEQ_MF_INVOICE_VEHICLE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceVehicle")
    public Long id;

    @Column(name = "CUSTOMER_ID", columnDefinition = "VARCHAR2(25) NOT NULL")
    private String customerId;

    @Column(name = "PLATE1", columnDefinition = "VARCHAR2(255)")
    private String plate1;

    @Column(name = "PLATE2", columnDefinition = "VARCHAR2(255)")
    private String plate2;

    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(25)")
    private String province;

    @Column(name = "REF_GROUP", columnDefinition = "VARCHAR2(20) NOT NULL")
    private String refGroup;

    @Column(name = "VEHICLE_TYPE", columnDefinition = "VARCHAR2(25)")
    private String vehicleType;

}
