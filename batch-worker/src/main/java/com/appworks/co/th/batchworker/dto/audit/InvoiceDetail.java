package com.appworks.co.th.batchworker.dto.audit;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class InvoiceDetail {
    private String invoiceNo;
    private String transactionId;
    private String transactionDate;
    private String plate1;
    private String plate2;
    private Province provice;
    private Origin origin;
    private Destination destination;
    private BigDecimal feeAmount;
    private BigDecimal fineAmount;
    private BigDecimal collectionAmount;
    private BigDecimal totalAmount;
    private OriginTranType originTranType;
    private List<Evidences> evidences;
}
