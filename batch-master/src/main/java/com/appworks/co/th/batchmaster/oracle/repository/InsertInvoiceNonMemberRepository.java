package com.appworks.co.th.batchmaster.oracle.repository;


import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberColor;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberDetail;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberEvidence;

import java.util.List;

public interface InsertInvoiceNonMemberRepository {
    void insertInvoiceNonMemBerRepository(InvoiceNonMember invoiceNonMember) throws InsertInvoiceException;

    void insertInvoiceDetailNonmemberRepository(List<InvoiceNonMemberDetail> invoiceDetailList) throws InsertInvoiceException;

    void insertInvoiceNonmemberColorRepository(List<InvoiceNonMemberColor> colorsList) throws InsertInvoiceException;

    void updateInvoiceNoTypeOfNonmember(int invoiceType, String updateBy, String invoiceNo) throws InsertInvoiceException;

    void insertInvoiceNoNonmemberOld(String invoiceNo, String invoiceNoOld) throws InsertInvoiceException;

    boolean insertInvoiceNoNonmember(InvoiceNonMember invoiceNonMember,List<String> invoiceNoOlds) throws InsertInvoiceException;

    int getInvoiceDetailNonmemberRepository(InvoiceNonMemberDetail invoiceDetailList) throws InsertInvoiceException;

    void insertInvoiceNonmemberEvidenceRepository(int detailId, List<InvoiceNonMemberEvidence> evidenceList) throws InsertInvoiceException;
}
