package com.appworks.co.th.batchmaster.oracle.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import static org.apache.commons.lang3.StringUtils.leftPad;

@Repository
public class InvoiceJdbcRepository {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceJdbcRepository.class);

    @Autowired
    @Qualifier("oracleJdbcTemplate")
    JdbcTemplate jdbcTemplate;

    public Long getSeqInvoice() {
        try {
            String sql = "SELECT invoice_daily_seq.nextval FROM DUAL";
            return jdbcTemplate.queryForObject(sql, Long.class);
        } catch (Exception e) {
            logger.error("InvoiceJdbcRepository getSeq Exception : {}", e);
        }
        return null;
    }

}



