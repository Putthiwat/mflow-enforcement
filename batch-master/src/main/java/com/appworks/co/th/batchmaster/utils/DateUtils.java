package com.appworks.co.th.batchmaster.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static Date atStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        return calendar.getTime();
    }

    public static Timestamp toTimestamp(Instant instant){
        return instant != null ? Timestamp.from(instant) : null;
    }
    public static Instant DateInstantTimeBangkok(Instant data){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date d = null;
        try {
            d = input.parse(String.valueOf(Timestamp.from(data)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedTime = output.format(d);
        Instant instant = Instant.parse(formattedTime);
        return instant;
    }
}
