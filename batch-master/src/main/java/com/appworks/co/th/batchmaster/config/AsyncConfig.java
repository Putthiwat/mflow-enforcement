package com.appworks.co.th.batchmaster.config;


import com.appworks.co.th.batchmaster.config.prop.AsyncFTPProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Slf4j
@EnableAsync
@Configuration
@RequiredArgsConstructor
public class AsyncConfig implements AsyncConfigurer {


    private final AsyncFTPProperty asyncFTPProperty;

    @Bean(name = "FTPThreadPoolTaskExecutor")
    public ThreadPoolTaskExecutor asyncFTPExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setWaitForTasksToCompleteOnShutdown(true);
        executor.setCorePoolSize(asyncFTPProperty.getCorePoolSize());
        executor.setMaxPoolSize(asyncFTPProperty.getMaxPoolSize());
        executor.setQueueCapacity(asyncFTPProperty.getQueueCapacity());
        executor.setThreadNamePrefix("AsyncFTP-");
        executor.initialize();
        return executor;
    }


    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) -> {
            log.error("AsyncMethod::{}({})", method.getName(), params);
            log.error("Exception::{}", ex.getMessage());
        };
    }
}