package com.appworks.co.th.batchmaster.payload;

import lombok.Data;

import java.nio.file.Path;

@Data
public class FileResponse {
    private String name;
    private Path filePathObj;

}
