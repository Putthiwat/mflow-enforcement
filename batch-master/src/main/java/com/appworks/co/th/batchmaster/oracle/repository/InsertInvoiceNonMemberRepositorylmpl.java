package com.appworks.co.th.batchmaster.oracle.repository;


import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberColor;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberDetail;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberEvidence;
import com.appworks.co.th.batchmaster.utils.DateUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional(value = "oracleTransactionManager")
@Repository
public class InsertInvoiceNonMemberRepositorylmpl implements InsertInvoiceNonMemberRepository {

    private static final Logger logger = LoggerFactory.getLogger(InsertInvoiceNonMemberRepositorylmpl.class);

    @Autowired
    @Qualifier("oracleNamedParameterJdbcTemplate")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceNonMemBerRepository(InvoiceNonMember invoiceNonMember) throws InsertInvoiceException {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("address", invoiceNonMember.getAddress());
        paramMap.put("collectionAmount", invoiceNonMember.getCollectionAmount());
        paramMap.put("discount", invoiceNonMember.getDiscount());
        paramMap.put("dueDate", invoiceNonMember.getDueDate() != null ? new java.sql.Date(invoiceNonMember.getDueDate().getTime()) : "");
        paramMap.put("errorMessage", invoiceNonMember.getErrorMessage());
        paramMap.put("feeAmount", invoiceNonMember.getFeeAmount());
        paramMap.put("fineAmount", invoiceNonMember.getFineAmount());
        paramMap.put("fullName", invoiceNonMember.getFullName());
        paramMap.put("hqCode", invoiceNonMember.getHqCode());
        paramMap.put("invoiceNo", invoiceNonMember.getInvoiceNo());
        paramMap.put("invoiceNoRef", invoiceNonMember.getInvoiceNoRef());
        paramMap.put("invoiceType", invoiceNonMember.getInvoiceType());
        paramMap.put("issueDate", new java.sql.Date(invoiceNonMember.getIssueDate().getTime()));
        paramMap.put("paymentDate", invoiceNonMember.getPaymentDate() != null ? new java.sql.Date(invoiceNonMember.getPaymentDate().getTime()) : "");
        paramMap.put("plate1", invoiceNonMember.getPlate1());
        paramMap.put("plate2", invoiceNonMember.getPlate2());
        paramMap.put("printFlag", invoiceNonMember.getPrintFlag());
        paramMap.put("province", invoiceNonMember.getProvince());
        paramMap.put("status", invoiceNonMember.getStatus());
        paramMap.put("totalAmount", invoiceNonMember.getTotalAmount());
        paramMap.put("transactionType", invoiceNonMember.getTransactionType());
        paramMap.put("vat", invoiceNonMember.getVat());
        paramMap.put("brand", invoiceNonMember.getBrand());
        paramMap.put("eBillFileId", invoiceNonMember.getEBillFileId());
        paramMap.put("receiptFileId", invoiceNonMember.getReceiptFileId());
        paramMap.put("ref1", invoiceNonMember.getRef1());
        paramMap.put("ref2", invoiceNonMember.getRef2());
        paramMap.put("paymentChannel", "");
        paramMap.put("docType", "");
        paramMap.put("docNo", "");
        paramMap.put("vehicleCode", invoiceNonMember.getVehicleCode());
        paramMap.put("vehicleType", invoiceNonMember.getVehicleType());
        paramMap.put("tranxID", "");
        paramMap.put("refGroupInvoice", "");
        paramMap.put("receipt", "");
        paramMap.put("auditFlag", "N");
        paramMap.put("operationFee", invoiceNonMember.getOperationFee());
        sql.append(" INSERT INTO MF_INVOICE_NONMEMBER  (ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, ADDRESS, COLLECTION_AMOUNT, DISCOUNT, DUE_DATE, ERROR_MESSAGE, FEE_AMOUNT, FINE_AMOUNT, FULL_NAME, HQ_CODE, INVOICE_NO, INVOICE_NO_REF, INVOICE_TYPE, ISSUE_DATE, PAYMENT_DATE, PLATE1, PLATE2, PRINT_FLAG, PROVINCE, STATUS, TOTAL_AMOUNT, TRANSACTION_TYPE, VAT, BRAND, E_BILL_FILE_ID, RECEIPT_FILE_ID, REF_GROUP, REF2, PAYMENT_CHANNEL, DOC_TYPE, DOC_NO, VEHICLE_CODE, TRANX_ID, REF_GROUP_INVOICE, RECEIPT_NO, AUDIT_FLAG, OPERATION_FEE ,VEHICLE_TYPE)");
        sql.append(" VALUES(SEQ_MF_INVOICE_NONM.nextval,'System', 'SYSTEM', 'System', sysdate , :address ," +
                " :collectionAmount , :discount , :dueDate  , :errorMessage , :feeAmount , :fineAmount ," +
                " :fullName , :hqCode , :invoiceNo  , :invoiceNoRef , :invoiceType , :issueDate , :paymentDate , :plate1 , :plate2 ," +
                " :printFlag , :province , :status , :totalAmount , :transactionType  , :vat , :brand , :eBillFileId , :receiptFileId ," +
                " :ref1 , :ref2 , :paymentChannel ,  :docType  ,  :docNo , :vehicleCode , :tranxID  , :refGroupInvoice  , :receipt ," +
                " :auditFlag , :operationFee , :vehicleType)");
        namedParameterJdbcTemplate.update(sql.toString(), paramMap);
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public int getInvoiceDetailNonmemberRepository(InvoiceNonMemberDetail invoiceDetailList) throws InsertInvoiceException {
        String insertSql = " INSERT INTO MF_INVOICE_DETAIL_NONMEMBER\n" +
                "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE,  COLLECTION_AMOUNT, DEST_HQ_CODE, DEST_HQ_NAME, DEST_LANE_CODE, DEST_LANE_NAME, DEST_PLAZA_CODE, DEST_PLAZA_NAME, DISCOUNT, FEE_AMOUNT, FINE_AMOUNT, HQ_CODE, HQ_NAME, LANE_CODE, LANE_NAME, ORIGIN_TRAN_TYPE, PLATE1, PLATE2, PLAZA_CODE, PLAZA_NAME, PROVINCE, PROVINCE_NAME, TOTAL_AMOUNT, TRANSACTION_DATE, TRANSACTION_ID, VAT, VEHICLE_WHEEL, INVOICE_NO, RAW_FEE, OPERATION_FEE,  FEE_AMOUNT_OLD)\n" +
                "VALUES(SEQ_MF_INVOICE_NONM_DETAIL.nextval, 'System', 'SYSTEM', 'System', sysdate , :collectionAmount  , :destHqCode  , :destHqName , :destLaneCode ,  :destLaneName , :destPlazaCod , :destPlazaName , :discount , :feeAmount  , :fineAmount , :hqCode ,:hqName , :laneCode  , :laneName  , :originTranType , :plate1 , :plate2 , :plazaCode , :plazaName , :province , :provinceName , :totalAmount  , :transactionDate , :transactionId  , :vat , :vehicleWheel , :invoiceNo, :rawFee, :operationFee, :feeAmountOld)";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("collectionAmount", invoiceDetailList.getCollectionAmount())
                .addValue("destHqCode", invoiceDetailList.getDestHqCode())
                .addValue("destHqName", invoiceDetailList.getDestHqName())
                .addValue("destLaneCode", invoiceDetailList.getDestLaneCode())
                .addValue("destLaneName", invoiceDetailList.getDestLaneName())
                .addValue("destPlazaCod", invoiceDetailList.getDestPlazaCode())
                .addValue("destPlazaName", invoiceDetailList.getDestPlazaName())
                .addValue("discount", invoiceDetailList.getDiscount())
                .addValue("feeAmount", invoiceDetailList.getFeeAmount())
                .addValue("fineAmount", invoiceDetailList.getFineAmount())
                .addValue("hqCode", invoiceDetailList.getHqCode())
                .addValue("hqName", invoiceDetailList.getHqName())
                .addValue("laneCode", invoiceDetailList.getLaneCode())
                .addValue("laneName", invoiceDetailList.getLaneName())
                .addValue("originTranType", invoiceDetailList.getOriginTranType())
                .addValue("plate1", invoiceDetailList.getPlate1())
                .addValue("plate2", invoiceDetailList.getPlate2())
                .addValue("plazaCode", invoiceDetailList.getPlazaCode())
                .addValue("plazaName", invoiceDetailList.getPlazaName())
                .addValue("province", invoiceDetailList.getProvince())
                .addValue("provinceName", invoiceDetailList.getProvinceName())
                .addValue("totalAmount", invoiceDetailList.getTotalAmount())
                .addValue("transactionDate", DateUtils.toTimestamp(invoiceDetailList.getTransactionDate()))
                .addValue("transactionId", invoiceDetailList.getTransactionId())
                .addValue("vat", invoiceDetailList.getVat())
                .addValue("vehicleWheel", invoiceDetailList.getVehicleWheel())
                .addValue("invoiceNo", invoiceDetailList.getInvoice().getInvoiceNo())
                .addValue("rawFee", invoiceDetailList.getRawFee())
                .addValue("operationFee", invoiceDetailList.getOperationFee())
                .addValue("feeAmountOld",invoiceDetailList.getFeeAmountOld());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(insertSql, parameters, keyHolder, new String[]{"ID"});
        int id = keyHolder.getKey() != null ? keyHolder.getKey().intValue() : 0;
        return id;
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceDetailNonmemberRepository(List<InvoiceNonMemberDetail> invoiceDetailList) throws InsertInvoiceException {
        for (InvoiceNonMemberDetail invoiceDetail : invoiceDetailList) {
            int value = this.getInvoiceDetailNonmemberRepository(invoiceDetail);
            if (value > 0) {
                logger.info("get Id of getInvoiceDetailNonmemberRepository  :  {} ", value);
                this.insertInvoiceNonmemberEvidenceRepository(value, invoiceDetail.getEvidences());
            }
        }
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceNonmemberEvidenceRepository(int detailId, List<InvoiceNonMemberEvidence> evidenceList) throws InsertInvoiceException {
        for (InvoiceNonMemberEvidence invoiceEvidence : evidenceList) {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("file", invoiceEvidence.getFile());
            paramMap.put("transactionId", invoiceEvidence.getTransactionId());
            paramMap.put("type", invoiceEvidence.getType());
            paramMap.put("detailId", detailId);
            sql.append("INSERT INTO MF_INVOICE_EVI_NONMEMBER\n" +
                    "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, FILE_ID, TRANSACTION_ID, TYPE, INVOICE_DETAIL_ID)\n" +
                    "VALUES(SEQ_MF_INVOICE_NONM_EVI.nextval, 'System', 'SYSTEM', 'System', sysdate , :file , :transactionId , :type , :detailId ) ");
            namedParameterJdbcTemplate.update(sql.toString(), paramMap);
        }
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceNonmemberColorRepository(List<InvoiceNonMemberColor> colorsList) throws InsertInvoiceException {
        for (InvoiceNonMemberColor invoiceColor : colorsList) {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("code", invoiceColor.getCode());
            paramMap.put("invoiceNo", invoiceColor.getInvoice().getInvoiceNo());
            sql.append("INSERT INTO MF_INVOICE_NONM_VHC_COLOR\n" +
                    "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, CODE, INVOICE_NO)\n" +
                    "VALUES(SEQ_MF_INV_NONM_COLOR.nextval, 'System', 'SYSTEM', 'System', sysdate  , :code , :invoiceNo  )");
            namedParameterJdbcTemplate.update(sql.toString(), paramMap);
        }
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void updateInvoiceNoTypeOfNonmember(int invoiceType, String updateBy, String invoiceNo) throws InsertInvoiceException {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("invoiceType", invoiceType);
        paramMap.put("updateBy", updateBy);
        paramMap.put("invoiceNo", invoiceNo);
        sql.append(" update MF_INVOICE_NONMEMBER ");
        sql.append(" set INVOICE_TYPE = :invoiceType , UPDATE_BY = :updateBy ");
        sql.append(" where INVOICE_NO = :invoiceNo and  DELETE_FLAG =0 ");
        namedParameterJdbcTemplate.update(sql.toString(), paramMap);
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceNoNonmemberOld(String invoiceNo,String invoiceNoOld) throws InsertInvoiceException {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("invoice", invoiceNo);
        paramMap.put("invoiceOld", invoiceNoOld);
        sql.append("INSERT INTO MF_INVOICE_NONMEMBER_HISTORY\n" +
                "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, INVOICE_NO, INVOICE_OLD_NO)\n" +
                "VALUES(SEQ_MF_INVOICE_NONMEMBER_HISTORY.nextval, 'System', 'SYSTEM', 'System', sysdate, :invoice  , :invoiceOld )");
        namedParameterJdbcTemplate.update(sql.toString(), paramMap);
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED,rollbackFor = InsertInvoiceException.class)
    public boolean insertInvoiceNoNonmember(InvoiceNonMember invoiceNonMember,List<String> invoiceOlds) throws InsertInvoiceException {

        insertInvoiceNonMemBerRepository(invoiceNonMember);

        if (ObjectUtils.isNotEmpty(invoiceNonMember.getDetails())) {
            insertInvoiceDetailNonmemberRepository(invoiceNonMember.getDetails());
        }

        if (ObjectUtils.isNotEmpty(invoiceNonMember.getColors())) {
            insertInvoiceNonmemberColorRepository(invoiceNonMember.getColors());
        }

        if (invoiceOlds!=null && invoiceOlds.size() >0) {
            for (String invoiceNoOld : invoiceOlds){
                updateInvoiceNoTypeOfNonmember(3, "System", invoiceNoOld);
                insertInvoiceNoNonmemberOld(invoiceNonMember.getInvoiceNo(),invoiceNoOld);
            }
        }

        return true;
    }
}

