package com.appworks.co.th.batchmaster.oracle.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Entity
@ToString(exclude = {"colors","details"})
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE")
@Where(clause = "DELETE_FLAG = 0")
public class Invoice extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoice", sequenceName = "SEQ_MF_INVOICE", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "seqInvoice")
    public Long id;

    @NaturalId
    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(20)")
    private String invoiceNo;

    @Column(name = "INVOICE_NO_REF", columnDefinition = "VARCHAR2(50)")
    private String invoiceNoRef;
    @Column(name="INVOICE_TYPE", columnDefinition = "SMALLINT")
    @NotNull
    private Integer invoiceType;
    @Column(name="TRANSACTION_TYPE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String transactionType;
    @Column(name="CUSTOMER_ID", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String customerId;
    @Column(name = "PLATE1", columnDefinition = "VARCHAR2(255)")
    private String plate1;
    @Column(name = "PLATE2", columnDefinition = "VARCHAR2(255)")
    private String plate2;
    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(25)")
    private String province;
    @Column(name="FULL_NAME", columnDefinition = "VARCHAR2(500)")
    @NotNull
    private String fullName;
    @Column(name="ADDRESS", columnDefinition = "VARCHAR2(1500)")
    @NotNull
    private String address;
    @Column(name="ISSUE_DATE", columnDefinition = "DATE")
    @NotNull
    private Date issueDate;

    @Column(name="DUE_DATE", columnDefinition = "DATE")
    private Date dueDate;

    @Column(name="PAYMENT_DATE", columnDefinition = "DATE")
    private Date paymentDate ;
    @Column(name="FEE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    @NotNull
    private BigDecimal feeAmount;

    @Column(name="FINE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal fineAmount;

    @Column(name="COLLECTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    @NotNull
    private BigDecimal collectionAmount;

    @Column(name="TOTAL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    @NotNull
    private BigDecimal totalAmount;

    @Column(name="VAT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal vat;

    @Column(name = "DISCOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal discount;

    @Column(name="PRINT_FLAG", columnDefinition = "SMALLINT")
    private Integer printFlag;

    @Column(name="STATUS", columnDefinition = "VARCHAR2(25)")
    private String status;
    @Column(name="ERROR_MESSAGE", columnDefinition = "VARCHAR2(4000)")
    private String errorMessage;

    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String hqCode;

    @Column(name = "BRAND", columnDefinition = "VARCHAR2(25)")
    private String brand;

    @Column(name = "E_BILL_FILE_ID", columnDefinition = "VARCHAR2(255)")
    private String eBillFileId;

    @Column(name = "RECEIPT_FILE_ID", columnDefinition = "VARCHAR2(255)")
    private String receiptFileId;

    @Column(name = "REF_GROUP", columnDefinition = "VARCHAR2(20)")
    private String ref1;

    @Column(name = "REF2", columnDefinition = "VARCHAR2(20)")
    private String ref2;

    @Column(name = "PAYMENT_CHANNEL", columnDefinition = "VARCHAR2(25)")
    private String paymentChannel;

    @Column(name = "VEHICLE_CODE", columnDefinition = "VARCHAR2(25)")
    private String vehicleCode;

    @Column(name = "VEHICLE_TYPE", columnDefinition = "VARCHAR2(25)")
    private String vehicleType;

    @Column(name = "INVOICE_CHANNEL", columnDefinition = "VARCHAR2(50)")
    private String invoiceChannel;

    @ColumnDefault("N")
    @Column(name = "AUDIT_FLAG ", columnDefinition = "VARCHAR2(1)")
    private String auditFlag = "N";

    @Column(name="OPERATION_FEE", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal operationFee = BigDecimal.ZERO;

    @ColumnDefault("N")
    @Column(name = "NOTICE_RECEIPT ", columnDefinition = "VARCHAR2(1)")
    private String noticeReceipt = "N";

    @ColumnDefault("N")
    @Column(name = "NOTICE_EBILL ", columnDefinition = "VARCHAR2(1)")
    private String noticeEbill = "N";

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceDetail> details;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoice", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceColor> colors;

}
