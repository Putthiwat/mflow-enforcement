/*
 * Copyright 2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.appworks.co.th.batchmaster.master;


import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

//@Configuration
//@EnableBatchProcessing
//@EnableBatchIntegration
//@Import(value = {BrokerConfiguration.class})
public class MasterConfiguration {
//
//    public final Logger log = LoggerFactory.getLogger(this.getClass());
//
//    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
//
//    private static final int GRID_SIZE = 4;
//
//    private final JobBuilderFactory jobBuilderFactory;
//
//    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;
//
//
//    @Autowired
//    @Qualifier("oracleDataSource")
//    private DataSource dataSource;
//
//    public MasterConfiguration(JobBuilderFactory jobBuilderFactory,
//                               RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {
//
//        this.jobBuilderFactory = jobBuilderFactory;
//        this.masterStepBuilderFactory = masterStepBuilderFactory;
//    }
//
//    /*
//     * Configure outbound flow (requests going to workers)
//     */
//    @Bean(name = "requests")
//    public DirectChannel requests() {
//        return new DirectChannel();
//    }
//
//    @Bean(name = "outboundFlow")
//    public IntegrationFlow outboundFlow(ActiveMQConnectionFactory connectionFactory) {
//        return IntegrationFlows
//                .from(requests())
//                .handle(Jms.outboundAdapter(connectionFactory).destination("requests"))
//                .get();
//    }
//
//    /*
//     * Configure inbound flow (replies coming from workers)
//     */
//    @Bean(name = "replies")
//    public DirectChannel replies() {
//        return new DirectChannel();
//    }
//
//    @Bean(name = "inboundFlow")
//    public IntegrationFlow inboundFlow(ActiveMQConnectionFactory connectionFactory) {
//        return IntegrationFlows
//                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("replies"))
//                .channel(replies())
//                .get();
//    }




}
