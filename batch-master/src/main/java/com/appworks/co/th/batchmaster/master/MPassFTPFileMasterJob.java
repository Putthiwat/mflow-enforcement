package com.appworks.co.th.batchmaster.master;


import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;


@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class MPassFTPFileMasterJob {

    public final Logger log = LoggerFactory.getLogger(this.getClass());
    private final JobBuilderFactory jobBuilderFactory;
    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public MPassFTPFileMasterJob(JobBuilderFactory jobBuilderFactory,
                                 RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "mPassFTPFileJobRequests")
    public DirectChannel mPassFTPFileJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "mPassFTPFileJobRequestsOutboundFlow")
    public IntegrationFlow mPassFTPFileJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(mPassFTPFileJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("mPassFTPFileJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "mPassFTPFileJobReplies")
    public DirectChannel mPassFTPFileJobReplies() {

        return new DirectChannel();
    }

    @Bean(name = "mPassFTPFileJobRepliesInboundFlow")
    public IntegrationFlow mPassFTPFileJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("mPassFTPFileJobReplies"))
                .channel(mPassFTPFileJobReplies())
                .get();
    }

    public ColumnRangePartitioner mPassFTPPartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  and invoice_type in (0,1,2)  and status in (\'PAYMENT_WAITING\', \'PAYMENT_FAILED\') ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMPassFTPFile() {
        return this.masterStepBuilderFactory.get("masterStepMPassFTPFile")
                .partitioner("mPassFTPFileStep", mPassFTPPartitioner())
                .gridSize(1)
                .outputChannel(mPassFTPFileJobRequests())
                .inputChannel(mPassFTPFileJobReplies())
                .build();
    }

    @Bean
    public Job mPassFTPFileJob() {
        return this.jobBuilderFactory.get("mPassFTPFileJob")
                .incrementer(new RunIdIncrementer())
                .start(masterStepMPassFTPFile())
                .build();
    }


}
