package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class RegenerateInvoiceNonMemberJob {
    public static final Logger log = LoggerFactory.getLogger(RegenerateInvoiceNonMemberJob.class);

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;


    @Value("${confing.day.invoiceNonMember}")
    private String dayInvoiceNonMember;

    public RegenerateInvoiceNonMemberJob(JobBuilderFactory jobBuilderFactory,
                                         RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "regenerateInvoiceNonMemberJobReplies")
    public DirectChannel regenerateInvoiceNonMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceNonMemberJobRepliesInboundFlow")
    public IntegrationFlow regenerateInvoiceNonMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateInvoiceNonMemberJobReplies"))
                .channel(regenerateInvoiceNonMemberJobReplies())
                .get();
    }


    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "regenerateInvoiceNonMemberJobRequests")
    public DirectChannel regenerateInvoiceNonMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceNonMemberJobRequestsOutboundFlow")
    public IntegrationFlow regenerateInvoiceNonMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateInvoiceNonMemberJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateInvoiceNonMemberJobRequests"))
                .get();
    }


    public ColumnRangePartitioner regenerateInvoiceNonMemberPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_PROCEED invoiceProceed");
        columnRangePartitioner.setWhere("WHERE  invoiceProceed.STATE IN (\'APPROVAL_PENDING\') and invoiceProceed.COMMAND IN (\'InvoiceNonMemberCommand\') ");
        return columnRangePartitioner;
    }

    private String currentEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, -1);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return df.format(calendar.getTime());
    }

    private String currentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.HOUR, -1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return df.format(calendar.getTime());
    }

    @Bean
    public Step regenerateInvoiceNonMember() {
        return this.masterStepBuilderFactory.get("regenerateInvoiceNonMember")
                .partitioner("regenerateInvoiceNonMemberStep", regenerateInvoiceNonMemberPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(regenerateInvoiceNonMemberJobRequests())
                .inputChannel(regenerateInvoiceNonMemberJobReplies())
                .build();
    }

    @Bean
    public Job invoiceRegenerateNonMemberJob() {
        return this.jobBuilderFactory.get("invoiceRegenerateNonMemberJob")
                .incrementer(new RunIdIncrementer())
                .start(regenerateInvoiceNonMember())
                .build();
    }

}
