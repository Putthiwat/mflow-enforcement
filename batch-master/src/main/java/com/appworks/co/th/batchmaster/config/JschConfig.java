package com.appworks.co.th.batchmaster.config;

import com.jcraft.jsch.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JschConfig {
    @Value("${sftp.ktb.host}")
    private String ktbHost;

    @Value("${sftp.ktb.port}")
    private int ktbProd;

    @Value("${sftp.ktb.username}")
    private String ktbUsername;

    @Value("${sftp.ktb.password}")
    private String ktbPassword;

    public ChannelSftp setupJsch() throws IOException {
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession( ktbUsername, ktbHost, ktbProd );
            session.setConfig("StrictHostKeyChecking", "no");
            session.setPassword( ktbPassword );
            session.connect();

            Channel channel = session.openChannel("sftp");

            return (ChannelSftp) channel;
        }catch (JSchException e){
            e.printStackTrace();
            throw new IOException(e);
        }

    }
}
