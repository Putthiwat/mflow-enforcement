package com.appworks.co.th.batchmaster.service;

import com.appworks.co.th.batchmaster.payload.FileResponse;

import java.nio.file.Path;

public interface FileService {
    void writeFileBatchCardDayFile(Integer numberFile,Object record,Object amount);
    void writeFileCardBillCycle(Integer numberFile,Object record,Object amount);
    void moveFile(FileResponse fileResponse, String pathNew, String name);
    String dateFormatter();

    String seqFormat(int seq);
}
