package com.appworks.co.th.batchmaster.oracle.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_VHC_COLOR")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceColor extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceColor", sequenceName = "SEQ_MF_INVOICE_COLOR", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceColor")
    public Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(
            name = "INVOICE_NO",
            referencedColumnName = "INVOICE_NO",
            foreignKey = @ForeignKey(name = "MF_INV_INVOICE_COLOR_FK1"),
            nullable = false)
    private Invoice invoice;

    @Column(name = "CODE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String code;

}
