package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class RegenerateInvoiceMemberJob {
    public static final Logger log = LoggerFactory.getLogger(RegenerateInvoiceMemberJob.class);

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    @Value("${confing.day.invoiceMember}")
    private String dayInvoiceMember;

    public RegenerateInvoiceMemberJob(JobBuilderFactory jobBuilderFactory,
                                      RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "regenerateInvoiceMemberJobReplies")
    public DirectChannel regenerateInvoiceMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceMemberJobRepliesInboundFlow")
    public IntegrationFlow regenerateInvoiceMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateInvoiceMemberJobReplies"))
                .channel(regenerateInvoiceMemberJobReplies())
                .get();
    }


    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "regenerateInvoiceMemberJobRequests")
    public DirectChannel regenerateInvoiceMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateInvoiceMemberJobRequestsOutboundFlow")
    public IntegrationFlow regenerateInvoiceMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateInvoiceMemberJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateInvoiceMemberJobRequests"))
                .get();
    }


    public ColumnRangePartitioner regenerateInvoiceMemberPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_PROCEED invoiceProceed");
        columnRangePartitioner.setWhere("WHERE  invoiceProceed.STATE IN (\'APPROVAL_PENDING\') and invoiceProceed.COMMAND IN (\'InvoiceMemberCommand\') ");
        return columnRangePartitioner;
    }

    private String currentEndDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -1);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return df.format(calendar.getTime());
    }

    private String currentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -1);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return df.format(calendar.getTime());
    }

    @Bean
    public Step regenerateInvoiceMember() {
        return this.masterStepBuilderFactory.get("regenerateInvoiceMember")
                .partitioner("regenerateInvoiceMemberStep", regenerateInvoiceMemberPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(regenerateInvoiceMemberJobRequests())
                .inputChannel(regenerateInvoiceMemberJobReplies())
                .build();
    }

    @Bean
    public Job invoiceRegenerateMemberJob() {
        return this.jobBuilderFactory.get("invoiceRegenerateMemberJob")
                .incrementer(new RunIdIncrementer())
                .start(regenerateInvoiceMember())
                .build();
    }

}
