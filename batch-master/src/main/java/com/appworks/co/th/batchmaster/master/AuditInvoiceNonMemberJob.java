package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitionerAuditDOH;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class AuditInvoiceNonMemberJob {
    public static final Logger log = LoggerFactory.getLogger(AuditInvoiceNonMemberJob.class);

    public DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:2}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;


    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public AuditInvoiceNonMemberJob(JobBuilderFactory jobBuilderFactory,
                                 RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "auditInvoiceNonMemberJobRequests")
    public DirectChannel auditInvoiceNonMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "auditInvoiceNonMemberJobRequestsOutboundFlow")
    public IntegrationFlow auditInvoiceNonMemberJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(auditInvoiceNonMemberJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("auditInvoiceNonMemberJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "auditInvoiceNonMemberJobReplies")
    public DirectChannel auditInvoiceNonMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "auditInvoiceNonMemberJobRepliesInboundFlow")
    public IntegrationFlow auditInvoiceNonMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("auditInvoiceNonMemberJobReplies"))
                .channel(auditInvoiceNonMemberJobReplies())
                .get();
    }

    public ColumnRangePartitionerAuditDOH auditInvoiceNonMemberPartitioner()
    {
        ColumnRangePartitionerAuditDOH columnRangePartitioner = new ColumnRangePartitionerAuditDOH();
        columnRangePartitioner.setColumn("b.id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER_AUDIT_DOH a");
        columnRangePartitioner.setRightJoin("MF_INVOICE_NONMEMBER b");
        columnRangePartitioner.setOn("a.INVOICE_NO = b.INVOICE_NO");
        columnRangePartitioner.setWhere("WHERE a.AUDIT_FLAG = 'N' AND a.DELETE_FLAG = 0 AND a.CREATE_DATE < TO_DATE(TO_CHAR (SYSDATE, 'YYYY-MM-DD'),'YYYY-MM-DD')");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepInvoiceNonMemberAudit() {
        return this.masterStepBuilderFactory.get("masterStepInvoiceNonMemberAudit")
                .partitioner("auditInvoiceNonMemberStep", auditInvoiceNonMemberPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(auditInvoiceNonMemberJobRequests())
                .inputChannel(auditInvoiceNonMemberJobReplies())
                .build();
    }

    @Bean
    public Job auditInvoiceNonMemberTransactionJob() {
        return this.jobBuilderFactory.get("auditInvoiceNonMemberTransactionJob")
                .incrementer(new RunIdIncrementer())
                .start(masterStepInvoiceNonMemberAudit())
                .build();
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE,-1);
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        log.info("currentDate : {}", currentDate);
        return currentDateString;
    }
}
