package com.appworks.co.th.batchmaster.master;


import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class RegenerateEBillMemberJob {

    public static final Logger log = LoggerFactory.getLogger(RegenerateEBillMemberJob.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;


    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public RegenerateEBillMemberJob(JobBuilderFactory jobBuilderFactory,
                                    RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "regenerateEBillFileMemberJobRequests")
    public DirectChannel regenerateEBillFileMemberJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateEBillFileMemberJobRequestsOutboundFlow")
    public IntegrationFlow invoiceNonMemStagingJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(regenerateEBillFileMemberJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("regenerateEBillFileMemberJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "regenerateEBillFileMemberJobReplies")
    public DirectChannel regenerateEBillFileMemberJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "regenerateEBillFileMemberJobRepliesInboundFlow")
    public IntegrationFlow regenerateEBillFileMemberJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("regenerateEBillFileMemberJobReplies"))
                .channel(regenerateEBillFileMemberJobReplies())
                .get();
    }

    ////////////

    public ColumnRangePartitioner regenerateEbillFileStagePartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL AND INVOICE_TYPE !=3");
        return columnRangePartitioner;
    }

    @Bean
    public Step regenerateEBillMember() {
        return this.masterStepBuilderFactory.get("regenerateEBillMember")
                .partitioner("regenerateEBillMemberStep", regenerateEbillFileStagePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(regenerateEBillFileMemberJobRequests())
                .inputChannel(regenerateEBillFileMemberJobReplies())
                .build();
    }

    @Bean
    public Job invoiceRegenerateEBillMemberJob() {
        return this.jobBuilderFactory.get("invoiceRegenerateEBillMemberJob")
                .incrementer(new RunIdIncrementer())
                .start(regenerateEBillMember())
                .build();
    }
}