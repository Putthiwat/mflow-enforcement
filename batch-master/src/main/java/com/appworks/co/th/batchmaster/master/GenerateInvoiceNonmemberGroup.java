package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchmaster.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchmaster.service.InvoiceNonMemberService;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.appworks.co.th.sagaappworks.batch.generateInvoice.InvoiceObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.appworks.co.th.batchmaster.commons.Constant.generateKeyInvoiceNonmember.CREATE_INVOICE_NONMEMBER;
import static org.apache.commons.lang3.StringUtils.leftPad;

public class GenerateInvoiceNonmemberGroup implements Tasklet {
    public static final Logger log = LoggerFactory.getLogger(GenerateInvoiceNonmemberGroup.class);
    private RedisService redisService;
    private static final ObjectMapper objectMapper = new ObjectMapper();
    final DateFormat df = new SimpleDateFormat("yyMMdd");
    private InvoiceJdbcRepository invoiceJdbcRepository;
    private InvoiceNonMemberService invoiceNonMemberService;
    private Long operationFeeAmount;

    public GenerateInvoiceNonmemberGroup(RedisService mredisService, InvoiceJdbcRepository mInvoiceJdbcRepository, InvoiceNonMemberService mInvoiceNonMemberService, Long mOperationFeeAmount) {
        invoiceJdbcRepository = mInvoiceJdbcRepository;
        invoiceNonMemberService = mInvoiceNonMemberService;
        redisService = mredisService;
        operationFeeAmount = mOperationFeeAmount;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        log.info("open Generate InvoiceNonmember Group");
        try {
            String key = CREATE_INVOICE_NONMEMBER + ":" + "*" + ":" + "*";
            Set<String> keys = redisService.getKeysWithPattern(key);
            if (keys != null && keys.size() > 0) {
                for (String keyString : keys) {
                    List<Object> objects = redisService.getOpsForList(keyString);
                    if (ObjectUtils.isNotEmpty(objects) && objects.size() > 1) {
                        this.createInvoiceNonMember(objects, keyString);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error Generate InvoiceNonmember Group in master : {}", e.getMessage());
        }
        return RepeatStatus.FINISHED;
    }

    private void createInvoiceNonMember(List<Object> objects, String keyString) {
        List<String> invoiceNoOlds = new ArrayList<>();
        try {
            String invoiceNo = null;
            InvoiceNonMember invoice = new InvoiceNonMember();
            invoice.setFeeAmount(BigDecimal.ZERO);
            invoice.setFineAmount(BigDecimal.ZERO);
            invoice.setCollectionAmount(BigDecimal.ZERO);
            invoice.setTotalAmount(BigDecimal.ZERO);
            invoice.setDiscount(BigDecimal.ZERO);
            invoice.setOperationFee(BigDecimal.valueOf(operationFeeAmount));
            invoice.setVat(BigDecimal.ZERO);
            invoice.setTotalAmount(invoice.getOperationFee());

            for (int i = 0; i < objects.size(); i++) {
                InvoiceObject invoiceObject = objectMapper.convertValue(objects.get(i), InvoiceObject.class);
                if (i == 0) {
                    invoiceNo = this.createInvoice(invoiceObject);
                }
                if (StringUtils.isNotEmpty(invoiceNo)) {
                    if (StringUtils.isNotEmpty(invoiceObject.getCreateInvoiceHeader().getInvoiceRefNo())) {
                        invoiceNoOlds.add(invoiceObject.getCreateInvoiceHeader().getInvoiceRefNo());
                    }
                    invoice = invoiceNonMemberService.createNonMemberInvoice(invoiceNo, invoiceObject.getCreateInvoiceHeader(), invoiceObject.getCreateInvoiceDetail(), invoice);
                }
            }
            invoiceNonMemberService.insertInvoiceNonMember(invoice,invoiceNoOlds);
            log.info("Obj Data map invoice nonmember : {}", invoice);
        } catch (Exception e) {
            log.error("Error createInvoiceMemBer : {}", e);
        } finally {
            redisService.delete(keyString);
        }
    }

    private String createInvoice(InvoiceObject obj) {
        try {
            Long seq = invoiceJdbcRepository.getSeqInvoice();
            if (seq != null) {
                String number = leftPad(String.valueOf(seq), 9, "0");
                String memberType = StringUtils.isEmpty(obj.getCreateInvoiceHeader().getCustomerId()) ? "2" : "1";
                return "0" + memberType + obj.getCreateInvoiceHeader().getInvoiceType() + df.format(new Date()) + number;
            }
        } catch (Exception e) {
            log.error("Error createInvoiceNonmember : {}", e);
        }
        return null;
    }


}