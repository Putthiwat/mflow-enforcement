package com.appworks.co.th.batchmaster.service;


import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.Invoice;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceColor;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceDetail;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceEvidence;
import com.appworks.co.th.batchmaster.oracle.repository.InsertInvoiceMemberRepository;
import com.appworks.co.th.batchmaster.service.customerService.CustomerVehicleInfoService;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.appworks.co.th.batchmaster.commons.Constant.Status.PAYMENT_SUCCESS;
import static com.appworks.co.th.batchmaster.commons.Constant.Status.PAYMENT_WAITING;


@Slf4j
@Service
public class InvoiceMemberServiceImpl implements InvoiceMemberService {

    private static final Logger logger = LoggerFactory.getLogger(InvoiceMemberServiceImpl.class);
    final DateFormat df = new SimpleDateFormat("yyMMdd");

    public static final FastDateFormat YYYYMMDD_HHMMSS_SSS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("Asia/Bangkok"));

    @Value("${fine.invoice.member.Type.zero}")
    private Long invoiceMemberTypeZero;
    @Value("${fine.invoice.member.Type.one}")
    private Long invoiceMemberTypeOne;
    @Value("${fine.invoice.member.Type.three}")
    private Long invoiceMemberTypethree;
    @Value("${fine.time.multiply}")
    private Long fineTimeMultiply;
    @Value("${fine.fineAmount.member.multiply}")
    private Long fineAmount;
    @Value("${fine.collectionAmount.member.multiply}")
    private Long collectionAmount;
    @Value("${operation.fee.member.multiply}")
    private Long operationFeeAmount;

    @Autowired
    InsertInvoiceMemberRepository insertInvoiceByStagingRepository;

    @Autowired
    CustomerVehicleInfoService customerVehicleInfoService;

    @Override
    public Invoice setObjInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, Invoice invoice) throws InsertInvoiceException {
        try {
            Instant dueDate = null;
            Date issueDate = null;
            if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 2) {
                //issueDate = issueAddDateOfBillCycle(YYYYMMDD_HHMMSS_SSS.format(createInvoiceHeader.getIssueDate()), invoiceMemberTypeOne);
/*                if (StringUtils.isNotEmpty(createInvoiceHeader.getInvoiceRefNo())) {
                    insertInvoiceByStagingRepository.updateInvoiceNoTypeOfMember(3, createInvoiceHeader.getCreateBy(), createInvoiceHeader.getInvoiceRefNo());
                    insertInvoiceByStagingRepository.insertInvoiceNoMemberOld(invoiceNo,createInvoiceHeader.getInvoiceRefNo());
                }*/
                issueDate = this.issueDate();
                invoice.setInvoiceNoRef(createInvoiceHeader.getInvoiceRefNo());
                invoice.setCreateBy(createInvoiceHeader.getCreateBy());
                invoice.setCreateChannel(createInvoiceHeader.getChannel());
                invoice.setHqCode(createInvoiceHeader.getHqCode());
                invoice.setInvoiceNo(invoiceNo);
                invoice.setTransactionType("Member");
                invoice.setInvoiceType(Integer.valueOf(createInvoiceHeader.getInvoiceType()));
                invoice.setCustomerId(createInvoiceHeader.getCustomerId());
                invoice.setFullName(createInvoiceHeader.getFullName());
                invoice.setAddress(createInvoiceHeader.getAddress());
                invoice.setIssueDate(issueDate);
                if (dueDate != null) {
                    invoice.setDueDate(Date.from(dueDate));
                }
                invoice.setCreateChannel(createInvoiceHeader.getChannel());
                invoice.setCreateBy(createInvoiceHeader.getCreateBy());
                invoice.setStatus(PAYMENT_WAITING);
                invoice.setPlate1(createInvoiceHeader.getPlate1());
                invoice.setPlate2(createInvoiceHeader.getPlate2());
                invoice.setProvince(createInvoiceHeader.getPlateProvince());
                invoice.setBrand(customerVehicleInfoService.getBrand(createInvoiceHeader.getVehicleCode()));
                invoice.setVehicleCode(createInvoiceHeader.getVehicleCode());
                if (!createInvoiceHeader.getColor().isEmpty()) {
                    invoice = this.setInvoiceMemberColor(createInvoiceHeader, invoice);
                }
                invoice = this.setinvoiceDetails(createInvoiceHeader, createInvoiceDetail, invoice);
                log.info("sendPaymentHistory member : {} , invoice : {}", invoice.getTotalAmount(), invoice);
                if (invoice.getTotalAmount().compareTo(BigDecimal.ZERO) == 0) {
                    log.debug("Opend sendPaymentHistory member : {} , invoice : {}", invoice.getFeeAmount(), invoice.getInvoiceNo());
                    invoice.setStatus(PAYMENT_SUCCESS);
                }
            } else {
                throw new InsertInvoiceException("Error invoice No type 2");
            }
        } catch (Exception e) {
            logger.info("Error  setObjInvoice : {}",e);
            throw new InsertInvoiceException(e.getMessage());
        }
        return invoice;
    }

    @Override
    public boolean insertInvoice(Invoice invoice,List<String> invoiceNoOlds) throws InsertInvoiceException {
        return insertInvoiceByStagingRepository.insertInvoice(invoice,invoiceNoOlds);
    }

    private Invoice setInvoiceMemberColor(CreateInvoiceHeader createInvoiceHeader, Invoice invoice) {
        if (ObjectUtils.isEmpty(invoice.getColors())) {
            List<InvoiceColor> colorList = new ArrayList<>();
            for (String colorString : createInvoiceHeader.getColor()) {
                if (colorString != null && StringUtils.isNotEmpty(colorString)) {
                    InvoiceColor color = new InvoiceColor();
                    color.setInvoice(invoice);
                    color.setCode(colorString);
                    colorList.add(color);
                }
            }
            if (!colorList.isEmpty()) {
                invoice.setColors(colorList);
            }
        } else {
            for (String colorString : createInvoiceHeader.getColor()) {
                if (colorString != null && StringUtils.isNotEmpty(colorString)) {
                    InvoiceColor color = new InvoiceColor();
                    color.setInvoice(invoice);
                    color.setCode(colorString);
                    invoice.getColors().add(color);
                }
            }
        }
        return invoice;
    }


    private Invoice setinvoiceDetails(CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, Invoice invoice) {
        if (ObjectUtils.isEmpty(invoice.getDetails())) {
            List<InvoiceDetail> invoiceDetails = createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                InvoiceDetail invoiceDetail = new InvoiceDetail();
                invoiceDetail.setCollectionAmount(new BigDecimal(0));
                invoiceDetail.setOperationFee(new BigDecimal(0));
                invoiceDetail.setFineAmount(new BigDecimal(0));
                BigDecimal sumFineAmount = new BigDecimal(0);
                BigDecimal sumCollectionAmount = new BigDecimal(0);
                BigDecimal operationFee = new BigDecimal(0);
                invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
                invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
                invoiceDetail.setInvoice(invoice);
                invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
                invoiceDetail.setTransactionDate(transactionDate(createInvoiceTransaction.getTransactionDate()));
                invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
                invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
                invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
                invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
                invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
                invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());
                invoiceDetail.setDestHqCode(createInvoiceTransaction.getDestinationHqCode());
                invoiceDetail.setDestPlazaCode(createInvoiceTransaction.getDestinationPlazaCode());
                invoiceDetail.setDestLaneCode(createInvoiceTransaction.getDestinationLaneCode());
                BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());
                invoiceDetail.setRawFee(rawFee);
                invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
                invoiceDetail.setFeeAmount(rawFee);
                //invoiceDetail.setFeeAmount(rawFee.multiply(BigDecimal.valueOf(100).subtract(invoiceDetail.getDiscount()).divide(BigDecimal.valueOf(100))));
                if (StringUtils.equals("0", createInvoiceHeader.getInvoiceType())) {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceHeader.getVat()).setScale(2, RoundingMode.DOWN));
                } else {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
                }
                if(createInvoiceTransaction.getFeeAmountOld()!=null) {
                    invoiceDetail.setFeeAmountOld(BigDecimal.valueOf(createInvoiceTransaction.getFeeAmountOld()));
                }else {
                    invoiceDetail.setFeeAmountOld(BigDecimal.ZERO);
                }
                switch (Integer.valueOf(createInvoiceHeader.getInvoiceType())) {
                    case 1:
                        sumCollectionAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumCollectionAmount = sumCollectionAmount.multiply(BigDecimal.valueOf(fineAmount));
                        invoiceDetail.setCollectionAmount(sumCollectionAmount);
                        break;
                    case 2:
                        sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumCollectionAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(collectionAmount));
                        sumCollectionAmount = sumCollectionAmount.multiply(BigDecimal.valueOf(fineAmount));
                        operationFee = operationFee.add(BigDecimal.ZERO);
                        invoiceDetail.setCollectionAmount(sumCollectionAmount);
                        invoiceDetail.setOperationFee(operationFee);
                        invoiceDetail.setFineAmount(sumFineAmount);
                        break;
                }
                //  invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getOperationFee()).add(invoiceDetail.getCollectionAmount()));
                invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getOperationFee()).add(invoiceDetail.getCollectionAmount()));
                invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
                invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
                invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                    InvoiceEvidence evidence = new InvoiceEvidence();
                    evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                    evidence.setCreateChannel(createInvoiceHeader.getChannel());
                    evidence.setDetail(invoiceDetail);
                    evidence.setTransactionId(invoiceDetail.getTransactionId());
                    evidence.setFile(evd.getFile());
                    evidence.setType(evd.getType());
                    return evidence;
                }).collect(Collectors.toList()));

                invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
                invoice.setFineAmount(invoice.getFineAmount().add(invoiceDetail.getFineAmount()));
                invoice.setCollectionAmount(invoice.getCollectionAmount().add(invoiceDetail.getCollectionAmount()));
                invoice.setTotalAmount(invoice.getTotalAmount().add(invoiceDetail.getTotalAmount()));
                invoice.setOperationFee(invoice.getOperationFee().add(invoiceDetail.getOperationFee()));
                invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
                return invoiceDetail;
            }).collect(Collectors.toList());
            invoice.setDetails(invoiceDetails);
        } else {
            createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                InvoiceDetail invoiceDetail = new InvoiceDetail();
                invoiceDetail.setCollectionAmount(new BigDecimal(0));
                invoiceDetail.setOperationFee(new BigDecimal(0));
                invoiceDetail.setFineAmount(new BigDecimal(0));
                BigDecimal sumFineAmount = new BigDecimal(0);
                BigDecimal sumCollectionAmount = new BigDecimal(0);
                BigDecimal operationFee = new BigDecimal(0);
                invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
                invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
                invoiceDetail.setInvoice(invoice);
                invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
                invoiceDetail.setTransactionDate(transactionDate(createInvoiceTransaction.getTransactionDate()));
                invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
                invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
                invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
                invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
                invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
                invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());
                invoiceDetail.setDestHqCode(createInvoiceTransaction.getDestinationHqCode());
                invoiceDetail.setDestPlazaCode(createInvoiceTransaction.getDestinationPlazaCode());
                invoiceDetail.setDestLaneCode(createInvoiceTransaction.getDestinationLaneCode());
                BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());
                invoiceDetail.setRawFee(rawFee);
                invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
                //invoiceDetail.setFeeAmount(rawFee.multiply(BigDecimal.valueOf(100).subtract(invoiceDetail.getDiscount()).divide(BigDecimal.valueOf(100))));
                invoiceDetail.setFeeAmount(rawFee);
                if (StringUtils.equals("0", createInvoiceHeader.getInvoiceType())) {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceHeader.getVat()).setScale(2, RoundingMode.DOWN));
                } else {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
                }
                if(createInvoiceTransaction.getFeeAmountOld()!=null) {
                    invoiceDetail.setFeeAmountOld(BigDecimal.valueOf(createInvoiceTransaction.getFeeAmountOld()));
                }else {
                    invoiceDetail.setFeeAmountOld(BigDecimal.ZERO);
                }

                switch (Integer.valueOf(createInvoiceHeader.getInvoiceType())) {
                    case 1:
                        sumCollectionAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumCollectionAmount = sumCollectionAmount.multiply(BigDecimal.valueOf(fineAmount));
                        invoiceDetail.setCollectionAmount(sumCollectionAmount);
                        break;
                    case 2:
                        sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumCollectionAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                        sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(collectionAmount));
                        sumCollectionAmount = sumCollectionAmount.multiply(BigDecimal.valueOf(fineAmount));
                        operationFee = operationFee.add(BigDecimal.ZERO);
                        invoiceDetail.setCollectionAmount(sumCollectionAmount);
                        invoiceDetail.setOperationFee(operationFee);
                        invoiceDetail.setFineAmount(sumFineAmount);
                        break;
                }
              //  invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getOperationFee()).add(invoiceDetail.getCollectionAmount()));
                invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getOperationFee()).add(invoiceDetail.getCollectionAmount()));
                invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
                invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
                invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                    InvoiceEvidence evidence = new InvoiceEvidence();
                    evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                    evidence.setCreateChannel(createInvoiceHeader.getChannel());
                    evidence.setDetail(invoiceDetail);
                    evidence.setTransactionId(invoiceDetail.getTransactionId());
                    evidence.setFile(evd.getFile());
                    evidence.setType(evd.getType());
                    return evidence;
                }).collect(Collectors.toList()));

                invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
                invoice.setFineAmount(invoice.getFineAmount().add(invoiceDetail.getFineAmount()));
                invoice.setCollectionAmount(invoice.getCollectionAmount().add(invoiceDetail.getCollectionAmount()));
                invoice.setTotalAmount(invoice.getTotalAmount().add(invoiceDetail.getTotalAmount()));
                invoice.setOperationFee(invoice.getOperationFee().add(invoiceDetail.getOperationFee()));
                invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
                invoice.getDetails().add(invoiceDetail);
                return invoiceDetail;
            }).collect(Collectors.toList());
        }
        return invoice;
    }


    public static Instant transactionDate(String data) {
        Instant anotherInstant = null;
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        isoFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        try {
            Date dateFormat = isoFormat.parse(data);
            anotherInstant = dateFormat.toInstant();
        } catch (ParseException e) {
            e.printStackTrace();
            anotherInstant = Instant.now();
        } catch (Exception e) {
            log.error("Error Date : {} ", e);
            anotherInstant = Instant.now();
        }
        return anotherInstant;
    }


    private Date issueAddDateOfBillCycle(String issue, Long invoiceMemberTypeZero) {
        Date dateTo = null;
        try {
            dateTo = DateUtils.parseDateStrictly(issue,
                    new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});
            int newDate = 1;
            Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(dateTo);
            if (invoiceMemberTypeZero != null) {
                c.add(Calendar.DATE, Integer.valueOf(String.valueOf(invoiceMemberTypeZero)) + newDate);
            }
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            dateTo = c.getTime();
        } catch (Exception e) {
            log.error("Error issueAddDateOfBillCycle : {}", e.getMessage());
        }
        return dateTo;
    }

    private Date issueDate() {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

}
