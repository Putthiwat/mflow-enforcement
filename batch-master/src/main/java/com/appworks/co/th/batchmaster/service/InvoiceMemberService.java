package com.appworks.co.th.batchmaster.service;

import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.Invoice;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InvoiceMemberService {

    Invoice setObjInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail,  Invoice invoice) throws InsertInvoiceException;

    boolean insertInvoice(Invoice invoice, List<String> invoiceNoOld)  throws InsertInvoiceException;
}
