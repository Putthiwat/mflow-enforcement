package com.appworks.co.th.batchmaster.oracle.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_HISTORY")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceMemberHistory extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceHistory", sequenceName = "SEQ_MF_INVOICE_HISTORY", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "seqInvoiceHistory")
    public Long id;

    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(20)")
    private String invoiceNo;

    @Column(name = "INVOICE_OLD_NO", columnDefinition = "VARCHAR2(20)")
    private String invoiceOldNo;
}
