package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import com.appworks.co.th.batchmaster.service.RedisService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class EasyPassRepayMasterJob {
    public final Logger log = LoggerFactory.getLogger(this.getClass());
    private final JobBuilderFactory jobBuilderFactory;
    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    @Autowired
    private RedisService redisService;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;
    private final StepBuilderFactory steps;
    public EasyPassRepayMasterJob(JobBuilderFactory jobBuilderFactory,
                               RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory, StepBuilderFactory steps) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
        this.steps = steps;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "easyPassRepayJobRequests")
    public DirectChannel easyPassRepayJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "easyPassRepayJobRequestsOutboundFlow")
    public IntegrationFlow easyPassRepayJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(easyPassRepayJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("easyPassRepayJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "easyPassRepayJobReplies")
    public DirectChannel easyPassRepayJobReplies() {

        return new DirectChannel();
    }

    @Bean(name = "easyPassRepayJobRepliesInboundFlow")
    public IntegrationFlow easyPassRepayJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("easyPassRepayJobReplies"))
                .channel(easyPassRepayJobReplies())
                .get();
    }

    public ColumnRangePartitioner easyPassRepayPartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 and payment_channel = \'EASYPASS\' and invoice_type = 0 and status = \'PAYMENT_FAILED\' and pay_count <=2 ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepEasyPassRepay() {
        return this.masterStepBuilderFactory.get("masterStepEasyPassRepay")
                .partitioner("easyPassRepayStep", easyPassRepayPartitioner())
                .gridSize(1)
                .outputChannel(easyPassRepayJobRequests())
                .inputChannel(easyPassRepayJobReplies())
                .build();
    }

    @Bean
    public Job easyPassRepayJob() {
        return this.jobBuilderFactory.get("easyPassRepayJob")
                .incrementer(new RunIdIncrementer())
                .start(stepDeleteRadisEasyPassRepay())
                .next(masterStepEasyPassRepay())
                .build();
    }

    @Bean
    public Step stepDeleteRadisEasyPassRepay() {
        return this.steps.get("stepDeleteRadisEasyPassRepay")
                .tasklet(new DeleteRadisEasyPass(redisService))
                .build();
    }
}
