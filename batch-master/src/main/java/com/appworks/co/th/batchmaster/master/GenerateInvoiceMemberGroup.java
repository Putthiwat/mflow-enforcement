package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.oracle.entity.Invoice;
import com.appworks.co.th.batchmaster.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchmaster.oracle.repository.InsertInvoiceMemberRepository;
import com.appworks.co.th.batchmaster.service.InvoiceMemberService;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.appworks.co.th.sagaappworks.batch.generateInvoice.InvoiceObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.appworks.co.th.batchmaster.commons.Constant.generateKeyInvoiceNonmember.CREATE_INVOICE_MEMBER;
import static org.apache.commons.lang3.StringUtils.leftPad;

@AllArgsConstructor
public class GenerateInvoiceMemberGroup implements Tasklet {
    public static final Logger log = LoggerFactory.getLogger(GenerateInvoiceMemberGroup.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();
    final DateFormat df = new SimpleDateFormat("yyMMdd");
    private InvoiceJdbcRepository invoiceJdbcRepository;
    private RedisService redisService;
    private InvoiceMemberService invoiceMemberService;
    private InsertInvoiceMemberRepository insertInvoiceByStagingRepository;
    private Long operationFeeAmount;

    public GenerateInvoiceMemberGroup(RedisService mredisService, InvoiceJdbcRepository mInvoiceJdbcRepository, InvoiceMemberService mInvoiceMemberService, InsertInvoiceMemberRepository mInsertInvoiceByStagingRepository, Long mOperationFeeAmount) {
        invoiceJdbcRepository = mInvoiceJdbcRepository;
        redisService = mredisService;
        invoiceMemberService = mInvoiceMemberService;
        insertInvoiceByStagingRepository = mInsertInvoiceByStagingRepository;
        operationFeeAmount = mOperationFeeAmount;
    }

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        log.info("open Generate InvoiceMember Group");
        try {
            String key = CREATE_INVOICE_MEMBER + ":" + "*" + ":" + "*";
            Set<String> keys = redisService.getKeysWithPattern(key);
            if (keys != null && keys.size() > 0) {
                for (String keyString : keys) {
                    List<Object> objects = redisService.getOpsForList(keyString);
                    if (ObjectUtils.isNotEmpty(objects) && objects.size() > 1) {
                        this.createInvoiceMember(objects, keyString);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error Generate InvoiceMember Group in master : {}", e);
        }
        return RepeatStatus.FINISHED;
    }


    private void createInvoiceMember(List<Object> objects, String keyString) {
        List<String> invoiceNoOlds =new ArrayList<>();
        try {
            String invoiceNo = null;
            Invoice invoice = new Invoice();
            invoice.setFeeAmount(BigDecimal.ZERO);
            invoice.setFineAmount(BigDecimal.ZERO);
            invoice.setCollectionAmount(BigDecimal.ZERO);
            invoice.setDiscount(BigDecimal.ZERO);
            invoice.setOperationFee(BigDecimal.valueOf(operationFeeAmount));
            invoice.setVat(BigDecimal.ZERO);
            invoice.setTotalAmount(invoice.getOperationFee());
            for (int i = 0; i < objects.size(); i++) {
                InvoiceObject invoiceObject = objectMapper.convertValue(objects.get(i), InvoiceObject.class);
                if (i == 0) {
                    invoiceNo = this.createInvoice(invoiceObject);
                }
                if (StringUtils.isNotEmpty(invoiceNo)) {
                    if (StringUtils.isNotEmpty(invoiceObject.getCreateInvoiceHeader().getInvoiceRefNo())){
                        invoiceNoOlds.add(invoiceObject.getCreateInvoiceHeader().getInvoiceRefNo());
                    }
                    invoice = invoiceMemberService.setObjInvoice(invoiceNo, invoiceObject.getCreateInvoiceHeader(), invoiceObject.getCreateInvoiceDetail(), invoice);
                }
            }
            invoiceMemberService.insertInvoice(invoice,invoiceNoOlds);
            log.info("Obj Data map invoice member : {}", invoice);
        } catch (Exception e) {
            log.error("Error createInvoiceMemBer : {}", e);
        }finally {
            redisService.delete(keyString);
        }
    }


    private String createInvoice(InvoiceObject obj) {
        try {
            Long seq = invoiceJdbcRepository.getSeqInvoice();
            if (seq != null) {
                String number = leftPad(String.valueOf(seq), 9, "0");
                String memberType = StringUtils.isEmpty(obj.getCreateInvoiceHeader().getCustomerId()) ? "2" : "1";
                return "0" + memberType + obj.getCreateInvoiceHeader().getInvoiceType() + df.format(new Date()) + number;
            }
        } catch (Exception e) {
            log.error("Error createInvoiceNonmember : {}", e);
        }
        return null;
    }

}
