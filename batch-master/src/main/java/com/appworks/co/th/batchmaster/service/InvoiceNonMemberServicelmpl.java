package com.appworks.co.th.batchmaster.service;

import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberColor;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberDetail;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberEvidence;
import com.appworks.co.th.batchmaster.oracle.repository.InsertInvoiceNonMemberRepository;
import com.appworks.co.th.sagaappworks.batch.*;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.appworks.co.th.batchmaster.commons.Constant.Status.PAYMENT_SUCCESS;
import static com.appworks.co.th.batchmaster.commons.Constant.Status.PAYMENT_WAITING;

@Slf4j
@Service(value = "invoiceNonMemberService")
public class InvoiceNonMemberServicelmpl implements InvoiceNonMemberService {
    public static final FastDateFormat YYYYMMDD_HHMMSS_SSS = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", TimeZone.getTimeZone("Asia/Bangkok"));

    @Value("${fine.invoice.nonMember.Type.zero}")
    private Long invoiceMemberTypeZero;
    @Value("${fine.invoice.nonMember.Type.one}")
    private Long invoiceMemberTypeOne;
    @Value("${fine.invoice.nonMember.Type.three}")
    private Long invoiceMemberTypethree;
    @Value("${fine.time.multiply}")
    private Long fineTimeMultiply;
    @Value("${fine.fineAmount.nonMember.multiply}")
    private Long fineAmount;

    @Value("${operation.fee.nonMember.multiply}")
    private Long operationFeeAmount;

    @Autowired
    InsertInvoiceNonMemberRepository insertInvoiceNonMemBerRepository;

    @Override
    public InvoiceNonMember createNonMemberInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, InvoiceNonMember invoice) throws InsertInvoiceException {
        try {
            Date issueDate = null;
            Instant dueDate = null;
            if (Integer.valueOf(createInvoiceHeader.getInvoiceType()) == 2) {
              //  issueDate = issueAddDate(createInvoiceDetail, invoiceMemberTypethree);
               // issueDate = issueAddDate(YYYYMMDD_HHMMSS_SSS.format(createInvoiceHeader.getIssueDate()), invoiceMemberTypeOne);
                /*if (StringUtils.isNotEmpty(createInvoiceHeader.getInvoiceRefNo())) {
                    insertInvoiceNonMemBerRepository.updateInvoiceNoTypeOfNonmember(3, createInvoiceHeader.getCreateBy(), createInvoiceHeader.getInvoiceRefNo());
                    insertInvoiceNonMemBerRepository.insertInvoiceNoNonmemberOld(invoiceNo,createInvoiceHeader.getInvoiceRefNo());
                }*/
                issueDate = this.issueDate();
                invoice.setInvoiceNo(invoiceNo);
                invoice.setInvoiceNoRef(createInvoiceHeader.getInvoiceRefNo());
                invoice.setCreateBy(createInvoiceHeader.getCreateBy());
                invoice.setCreateChannel(createInvoiceHeader.getChannel());
                invoice.setHqCode(createInvoiceHeader.getHqCode());
                invoice.setTransactionType("Non Member");
                invoice.setInvoiceType(Integer.valueOf(createInvoiceHeader.getInvoiceType()));
                invoice.setFullName(createInvoiceHeader.getFullName());
                invoice.setDocNo(createInvoiceHeader.getDocNo());
                invoice.setDocType(createInvoiceHeader.getDocType());
                invoice.setAddress(createInvoiceHeader.getAddress());
                invoice.setIssueDate(issueDate);
                if (dueDate != null) {
                    invoice.setDueDate(Date.from(dueDate));
                }
                invoice.setCreateChannel(createInvoiceHeader.getChannel());
                invoice.setCreateBy(createInvoiceHeader.getCreateBy());
                invoice.setStatus(PAYMENT_WAITING);
                invoice.setPlate1(createInvoiceHeader.getPlate1());
                invoice.setPlate2(createInvoiceHeader.getPlate2());
                invoice.setProvince(createInvoiceHeader.getPlateProvince());
                invoice.setBrand(createInvoiceHeader.getBrand());
                invoice.setVehicleCode(createInvoiceHeader.getVehicleCode());
                invoice.setVehicleType(createInvoiceHeader.getVehicleTypeCode());
                if (!createInvoiceHeader.getColor().isEmpty()) {
                    invoice = this.setInvoiceNonMemberColor(createInvoiceHeader, invoice);
                }
                invoice = this.setinvoiceDetails(createInvoiceHeader, createInvoiceDetail, invoice);
                log.debug("sendPaymentHistory nonmember: " + invoice.getFeeAmount());
                if (invoice.getTotalAmount().compareTo(BigDecimal.ZERO) == 0) {
                    log.debug("opend sendPaymentHistory nonmember: " + invoice.getFeeAmount());
                    invoice.setStatus(PAYMENT_SUCCESS);
                }
            } else {
                throw new InsertInvoiceException("Error invoice No type 2");
            }
        } catch (InsertInvoiceException e) {
            throw new InsertInvoiceException(e.getMessage());
        }
        return invoice;
    }

    private InvoiceNonMember setInvoiceNonMemberColor(CreateInvoiceHeader createInvoiceHeader, InvoiceNonMember invoice) {
        if (ObjectUtils.isEmpty(invoice.getColors())) {
            List<InvoiceNonMemberColor> colorList = new ArrayList<>();
            for (String colorString : createInvoiceHeader.getColor()) {
                if (colorString != null && StringUtils.isNotEmpty(colorString)) {
                    InvoiceNonMemberColor color = new InvoiceNonMemberColor();
                    color.setInvoice(invoice);
                    color.setCode(colorString);
                    colorList.add(color);
                }
            }
            if (!colorList.isEmpty()) {
                invoice.setColors(colorList);
            }
        } else {
            for (String colorString : createInvoiceHeader.getColor()) {
                if (colorString != null && StringUtils.isNotEmpty(colorString)) {
                    InvoiceNonMemberColor color = new InvoiceNonMemberColor();
                    color.setInvoice(invoice);
                    color.setCode(colorString);
                    invoice.getColors().add(color);
                }
            }
        }
        return invoice;
    }


    private InvoiceNonMember setinvoiceDetails(CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, InvoiceNonMember invoice) {
        if (ObjectUtils.isEmpty(invoice.getDetails())) {
            List<InvoiceNonMemberDetail> invoiceDetails = createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                InvoiceNonMemberDetail invoiceDetail = new InvoiceNonMemberDetail();
                invoiceDetail.setCollectionAmount(BigDecimal.valueOf(0));
                invoiceDetail.setFineAmount(BigDecimal.valueOf(0));
                invoiceDetail.setOperationFee(new BigDecimal(0));
                BigDecimal sumFineAmount = new BigDecimal(0);
                BigDecimal operationFee = new BigDecimal(0);
                invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
                invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
                invoiceDetail.setInvoice(invoice);
                invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
                invoiceDetail.setTransactionDate(this.transactionDate(createInvoiceTransaction.getTransactionDate()));
                invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
                invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
                invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
                invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
                invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
                invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());
                BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());
                invoiceDetail.setRawFee(rawFee);
                invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
              //  invoiceDetail.setFeeAmount(rawFee.multiply(BigDecimal.valueOf(100).subtract(invoiceDetail.getDiscount()).divide(BigDecimal.valueOf(100))));
                invoiceDetail.setFeeAmount(rawFee);
                if (StringUtils.equals("0", createInvoiceHeader.getInvoiceType())) {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceHeader.getVat()).setScale(2, RoundingMode.DOWN));
                } else {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
                }
                if(createInvoiceTransaction.getFeeAmountOld()!=null) {
                    invoiceDetail.setFeeAmountOld(BigDecimal.valueOf(createInvoiceTransaction.getFeeAmountOld()));
                }else {
                    invoiceDetail.setFeeAmountOld(BigDecimal.ZERO);
                }
                switch (Integer.valueOf(createInvoiceHeader.getInvoiceType())) {
                    case 1:
                        if(invoiceDetail.getFeeAmount().compareTo(BigDecimal.ZERO)==1){
                            sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                            sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(fineAmount));
                        }
                        invoiceDetail.setFineAmount(sumFineAmount);
                        break;
                    case 2:
                        if(invoiceDetail.getFeeAmount().compareTo(BigDecimal.ZERO)==1){
                            sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                            sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(fineAmount));
                            operationFee = operationFee.add(BigDecimal.ZERO);
                        }
                        invoiceDetail.setFineAmount(sumFineAmount);
                        invoiceDetail.setOperationFee(operationFee);
                        break;
                }
//                invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount()).add(invoiceDetail.getOperationFee()));
                invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount()).add(invoiceDetail.getOperationFee()));
                invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
                invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
                invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                    InvoiceNonMemberEvidence evidence = new InvoiceNonMemberEvidence();
                    evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                    evidence.setCreateChannel(createInvoiceHeader.getChannel());
                    evidence.setDetail(invoiceDetail);
                    evidence.setTransactionId(invoiceDetail.getTransactionId());
                    evidence.setFile(evd.getFile());
                    evidence.setType(evd.getType());
                    return evidence;
                }).collect(Collectors.toList()));

                invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
                invoice.setFineAmount(invoice.getFineAmount().add(invoiceDetail.getFineAmount()));
                invoice.setCollectionAmount(invoice.getCollectionAmount().add(invoiceDetail.getCollectionAmount()));
                invoice.setTotalAmount(invoice.getTotalAmount().add(invoiceDetail.getTotalAmount()));
                invoice.setOperationFee(invoice.getOperationFee().add(invoiceDetail.getOperationFee()));
                invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
                InvoiceInfo invoiceInfo = new InvoiceInfo();
                invoiceInfo.setId(createInvoiceTransaction.getId());
                invoiceInfo.setInvoiceNo(invoice.getInvoiceNo());
                return invoiceDetail;
            }).collect(Collectors.toList());
            invoice.setDetails(invoiceDetails);
        } else {
            createInvoiceDetail.getDetails().stream().map(createInvoiceTransaction -> {
                InvoiceNonMemberDetail invoiceDetail = new InvoiceNonMemberDetail();
                invoiceDetail.setCollectionAmount(BigDecimal.valueOf(0));
                invoiceDetail.setFineAmount(BigDecimal.valueOf(0));
                invoiceDetail.setOperationFee(new BigDecimal(0));
                BigDecimal sumFineAmount = new BigDecimal(0);
                BigDecimal operationFee = new BigDecimal(0);
                invoiceDetail.setCreateBy(createInvoiceHeader.getCreateBy());
                invoiceDetail.setCreateChannel(createInvoiceHeader.getChannel());
                invoiceDetail.setInvoice(invoice);
                invoiceDetail.setTransactionId(createInvoiceTransaction.getId());
                invoiceDetail.setTransactionDate(this.transactionDate(createInvoiceTransaction.getTransactionDate()));
                invoiceDetail.setPlate1(createInvoiceTransaction.getPlate1());
                invoiceDetail.setPlate2(createInvoiceTransaction.getPlate2());
                invoiceDetail.setProvince(createInvoiceTransaction.getProvince());
                invoiceDetail.setHqCode(createInvoiceTransaction.getHqCode());
                invoiceDetail.setPlazaCode(createInvoiceTransaction.getPlazaCode());
                invoiceDetail.setLaneCode(createInvoiceTransaction.getLaneCode());
                BigDecimal rawFee = BigDecimal.valueOf(createInvoiceTransaction.getFeeAmount());
                invoiceDetail.setRawFee(rawFee);
                invoiceDetail.setDiscount(BigDecimal.valueOf(createInvoiceTransaction.getDiscount()));
                //invoiceDetail.setFeeAmount(rawFee.multiply(BigDecimal.valueOf(100).subtract(invoiceDetail.getDiscount()).divide(BigDecimal.valueOf(100))));
                invoiceDetail.setFeeAmount(rawFee);
                if (StringUtils.equals("0", createInvoiceHeader.getInvoiceType())) {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceHeader.getVat()).setScale(2, RoundingMode.DOWN));
                } else {
                    invoiceDetail.setVat(BigDecimal.valueOf(createInvoiceTransaction.getVat()).setScale(2, RoundingMode.DOWN));
                }
                if(createInvoiceTransaction.getFeeAmountOld()!=null) {
                    invoiceDetail.setFeeAmountOld(BigDecimal.valueOf(createInvoiceTransaction.getFeeAmountOld()));
                }else {
                    invoiceDetail.setFeeAmountOld(BigDecimal.ZERO);
                }
                switch (Integer.valueOf(createInvoiceHeader.getInvoiceType())) {
                    case 1:
                        if(invoiceDetail.getFeeAmount().compareTo(BigDecimal.ZERO)==1){
                            sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                            sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(fineAmount));
                        }
                        invoiceDetail.setFineAmount(sumFineAmount);
                        break;
                    case 2:
                        if(invoiceDetail.getFeeAmount().compareTo(BigDecimal.ZERO)==1){
                            sumFineAmount = new BigDecimal(String.valueOf(invoiceDetail.getFeeAmountOld()));
                            sumFineAmount = sumFineAmount.multiply(BigDecimal.valueOf(fineAmount));
                            operationFee = operationFee.add(BigDecimal.ZERO);
                        }
                        invoiceDetail.setFineAmount(sumFineAmount);
                        invoiceDetail.setOperationFee(operationFee);
                        break;
                }
//                invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount()).add(invoiceDetail.getOperationFee()));
                invoiceDetail.setTotalAmount(invoiceDetail.getFeeAmount().add(invoiceDetail.getFineAmount()).add(invoiceDetail.getCollectionAmount()).add(invoiceDetail.getOperationFee()));
                invoiceDetail.setVehicleWheel(createInvoiceTransaction.getVehicleWheel());
                invoiceDetail.setOriginTranType(createInvoiceTransaction.getOriginTranType());
                invoiceDetail.setEvidences(createInvoiceTransaction.getEvidences().stream().map(evd -> {
                    InvoiceNonMemberEvidence evidence = new InvoiceNonMemberEvidence();
                    evidence.setCreateBy(createInvoiceHeader.getCreateBy());
                    evidence.setCreateChannel(createInvoiceHeader.getChannel());
                    evidence.setDetail(invoiceDetail);
                    evidence.setTransactionId(invoiceDetail.getTransactionId());
                    evidence.setFile(evd.getFile());
                    evidence.setType(evd.getType());
                    return evidence;
                }).collect(Collectors.toList()));

                invoice.setFeeAmount(invoice.getFeeAmount().add(invoiceDetail.getFeeAmount()));
                invoice.setFineAmount(invoice.getFineAmount().add(invoiceDetail.getFineAmount()));
                invoice.setCollectionAmount(invoice.getCollectionAmount().add(invoiceDetail.getCollectionAmount()));
                invoice.setTotalAmount(invoice.getTotalAmount().add(invoiceDetail.getTotalAmount()));
                invoice.setOperationFee(invoice.getOperationFee().add(invoiceDetail.getOperationFee()));
                invoice.setVat(invoice.getVat().add(BigDecimal.ZERO));
                invoice.getDetails().add(invoiceDetail);
                return invoiceDetail;
            }).collect(Collectors.toList());
        }
        return invoice;
    }

    @Override
    public boolean insertInvoiceNonMember(InvoiceNonMember invoiceNonMember,List<String> invoiceOlds) throws InsertInvoiceException {
        return insertInvoiceNonMemBerRepository.insertInvoiceNoNonmember(invoiceNonMember,invoiceOlds);
    }

    private Date issueDate() {
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(new Date());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Instant transactionDate(String data) {
        Instant anotherInstant = null;
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        isoFormat.setTimeZone(TimeZone.getTimeZone("Asia/Bangkok"));
        try {
            Date dateFormat = isoFormat.parse(data);
            anotherInstant = dateFormat.toInstant();
        } catch (ParseException e) {
            e.printStackTrace();
            anotherInstant = Instant.now();
        } catch (Exception e) {
            log.error("Error Date : {} ", e);
            anotherInstant = Instant.now();
        }
        return anotherInstant;
    }

    private Date issueAddDate(String issue, Long invoiceMemberTypeZero) {
        Date dateTo = null;
        try {
            dateTo = DateUtils.parseDateStrictly(issue,
                    new String[]{"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", "yyyy-MM-dd'T'HH:mm:ss'Z'"});
            int newDate = 1;
            Calendar c = Calendar.getInstance();
            c.setLenient(false);
            c.setTime(dateTo);
            if (invoiceMemberTypeZero != null) {
                c.add(Calendar.DATE, Integer.valueOf(String.valueOf(invoiceMemberTypeZero)) + newDate);
            }
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            dateTo = c.getTime();
        } catch (Exception e) {
            log.error("Error issueAddDateOfBillCycle : {}", e.getMessage());
        }
        return dateTo;
    }

    private ResultInvoiceDetail mapToInvoiceData(InvoiceNonMember invoice) {
        ResultInvoiceDetail resultInvoiceDetail = new ResultInvoiceDetail();

        InvoiceHeader header = new InvoiceHeader();


        header.setInvoiceNo(invoice.getInvoiceNo());
        header.setInvoiceNoRef(invoice.getInvoiceNoRef());
        header.setInvoiceType(String.valueOf(invoice.getInvoiceType()));
        // ==== customer
//        header.setCustomerId(invoice.getCustomerId());
        header.setCustomerType("Non Member");
        header.setFullName(invoice.getFullName());
        header.setAddress(invoice.getAddress());

        // ==== date time
        header.setIssueDate(invoice.getIssueDate());
        header.setDueDate(invoice.getDueDate());

        header.setHqCode(invoice.getHqCode());
        // ====
        header.setFeeAmount(invoice.getFeeAmount().doubleValue());
        header.setFineAmount(invoice.getFineAmount().doubleValue());
        header.setCollectionAmount(invoice.getCollectionAmount().doubleValue());
        header.setTotalAmount(invoice.getTotalAmount().doubleValue());
        header.setVat(invoice.getVat().doubleValue());
        header.setOperationFee(invoice.getOperationFee().doubleValue());
        header.setPrintFlag(invoice.getPrintFlag());
        header.setStatus(invoice.getStatus());

        resultInvoiceDetail.setInvoiceHeader(header);

        resultInvoiceDetail.setInvoiceTransactions(invoice.getDetails().stream().map(detail -> {

            InvoiceTransaction tran = new InvoiceTransaction();

            tran.setId(detail.getTransactionId());
            tran.setTransactionDate(detail.getTransactionDate().toString());
            tran.setPlate1(detail.getPlate1());
            tran.setPlate2(detail.getPlate2());
            tran.setProvince(detail.getProvince());
            tran.setInvoiceNo(invoice.getInvoiceNo());

            tran.setHqCode(detail.getHqCode());
            tran.setPlazaCode(detail.getPlazaCode());
            tran.setLaneCode(detail.getLaneCode());
            tran.setDestinationHqCode(detail.getDestHqCode());
            tran.setDestinationPlazaCode(detail.getDestPlazaCode());
            tran.setDestinationLaneCode(detail.getDestLaneCode());
            tran.setFeeAmount(detail.getFeeAmount().doubleValue());
            tran.setFineAmount(detail.getFineAmount().doubleValue());
            tran.setCollectionAmount(detail.getCollectionAmount().doubleValue());
            tran.setOperationFee(detail.getOperationFee().doubleValue());
            tran.setTotalAmount(detail.getTotalAmount().doubleValue());
            tran.setVat(detail.getVat().doubleValue());
            tran.setEvidences(detail.getEvidences().stream().map(invoiceEvidence -> {
                InvoiceEvidence createEnforcementEvidence = new InvoiceEvidence();
                createEnforcementEvidence.setTransactionId(invoiceEvidence.getTransactionId());
                createEnforcementEvidence.setType(invoiceEvidence.getType());
                createEnforcementEvidence.setFile(invoiceEvidence.getFile());
                return createEnforcementEvidence;
            }).collect(Collectors.toList()));

            tran.setOriginTranType(detail.getOriginTranType());
            tran.setVehicleWheel(detail.getVehicleWheel());

            return tran;
        }).collect(Collectors.toList()));

        return resultInvoiceDetail;
    }

}
