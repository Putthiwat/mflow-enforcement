package com.appworks.co.th.batchmaster.master;


import com.appworks.co.th.batchmaster.config.JschConfig;
import com.appworks.co.th.batchmaster.payload.FileResponse;
import com.appworks.co.th.batchmaster.service.FileService;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import org.apache.commons.lang3.ObjectUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.*;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.*;


@DisallowConcurrentExecution

public class ScheduledMoveFileTasks implements Job {
    final String newLine = "\r\n";
    public static final Logger log = LoggerFactory.getLogger(ScheduledMoveFileTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

    @Autowired
    JschConfig jschConfig;

    @Autowired
    private RedisService redisService;

    @Value("${card.path.tmp}")
    private String tmpPath;

    @Value("${card.path.main}")
    private String mainPath;

    @Value("${account.path.tmp}")
    private String accountTmpPath;

    @Value("${account.path.main}")
    private String accountMainPath;

    @Value("${mpass.path.tmp}")
    private String mpassTmpPath;

    @Value("${mpass.path.main}")
    private String mpassMainPath;

    @Value("${easypass.path.tmp}")
    private String easypassTmpPath;

    @Value("${easypass.path.main}")
    private String easypassMainPath;

    @Value("${doh.merchant.id}")
    private String dohMerchantId;

    @Value("${card.pattern}")
    private String pattern;

    @Value("${card.empty.path}")
    private String cardEmptyPath;

    @Value("${sftp.ktb.remoteDir}")
    private String sftpKtbRemoteDir;

    @Value("#{'${day.bill.cycle}'.split(',')}")
    private List<Integer> dayOfBillCycle;

    @Value("${account.limit.time.out}")
    private int limitTimeOut;

    @Autowired
    private FileService fileService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        log.info("open createEmptyFile");
        //Account
        this.account();
        //Mpass
        this.mpass();
        //Easypass
        this.easypass();
        //create montly file
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        log.debug("createEmptyFile Date : {} ,DAY_OF_MONTH: {} ", date, c.get(Calendar.DAY_OF_MONTH));
        if (c.get(Calendar.DAY_OF_MONTH) == findMin(dayOfBillCycle)) {
            this.cradOfMonthly();
        }
        //Card
        this.cradOfDay();

    }


    public Integer findMin(List<Integer> list) {
        return list.stream()                        // Stream<Integer>
                .min(Comparator.naturalOrder()) // Optional<Integer>
                .get();                         // Integer
    }


    //================================================================MPASS==========================================================
    private void mpass() {
        try {
            String keyAccount = BATCH_MPASS + ":" + fileService.dateFormatter();
            Object keyObjAccount = redisService.get(keyAccount);
            if (keyObjAccount != null) {
                moveFileMPASSNfs();
            }

        } catch (Exception e) {
            log.error("Error move File mpass : {} ", e.getMessage());
        }
    }

    private void moveFileMPASSNfs() {
        try {
            String[] pathnames;
            // Creates a new File instance by converting the given pathname string
            // into an abstract pathname
            File f = new File(mpassTmpPath);
            // Populates the array with names of files and directories
            pathnames = f.list();
            // For each pathname in the pathnames array
            for (String pathname : pathnames) {
                // Print the names of files and directories
                if (pathname.endsWith(".mf")) {
                    FileResponse fileResponse = new FileResponse();
                    fileResponse.setName(pathname);
                    fileResponse.setFilePathObj(Paths.get(mpassTmpPath + pathname));
                    fileService.moveFile(fileResponse, mpassMainPath, "Mpass");
                }
            }
        } catch (Exception e) {
            log.error("Error move File mpass : {} ", e.getMessage());
        }
    }

    //================================================================EASYPASS==========================================================
    private void easypass() {
        try {
            String keyAccount = BATCH_EASYPASS + ":" + fileService.dateFormatter();
            Object keyObjAccount = redisService.get(keyAccount);
            if (keyObjAccount != null) {
                moveFileEASYPASSNfs();
            }

        } catch (Exception e) {
            log.error("Error move File easypass : {} ", e.getMessage());
        }
    }

    private void moveFileEASYPASSNfs() {
        try {
            String[] pathnames;
            // Creates a new File instance by converting the given pathname string
            // into an abstract pathname
            File f = new File(easypassTmpPath);
            // Populates the array with names of files and directories
            pathnames = f.list();
            // For each pathname in the pathnames array
            for (String pathname : pathnames) {
                // Print the names of files and directories
                if (pathname.endsWith(".mf")) {
                    FileResponse fileResponse = new FileResponse();
                    fileResponse.setName(pathname);
                    fileResponse.setFilePathObj(Paths.get(easypassTmpPath + pathname));
                    fileService.moveFile(fileResponse, easypassMainPath, "Easypass");
                }
            }
        } catch (Exception e) {
            log.error("Error move File easypass : {} ", e.getMessage());
        }
    }

    //================================================================account==========================================================
    private void account() {
        Object ObjTotalRecordAccount = null;
        try {
            String keyAccount = BATCH_ACCOUNT + ":" + fileService.dateFormatter();
            Object keyObjAccount = redisService.get(keyAccount);
            if (ObjectUtils.isNotEmpty(keyObjAccount)) {
                moveFileAccountNfs();
            }
        } catch (Exception e) {
            log.error("Error move File Account : {} ", e.getMessage());
        }
    }

    private void moveFileAccountNfs() {
        try {
            String[] pathnames;
            // Creates a new File instance by converting the given pathname string
            // into an abstract pathname
            File f = new File(accountTmpPath);
            // Populates the array with names of files and directories
            pathnames = f.list();
            // For each pathname in the pathnames array
            for (String pathname : pathnames) {
                // Print the names of files and directories
                if (pathname.endsWith(".txt")) {
                    FileResponse fileResponse = new FileResponse();
                    fileResponse.setName(pathname);
                    fileResponse.setFilePathObj(Paths.get(accountTmpPath + pathname));
                    fileService.moveFile(fileResponse, accountMainPath, "Account");
                }
            }
        } catch (Exception e) {
            log.error("Error move File Account : {} ", e.getMessage());
        }
    }


    //================================================================cradOfDay==========================================================
    private void cradOfDay() {
        Object ObjTotalRecordCard = null;
        int numberFile = 1;
        try {
            String keyCard = BATCH_CARD + ":" + fileService.dateFormatter();
            String keyTotalFileCard = TOTAL_FILE_CARD + ":" + fileService.dateFormatter();
            Object keyObjCard = redisService.get(keyCard);
            Object keyTotalFiled = redisService.get(keyTotalFileCard);
            if (ObjectUtils.isNotEmpty(keyTotalFiled)) {
                numberFile = ((Integer) keyTotalFiled).intValue();
            }
            if (ObjectUtils.isNotEmpty(keyObjCard)) {
                for (int i = numberFile; i <= ((Integer) keyObjCard).intValue(); i++) {
                    String keyRecord = TOTAL_RECORD_CARD + ":" + fileService.dateFormatter() + ":" + i;
                    String keyAmount = TOTAL_AMOUNT_CARD + ":" + fileService.dateFormatter() + ":" + i;
                    Object record = redisService.get(keyRecord);
                    Object amount = redisService.get(keyAmount);
                    if (ObjectUtils.isNotEmpty(record) || ObjectUtils.isNotEmpty(amount)) {
                        fileService.writeFileBatchCardDayFile(i, record, amount);
                    } else {
                        createEmptyDailyFile(keyObjCard);
                    }
                }
            } else {
                createEmptyDailyFile(keyObjCard);
            }
        } catch (Exception e) {
            log.error("Error move File cradOfDay : {} ", e.getMessage());
        }
    }

    private void createEmptyDailyFile(Object obj) {
        ChannelSftp channelSftp = null;
        String fileName = null;
        if (ObjectUtils.isEmpty(obj)) {
            fileName = "DMFlow" + "_" + "req" + "_" + dohMerchantId + "_" + fileService.dateFormatter() + "_01";
        } else {
            fileName = "DMFlow" + "_" + "req" + "_" + dohMerchantId + "_" + fileService.dateFormatter() + "_" + fileService.seqFormat(((Integer) obj).intValue());
        }
        try {
            String header = "H" + "|" + dohMerchantId + "|" + fileName + "|" + "TKFP" + "|" + "B.1.0";
            String footer = "F|0|00.00";
            Path filePathObj = Paths.get(cardEmptyPath + "/" + fileName + ".txt");
            if (!Files.exists(filePathObj)) {
                List<String> lines = Arrays.asList(header, footer);
                Files.write(filePathObj, lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW, StandardOpenOption.APPEND);
                String remoteDir = sftpKtbRemoteDir + "card_payment/input/";
                channelSftp = jschConfig.setupJsch();
                channelSftp.connect();
                SftpATTRS attrs = null;
                try {
                    attrs = channelSftp.stat(remoteDir + fileName + ".txt");
                } catch (Exception e) {
                    log.error(remoteDir + fileName + ".txt is not found.");
                }
                if (attrs == null) {
                    channelSftp.put(cardEmptyPath + "/" + fileName + ".txt", remoteDir + fileName + ".txt");
                    log.info("Put daily empty file completed.");
                }
                channelSftp.disconnect();
            }
        } catch (Exception e) {
            log.error("-> Problem occured while writing the daily empty file= " + e);
        } finally {
            if (channelSftp != null && channelSftp.isConnected()) {
                channelSftp.disconnect();
            }
            try {
                Path filePathObj = Paths.get(cardEmptyPath + fileName + ".txt");
                if (Files.exists(filePathObj)) {
                    Files.delete(filePathObj);
                    log.info("-> Delete daily empty file on local folder done.");
                }
            } catch (Exception e) {
                log.error("-> Problem occured while delete the daily empty file= " + e);
            }
        }
    }

    //================================================================cradOfMonthly==========================================================
    private void cradOfMonthly() {
        Object ObjTotalRecordBillCard = null;
        int numberFile = 1;
        try {
            String keyCard = BATCH_BILL_CYCLE + ":" + fileService.dateFormatter();
            String keyTotalFileBillCycle = TOTAL_FILE_BILL_CYCLE + ":" + fileService.dateFormatter();
            Object keyObjCard = redisService.get(keyCard);
            Object keyTotalFiled = redisService.get(keyTotalFileBillCycle);
            if (ObjectUtils.isNotEmpty(keyTotalFiled)) {
                numberFile = ((Integer) keyTotalFiled).intValue();
            }
            if (ObjectUtils.isNotEmpty(keyObjCard)) {
                for (int i = numberFile; i <= ((Integer) keyObjCard).intValue(); i++) {
                    String keyRecord = TOTAL_RECORD_BILL_CYCLE + ":" + fileService.dateFormatter() + ":" + i;
                    String keyAmount = TOTAL_AMOUNT_BILL_CYCLE + ":" + fileService.dateFormatter() + ":" + i;
                    Object record = redisService.get(keyRecord);
                    Object amount = redisService.get(keyAmount);
                    if (ObjectUtils.isNotEmpty(record) || ObjectUtils.isNotEmpty(amount)) {
                        fileService.writeFileCardBillCycle(i, record, amount);
                    } else {
                        createEmptyMonthlyFile(keyObjCard);
                    }
                }
            } else {
                createEmptyMonthlyFile(keyObjCard);
            }
        } catch (Exception e) {
            log.error("Error move File cradOfDay : {} ", e.getMessage());
        }
    }

    private void createEmptyMonthlyFile(Object obj) {
        ChannelSftp channelSftp = null;
        String fileName = null;
        if (ObjectUtils.isEmpty(obj)) {
            fileName = "MMFlow" + "_" + "req" + "_" + dohMerchantId + "_" + fileService.dateFormatter() + "_01";
        } else {
            fileName = "MMFlow" + "_" + "req" + "_" + dohMerchantId + "_" + fileService.dateFormatter() + "_" + fileService.seqFormat(((Integer) obj).intValue());
        }
        try {
            String header = "H" + "|" + dohMerchantId + "|" + fileName + "|" + "TKFP" + "|" + "B.1.0";
            String footer = "F|0|00.00";
            Path filePathObj = Paths.get(cardEmptyPath + fileName + ".txt");
            if (!Files.exists(filePathObj)) {
                List<String> lines = Arrays.asList(header, footer);
                Files.write(filePathObj, lines, StandardCharsets.UTF_8, StandardOpenOption.CREATE_NEW, StandardOpenOption.APPEND);
                String remoteDir = sftpKtbRemoteDir + "card_payment/input/";
                channelSftp = jschConfig.setupJsch();
                channelSftp.connect();
                SftpATTRS attrs = null;
                try {
                    attrs = channelSftp.stat(remoteDir + fileName + ".txt");
                } catch (Exception e) {
                    log.error(remoteDir + fileName + ".txt is not found.");
                }
                if (attrs == null) {
                    channelSftp.put(cardEmptyPath + "/" + fileName + ".txt", remoteDir + fileName + ".txt");
                    log.info("Put daily empty file completed.");
                }
                channelSftp.disconnect();
            }
        } catch (Exception e) {
            log.error("-> Problem occured while writing the monthly empty file= " + e);
        } finally {
            if (channelSftp != null && channelSftp.isConnected()) {
                channelSftp.disconnect();
            }
            try {
                Path filePathObj = Paths.get(cardEmptyPath + fileName + ".txt");
                if (Files.exists(filePathObj)) {
                    Files.delete(filePathObj);
                    log.info("-> Delete monthly empty file on local folder done.");
                }
            } catch (Exception e) {
                log.error("-> Problem occured while delete the monthly empty file= " + e);
            }
        }
    }

    //================================================================writeFileOfCard==========================================================
}