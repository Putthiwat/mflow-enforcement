package com.appworks.co.th.batchmaster.oracle.repository;

import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMemberHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceNonMemberHistoryRepository extends JpaRepository<InvoiceNonMemberHistory, Long> {
}
