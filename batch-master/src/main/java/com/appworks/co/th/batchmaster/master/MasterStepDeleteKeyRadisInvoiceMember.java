package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.Set;

import static com.appworks.co.th.batchmaster.commons.Constant.generateKeyInvoiceNonmember.CREATE_INVOICE_MEMBER;

public class MasterStepDeleteKeyRadisInvoiceMember implements Tasklet {
    public static final Logger log = LoggerFactory.getLogger(MasterStepDeleteKeyRadisInvoiceMember.class);
    private RedisService redisService;


    public MasterStepDeleteKeyRadisInvoiceMember(RedisService mredisService) {
        redisService = mredisService;
    }
    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        String key = CREATE_INVOICE_MEMBER + ":" + "*" + ":" + "*";
        Set<String> keys = redisService.getKeysWithPattern(key);
        if (keys != null && keys.size() > 0) {
            redisService.deleteList(keys);
        }
        return RepeatStatus.FINISHED;
    }
}
