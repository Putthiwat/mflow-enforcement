package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import com.appworks.co.th.batchmaster.service.InvoiceNonMemberService;
import com.appworks.co.th.batchmaster.service.RedisService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class NonMemberWarning2MasterJob {
    public final Logger log = LoggerFactory.getLogger(this.getClass());

    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    @Autowired
    private RedisService redisService;
    @Autowired
    private InvoiceJdbcRepository invoiceJdbcRepository;
    @Autowired
    InvoiceNonMemberService invoiceNonMemberService;

    @Autowired
    @Qualifier("oracleTransactionManager")
    private PlatformTransactionManager transactionManager;

    private final StepBuilderFactory steps;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    @Value("${operation.fee.member.multiply}")
    private Long operationFeeAmount;

    public NonMemberWarning2MasterJob(JobBuilderFactory jobBuilderFactory,
                                      RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory,
                                      StepBuilderFactory steps) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
        this.steps = steps;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "nonMemberWarning2JobRequests")
    public DirectChannel nonMemberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning2JobRequestsOutboundFlow")
    public IntegrationFlow nonMemberWarning2JobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemberWarning2JobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemberWarning2JobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemberWarning2JobReplies")
    public DirectChannel nonMemberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning2JobRepliesInboundFlow")
    public IntegrationFlow nonMemberWarning2JobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemberWarning2JobReplies"))
                .channel(nonMemberWarning2JobReplies())
                .get();
    }

    public ColumnRangePartitioner nonMemberWarning2Partitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  AND INVOICE_TYPE = 1 AND TOTAL_AMOUNT > 0  AND STATUS !='PAYMENT_SUCCESS' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberWarning2() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberWarning2")
                .partitioner("nonMemberWarning2Step", nonMemberWarning2Partitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemberWarning2JobRequests())
                .inputChannel(nonMemberWarning2JobReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceNonMemEBillPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemGenerateEBillWarning2() {
        return this.masterStepBuilderFactory.get("masterStepNonMemGenerateEBillWarning2")
                .partitioner("updateInvoiceNonMemEBillWarning2Step", invoiceNonMemEBillPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemGenerateEBillRequestsWarning2())
                .inputChannel(nonMemGenerateEBillRepliesWarning2())
                .build();
    }

    @Bean(name = "nonMemGenerateEBillRequestsWarning2")
    public DirectChannel nonMemGenerateEBillRequestsWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRequestsWarning2OutboundFlow")
    public IntegrationFlow nonMemGenerateEBillRequestsWarning2OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateEBillRequestsWarning2())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateEBillRequestsWarning2"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemGenerateEBillRepliesWarning2")
    public DirectChannel nonMemGenerateEBillRepliesWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRepliesWarning2InboundFlow")
    public IntegrationFlow nonMemGenerateEBillRepliesWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateEBillRepliesWarning2"))
                .channel(nonMemGenerateEBillRepliesWarning2())
                .get();
    }


    public ColumnRangePartitioner invoiceNonMemGroupPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND REF_GROUP IS NULL ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberInvoiceGenerateGroupWarning2() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberInvoiceGenerateGroupWarning2")
                .partitioner("updateNonMemInvoiceGroupWarning2Step", invoiceNonMemGroupPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemGenerateGroupRequestsWarning2())
                .inputChannel(nonMemGenerateGroupRepliesWarning2())
                .build();
    }


    @Bean(name = "nonMemGenerateGroupRequestsWarning2")
    public DirectChannel nonMemGenerateGroupRequestsWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRequestsWarning2OutboundFlow")
    public IntegrationFlow nonMemGenerateGroupRequestsWarning2OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateGroupRequestsWarning2())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateGroupRequestsWarning2"))
                .get();
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemGenerateGroupRepliesWarning2")
    public DirectChannel nonMemGenerateGroupRepliesWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRepliesWarning2InboundFlow")
    public IntegrationFlow nonMemGenerateGroupRepliesWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateGroupRepliesWarning2"))
                .channel(nonMemGenerateGroupRepliesWarning2())
                .get();
    }


    public ColumnRangePartitioner groupInvoiceNonmemberWarning2Partitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  and invoice_type = 1 and TOTAL_AMOUNT > 0  and STATUS !='PAYMENT_SUCCESS' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepGroupInvoiceNonmember() {
        return this.masterStepBuilderFactory.get("masterStepGroupInvoiceNonmember")
                .partitioner("groupInvoiceNonmemberWarning2", groupInvoiceNonmemberWarning2Partitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(groupInvoiceNonmemberWarning2JobRequests())
                .inputChannel(groupInvoiceNonmemberWarning2JobReplies())
                .build();
    }


    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "groupInvoiceNonmemberWarning2JobRequests")
    public DirectChannel groupInvoiceNonmemberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceNonmemberWarning2JobRequestsOutboundFlow")
    public IntegrationFlow groupInvoiceNonmemberWarning2JobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(groupInvoiceNonmemberWarning2JobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("groupInvoiceNonmemberWarning2JobRequests"))
                .get();
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "groupInvoiceNonmemberWarning2JobReplies")
    public DirectChannel groupInvoiceNonmemberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceNonmemberWarning2JobRepliesInboundFlow")
    public IntegrationFlow groupInvoiceNonmemberWarning2JobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("groupInvoiceNonmemberWarning2JobReplies"))
                .channel(groupInvoiceNonmemberWarning2JobReplies())
                .get();
    }

    @Bean
    public Job nonMemberWarning2Job() {
        return this.jobBuilderFactory.get("nonMemberWarning2Job")
                .incrementer(new RunIdIncrementer())
                .start(masterStepDeleteKeyRadisInvoiceNonMember())
                .next(masterStepGroupInvoiceNonmember())
                .next(stepGenerateInvoiceNonmemberGroup())
                .next(masterStepNonMemberWarning2())
                .next(masterStepNonMemGenerateEBillWarning2())
                .next(masterStepNonMemberInvoiceGenerateGroupWarning2())
                .build();
    }

    @Bean
    public Step stepGenerateInvoiceNonmemberGroup() {
        return this.steps.get("stepInvoiceNonmemberGroup")
                .transactionManager(transactionManager)
                .tasklet(new GenerateInvoiceNonmemberGroup(redisService,invoiceJdbcRepository,invoiceNonMemberService,operationFeeAmount))
                .build();
    }

    @Bean
    public Step masterStepDeleteKeyRadisInvoiceNonMember() {
        return this.steps.get("masterStepDeleteKeyRadisInvoiceNonMember")
                .tasklet(new MasterStepDeleteKeyRadisInvoiceNonMember(redisService))
                .build();
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }

}
