package com.appworks.co.th.batchmaster.oracle.repository;


import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.Invoice;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceColor;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceDetail;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceEvidence;

import java.util.List;

public interface InsertInvoiceMemberRepository {
    void updateInvoiceNoTypeOfMember(int invoiceType, String updateBy, String invoiceNo) throws InsertInvoiceException;

    void insertInvoiceNoMemberOld(String invoiceNo, String invoiceNoOld) throws InsertInvoiceException;

    boolean insertInvoice(Invoice invoice,List<String> invoiceNoOlds) throws InsertInvoiceException;

    void insertInvoiceByStagingRepository(Invoice invoice) throws InsertInvoiceException;

    void insertInvoiceDetailByStagingRepository(List<InvoiceDetail> invoiceDetailList) throws InsertInvoiceException;

    void insertInvoiceColorRepository(List<InvoiceColor> colorsList) throws InsertInvoiceException;

    int getInvoiceDetailByStagingRepository(InvoiceDetail invoiceDetailList) throws InsertInvoiceException;

    void insertInvoiceEvidenceRepository(int detailId, List<InvoiceEvidence> evidenceList) throws InsertInvoiceException;
}
