package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.JschConfig;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.*;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.*;

public class MoveFileCard implements Tasklet {
    private JschConfig jschConfig;

    private String cardPayment;

    private String cardPaymentArchice;

    private RedisService redisService;

    private List<Integer> dayOfBillCycle;

    public static final Logger log = LoggerFactory.getLogger(MoveFileCard.class);
    @Value("${account.limit.time.out}")
    private int limitTimeOut;

    public MoveFileCard(JschConfig mjschConfig, String mcardPayment, String mcardPaymentArchice, RedisService mredisService, int mLimitTimeOut, List<Integer> mDayOfBillCycle) {
        jschConfig = mjschConfig;
        cardPayment = mcardPayment;
        cardPaymentArchice = mcardPaymentArchice;
        redisService = mredisService;
        limitTimeOut = mLimitTimeOut;
        dayOfBillCycle = mDayOfBillCycle;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        log.info("open move file card");
        ChannelSftp channelSftp = null;
        try {
            channelSftp = jschConfig.setupJsch();
            channelSftp.connect();
            channelSftp.cd(cardPayment);
            Vector<ChannelSftp.LsEntry> listFileCsv = channelSftp.ls("*.txt");
            for (ChannelSftp.LsEntry entry : listFileCsv) {
                String dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS").format(LocalDateTime.now());
                String path = cardPaymentArchice + dateTimeFormatter + "/" + entry.getFilename();
                channelSftp.cd(cardPaymentArchice);
                channelSftp.mkdir(dateTimeFormatter);
                channelSftp.cd(cardPayment);
                channelSftp.rename(cardPayment + entry.getFilename(), path);
            }
            channelSftp.disconnect();
        } catch (IOException | SftpException | JSchException e) {
            log.error("Error Move File Of Card : {}", e.getMessage());
        }
//        ArrayList<String> arrayListCard = new ArrayList<String>();
        //       arrayListCard.add(BATCH_CARD + ":" + "*");
//        arrayListCard.add(TOTAL_RECORD_CARD + ":" + "*"+ ":" + "*");
//        arrayListCard.add(TOTAL_AMOUNT_CARD + ":" + "*"+ ":" + "*");
//        this.deleteRadis(arrayListCard);
            String key = BATCH_CARD + ":" + dateFormatter();
            String keyTotalFileBillCycle = TOTAL_FILE_CARD + ":" + dateFormatter();
            Long aLong = redisService.setKeyIncrementBySetValue(key, 1, limitTimeOut, TimeUnit.DAYS);
            if (aLong != null && aLong > 0) {
                redisService.setOfDay(keyTotalFileBillCycle, Integer.valueOf(String.valueOf(aLong)), limitTimeOut, TimeUnit.DAYS);
            } else {
                redisService.setOfDay(keyTotalFileBillCycle, 1, limitTimeOut, TimeUnit.DAYS);
            }
        return RepeatStatus.FINISHED;
    }
    private void deleteRadis(ArrayList<String> arrayListCard) {
        try {
            for (String keyPattern : arrayListCard) {
                Set<String> keys = redisService.getKeysWithPattern(keyPattern);
                if (keys != null && keys.size() > 0) {
                    for (String key : keys) {
                        redisService.delete(key);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error Radis Of Card : {}", e.getMessage());
        }
    }

    private String dateFormatter() {
        final DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String seq = df.format(new Date());
        return seq;
    }

}