package com.appworks.co.th.batchmaster.service;

import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMember;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InvoiceNonMemberService {
    InvoiceNonMember createNonMemberInvoice(String invoiceNo, CreateInvoiceHeader createInvoiceHeader, CreateInvoiceDetail createInvoiceDetail, InvoiceNonMember invoice) throws InsertInvoiceException;

    boolean insertInvoiceNonMember(InvoiceNonMember invoice, List<String> invoiceNoOlds) throws InsertInvoiceException;
}
