package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.JschConfig;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;


import static com.appworks.co.th.batchmaster.utils.DateUtils.DateInstantTimeBangkok;
import static com.appworks.co.th.batchmaster.utils.DateUtils.atStartOfDay;

@Component
@RequiredArgsConstructor
public class MoveFileFastPayMasterJob {

    @Autowired
    JschConfig jschConfig;
    @Value("${cardPayment.input}")
    private String cardPayment;

    @Value("${cardPayment.archice}")
    private String cardPaymentArchice;
    private static final DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    public static final Logger log = LoggerFactory.getLogger(MoveFileFastPayMasterJob.class);

//    @Scheduled(cron = "${fileFastPay.scheduled}")
    public void schedulingMoveFileFastPayMasterJob() {
        log.info("open file ");
        ChannelSftp channelSftp = null;
        try {
            channelSftp = jschConfig.setupJsch();
            channelSftp.connect();
            channelSftp.cd(cardPayment);
            Vector<ChannelSftp.LsEntry> listFileCsv = channelSftp.ls("*.TXT");
            for (ChannelSftp.LsEntry entry : listFileCsv) {
                String filename = entry.getFilename();
                String[] datas = StringUtils.split(filename, String.valueOf('_'));
                if ("FASTPAY".equals(datas[0])) {
                    Instant timestamp = checkData(datas[2]);
                    if (timestamp.isBefore(DateInstantTimeBangkok(Instant.now()))) {
                        List<String> dataReadFile = readFile(channelSftp, entry.getFilename());
                        if (!dataReadFile.isEmpty()) {
                            channelSftp.rename(cardPayment + filename, cardPaymentArchice + filename);
                        }
                    }
                }
            }
            channelSftp.disconnect();
        } catch (IOException | SftpException | JSchException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static List<String> readFile(ChannelSftp channelSftp, String file) throws IOException {
        List<String> sb = new ArrayList<>();
        InputStream inputStream = null;
        try {
            inputStream = channelSftp.get(file);
        } catch (SftpException e) {
            e.printStackTrace();
        }
        if (inputStream != null) {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                sb.add(line + System.lineSeparator());
                break;
            }
            inputStream.close();
        }
        return sb;
    }


//    public static String data(String stringData) {
//        String data = null;
//        String[] datas = StringUtils.split(stringData, String.valueOf('_'));
//        if ("FASTPAY".equals(datas[0])) {
//            data = datas[2];
//        }
//        return data;
//    }

    public static Instant checkData(String data) throws ParseException {
        Date dateTo = new SimpleDateFormat("yyyyMMdd").parse(data);
        Date startDate = atStartOfDay(dateTo);
        return Instant.parse(df1.format(startDate));
    }
}
