package com.appworks.co.th.batchmaster.exception;

public class InsertInvoiceException extends Exception {

    public InsertInvoiceException(String message) {
        super(message);
    }
}
