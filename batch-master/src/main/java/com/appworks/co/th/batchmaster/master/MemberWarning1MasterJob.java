package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class MemberWarning1MasterJob {
    public static final Logger log = LoggerFactory.getLogger(MemberWarning1MasterJob.class);

    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;


    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public MemberWarning1MasterJob(JobBuilderFactory jobBuilderFactory,
                                   RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "memberWarning1JobRequests")
    public DirectChannel memberWarning1JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning1JobOutboundFlow")
    public IntegrationFlow memberWarning1JobOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberWarning1JobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberWarning1JobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberWarning1JobReplies")
    public DirectChannel memberWarning1JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning1JobInboundFlow")
    public IntegrationFlow memberWarning1JobInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberWarning1JobReplies"))
                .channel(memberWarning1JobReplies())
                .get();
    }

    public ColumnRangePartitioner memberWarning1Partitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
//        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  AND INVOICE_TYPE = 0 AND TOTAL_AMOUNT > 0 AND STATUS IN (\'PAYMENT_WAITING\', \'PAYMENT_FAILED\') " +
//                " AND DUE_DATE < TO_DATE(\'"+currentDate()+"\', \'YYYY/MM/DD\')"
//        );
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  AND INVOICE_TYPE = 0 AND TOTAL_AMOUNT > 0 AND STATUS !='PAYMENT_SUCCESS' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberWarning1() {
        return this.masterStepBuilderFactory.get("masterStepMemberWarning1")
                .partitioner("memberWarning1Step", memberWarning1Partitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberWarning1JobRequests())
                .inputChannel(memberWarning1JobReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceGenerateEBillPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoiceGenerateEBillWarning1() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceGenerateEBillWarning1")
                .partitioner("updateInvoiceEBillWarning1Step", invoiceGenerateEBillPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceGenerateEBillRequestsWarning1())
                .inputChannel(memberInvoiceGenerateEBillRepliesWarning1())
                .build();
    }


    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning1")
    public DirectChannel memberInvoiceGenerateEBillRequestsWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning1OutboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRequestsWarning1OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateEBillRequestsWarning1())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRequestsWarning1"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceGenerateEBillRepliesWarning1")
    public DirectChannel memberInvoiceGenerateEBillRepliesWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillWarning1InboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRepliesWarning1"))
                .channel(memberInvoiceGenerateEBillRepliesWarning1())
                .get();
    }


    public ColumnRangePartitioner invoiceGenerateGroupPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND REF_GROUP IS NULL AND TOTAL_AMOUNT > 0 ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoiceGenerateGroupWarning1() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceGenerateGroupWarning1")
                .partitioner("updateInvoiceGroupWarning1Step", invoiceGenerateGroupPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceGenerateGroupWarning1Requests())
                .inputChannel(memberInvoiceGenerateGroupWarning1Replies())
                .build();
    }


    @Bean(name = "memberInvoiceGenerateGroupWarning1Requests")
    public DirectChannel memberInvoiceGenerateGroupWarning1Requests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning1OutboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning1OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateGroupWarning1Requests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning1Requests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceGenerateGroupWarning1Replies")
    public DirectChannel memberInvoiceGenerateGroupWarning1Replies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning1InboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning1Replies"))
                .channel(memberInvoiceGenerateGroupWarning1Replies())
                .get();
    }



    @Bean
    public Job memberWarning1Job() {
        return this.jobBuilderFactory.get("memberWarning1Job")
                .incrementer(new RunIdIncrementer())
                .start(masterStepMemberWarning1())
                .next(masterStepMemberInvoiceGenerateEBillWarning1()) // generate ebill
                .next(masterStepMemberInvoiceGenerateGroupWarning1()) // gen group ref
                .build();
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }


}
