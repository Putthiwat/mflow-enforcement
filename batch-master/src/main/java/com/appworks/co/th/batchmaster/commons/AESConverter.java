package com.appworks.co.th.batchmaster.commons;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import static com.appworks.co.th.batchmaster.commons.AESUtils.decrypt;
import static com.appworks.co.th.batchmaster.commons.AESUtils.encrypt;

@Converter
public class AESConverter implements AttributeConverter<String, String> {
    @Override
    public String convertToDatabaseColumn(String attribute) {
        return encrypt(attribute);
    }

    @Override
    public String convertToEntityAttribute(String dbData) {
        return decrypt(dbData);
    }
}
