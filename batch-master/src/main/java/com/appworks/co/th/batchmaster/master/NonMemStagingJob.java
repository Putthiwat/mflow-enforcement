package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class NonMemStagingJob {
    public static final Logger log = LoggerFactory.getLogger(NonMemStagingJob.class);

    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;


    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public NonMemStagingJob(JobBuilderFactory jobBuilderFactory,
                               RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "invoiceNonMemStagingJobRequests")
    public DirectChannel invoiceNonMemStagingJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceNonMemStagingJobRequestsOutboundFlow")
    public IntegrationFlow invoiceNonMemStagingJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceNonMemStagingJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceNonMemStagingJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "invoiceNonMemStagingJobReplies")
    public DirectChannel invoiceNonMemStagingJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceNonMemStagingJobRepliesInboundFlow")
    public IntegrationFlow invoiceNonMemStagingJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceNonMemStagingJobReplies"))
                .channel(invoiceNonMemStagingJobReplies())
                .get();
    }


    @Bean(name = "nonMemGenerateEBillRequests")
    public DirectChannel nonMemGenerateEBillRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRequestsOutboundFlow")
    public IntegrationFlow nonMemGenerateEBillRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateEBillRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateEBillRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemGenerateEBillReplies")
    public DirectChannel nonMemGenerateEBillReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRepliesInboundFlow")
    public IntegrationFlow nonMemGenerateEBillRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateEBillReplies"))
                .channel(nonMemGenerateEBillReplies())
                .get();
    }

    @Bean(name = "nonMemGenerateGroupRequests")
    public DirectChannel nonMemGenerateGroupRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRequestsOutboundFlow")
    public IntegrationFlow nonMemGenerateGroupRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateGroupRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateGroupRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemGenerateGroupReplies")
    public DirectChannel nonMemGenerateGroupReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRepliesInboundFlow")
    public IntegrationFlow nonMemGenerateGroupRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateGroupReplies"))
                .channel(nonMemGenerateGroupReplies())
                .get();
    }



    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "invoiceProceedNonMemberListJobRequests")
    public DirectChannel invoiceProceedNonMemberListJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedNonMemberListJobRequestsOutboundFlow")
    public IntegrationFlow invoiceProceedNonMemberListJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceProceedNonMemberListJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceProceedNonMemberListJobRequests"))
                .get();
    }



    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "invoiceProceedNonMemberListJobReplies")
    public DirectChannel invoiceProceedNonMemberListJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedNonMemberListJobRepliesInboundFlow")
    public IntegrationFlow invoiceProceedNonMemberListJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceProceedNonMemberListJobReplies"))
                .channel(invoiceProceedNonMemberListJobReplies())
                .get();
    }






    ////////////

    public ColumnRangePartitioner invoiceNonMemStagePartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NON_STAGING");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND STATUS=\'WAITING_TO_MOVE\' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberInvoice() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberInvoice")
                .partitioner("updateInvoiceNonMemStageStep", invoiceNonMemStagePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(invoiceNonMemStagingJobRequests())
                .inputChannel(invoiceNonMemStagingJobReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceNonMemEBillPartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL AND INVOICE_TYPE =0");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemGenerateEBill() {
        return this.masterStepBuilderFactory.get("masterStepNonMemGenerateEBill")
                .partitioner("updateInvoiceNonMemEBillStep", invoiceNonMemEBillPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemGenerateEBillRequests())
                .inputChannel(nonMemGenerateEBillReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceNonMemGroupPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND STATUS=\'PAYMENT_WAITING\' AND REF_GROUP IS NULL AND INVOICE_TYPE =0");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberInvoiceGenerateGroup() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberInvoiceGenerateGroup")
                .partitioner("updateNonMemInvoiceGroupStep", invoiceNonMemGroupPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemGenerateGroupRequests())
                .inputChannel(nonMemGenerateGroupReplies())
                .build();
    }
    public ColumnRangePartitioner invoiceProceedNonMemberListStagePartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_PROCEED_LIST invoiceProceed");
        columnRangePartitioner.setWhere("WHERE invoiceProceed.COMMAND = 'InvoiceNonMemberCommand' AND invoiceProceed.STATE='APPROVAL_PENDING' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberInvoiceProceedList() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberInvoiceProceedList")
                .partitioner("invoiceProceedNonMemberListStep", invoiceProceedNonMemberListStagePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(invoiceProceedNonMemberListJobRequests())
                .inputChannel(invoiceProceedNonMemberListJobReplies())
                .build();
    }
    @Bean
    public Job invoiceNonMemStagingJob() {
        return this.jobBuilderFactory.get("invoiceNonMemStagingJob")
                .incrementer(new RunIdIncrementer())
                //.start(masterStepNonMemberInvoiceProceedList()) //read InvoiceProceedList
                .start(masterStepNonMemberInvoice())
                .next(masterStepNonMemGenerateEBill())
                .next(masterStepNonMemberInvoiceGenerateGroup())
                .build();
    }
/////
}
