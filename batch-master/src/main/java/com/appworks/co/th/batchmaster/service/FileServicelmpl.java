package com.appworks.co.th.batchmaster.service;

import com.appworks.co.th.batchmaster.master.ScheduledMoveFileTasks;
import com.appworks.co.th.batchmaster.payload.FileResponse;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.*;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.TOTAL_AMOUNT_CARD;

@Service
public class FileServicelmpl implements FileService {
    final String newLine = "\r\n";
    public static final Logger log = LoggerFactory.getLogger(FileServicelmpl.class);
    @Autowired
    private RedisService redisService;

    @Value("${card.pattern}")
    private String pattern;
    @Value("${doh.merchant.id}")
    private String dohMerchantId;
    @Value("${card.path.tmp}")
    private String tmpPath;
    @Value("${card.path.main}")
    private String mainPath;

    @Override
    @Async(value = "generateMoveFileCardThreadPool")
    public void writeFileBatchCardDayFile(Integer numberFile,Object record,Object amount) {
        String footer =null;
        try {
            FileResponse fileResponse = new FileResponse();
//            String keyRecord = TOTAL_RECORD_CARD + ":" + dateFormatter() + ":" + numberFile;
//            String keyAmount = TOTAL_AMOUNT_CARD + ":" + dateFormatter() + ":" + numberFile;
//            Object record = redisService.get(keyRecord);
//            Object amount = redisService.get(keyAmount);
                String fileName = "DMFlow" + "_" + "req" + "_" + dohMerchantId + "_" + dateFormatter() + "_" + this.seqFormat(numberFile) + ".txt";
                String filePath = tmpPath + fileName;
                fileResponse.setFilePathObj(Paths.get(filePath));
                fileResponse.setName(fileName);
                // String footer = "F|" + 2 + "|" + 60;
                BigDecimal amountBigDecimal = new BigDecimal(0);
                if (amount != null) {
                    try {
                        amountBigDecimal = BigDecimal.valueOf(Double.valueOf(((Integer) amount).intValue()));
                    } catch (Exception e) {
                        amountBigDecimal = BigDecimal.valueOf(((Double) amount).doubleValue());
                    }
                } else {
                    amountBigDecimal = BigDecimal.valueOf(0);
                }
                if (ObjectUtils.isNotEmpty(record)) {
                    footer = "F|" + ((Integer) record).intValue() + "|" + decimalFormatter(amountBigDecimal);
                } else {
                    footer = "F|" + 0 + "|" + decimalFormatter(amountBigDecimal);
                }
                this.writeFileOfCard(fileResponse.getFilePathObj(), footer);
                this.moveFileCard(fileResponse, mainPath, "cardOfDay");
        } catch (Exception e) {
            log.error("Error write File BatchCard : {} ", e.getMessage());
        }
    }

    @Override
    @Async(value = "generateMoveFileCardThreadPool")
    public void writeFileCardBillCycle(Integer numberFile,Object record,Object amount) {
        String footer =null;
        try {
            FileResponse fileResponse = new FileResponse();
//            String keyRecord = TOTAL_RECORD_BILL_CYCLE + ":" + dateFormatter() + ":" + numberFile;
//            String keyAmount = TOTAL_AMOUNT_BILL_CYCLE + ":" + dateFormatter() + ":" + numberFile;
//            Object record = redisService.get(keyRecord);
//            Object amount = redisService.get(keyAmount);
                String fileName = "MMFlow" + "_" + "req" + "_" + dohMerchantId + "_" + dateFormatter() + "_" + this.seqFormat(numberFile) + ".txt";
                String filePath = tmpPath + fileName;
                fileResponse.setFilePathObj(Paths.get(filePath));
                fileResponse.setName(fileName);
                //               String footer = "F|" + 2 + "|" + 60;
                BigDecimal amountBigDecimal = new BigDecimal(0);
                if (amount != null) {
                    try {
                        amountBigDecimal = BigDecimal.valueOf(Double.valueOf(((Integer) amount).intValue()));
                    } catch (Exception e) {
                        amountBigDecimal = BigDecimal.valueOf(((Double) amount).doubleValue());
                    }
                } else {
                    amountBigDecimal = BigDecimal.valueOf(0);
                }
                if (ObjectUtils.isNotEmpty(record)) {
                    footer = "F|" + ((Integer) record).intValue() + "|" + decimalFormatter(amountBigDecimal);
                } else {
                    footer = "F|0|" + decimalFormatter(amountBigDecimal);
                }
                this.writeFileOfCard(fileResponse.getFilePathObj(), footer);
                this.moveFileCard(fileResponse, mainPath, "cardBillCycle");
        } catch (Exception e) {
            log.error("Error write File BatchCard : {} ", e.getMessage());
        }
    }


    public void writeFileOfCard(Path filePathObj, String footer) {
        String format = null;
        try {
            FileChannel fileChannel = new FileOutputStream(filePathObj.toFile(), true).getChannel();
            FileChannel finalFileChannel = fileChannel;
            try {
                format = footer + newLine;
                ByteBuffer buff = ByteBuffer.wrap(format.getBytes(StandardCharsets.UTF_8));
                finalFileChannel.write(buff);
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileChannel.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void moveFileCard(FileResponse fileResponse, String pathNew, String name) {
        log.info("open move file : {}", name);
        try {
            File filePathOld = new File(fileResponse.getFilePathObj().toString());
            File filePathNew = new File(pathNew + fileResponse.getName());
            FileUtils.moveFile(filePathOld, filePathNew);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Async(value = "generateMoveFileThreadPool")
    public void moveFile(FileResponse fileResponse, String pathNew, String name) {
        log.info("open move file : {}", name);
        try {
            File filePathOld = new File(fileResponse.getFilePathObj().toString());
            File filePathNew = new File(pathNew + fileResponse.getName());
            FileUtils.moveFile(filePathOld, filePathNew);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String decimalFormatter(BigDecimal bigDecimal) {
        return bigDecimal.setScale(2, RoundingMode.DOWN).toString();
    }


    public String dateFormatter() {
        final DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String seq = df.format(Calendar.getInstance().getTime());
        return seq;
    }

    public String seqFormat(int seq) {
        DecimalFormat formatter = new DecimalFormat("00");
        return formatter.format(seq);
    }

}
