package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class MemberWarningDueDateMasterJob {

    public final Logger log = LoggerFactory.getLogger(this.getClass());

    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;


    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public MemberWarningDueDateMasterJob(JobBuilderFactory jobBuilderFactory,
                               RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "memberWarningDueDateJobRequests")
    public DirectChannel memberWarningDueDateJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarningDueDateJobRequestsOutboundFlow")
    public IntegrationFlow memberWarningDueDateJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberWarningDueDateJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberWarningDueDateJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberWarningDueDateJobReplies")
    public DirectChannel memberWarningDueDateJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarningDueDateJobRepliesInboundFlow")
    public IntegrationFlow memberWarningDueDateJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberWarningDueDateJobReplies"))
                .channel(memberWarningDueDateJobReplies())
                .get();
    }

    public ColumnRangePartitioner memberWarningDueDatePartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  and invoice_type in (0,1,2) and TOTAL_AMOUNT > 0 and status in (\'PAYMENT_WAITING\', \'PAYMENT_FAILED\') ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberWarningDueDate() {
        return this.masterStepBuilderFactory.get("masterStepMemberWarningDueDate")
                .partitioner("memberWarningDueDateStep", memberWarningDueDatePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberWarningDueDateJobRequests())
                .inputChannel(memberWarningDueDateJobReplies())
                .build();
    }

    @Bean
    public Job memberWarningDueDateJob() {
        return this.jobBuilderFactory.get("memberWarningDueDateJob")
                .incrementer(new RunIdIncrementer())
                .start(masterStepMemberWarningDueDate())
                .build();
    }
}
