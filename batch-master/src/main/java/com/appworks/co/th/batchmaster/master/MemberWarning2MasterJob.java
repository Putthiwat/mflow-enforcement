package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.oracle.jdbc.InvoiceJdbcRepository;
import com.appworks.co.th.batchmaster.oracle.repository.InsertInvoiceMemberRepository;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import com.appworks.co.th.batchmaster.service.InvoiceMemberService;
import com.appworks.co.th.batchmaster.service.RedisService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class MemberWarning2MasterJob {
    public static final Logger log = LoggerFactory.getLogger(MemberWarning2MasterJob.class);

    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;
    private final StepBuilderFactory steps;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    @Autowired
    private RedisService redisService;

    @Autowired
    private InvoiceJdbcRepository invoiceJdbcRepository;
    @Autowired
    private InvoiceMemberService invoiceMemberService;
    @Autowired
    private InsertInvoiceMemberRepository insertInvoiceByStagingRepository;

    @Value("${operation.fee.member.multiply}")
    private Long operationFeeAmount;

    @Autowired
    @Qualifier("oracleTransactionManager")
    private PlatformTransactionManager transactionManager;

    public MemberWarning2MasterJob(JobBuilderFactory jobBuilderFactory,
                                   RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory,
                                   StepBuilderFactory steps) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
        this.steps = steps;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "memberWarning2JobRequests")
    public DirectChannel memberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning2JobRequestsOutboundFlow")
    public IntegrationFlow memberWarning2JobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberWarning2JobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberWarning2JobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberWarning2JobReplies")
    public DirectChannel memberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberWarning2JobRepliesInboundFlow")
    public IntegrationFlow memberWarning2JobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberWarning2JobReplies"))
                .channel(memberWarning2JobReplies())
                .get();
    }

    public ColumnRangePartitioner memberWarning2Partitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND INVOICE_TYPE = 1 AND TOTAL_AMOUNT > 0 AND STATUS !='PAYMENT_SUCCESS' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberWarning2() {
        return this.masterStepBuilderFactory.get("masterStepMemberWarning2")
                .partitioner("memberWarning2Step", memberWarning2Partitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberWarning2JobRequests())
                .inputChannel(memberWarning2JobReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceGenerateEBillPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoiceGenerateEBillWarning2() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceGenerateEBillWarning2")
                .partitioner("updateInvoiceEBillWarning2Step", invoiceGenerateEBillPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceGenerateEBillRequestsWarning2())
                .inputChannel(memberInvoiceGenerateEBillRepliesWarning2())
                .build();
    }


    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning2")
    public DirectChannel memberInvoiceGenerateEBillRequestsWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRequestsWarning2OutboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRequestsWarning2OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateEBillRequestsWarning2())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRequestsWarning2"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceGenerateEBillRepliesWarning2")
    public DirectChannel memberInvoiceGenerateEBillRepliesWarning2() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillWarning2InboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRepliesWarning2"))
                .channel(memberInvoiceGenerateEBillRepliesWarning2())
                .get();
    }





    public ColumnRangePartitioner invoiceGenerateGroupPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND REF_GROUP IS NULL AND TOTAL_AMOUNT > 0 ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoiceGenerateGroupWarning2() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceGenerateGroupWarning2")
                .partitioner("updateInvoiceGroupWarning2Step", invoiceGenerateGroupPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceGenerateGroupWarning2Requests())
                .inputChannel(memberInvoiceGenerateGroupWarning2Replies())
                .build();
    }


    @Bean(name = "memberInvoiceGenerateGroupWarning2Requests")
    public DirectChannel memberInvoiceGenerateGroupWarning2Requests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning2OutboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning2OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateGroupWarning2Requests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning2Requests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceGenerateGroupWarning2Replies")
    public DirectChannel memberInvoiceGenerateGroupWarning2Replies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupWarning2InboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupWarning2InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateGroupWarning2Replies"))
                .channel(memberInvoiceGenerateGroupWarning2Replies())
                .get();
    }


    @Bean
    public Step masterStepGroupInvoicemember() {
        return this.masterStepBuilderFactory.get("masterStepGroupInvoicemember")
                .partitioner("groupInvoiceMemberWarning2", groupInvoiceMemberWarning2Partitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(groupInvoiceMemberWarning2JobRequests())
                .inputChannel(groupInvoiceMemberWarning2JobReplies())
                .build();
    }

    public ColumnRangePartitioner groupInvoiceMemberWarning2Partitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  AND INVOICE_TYPE = 1 AND TOTAL_AMOUNT > 0 AND STATUS !='PAYMENT_SUCCESS' ");
        return columnRangePartitioner;
    }


    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "groupInvoiceMemberWarning2JobRequests")
    public DirectChannel groupInvoiceMemberWarning2JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceMemberWarning2JobRequestsOutboundFlow")
    public IntegrationFlow groupInvoiceMemberWarning2JobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(groupInvoiceMemberWarning2JobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("groupInvoiceMemberWarning2JobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "groupInvoiceMemberWarning2JobReplies")
    public DirectChannel groupInvoiceMemberWarning2JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "groupInvoiceMemberWarning2JobRepliesInboundFlow")
    public IntegrationFlow groupInvoiceMemberWarning2JobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("groupInvoiceMemberWarning2JobReplies"))
                .channel(groupInvoiceMemberWarning2JobReplies())
                .get();
    }


    @Bean
    public Job memberWarning2Job() {
        return this.jobBuilderFactory.get("memberWarning2Job")
                .incrementer(new RunIdIncrementer())
                .start(masterStepDeleteKeyRadisInvoiceMember())
                .next(masterStepGroupInvoicemember())
                .next(stepGenerateInvoiceMemberGroup())
                .next(masterStepMemberWarning2())
                .next(masterStepMemberInvoiceGenerateEBillWarning2()) // generate ebill
                .next(masterStepMemberInvoiceGenerateGroupWarning2()) // gen group ref
                .build();
    }

    @Bean
    public Step stepGenerateInvoiceMemberGroup() {
        return this.steps.get("generateInvoiceMemberGroup")
                .transactionManager(transactionManager)
                .tasklet(new GenerateInvoiceMemberGroup(redisService,invoiceJdbcRepository,invoiceMemberService,insertInvoiceByStagingRepository,operationFeeAmount))
                .build();
    }

    @Bean
    public Step masterStepDeleteKeyRadisInvoiceMember() {
        return this.steps.get("masterStepDeleteKeyRadisInvoiceMember")
                .tasklet(new MasterStepDeleteKeyRadisInvoiceMember(redisService))
                .build();
    }


    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }

}
