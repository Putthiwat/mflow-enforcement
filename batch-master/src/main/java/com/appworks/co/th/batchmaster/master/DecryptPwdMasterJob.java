package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;

@Configuration
public class DecryptPwdMasterJob {

    @Autowired
    @Qualifier("oracleSchemaDataSource")
    private DataSource dataSource;

    @Value("${service-schema.customer}")
    private String customerSchema;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean
    public DirectChannel decryptRequests() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow decryptOutboundFlow(@Qualifier("connectionFactory") ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(decryptRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("decrypt.requests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean
    public DirectChannel decryptReplies() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow decryptInboundFlow(@Qualifier("connectionFactory")ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("decrypt.replies"))
                .channel(decryptReplies())
                .get();
    }

    public ColumnRangePartitioner decryptPartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(this.dataSource);
        columnRangePartitioner.setTable(customerSchema + ".MF_CUST_VISA_MASTER_CARD");
        columnRangePartitioner.setWhere("WHERE RETRY > 0 AND RETRY <= 3 AND DELETE_FLAG = 0");
        return columnRangePartitioner;
    }

    @Bean
    public Step decryptMasterStep() {
        return this.masterStepBuilderFactory.get("decryptMasterStep")
                .partitioner("decryptPwdStep", this.decryptPartitioner())
                .gridSize(4)
                .outputChannel(decryptRequests())
                .inputChannel(decryptReplies())
                .build();
    }

    @Bean
    public Job decryptJob() {
        return this.jobBuilderFactory.get("decryptJob")
                .incrementer(new RunIdIncrementer())
                .start(decryptMasterStep())
                .build();
    }

}
