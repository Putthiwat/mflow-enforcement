package com.appworks.co.th.batchmaster.oracle.repository;


import com.appworks.co.th.batchmaster.oracle.entity.InvoiceNonMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface InvoiceNonMemberRepository extends JpaRepository<InvoiceNonMember, Long> {
}
