package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.commons.FormatUtils;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.Date;

@Configuration
public class ReconcileInvoiceNonmemberMasterJob {

    @Value("${service-schema.invoice}")
    private String invoiceSchema;

    @Autowired
    @Qualifier("oracleSchemaDataSource")
    private DataSource dataSource;

    @Value("${reconcile-invoice-nonmember.date.frequency}")
    private String reconcileDate;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    public ColumnRangePartitioner reconcileNonmemberPartitioner() {

//        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//        String yesterday = FormatUtils.toString(DateUtils.addDays(new Date(), -1) ,dateFormat);
//        String startOfDay = yesterday + " 00:00:00";
//        String endOfDay = yesterday + " 23:59:59";
//
//        if(!"ALL".equals(reconcileDate)){
//            startOfDay = reconcileDate + " 00:00:00";
//            endOfDay = reconcileDate + " 23:59:59";
//        }

        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("ID");
        columnRangePartitioner.setDataSource(this.dataSource);
        columnRangePartitioner.setTable(invoiceSchema + ".MF_INVOICE_NONMEMBER I");
//        columnRangePartitioner.setWhere("WHERE I.DELETE_FLAG = 0 AND I.TOTAL_AMOUNT > 0 AND I.INVOICE_TYPE !=3 AND I.CREATE_DATE >= TO_TIMESTAMP('" + startOfDay + "', 'dd-mm-yyyy hh24:mi:ss') AND I.CREATE_DATE <= TO_TIMESTAMP('" + endOfDay + "', 'dd-mm-yyyy hh24:mi:ss')");
        columnRangePartitioner.setWhere("WHERE I.DELETE_FLAG = 0 AND I.TOTAL_AMOUNT > 0 AND I.INVOICE_TYPE !=3 ");
        return columnRangePartitioner;
    }

    @Bean
    public Step reconcileNonmemberMasterStep() {
        return this.masterStepBuilderFactory.get("reconcileNonmemberMasterStep")
                .partitioner("reconcileNonmemberWorkerStep", this.reconcileNonmemberPartitioner())
                .gridSize(4)
                .outputChannel(this.reconcileNonmemberRequests())
                .inputChannel(this.reconcileNonmemberReplies())
                .build();
    }

    @Bean
    public Job reconcileInvoiceNonmemberJob(Step reconcileNonmemberMasterStep) {
        return this.jobBuilderFactory.get("reconcileInvoiceNonmemberJob")
                .incrementer(new RunIdIncrementer())
                .start(reconcileNonmemberMasterStep)
                .build();
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean
    public DirectChannel reconcileNonmemberRequests() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow reconcileNonmemberOutboundFlow(@Qualifier("connectionFactory") ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(this.reconcileNonmemberRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("reconcile.nonmember.requests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean
    public DirectChannel reconcileNonmemberReplies() {
        return new DirectChannel();
    }

    @Bean
    public IntegrationFlow reconcileNonmemberInboundFlow(@Qualifier("connectionFactory")ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("reconcile.nonmember.replies"))
                .channel(this.reconcileNonmemberReplies())
                .get();
    }

}
