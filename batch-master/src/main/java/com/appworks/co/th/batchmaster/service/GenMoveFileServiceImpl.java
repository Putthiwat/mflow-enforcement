package com.appworks.co.th.batchmaster.service;

import com.appworks.co.th.batchmaster.config.prop.FTPProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class GenMoveFileServiceImpl implements GenMoveFileService {
    @Autowired
    private FTPProperties ftpProperties;

    @Autowired
    @Qualifier("FTPThreadPoolTaskExecutor")
    ThreadPoolTaskExecutor taskExecutor;

    @Override
    public CompletableFuture<Void> saveFileAsync(String filename, InputStream inputStream, String datePath,
                                                 String subPath) {
        return CompletableFuture.runAsync(() -> saveFile(filename, inputStream, datePath, subPath),
                taskExecutor);
    }

    @Override
    public boolean saveFile(String filename, InputStream inputStream,  String datePath,
                            String subPath) {
        log.info("Start save file {}", filename);
        FTPClient ftpClient = null;
        boolean result = false;
        String path = String.format("%s/%s/%s", ftpProperties.getDirectory(), datePath, subPath);
        log.info("Start save file at path {}", path);
        try {
            ftpClient = new FTPClient();
            ftpClient.connect(ftpProperties.getServer(), ftpProperties.getPort());
            ftpClient.login(ftpProperties.getUsername(), ftpProperties.getPassword());
            ftpClient.enterLocalPassiveMode();
            ftpClient.makeDirectory(path);
            ftpClient.changeWorkingDirectory(path);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            result = ftpClient.storeFile(filename, inputStream);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        } finally {

            if (ftpClient != null) {
                try {
                    Thread.sleep(5000);
                    ftpClient.disconnect();
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
        }
        log.info("End save file {} : result {}", filename, result);
        return result;
    }


}
