package com.appworks.co.th.batchmaster.oracle.repository;

import com.appworks.co.th.batchmaster.exception.InsertInvoiceException;
import com.appworks.co.th.batchmaster.oracle.entity.Invoice;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceColor;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceDetail;
import com.appworks.co.th.batchmaster.oracle.entity.InvoiceEvidence;
import com.appworks.co.th.batchmaster.utils.DateUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Transactional(value = "oracleTransactionManager")
@Repository
public class InsertInvoiceMemberRepositorylmpl implements InsertInvoiceMemberRepository {
    private static final Logger logger = LoggerFactory.getLogger(InsertInvoiceMemberRepositorylmpl.class);

    @Autowired
    @Qualifier("oracleNamedParameterJdbcTemplate")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceByStagingRepository(Invoice invoice) throws InsertInvoiceException {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("address", invoice.getAddress());
        paramMap.put("collectionAmount", invoice.getCollectionAmount());
        paramMap.put("customerId", invoice.getCustomerId());
        paramMap.put("discount", invoice.getDiscount());
        paramMap.put("dueDate", invoice.getDueDate() != null ? new java.sql.Date(invoice.getDueDate().getTime()) : "");
        paramMap.put("errorMessage", invoice.getErrorMessage());
        paramMap.put("feeAmount", invoice.getFeeAmount());
        paramMap.put("fineAmount", invoice.getFineAmount());
        paramMap.put("fullName", invoice.getFullName());
        paramMap.put("hqCode", invoice.getHqCode());
        paramMap.put("invoiceNo", invoice.getInvoiceNo());
        paramMap.put("invoiceNoRef", invoice.getInvoiceNoRef());
        paramMap.put("invoiceType", invoice.getInvoiceType());
        paramMap.put("issueDate", new java.sql.Date(invoice.getIssueDate().getTime()));
        paramMap.put("paymentDate", invoice.getPaymentDate() != null ? new java.sql.Date(invoice.getPaymentDate().getTime()) : "");
        paramMap.put("plate1", invoice.getPlate1());
        paramMap.put("plate2", invoice.getPlate2());
        paramMap.put("printFlag", invoice.getPrintFlag());
        paramMap.put("province", invoice.getProvince());
        paramMap.put("status", invoice.getStatus());
        paramMap.put("totalAmount", invoice.getTotalAmount());
        paramMap.put("transactionType", invoice.getTransactionType());
        paramMap.put("vat", invoice.getVat());
        paramMap.put("brand", invoice.getBrand());
        paramMap.put("eBillFileId", invoice.getEBillFileId());
        paramMap.put("receiptFileId", invoice.getReceiptFileId());
        paramMap.put("ref1", invoice.getRef1());
        paramMap.put("ref2", invoice.getRef2());
        paramMap.put("paymentChannel", invoice.getPaymentChannel());
        paramMap.put("vehicleCode", invoice.getVehicleCode());
        paramMap.put("vehicleType", invoice.getVehicleType());
        paramMap.put("tranxID", "");
        paramMap.put("invoiceChannel", invoice.getInvoiceChannel());
        paramMap.put("refGroupInvoice", "");
        paramMap.put("payCount", 0);
        paramMap.put("receipt", "");
        paramMap.put("auditFlag", "N");
        paramMap.put("operationFee", invoice.getOperationFee());
        paramMap.put("noticeEbill", "N");
        paramMap.put("noticeReceipt", "N");
        paramMap.put("voidFlag", 0);
//        sql.append(" INSERT INTO MF_INVOICE (ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, ADDRESS, COLLECTION_AMOUNT, CUSTOMER_ID, DISCOUNT, DUE_DATE, ERROR_MESSAGE, FEE_AMOUNT, FINE_AMOUNT, FULL_NAME, HQ_CODE, INVOICE_NO, INVOICE_NO_REF, INVOICE_TYPE, ISSUE_DATE, PAYMENT_DATE, PLATE1, PLATE2, PRINT_FLAG, PROVINCE, STATUS, TOTAL_AMOUNT, TRANSACTION_TYPE, VAT, BRAND, E_BILL_FILE_ID, RECEIPT_FILE_ID, REF_GROUP, REF2, PAYMENT_CHANNEL, VEHICLE_CODE, TRANX_ID, INVOICE_CHANNEL, REF_GROUP_INVOICE, PAY_COUNT, RECEIPT_NO, AUDIT_FLAG, OPERATION_FEE, NOTICE_EBILL, NOTICE_RECEIPT, VOID_FLAG ,VEHICLE_TYPE) ");
//        sql.append(" VALUES(SEQ_MF_INVOICE.nextval, 'System', 'SYSTEM', 'System', sysdate, :address ," +
//                " :collectionAmount , :customerId , :discount , :dueDate  , :errorMessage , :feeAmount , :fineAmount ," +
//                " :fullName , :hqCode , :invoiceNo  , :invoiceNoRef , :invoiceType , :issueDate , :paymentDate , :plate1 , :plate2 ," +
//                " :printFlag , :province , :status , :totalAmount , :transactionType  , :vat , :brand , :eBillFileId , :receiptFileId ," +
//                " :ref1 , :ref2 , :paymentChannel , :vehicleCode , :tranxID , :invoiceChannel , :refGroupInvoice , :payCount , :receipt ," +
//                " :auditFlag , :operationFee , :noticeEbill , :noticeReceipt , :voidFlag , :vehicleType )");

        sql.append(" INSERT INTO MF_INVOICE (ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, ADDRESS, COLLECTION_AMOUNT, CUSTOMER_ID, DISCOUNT, DUE_DATE, ERROR_MESSAGE, FEE_AMOUNT, FINE_AMOUNT, FULL_NAME, HQ_CODE, INVOICE_NO, INVOICE_NO_REF, INVOICE_TYPE, ISSUE_DATE, PAYMENT_DATE, PLATE1, PLATE2, PRINT_FLAG, PROVINCE, STATUS, TOTAL_AMOUNT, TRANSACTION_TYPE, VAT, BRAND, E_BILL_FILE_ID, RECEIPT_FILE_ID, REF_GROUP, REF2, PAYMENT_CHANNEL, VEHICLE_CODE, TRANX_ID, INVOICE_CHANNEL, REF_GROUP_INVOICE, PAY_COUNT, RECEIPT_NO, AUDIT_FLAG, OPERATION_FEE, NOTICE_EBILL, NOTICE_RECEIPT , VEHICLE_TYPE) ");
        sql.append(" VALUES(SEQ_MF_INVOICE.nextval, 'System', 'SYSTEM', 'System', sysdate, :address ," +
                " :collectionAmount , :customerId , :discount , :dueDate  , :errorMessage , :feeAmount , :fineAmount ," +
                " :fullName , :hqCode , :invoiceNo  , :invoiceNoRef , :invoiceType , :issueDate , :paymentDate , :plate1 , :plate2 ," +
                " :printFlag , :province , :status , :totalAmount , :transactionType  , :vat , :brand , :eBillFileId , :receiptFileId ," +
                " :ref1 , :ref2 , :paymentChannel , :vehicleCode , :tranxID , :invoiceChannel , :refGroupInvoice , :payCount , :receipt ," +
                " :auditFlag , :operationFee , :noticeEbill , :noticeReceipt  , :vehicleType )");
        namedParameterJdbcTemplate.update(sql.toString(), paramMap);
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceDetailByStagingRepository(List<InvoiceDetail> invoiceDetailList) throws InsertInvoiceException {
        for (InvoiceDetail invoiceDetail : invoiceDetailList) {
            int value = getInvoiceDetailByStagingRepository(invoiceDetail);
            if (value > 0) {
                logger.info("get Id of getInvoiceDetailByStagingRepository  :  {} ", value);
                this.insertInvoiceEvidenceRepository(value, invoiceDetail.getEvidences());
            }
        }
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public int getInvoiceDetailByStagingRepository(InvoiceDetail invoiceDetailList) throws InsertInvoiceException {
        String insertSql = " INSERT INTO MF_INVOICE_DETAIL\n" +
                "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE,  COLLECTION_AMOUNT, DEST_HQ_CODE, DEST_LANE_CODE, DEST_PLAZA_CODE, DISCOUNT, FEE_AMOUNT, FINE_AMOUNT, HQ_CODE, LANE_CODE, ORIGIN_TRAN_TYPE, PLATE1, PLATE2, PLAZA_CODE, PROVINCE, TOTAL_AMOUNT, TRANSACTION_DATE, TRANSACTION_ID, VAT, VEHICLE_WHEEL, INVOICE_NO, RAW_FEE, OPERATION_FEE,  FEE_AMOUNT_OLD)\n" +
                "VALUES(SEQ_MF_INVOICE_DETAIL.nextval, 'System', 'SYSTEM', 'System', sysdate , :collectionAmount  , :destHqCode , :destLaneCode , :destPlazaCod , :discount , :feeAmount  , :fineAmount , :hqCode , :laneCode  , :originTranType , :plate1 , :plate2 , :plazaCode , :province , :totalAmount  , :transactionDate , :transactionId  , :vat , :vehicleWheel , :invoiceNo, :rawFee, :operationFee, :feeAmountOld)";
        MapSqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("collectionAmount", invoiceDetailList.getCollectionAmount())
                .addValue("destHqCode", invoiceDetailList.getDestHqCode())
                .addValue("destLaneCode", invoiceDetailList.getDestLaneCode())
                .addValue("destPlazaCod", invoiceDetailList.getDestPlazaCode())
                .addValue("discount", invoiceDetailList.getDiscount())
                .addValue("feeAmount", invoiceDetailList.getFeeAmount())
                .addValue("fineAmount", invoiceDetailList.getFineAmount())
                .addValue("hqCode", invoiceDetailList.getHqCode())
                .addValue("laneCode", invoiceDetailList.getLaneCode())
                .addValue("originTranType", invoiceDetailList.getOriginTranType())
                .addValue("plate1", invoiceDetailList.getPlate1())
                .addValue("plate2", invoiceDetailList.getPlate2())
                .addValue("plazaCode", invoiceDetailList.getPlazaCode())
                .addValue("province", invoiceDetailList.getProvince())
                .addValue("totalAmount", invoiceDetailList.getTotalAmount())
                .addValue("transactionDate", DateUtils.toTimestamp(invoiceDetailList.getTransactionDate()))
                .addValue("transactionId", invoiceDetailList.getTransactionId())
                .addValue("vat", invoiceDetailList.getVat())
                .addValue("vehicleWheel", invoiceDetailList.getVehicleWheel())
                .addValue("invoiceNo", invoiceDetailList.getInvoice().getInvoiceNo())
                .addValue("rawFee", invoiceDetailList.getRawFee())
                .addValue("operationFee", invoiceDetailList.getOperationFee())
                .addValue("feeAmountOld",invoiceDetailList.getFeeAmountOld());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        namedParameterJdbcTemplate.update(insertSql, parameters, keyHolder, new String[]{"ID"});
        int id = keyHolder.getKey() != null ? keyHolder.getKey().intValue() : 0;
        return id;
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceColorRepository(List<InvoiceColor> colorsList) throws InsertInvoiceException {
        for (InvoiceColor invoiceColor : colorsList) {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("code", invoiceColor.getCode());
            paramMap.put("invoiceNo", invoiceColor.getInvoice().getInvoiceNo());
            sql.append("INSERT INTO MF_INVOICE_VHC_COLOR\n" +
                    "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, CODE, INVOICE_NO)\n" +
                    "VALUES(SEQ_MF_INVOICE_COLOR.nextval, 'System', 'SYSTEM', 'System', sysdate  , :code , :invoiceNo  )");
            namedParameterJdbcTemplate.update(sql.toString(), paramMap);
        }
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceEvidenceRepository(int detailId, List<InvoiceEvidence> evidenceList) throws InsertInvoiceException {
        for (InvoiceEvidence invoiceEvidence : evidenceList) {
            StringBuilder sql = new StringBuilder();
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("file", invoiceEvidence.getFile());
            paramMap.put("transactionId", invoiceEvidence.getTransactionId());
            paramMap.put("type", invoiceEvidence.getType());
            paramMap.put("id", detailId);
            sql.append("INSERT INTO MF_INVOICE_EVIDENCE\n" +
                    "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, FILE_ID, TRANSACTION_ID, TYPE, INVOICE_DETAIL_ID)\n" +
                    "VALUES(SEQ_MF_INVOICE_EVI.nextval, 'System', 'SYSTEM', 'System', sysdate , :file , :transactionId , :type , :id ) ");
            namedParameterJdbcTemplate.update(sql.toString(), paramMap);
        }
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void updateInvoiceNoTypeOfMember(int invoiceType, String updateBy, String invoiceNo) throws InsertInvoiceException {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("invoiceType", invoiceType);
        paramMap.put("updateBy", updateBy);
        paramMap.put("invoiceNo", invoiceNo);
        sql.append(" update MF_INVOICE ");
        sql.append(" set INVOICE_TYPE = :invoiceType , UPDATE_BY = :updateBy ");
        sql.append(" where INVOICE_NO = :invoiceNo and  DELETE_FLAG =0 ");
        namedParameterJdbcTemplate.update(sql.toString(), paramMap);
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED)
    public void insertInvoiceNoMemberOld(String invoiceNo, String invoiceNoOld) throws InsertInvoiceException {
        StringBuilder sql = new StringBuilder();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("invoice", invoiceNo);
        paramMap.put("invoiceOld", invoiceNoOld);
        sql.append("INSERT INTO MF_INVOICE_HISTORY\n" +
                "(ID, CREATE_BY, CREATE_BY_ID, CREATE_CHANNEL, CREATE_DATE, INVOICE_NO, INVOICE_OLD_NO)\n" +
                "VALUES(SEQ_MF_INVOICE_HISTORY.nextval, 'System', 'SYSTEM', 'System', sysdate, :invoice  , :invoiceOld )");
        namedParameterJdbcTemplate.update(sql.toString(), paramMap);
    }

    @Override
    @Transactional(value = "oracleTransactionManager",propagation = Propagation.REQUIRED, rollbackFor = InsertInvoiceException.class)
    public boolean insertInvoice(Invoice invoice,List<String> invoiceNoOlds) throws InsertInvoiceException {

        insertInvoiceByStagingRepository(invoice);

        if (ObjectUtils.isNotEmpty(invoice.getDetails())) {
            insertInvoiceDetailByStagingRepository(invoice.getDetails());
        }

        if (ObjectUtils.isNotEmpty(invoice.getColors())) {
            logger.info("Data insert invoice member : {}", invoice);
            insertInvoiceColorRepository(invoice.getColors());
        }

        if (invoiceNoOlds!=null && invoiceNoOlds.size()>0) {
            for(String invoiceNoOld: invoiceNoOlds){
                updateInvoiceNoTypeOfMember(3, "System", invoiceNoOld);
                insertInvoiceNoMemberOld(invoice.getInvoiceNo(),invoiceNoOld);
            }
        }

        return true;
    }

}
