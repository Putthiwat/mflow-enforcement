package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class EasyPassFTPFileMasterJob {
    public final Logger log = LoggerFactory.getLogger(this.getClass());
    private final JobBuilderFactory jobBuilderFactory;
    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public EasyPassFTPFileMasterJob(JobBuilderFactory jobBuilderFactory,
                                 RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "easyPassFTPFileJobRequests")
    public DirectChannel easyPassFTPFileJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "easyPassFTPFileJobRequestsOutboundFlow")
    public IntegrationFlow easyPassFTPFileJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(easyPassFTPFileJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("easyPassFTPFileJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "easyPassFTPFileJobReplies")
    public DirectChannel easyPassFTPFileJobReplies() {

        return new DirectChannel();
    }

    @Bean(name = "easyPassFTPFileJobRepliesInboundFlow")
    public IntegrationFlow easyPassFTPFileJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("easyPassFTPFileJobReplies"))
                .channel(easyPassFTPFileJobReplies())
                .get();
    }

    public ColumnRangePartitioner easyPassFTPPartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  and invoice_type in (0,1,2)  and status in (\'PAYMENT_WAITING\', \'PAYMENT_FAILED\') ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepEasyPassFTPFile() {
        return this.masterStepBuilderFactory.get("masterStepEasyPassFTPFile")
                .partitioner("easyPassFTPFileStep", easyPassFTPPartitioner())
                .gridSize(1)
                .outputChannel(easyPassFTPFileJobRequests())
                .inputChannel(easyPassFTPFileJobReplies())
                .build();
    }

    @Bean
    public Job easyPassFTPFileJob() {
        return this.jobBuilderFactory.get("easyPassFTPFileJob")
                .incrementer(new RunIdIncrementer())
                .start(masterStepEasyPassFTPFile())
                .build();
    }
}
