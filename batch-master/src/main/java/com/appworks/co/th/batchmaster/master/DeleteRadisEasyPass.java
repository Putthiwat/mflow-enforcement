package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.ArrayList;
import java.util.Set;

import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_EASYPASS;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.TOTAL_AMOUNT_EASYPASS;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.TOTAL_RECORD_EASYPASS;

public class DeleteRadisEasyPass implements Tasklet {
    private RedisService redisService;

    public static final Logger log = LoggerFactory.getLogger(DeleteRadisEasyPass.class);


    public DeleteRadisEasyPass(RedisService mredisService) {
        redisService = mredisService;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception
    {
        log.info("open delete radis easypass");
        ArrayList<String> arrayListCard = new ArrayList<String>();
        arrayListCard.add(BATCH_EASYPASS + ":" + "*");
        arrayListCard.add(TOTAL_RECORD_EASYPASS + ":" + "*"+ ":" + "*");
        arrayListCard.add(TOTAL_AMOUNT_EASYPASS + ":" + "*"+ ":" + "*");
        try {
            for (String keyPattern: arrayListCard) {
                Set<String> keys = redisService.getKeysWithPattern(keyPattern);
                if (keys != null && keys.size() > 0) {
                    for (String key : keys) {
                        redisService.delete(key);
                    }
                }
            }
        }catch (Exception e){
            log.error("Error Radis Of EasyPass : {}",e.getMessage());
        }
        return RepeatStatus.FINISHED;
    }
}
