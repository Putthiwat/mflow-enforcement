// File location --> batch-master\enforceText\EnforcementData???.txt

package com.appworks.co.th.batchmaster.job;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.io.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.stream.Stream;

@Component
@AllArgsConstructor

public class EnforcementJob
{
    @Qualifier("oracleJdbcTemplate")

    private static int row = 0;
    private static int num = 0;
    private static int totalFee = 0;
    private static int totalColl = 0;
    private static int totalFine = 0;
    private final JdbcTemplate jdbcTemplate;
    private final DateTimeFormatter logD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final DateTimeFormatter logT = DateTimeFormatter.ofPattern("HH:mm:ss");
    private final DateTimeFormatter date = DateTimeFormatter.ofPattern("yyyyMMdd");
    private final DateTimeFormatter time = DateTimeFormatter.ofPattern("HH:mm");
    private final LocalDateTime now = LocalDateTime.now();
    private final List<String> cusAddr = new ArrayList<>();
    private final List<String> issDate = new ArrayList<>();
    private final List<String> cusType = new ArrayList<>();
    private final List<String> cusId = new ArrayList<>();
    private final List<String> cusName = new ArrayList<>();
    private final List<String> carNo1 = new ArrayList<>();
    private final List<String> carNo2 = new ArrayList<>();
    private final List<String> carPro = new ArrayList<>();
    private final List<String> dueDate = new ArrayList<>();
    private final List<String> dueTime = new ArrayList<>();
    private final List<String> serveId = new ArrayList<>();
    private final List<String> billFee = new ArrayList<>();
    private final List<String> billColl = new ArrayList<>();
    private final List<String> feeColl = new ArrayList<>();
    private final List<String> inDate = new ArrayList<>();
    private final List<String> billFine = new ArrayList<>();
    private final List<String> fecoFine = new ArrayList<>();
    private final List<String> invNo = new ArrayList<>();
    private final List<String> invRef = new ArrayList<>();
    private final List<String> billTotal = new ArrayList<>();
    private final List<String> billOpera = new ArrayList<>();
    private final List<String> billVat = new ArrayList<>();
    private final List<String> tranDate = new ArrayList<>();
    private final List<String> tranTime = new ArrayList<>();
    private final List<String> hqName = new ArrayList<>();
    private final List<String> plazaName = new ArrayList<>();
    private final List<String> laneName = new ArrayList<>();
    private final List<String> notiType = new ArrayList<>();
    private final List<String> notiMail = new ArrayList<>();
    private final List<String> issTime = new ArrayList<>();
    private final List<String> lettDate = new ArrayList<>();
    private final List<String> qrCode = new ArrayList<>();
    private final List<String> cusNo = new ArrayList<>();

    @Scheduled(cron = "${enforcement.frequency}", zone = "Asia/Bangkok")

    private void mainEnforceJob()
    {
        System.out.println
        (
            logD.format(now) + " " + logT.format(now) +
            " [EnforcementJob.org.springframework.jms.listener.DefaultMessageListenerContainer#0-1]" +
            " SUCCESS o.s.j.l.DefaultMessageListenerContainer -" +
            " Job has been triggered according to schedule"
        );

        String query = "select a.ADDRESS, to_char(a.ISSUE_DATE, 'yyyymmdd'), \n" +
                       "a.CUSTOMER_TYPE, a.CUSTOMER_ID, a.FULL_NAME, b.PLATE1, b.PLATE2, \n" +
                       "c.DESCRIPTION, to_char(a.DUE_DATE + 12, 'yyyymmdd'), \n" +
                       "to_char(a.DUE_DATE + 12, 'hh:mm'), a.SERVICE_PROVIDER, a.FEE_AMOUNT, \n" +
                       "a.COLLECTION_AMOUNT,(a.FEE_AMOUNT + a.COLLECTION_AMOUNT), \n" +
                       "to_char(a.DUE_DATE, 'yyyymmdd'), a.FINE_AMOUNT, \n" +
                       "(a.FEE_AMOUNT + a.COLLECTION_AMOUNT + a.FINE_AMOUNT), a.INVOICE_NO, \n" +
                       "a.INVOICE_NO_REF, a.TOTAL_AMOUNT, a.OPERATION_FEE, a.VAT, \n" +
                       "to_char(b.TRANSACTION_DATE, 'yyyymmdd'), to_char(b.TRANSACTION_DATE, 'hh:mm'), \n" +
                       "d.NAME, e.NAME, f.NAME, g.TYPE, g.DESCRIPTION, to_char(a.ISSUE_DATE, 'hh:mm'), \n" +
                       "to_char(a.ISSUE_DATE + 3, 'yyyymmdd'), a.QR_CODE, \n" +

                       "case when h.CUSTOMER_TYPE = 'Individual' then '1' \n" +
                       "else '2' end as CUST_NO \n" +

                       "from MF_ENFORCEMENT a \n" +

                       "inner join MF_ENFORCEMENT_DETAIL b on a.INVOICE_NO = b.INVOICE_NO \n" +
                       "inner join MF_ENFORCEMENT_MASTER_V_OFFICE c on b.PROVINCE = c.CODE \n" +
                       "inner join MF_ENFORCEMENT_MASTER_HQ d on b.HQ_CODE = d.CODE \n" +
                       "inner join MF_ENFORCEMENT_MASTER_PLAZA e on b.PLAZA_CODE = e.CODE \n" +
                       "inner join MF_ENFORCEMENT_MASTER_LANE f on b.LANE_CODE = f.CODE \n" +
                       "inner join MF_CUST_NOTIFICATION_HISTORY g on a.CUSTOMER_ID = g.CUSTOMER_ID \n" +
                       "inner join MF_CUST_INFO h on a.CUSTOMER_ID = h.CUSTOMER_ID \n" +

                       "where a.STATUS = 'PAYMENT_WAITING' \n" +
                       "and a.CUSTOMER_ID is not null \n" +
                       "and a.TOTAL_AMOUNT <> 0";
                       // "and a.ISSUE_DATE = TO_DATE('20211007','yyyyMMdd')";

        List<String> data = jdbcTemplate.query
        (
            query, (rs, rowNum) ->
            {
                cusAddr.add(rs.getString(1));
                issDate.add(rs.getString(2));
                cusType.add(rs.getString(3));
                cusId.add(rs.getString(4));
                cusName.add(rs.getString(5));
                carNo1.add(rs.getString(6));
                carNo2.add(rs.getString(7));
                carPro.add(rs.getString(8));
                dueDate.add(rs.getString(9));
                dueTime.add(rs.getString(10));
                serveId.add(rs.getString(11));
                billFee.add(rs.getString(12));
                billColl.add(rs.getString(13));
                feeColl.add(rs.getString(14));
                inDate.add(rs.getString(15));
                billFine.add(rs.getString(16));
                fecoFine.add(rs.getString(17));
                invNo.add(rs.getString(18));
                invRef.add(rs.getString(19));
                billTotal.add(rs.getString(20));
                billOpera.add(rs.getString(21));
                billVat.add(rs.getString(22));
                tranDate.add(rs.getString(23));
                tranTime.add(rs.getString(24));
                hqName.add(rs.getString(25));
                plazaName.add(rs.getString(26));
                laneName.add(rs.getString(27));
                notiType.add(rs.getString(28));
                notiMail.add(rs.getString(29));
                issTime.add(rs.getString(30));
                lettDate.add(rs.getString(31));
                qrCode.add(rs.getString(32));
                cusNo.add(rs.getString(33));

                row = row + 1;
                return null;
            }
        );

        try
        {
            PrintWriter temp = new PrintWriter("enforceText/Data0.txt");

            temp.println("0000|" +
                         "EnforcementData" + date.format(now) + ".txt|" +
                         date.format(now) + "|" +
                         time.format(now) + "|" +
                         "2|EXACT");

            temp.flush();
            temp.close();
        }

        catch(IOException ex) {}

        while(num < row)
        {
            // Get table data
            List<String> stringList = Stream.of
            (
                "1000|" +
                issDate.get(num) + "|" +
                cusNo.get(num) + "|" +
                cusId.get(num) + "||" +
                cusName.get(num) + "|" +
                cusAddr.get(num) + "||||" +
                carNo1.get(num) + " " +
                carNo2.get(num) + "|" +
                carPro.get(num) + "|" +
                dueDate.get(num) + "|" +
                dueTime.get(num) + "|" +
                serveId.get(num) + "|" +
                billFee.get(num) + "|" +
                billColl.get(num) + "|" +
                feeColl.get(num) + "|" +
                inDate.get(num) + "|" +
                billFine.get(num) + "|" +
                fecoFine.get(num) + "|||\n" +

                "2000|" +
                issDate.get(num) + "|" +
                cusNo.get(num) + "|" +
                cusId.get(num) + "|" +
                cusName.get(num) + "|" +
                cusAddr.get(num) + "||||" +
                carNo1.get(num) + " " +
                carNo2.get(num) + "|" +
                carPro.get(num) + "||" +
                invNo.get(num) + "|" +
                invRef.get(num) + "|" +
                billTotal.get(num) + "|" +
                inDate.get(num) + "|" +
                billFee.get(num) + "|" +
                billColl.get(num) + "|" +
                billFine.get(num) + "|" +
                billOpera.get(num) + "|" +
                billTotal.get(num) + "|" +
                qrCode.get(num) + "|||" +
                billVat.get(num) + "\n" +

                "3000|" +
                issDate.get(num) + "|" +
                cusId.get(num) + "|" +
                carNo1.get(num) + " " +
                carNo2.get(num) + "|" +
                carPro.get(num) + "|" +
                tranDate.get(num) + "|" +
                tranTime.get(num) + "|" +
                hqName.get(num) + "|" +
                plazaName.get(num) + "|" +
                laneName.get(num) + "|" +
                billFee.get(num) + "\n" +

                "4000|" +
                lettDate.get(num) + "|" +
                cusNo.get(num) + "|" +
                cusId.get(num) + "|" +
                issDate.get(num) + "|" +
                issTime.get(num) + "|" +
                notiType.get(num) + "|" +
                "mflow@doh.go.th" + "|" +
                notiMail.get(num) + "|||||\n" +

                "8888|End of Customer"
            ).collect(Collectors.toList());

            String textContent = stringList.toString()
                                 .replace("[", "")
                                 .replace("]", "");

            try
            {
                // Write new file
                PrintWriter pw1 = new PrintWriter("enforceText/Data" + (num + 1) + ".txt");

                // Read previous file
                BufferedReader br1 = new BufferedReader(new FileReader("enforceText/Data" + num + ".txt"));
                String line = br1.readLine();

                // Import previous content
                while(line != null)
                {
                    pw1.println(line);
                    line = br1.readLine();
                }

                pw1.println(textContent);
                pw1.flush();
                br1.close();
                pw1.close();
                Files.deleteIfExists(Paths.get("enforceText/Data" + num + ".txt"));
            }

            catch(IOException ex) {}

            num = num + 1;
        }

        num = num - 1;

        try
        {
            // Write new file
            PrintWriter pw2 = new PrintWriter("enforceText/EnforcementData" + date.format(now) + ".txt");

            // Read previous file
            BufferedReader br2 = new BufferedReader(new FileReader("enforceText/Data" + (num + 1) + ".txt"));
            String line = br2.readLine();

            // Import previous content
            while(line != null)
            {
                pw2.println(line);
                line = br2.readLine();
            }

            for(int i = 0; i < num; i++)
            {
                totalFee = totalFee + Integer.parseInt(billFee.get(i));
            }

            for(int j = 0; j < num; j++)
            {
                totalColl = totalColl + Integer.parseInt(billColl.get(j));
            }

            for(int k = 0; k < num; k++)
            {
                totalFine = totalFine + Integer.parseInt(billFine.get(k));
            }

            pw2.println
            (
                "9999|" +
                "End of File|" +
                date.format(now) + "|" +
                totalFee + "|" +
                totalColl + "|" +
                totalFine + "|" + (num + 1) + "|" + (num + 1)
            );

            pw2.flush();
            br2.close();
            pw2.close();
            Files.deleteIfExists(Paths.get("enforceText/Data" + (num + 1) + ".txt"));
        }

        catch(IOException ex) {}

        System.out.println
        (
            logD.format(now) + " " + logT.format(now) +
            " [EnforcementJob.org.springframework.jms.listener.DefaultMessageListenerContainer#0-1]" +
            " SUCCESS o.s.j.l.DefaultMessageListenerContainer -" +
            " Job has been finished successfully"
        );
    }
}