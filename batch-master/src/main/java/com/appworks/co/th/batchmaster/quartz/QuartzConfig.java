package com.appworks.co.th.batchmaster.quartz;

import com.appworks.co.th.batchmaster.config.AutowiringSpringBeanJobFactory;
import com.appworks.co.th.batchmaster.master.RegenerateEBillMemberJob;
import com.appworks.co.th.batchmaster.master.ScheduledMoveFileTasks;
import org.quartz.*;
import org.quartz.spi.JobFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.boot.autoconfigure.batch.BasicBatchConfigurer;
import org.springframework.boot.autoconfigure.batch.BatchProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;


@Configuration
@ConditionalOnProperty(name = "memberinvoicejob.enabled")
public class QuartzConfig extends BasicBatchConfigurer {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private JobLocator jobLocator;

    private PlatformTransactionManager transactionManager;

    /**
     * Create a new {@link BasicBatchConfigurer} instance.
     *
     * @param properties                    the batch properties
     * @param dataSource                    the underlying data source
     * @param transactionManagerCustomizers transaction manager customizers (or
     *                                      {@code null})
     */
    protected QuartzConfig(BatchProperties properties, DataSource dataSource, TransactionManagerCustomizers transactionManagerCustomizers) {
        super(properties, dataSource, transactionManagerCustomizers);
    }

    @Override
    protected String determineIsolationLevel() {
        return "ISOLATION_READ_COMMITTED";
    }

    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
        JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
        jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
        return jobRegistryBeanPostProcessor;
    }

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }


    //    @Bean
//    public Trigger jobOneTrigger()
//    {
//        SimpleScheduleBuilder scheduleBuilder = SimpleScheduleBuilder
//                .simpleSchedule()
//                .withIntervalInSeconds(10)
//                .repeatForever();
//
//        return TriggerBuilder
//                .newTrigger()
//                .forJob(jobOneDetail())
//                .withIdentity("jobOneTrigger")
//                .withSchedule(scheduleBuilder)
//                .build();
//    }
    @Bean(name = "updateInvoiceStageDetail")
    public JobDetail updateInvoiceStageDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "invoiceStagingJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("invoiceStagingJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "updateInvoiceStageTrigger")
    public CronTriggerFactoryBean updateInvoiceStageTrigger(@Qualifier("updateInvoiceStageDetail") JobDetail jobDetail,
                                                            @Value("${memberinvoicejob.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    private static CronTriggerFactoryBean createCronTrigger(JobDetail jobDetail, String cronExpression) {
        CronTriggerFactoryBean factoryBean = new CronTriggerFactoryBean();
        factoryBean.setJobDetail(jobDetail);
        factoryBean.setCronExpression(cronExpression);
        factoryBean.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

        return factoryBean;
    }


    // ------------  move non mem staging

    @Bean(name = "updateInvoiceNonMemStageDetail")
    public JobDetail updateInvoiceNonMemStageDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "invoiceNonMemStagingJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("invoiceNonMemStagingJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "updateInvoiceNonMemStageTrigger")
    public CronTriggerFactoryBean updateInvoiceNonMemStageTrigger(@Qualifier("updateInvoiceNonMemStageDetail") JobDetail jobDetail,
                                                                  @Value("${nonmemberinvoicejob.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }


    // ------------  member warn 1
    @Bean(name = "memberWarning1Detail")
    public JobDetail memberWarning1Detail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "memberWarning1Job");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("memberWarning1Job")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "memberWarning1Trigger")
    public CronTriggerFactoryBean memberWarning1Trigger(@Qualifier("memberWarning1Detail") JobDetail jobDetail,
                                                        @Value("${memberwarning1.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //--------------------------


    // ------------  invoiceRegenerateMemberDetail
    @Bean(name = "invoiceRegenerateMemberDetail")
    public JobDetail invoiceRegenerateMemberDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "invoiceRegenerateMemberJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("invoiceRegenerateMemberJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "invoiceRegenerateMemberTrigger")
    public CronTriggerFactoryBean invoiceRegenerateMemberTrigger(@Qualifier("invoiceRegenerateMemberDetail") JobDetail jobDetail,
                                                                 @Value("${regenerate.invoice.member}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //--------------------------


    // ------------  invoiceRegenerateNonMemberDetail
    @Bean(name = "invoiceRegenerateNonMemberDetail")
    public JobDetail invoiceRegenerateNonMemberDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "invoiceRegenerateNonMemberJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("invoiceRegenerateNonMemberJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "invoiceRegenerateNonMemberTrigger")
    public CronTriggerFactoryBean invoiceRegenerateNonMemberTrigger(@Qualifier("invoiceRegenerateNonMemberDetail") JobDetail jobDetail,
                                                                 @Value("${regenerate.invoice.nonmember}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //--------------------------



    // ------------  member Invoice ProceedList BillTime
    @Bean(name = "memberInvoiceProceedListBillTimeDetail")
    public JobDetail memberInvoiceProceedListBillTimeDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "masterMemberInvoiceProceedListBillTimeJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("masterMemberInvoiceProceedListBillTimeJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "memberInvoiceProceedListBillTimeTrigger")
    public CronTriggerFactoryBean memberInvoiceProceedListBillTimeTrigger(@Qualifier("memberInvoiceProceedListBillTimeDetail") JobDetail jobDetail,
                                                                          @Value("${memberinvoiceproceedlistbilltime.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //--------------------------


    // ------------  member warn 2
    @Bean(name = "memberWarning2Detail")
    public JobDetail memberWarning2Detail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "memberWarning2Job");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("memberWarning2Job")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "memberWarning2Trigger")
    public CronTriggerFactoryBean memberWarning2Trigger(@Qualifier("memberWarning2Detail") JobDetail jobDetail,
                                                        @Value("${memberwarning2.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    //--------------------------

    // ------------  non member warn 1
    @Bean(name = "nonMemberWarning1Detail")
    public JobDetail nonMemberWarning1Detail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "nonMemberWarning1Job");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("nonMemberWarning1Job")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "nonMemberWarning1Trigger")
    public CronTriggerFactoryBean nonMemberWarning1Trigger(@Qualifier("nonMemberWarning1Detail") JobDetail jobDetail,
                                                           @Value("${nonmemberwarning1.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    //--------------------------

    // ------------  non member warn 2
    @Bean(name = "nonMemberWarning2Detail")
    public JobDetail nonMemberWarning2Detail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "nonMemberWarning2Job");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("nonMemberWarning2Job")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "nonMemberWarning2Trigger")
    public CronTriggerFactoryBean nonMemberWarning2Trigger(@Qualifier("nonMemberWarning2Detail") JobDetail jobDetail,
                                                           @Value("${nonmemberwarning2.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    //--------------------------

    // ------------  member warn duedate
    @Bean(name = "memberWarningDueDateDetail")
    public JobDetail memberWarningDueDateDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "memberWarningDueDateJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("memberWarningDueDateJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "memberWarningDueDateTrigger")
    public CronTriggerFactoryBean memberWarningDueDateTrigger(@Qualifier("memberWarningDueDateDetail") JobDetail jobDetail,
                                                              @Value("${memberwarningduedate.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    // ------------  MPass file FTP
    @Bean(name = "mPassFTPFileDetail")
    public JobDetail mPassFTPFileDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "mPassFTPFileJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("mPassFTPFileJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "mPassFTPFileTrigger")
    public CronTriggerFactoryBean mPassFTPFileTrigger(@Qualifier("mPassFTPFileDetail") JobDetail jobDetail,
                                                      @Value("${mpassftpfile.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    // ------------  MPass Repay
    @Bean(name = "mPassRepayDetail")
    public JobDetail mPassRepayDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "mPassRepayJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("mPassRepayJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "mPassRepayTrigger")
    public CronTriggerFactoryBean mPassRepayTrigger(@Qualifier("mPassRepayDetail") JobDetail jobDetail,
                                                    @Value("${mpassrepay.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    @Bean(name = "decryptJobDetail")
    public JobDetail decryptJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "decryptJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("decryptJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "decryptTrigger")
    public CronTriggerFactoryBean decryptTrigger(
            @Qualifier("decryptJobDetail") JobDetail jobDetail,
            @Value("${decrypt.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    @Bean(name = "reconcileInvoiceMemberJobDetail")
    public JobDetail reconcileInvoiceMemberJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "reconcileInvoiceMemberJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("reconcileInvoiceMemberJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "reconcileInvoiceMemberTrigger")
    public CronTriggerFactoryBean reconcileInvoiceMemberTrigger(
            @Qualifier("reconcileInvoiceMemberJobDetail") JobDetail jobDetail,
            @Value("${reconcile-invoice-member.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    @Bean(name = "reconcileInvoiceNonmemberJobDetail")
    public JobDetail reconcileInvoiceNonmemberJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "reconcileInvoiceNonmemberJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("reconcileInvoiceNonmemberJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "reconcileInvoiceNonmemberTrigger")
    public CronTriggerFactoryBean reconcileInvoiceNonmemberTrigger(
            @Qualifier("reconcileInvoiceNonmemberJobDetail") JobDetail jobDetail,
            @Value("${reconcile-invoice-nonmember.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    // ------------  EasyPass file FTP
    @Bean(name = "easyPassFTPFileDetail")
    public JobDetail easyPassFTPFileDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "easyPassFTPFileJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("easyPassFTPFileJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "easyPassFTPFileTrigger")
    public CronTriggerFactoryBean easyPassFTPFileTrigger(@Qualifier("easyPassFTPFileDetail") JobDetail jobDetail,
                                                         @Value("${easypassftpfile.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    // ------------  EasyPass Repay
    @Bean(name = "easyPassRepayDetail")
    public JobDetail easyPassRepayDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "easyPassRepayJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("easyPassRepayJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }

    @Bean(name = "easyPassRepayTrigger")
    public CronTriggerFactoryBean easyPassRepayTrigger(@Qualifier("easyPassRepayDetail") JobDetail jobDetail,
                                                       @Value("${easypassrepay.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    //----Audit Job----

    @Bean(name = "auditInvoiceMemberJobDetail")
    public JobDetail auditInvoiceMemberJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "auditInvoiceMemberTransactionJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("auditInvoiceMemberTransactionJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "auditInvoiceMemberTrigger")
    public CronTriggerFactoryBean auditInvoiceMemberTrigger(@Qualifier("auditInvoiceMemberJobDetail") JobDetail jobDetail,
                                                            @Value("${audit.invoice.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }

    @Bean(name = "auditInvoiceNonMemberJobDetail")
    public JobDetail auditInvoiceNonMemberJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "auditInvoiceNonMemberTransactionJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("auditInvoiceNonMemberTransactionJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "auditInvoiceNonMemberTrigger")
    public CronTriggerFactoryBean auditInvoiceNonMemberTrigger(@Qualifier("auditInvoiceNonMemberJobDetail") JobDetail jobDetail,
                                                               @Value("${audit.invoice.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //----------------------------------------------------------


    //------------------------- Move File---------------------------------
    @Bean(name = "moveInvoicePaymentFileJobDetail")
    public JobDetail moveInvoicePaymentFileJobDetail() {
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "moveInvoiePaymentFileJob");
        return JobBuilder.newJob(ScheduledMoveFileTasks.class)
                .withIdentity("moveInvoiePaymentFileJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "moveInvoicePaymentFileTrigger")
    public CronTriggerFactoryBean moveInvoicePaymentFileTrigger(@Qualifier("moveInvoicePaymentFileJobDetail") JobDetail jobDetail,
                                                                @Value("${move.file}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //----------------------------------------------------------

    //------------------------- Regenerate EBill File---------------------------------
    @Bean(name = "regenerateEbillFileMemberJobDetail")
    public JobDetail regenerateEbillFileMemberJobDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "invoiceRegenerateEBillMemberJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("invoiceRegenerateEBillMemberJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "regenerateEbillFileMemberJobTrigger")
    public CronTriggerFactoryBean regenerateEbillFileMemberJobTrigger(@Qualifier("regenerateEbillFileMemberJobDetail") JobDetail jobDetail,
                                                                      @Value("${regenerate.invoice.ebill.file.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //----------------------------------------------------------

    //------------------------- Regenerate EBill File Nonmember---------------------------------
    @Bean(name = "regenerateEbillFileNonMemberJobDetail")
    public JobDetail regenerateEbillFileNonMemberJobDetail() {
        //Set Job data map
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("jobName", "invoiceRegenerateEBillNonMemberJob");
        return JobBuilder.newJob(CustomQuartzJob.class)
                .withIdentity("invoiceRegenerateEBillNonMemberJob")
                .setJobData(jobDataMap)
                .storeDurably()
                .build();
    }


    @Bean(name = "regenerateEbillFileNonMemberJobTrigger")
    public CronTriggerFactoryBean regenerateEbillFileNonMemberJobTrigger(@Qualifier("regenerateEbillFileNonMemberJobDetail") JobDetail jobDetail,
                                                                         @Value("${regenerate.invoice.ebill.nonmember.file.frequency}") String frequency) {
        return createCronTrigger(jobDetail, frequency);
    }
    //----------------------------------------------------------


    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(

            @Qualifier("oracleDataSource") DataSource dataSource, JobFactory jobFactory
            , @Qualifier("updateInvoiceStageTrigger") Trigger updateInvoiceStageTrigger
            , @Qualifier("updateInvoiceNonMemStageTrigger") Trigger updateInvoiceNonMemStageTrigger
            , @Qualifier("memberWarning1Trigger") Trigger memberWarning1Trigger
            , @Qualifier("memberWarning2Trigger") Trigger memberWarning2Trigger
            , @Qualifier("nonMemberWarning1Trigger") Trigger nonMemberWarning1Trigger
            , @Qualifier("nonMemberWarning2Trigger") Trigger nonMemberWarning2Trigger
            , @Qualifier("memberWarningDueDateTrigger") Trigger memberWarningDueDateTrigger
            , @Qualifier("mPassFTPFileTrigger") Trigger mPassFTPFileTrigger
            , @Qualifier("mPassRepayTrigger") Trigger mPassRepayTrigger
            , @Qualifier("decryptTrigger") Trigger decryptTrigger
            , @Qualifier("reconcileInvoiceMemberTrigger") Trigger reconcileInvoiceMemberTrigger
            , @Qualifier("reconcileInvoiceNonmemberTrigger") Trigger reconcileInvoiceNonmemberTrigger
            , @Qualifier("easyPassFTPFileTrigger") Trigger easyPassFTPFileTrigger
            , @Qualifier("easyPassRepayTrigger") Trigger easyPassRepayTrigger
            , @Qualifier("auditInvoiceMemberTrigger") Trigger auditInvoiceMemberTrigger
            , @Qualifier("auditInvoiceNonMemberTrigger") Trigger auditInvoiceNonMemberTrigger
            , @Qualifier("moveInvoicePaymentFileTrigger") Trigger moveInvoicePaymentFileTrigger
            , @Qualifier("regenerateEbillFileMemberJobTrigger") Trigger regenerateEbillFileMemberJobTrigger
            , @Qualifier("regenerateEbillFileNonMemberJobTrigger") Trigger regenerateEbillFileNonMemberJobTrigger
            , @Qualifier("invoiceRegenerateMemberTrigger") Trigger invoiceRegenerateMemberTrigger
            ,  @Qualifier("invoiceRegenerateNonMemberTrigger") Trigger  invoiceRegenerateNonMemberTrigger
            //,@Qualifier("memberInvoiceProceedListBillTimeTrigger") Trigger memberInvoiceProceedListBillTimeTrigger
    ) throws IOException {
        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
        scheduler.setOverwriteExistingJobs(true);
        scheduler.setDataSource(dataSource);
        scheduler.setJobFactory(jobFactory);
        scheduler.setTriggers(
                updateInvoiceStageTrigger
                , updateInvoiceNonMemStageTrigger
                , memberWarning1Trigger
                , memberWarning2Trigger
                , nonMemberWarning1Trigger
                , nonMemberWarning2Trigger
                , memberWarningDueDateTrigger
                , mPassFTPFileTrigger
                , mPassRepayTrigger
                , decryptTrigger
                , reconcileInvoiceMemberTrigger
                , reconcileInvoiceNonmemberTrigger
                , easyPassFTPFileTrigger
                , easyPassRepayTrigger
                , auditInvoiceMemberTrigger
                , auditInvoiceNonMemberTrigger
                , moveInvoicePaymentFileTrigger
                , regenerateEbillFileMemberJobTrigger
                , regenerateEbillFileNonMemberJobTrigger
                , invoiceRegenerateMemberTrigger
                ,invoiceRegenerateNonMemberTrigger
        );
        scheduler.setQuartzProperties(quartzProperties());
        scheduler.setJobDetails(updateInvoiceStageDetail()
                , updateInvoiceNonMemStageDetail()
                , memberWarning1Detail()
                , memberWarning2Detail()
                , nonMemberWarning1Detail()
                , nonMemberWarning2Detail()
                , memberWarningDueDateDetail()
                , mPassFTPFileDetail()
                , mPassRepayDetail()
                , decryptJobDetail()
                , reconcileInvoiceMemberJobDetail()
                , reconcileInvoiceNonmemberJobDetail()
                , easyPassFTPFileDetail()
                , easyPassRepayDetail()
                , auditInvoiceMemberJobDetail()
                , auditInvoiceNonMemberJobDetail()
                , moveInvoicePaymentFileJobDetail()
                , regenerateEbillFileMemberJobDetail()
                , regenerateEbillFileNonMemberJobDetail()
                , invoiceRegenerateMemberDetail()
                , invoiceRegenerateNonMemberDetail()
        );
        scheduler.start();
        return scheduler;
    }

    @Bean
    public Properties quartzProperties
            () throws IOException {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        propertiesFactoryBean.afterPropertiesSet();
        return propertiesFactoryBean.getObject();
    }
}
