package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.JschConfig;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_ACCOUNT;
import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_BILL_CYCLE;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.*;

public class MoveFileAccount implements Tasklet {
    private JschConfig jschConfig;

    private String accountPayment;

    private String accountPaymentArchice;

    private RedisService redisService;

    public static final Logger log = LoggerFactory.getLogger(MoveFileAccount.class);

    private int limitTimeOut;

    public MoveFileAccount(JschConfig mjschConfig, String maccountPayment, String maccountPaymentArchice, RedisService mredisService, int mLimitTimeOut) {
        jschConfig = mjschConfig;
        accountPayment = maccountPayment;
        accountPaymentArchice = maccountPaymentArchice;
        redisService = mredisService;
        limitTimeOut = mLimitTimeOut;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        log.info("open move file card");
        ChannelSftp channelSftp = null;
        try {
            channelSftp = jschConfig.setupJsch();
            channelSftp.connect();
            channelSftp.cd(accountPayment);
            Vector<ChannelSftp.LsEntry> listFileCsv = channelSftp.ls("*.txt");
            for (ChannelSftp.LsEntry entry : listFileCsv) {
                String dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS").format(LocalDateTime.now());
                String path = accountPaymentArchice + dateTimeFormatter + "/" + entry.getFilename();
                channelSftp.cd(accountPaymentArchice);
                channelSftp.mkdir(dateTimeFormatter);
                channelSftp.cd(accountPayment);
                channelSftp.rename(accountPayment + entry.getFilename(), path);
            }
            channelSftp.disconnect();
        } catch (IOException | SftpException | JSchException e) {
            log.error("Error Move File Of Account : {}", e.getMessage());
        }
//        ArrayList<String> arrayListAccount = new ArrayList<String>();
//        arrayListAccount.add(BATCH_ACCOUNT + ":" + "*");
//        arrayListAccount.add(TOTAL_AMOUNT_ACCOUNT + ":" + "*" + ":" + "*");
//        arrayListAccount.add(TOTAL_RECORD_ACCOUNT + ":" + "*" + ":" + "*");
//        deleteRadis(arrayListAccount);

        String key = BATCH_ACCOUNT + ":" + dateFormatter();
        redisService.setKeyIncrementBySetValue(key, 1, limitTimeOut, TimeUnit.DAYS);

        return RepeatStatus.FINISHED;
    }


    private void deleteRadis(ArrayList<String> arrayListAccount) {
        try {
            for (String keyPattern : arrayListAccount) {
                Set<String> keys = redisService.getKeysWithPattern(keyPattern);
                if (keys != null && keys.size() > 0) {
                    for (String key : keys) {
                        redisService.delete(key);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error Radis Of Account : {}", e.getMessage());
        }
    }

    private String dateFormatter() {
        final DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String seq = df.format(new Date());
        return seq;
    }

}