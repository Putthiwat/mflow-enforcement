package com.appworks.co.th.batchmaster.commons;

public class Constant {

    public static class FilePayment{
        public static final String BATCH_ACCOUNT = "BATCH_ACCOUNT";
        public static final String BATCH_CARD = "BATCH_CARD";
        public static final String BATCH_BILL_CYCLE = "BATCH_BILL_CYCLE";
        public static final String BATCH_MPASS = "BATCH_MPASS";
        public static final String BATCH_EASYPASS = "BATCH_EASYPASS";
    }

    public static class SummaryPayment{
        public static final String TOTAL_AMOUNT_ACCOUNT = "TOTAL_AMOUNT_ACCOUNT";
        public static final String TOTAL_RECORD_ACCOUNT = "TOTAL_RECORD_ACCOUNT";
        public static final String TOTAL_RECORD_CARD = "TOTAL_RECORD_CARD";
        public static final String TOTAL_FILE_CARD = "TOTAL_FILE_CARD";
        public static final String TOTAL_AMOUNT_CARD = "TOTAL_AMOUNT_CARD";
        public static final String TOTAL_RECORD_BILL_CYCLE = "TOTAL_RECORD_BILL_CYCLE";
        public static final String TOTAL_AMOUNT_BILL_CYCLE = "TOTAL_AMOUNT_BILL_CYCLE";
        public static final String TOTAL_FILE_BILL_CYCLE = "TOTAL_FILE_BILL_CYCLE";
        public static final String TOTAL_RECORD_MPASS = "TOTAL_RECORD_MPASS";
        public static final String TOTAL_AMOUNT_MPASS = "TOTAL_AMOUNT_MPASS";
        public static final String TOTAL_RECORD_EASYPASS = "TOTAL_RECORD_EASYPASS";
        public static final String TOTAL_AMOUNT_EASYPASS = "TOTAL_AMOUNT_EASYPASS";
    }

    public static class generateKeyInvoiceNonmember {
        public static final String CREATE_INVOICE_NONMEMBER = "CREATE_INVOICE_NONMEMBER";
        public static final String CREATE_INVOICE_MEMBER = "CREATE_INVOICE_MEMBER";
    }

    public static class Status {
        public static final String MOVED = "MOVED";
        public static final String WAITING_TO_MOVE = "WAITING_TO_MOVE";
        public static final String PAYMENT_WAITING = "PAYMENT_WAITING";
        public static final String PAYMENT_FAILED = "PAYMENT_FAILED";
        public static final String PAYMENT_SUCCESS = "PAYMENT_SUCCESS";
        public static final String PAYMENT_CANCEL = "PAYMENT_CANCEL";
        public static final String PAYMENT_NOCHARGE = "PAYMENT_NOCHARGE";
        public static final String PAYMENT_INPROGRESS = "PAYMENT_INPROGRESS";
        public static final String BATCH_PROCESSING = "BATCH_PROCESSING";
    }


    public static class BillCycle{
        public static final String BILL_CYCLE = "BILL_CYCLE";
        public static final String BILL_TIME = "BILL_TIME";
    }
}
