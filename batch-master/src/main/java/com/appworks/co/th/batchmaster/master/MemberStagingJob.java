package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.commons.FormatUtils;
import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.config.JschConfig;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import com.appworks.co.th.batchmaster.service.RedisService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class MemberStagingJob {
    public static final Logger log = LoggerFactory.getLogger(MemberStagingJob.class);

    public DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;
    private final StepBuilderFactory steps;
    @Autowired
    private JschConfig jschConfig;

    @Value("${cardPayment.input}")
    private String cardPayment;

    @Value("${cardPayment.archice}")
    private String cardPaymentArchice;

    @Value("${account.payment.input}")
    private String accountPayment;

    @Value("${account.payment.archice}")
    private String accountPaymentArchice;

    @Value("${bill.payment.normal}")
    private String billPayment;

    @Value("${bill.payment.normal.archice}")
    private String billPaymentArchice;

    @Value("${account.limit.time.out}")
    private int accountLimitTimeOut;

    @Value("${cardinvoice.limit.time.out}")
    private int cardInvoiceLimitTimeOut;

    @Value("${cardInvoiceBill.limit.time.out}")
    private int cardInvoiceBillLimitTimeOut;

    @Value("#{'${day.bill.cycle}'.split(',')}")
    private List<Integer> dayOfBillCycle;

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;
    @Autowired
    private RedisService redisService;

    public MemberStagingJob(JobBuilderFactory jobBuilderFactory,
                            RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory,
                            StepBuilderFactory steps) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
        this.steps = steps;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "invoiceStagingJobRequests")
    public DirectChannel invoiceStagingJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceStagingJobRequestsOutboundFlow")
    public IntegrationFlow invoiceStagingJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceStagingJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceStagingJobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "invoiceStagingJobReplies")
    public DirectChannel invoiceStagingJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceStagingJobRepliesInboundFlow")
    public IntegrationFlow invoiceStagingJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceStagingJobReplies"))
                .channel(invoiceStagingJobReplies())
                .get();
    }


    @Bean(name = "memberInvoiceGenerateEBillRequests")
    public DirectChannel memberInvoiceGenerateEBillRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillRequestsOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateEBillRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateEBillRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceGenerateEBillReplies")
    public DirectChannel memberInvoiceGenerateEBillReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateEBillInboundFlow")
    public IntegrationFlow memberInvoiceGenerateEBillInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateEBillReplies"))
                .channel(memberInvoiceGenerateEBillReplies())
                .get();
    }

    @Bean(name = "memberInvoiceGenerateGroupRequests")
    public DirectChannel memberInvoiceGenerateGroupRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupOutboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceGenerateGroupRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceGenerateGroupRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceGenerateGroupReplies")
    public DirectChannel memberInvoiceGenerateGroupReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceGenerateGroupInboundFlow")
    public IntegrationFlow memberInvoiceGenerateGroupInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceGenerateGroupReplies"))
                .channel(memberInvoiceGenerateGroupReplies())
                .get();
    }


    @Bean(name = "invoiceMemberWriterFileGenerateRequests")
    public DirectChannel invoiceMemberWriterFileGenerateRequests() {
        return new DirectChannel();
    }


    @Bean(name = "invoiceMemberWriterFileGenerateOutboundFlow")
    public IntegrationFlow invoiceMemberWriterFileGenerateOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceMemberWriterFileGenerateRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceMemberWriterFileGenerateRequests"))
                .get();
    }


    @Bean(name = "invoiceMemberWriterFileGenerateReplies")
    public DirectChannel invoiceMemberWriterFileGenerateReplies() {
        return new DirectChannel();
    }


    @Bean(name = "memberInvoiceFailedGenerateInboundFlow")
    public IntegrationFlow memberInvoiceFailedGenerateInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceMemberWriterFileGenerateReplies"))
                .channel(invoiceMemberWriterFileGenerateReplies())
                .get();
    }


    public ColumnRangePartitioner invoiceStagePartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_STAGING");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND STATUS=\'WAITING_TO_MOVE\'");
//        columnRangePartitioner.setWhere("WHERE (DELETE_FLAG = 0 AND STATUS=\'WAITING_TO_MOVE\' AND invoice_channel = \'BILL_TIME\') OR (DELETE_FLAG = 0 AND STATUS=\'WAITING_TO_MOVE\' AND invoice_channel = \'BILL_CYCLE\' AND issue_date = to_date(\'" + currentDate() + "\',\'YYYY/MM/DD\'))");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoice() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoice")
                .partitioner("updateInvoiceStageStep", invoiceStagePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(invoiceStagingJobRequests())
                .inputChannel(invoiceStagingJobReplies())
                .build();
    }


    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "invoiceProceedListJobRequests")
    public DirectChannel invoiceProceedListJobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedListJobRequestsOutboundFlow")
    public IntegrationFlow invoiceProceedListJobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(invoiceProceedListJobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("invoiceProceedListJobRequests"))
                .get();
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "invoiceProceedListJobReplies")
    public DirectChannel invoiceProceedListJobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "invoiceProceedListJobRepliesInboundFlow")
    public IntegrationFlow invoiceProceedListJobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("invoiceProceedListJobReplies"))
                .channel(invoiceProceedListJobReplies())
                .get();
    }



    @Bean
    public Step masterStepMemberInvoiceProceedList() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceProceedList")
                .partitioner("invoiceProceedListStep", invoiceProceedListStagePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(invoiceProceedListJobRequests())
                .inputChannel(invoiceProceedListJobReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceProceedListStagePartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_PROCEED_LIST invoiceProceed");
        columnRangePartitioner.setWhere("WHERE invoiceProceed.COMMAND = 'InvoiceMemberCommand' AND invoiceProceed.STATE='APPROVAL_PENDING' ");
        return columnRangePartitioner;
    }


    public ColumnRangePartitioner invoiceGenerateEBillPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL AND INVOICE_TYPE = 0");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoiceGenerateEBill() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceGenerateEBill")
                .partitioner("updateInvoiceEBillStep", invoiceGenerateEBillPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceGenerateEBillRequests())
                .inputChannel(memberInvoiceGenerateEBillReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceGenerateGroupPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND REF_GROUP IS NULL AND INVOICE_TYPE = 0 AND TOTAL_AMOUNT >0 ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepMemberInvoiceGenerateGroup() {
        return this.masterStepBuilderFactory.get("masterStepMemberInvoiceGenerateGroup")
                .partitioner("updateInvoiceGroupStep", invoiceGenerateGroupPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceGenerateGroupRequests())
                .inputChannel(memberInvoiceGenerateGroupReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceMemberWriterFilePartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND STATUS IN (\'PAYMENT_FAILED\',\'PAYMENT_WAITING\') AND INVOICE_TYPE IN (0) AND TOTAL_AMOUNT > 0 ");

//        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND STATUS IN (\'PAYMENT_FAILED\',\'PAYMENT_WAITING\') AND INVOICE_TYPE IN (0) AND TOTAL_AMOUNT > 0 " +
//                " AND ISSUE_DATE <= TO_DATE(\'"+currentDate()+"\', 'YYYY/MM/DD') " +
//                " AND TO_DATE(\'"+currentDate()+"\', 'YYYY/MM/DD') <= DUE_DATE ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepWriterFileMemberInvoice() {
        return this.masterStepBuilderFactory.get("masterStepWriterFileMemberInvoice")
                .partitioner("moveFileInvoiceFailedStep", invoiceMemberWriterFilePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(invoiceMemberWriterFileGenerateRequests())
                .inputChannel(invoiceMemberWriterFileGenerateReplies())
                .build();
    }


    @Bean
    public Job invoiceStagingJob() {
        return this.jobBuilderFactory.get("invoiceStagingJob")
                .incrementer(new RunIdIncrementer())
                .start(stepMoveFileAccount())
                .next(stepMoveFileCard())
                .next(stepMoveFileBill())
                .next(stepDeleteRadisMpass())
                .next(stepDeleteRadisEasyPass())
                .next(masterStepMemberInvoice())// move staging
                .next(masterStepWriterFileMemberInvoice()) // gen invoice failed
                .next(masterStepMemberInvoiceGenerateEBill()) // generate ebill
                .next(masterStepMemberInvoiceGenerateGroup()) // gen group ref
                .build();
    }

    @Bean
    public Step stepMoveFileCard() {
        return this.steps.get("stepMoveFileCard")
                .tasklet(new MoveFileCard(jschConfig, cardPayment, cardPaymentArchice, redisService, cardInvoiceLimitTimeOut, dayOfBillCycle))
                .build();
    }

    @Bean
    public Step stepMoveFileAccount() {
        return this.steps.get("stepMoveFileAccount")
                .tasklet(new MoveFileAccount(jschConfig, accountPayment, accountPaymentArchice, redisService, accountLimitTimeOut))
                .build();
    }


    @Bean
    public Step stepMoveFileBill() {
        return this.steps.get("stepMoveFileBill")
                .tasklet(new MoveFileBill(jschConfig, billPayment, billPaymentArchice, redisService, cardInvoiceBillLimitTimeOut, dayOfBillCycle))
                .build();
    }

    @Bean
    public Step stepDeleteRadisMpass() {
        return this.steps.get("stepDeleteRadisMpass")
                .tasklet(new DeleteRadisMpass(redisService))
                .build();
    }

    @Bean
    public Step stepDeleteRadisEasyPass() {
        return this.steps.get("stepDeleteRadisEasyPass")
                .tasklet(new DeleteRadisEasyPass(redisService))
                .build();
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }

    private String previousIssueDate(){
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE,-1);
        Date date = currentDate.getTime();
        String issueDate = df.format(date);
        return issueDate;
    }


}
