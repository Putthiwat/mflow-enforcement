package com.appworks.co.th.batchmaster.oracle.entity;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_NONMEMBER_AUDIT_DOH")
public class InvoiceNonMemberAuditDoh extends BaseEntity implements Serializable {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceNonMemAuditDoh", sequenceName = "SEQ_MF_INVOICE_NONMEM_AUDIT_DOH", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceNonMemAuditDoh")
    public Long id;

    @NaturalId
    @Column(name = "INVOICE_NO", columnDefinition = "VARCHAR2(20)")
    private String invoiceNo;

    @ColumnDefault("N")
    @Column(name = "AUDIT_FLAG", columnDefinition = "VARCHAR2(1)")
    private String auditFlag;

}