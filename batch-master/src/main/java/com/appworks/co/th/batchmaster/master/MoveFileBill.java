package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.JschConfig;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_ACCOUNT;
import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_BILL_CYCLE;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.*;

public class MoveFileBill implements Tasklet {
    private JschConfig jschConfig;

    private String billPayment;

    private String billPaymentArchice;

    private RedisService redisService;

    private int limitTimeOut;

    private List<Integer> dayOfBillCycle;

    public static final Logger log = LoggerFactory.getLogger(MoveFileBill.class);


    public MoveFileBill(JschConfig mjschConfig, String mbillPayment, String mbillPaymentArchice, RedisService mredisService, int mLimitTimeOut, List<Integer> mDayOfBillCycle) {
        jschConfig = mjschConfig;
        billPayment = mbillPayment;
        billPaymentArchice = mbillPaymentArchice;
        redisService = mredisService;
        limitTimeOut = mLimitTimeOut;
        dayOfBillCycle = mDayOfBillCycle;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        log.info("open move file card");
        ChannelSftp channelSftp = null;
        try {
            channelSftp = jschConfig.setupJsch();
            channelSftp.connect();
            channelSftp.cd(billPayment);
            Vector<ChannelSftp.LsEntry> listFileCsv = channelSftp.ls("*.pdf");
            for (ChannelSftp.LsEntry entry : listFileCsv) {
                String dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd_HHmmssSSS").format(LocalDateTime.now());
                String path = billPaymentArchice + dateTimeFormatter + "/" + entry.getFilename();
                channelSftp.cd(billPaymentArchice);
                channelSftp.mkdir(dateTimeFormatter);
                channelSftp.cd(billPayment);
                channelSftp.rename(billPayment + entry.getFilename(), path);
            }
            channelSftp.disconnect();
        } catch (IOException | SftpException | JSchException e) {
            log.error("Error Move File Of Bill : {}", e.getMessage());
        }
 //       ArrayList<String> arrayListBill = new ArrayList<String>();
//        arrayListBill.add(BATCH_BILL_CYCLE + ":" + "*");
//        arrayListBill.add(TOTAL_RECORD_BILL_CYCLE + ":" + "*" + ":" + "*");
//        arrayListBill.add(TOTAL_AMOUNT_BILL_CYCLE + ":" + "*" + ":" + "*");
//        deleteRadis(arrayListBill);

        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setLenient(false);
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        if (c.get(Calendar.DAY_OF_MONTH) == findMin(dayOfBillCycle)) {
            String key = BATCH_BILL_CYCLE + ":" + dateFormatter();
            String keyTotalFileCard = TOTAL_FILE_BILL_CYCLE + ":" + this.dateFormatter();
            Long aLong = redisService.setKeyIncrementBySetValue(key, 1, limitTimeOut, TimeUnit.DAYS);
            if (aLong != null && aLong > 0) {
                redisService.setOfDay(keyTotalFileCard, Integer.valueOf(String.valueOf(aLong)), limitTimeOut, TimeUnit.DAYS);
            } else {
                redisService.setOfDay(keyTotalFileCard, 1, limitTimeOut, TimeUnit.DAYS);
            }
        }
        return RepeatStatus.FINISHED;
    }

    public static Integer findMin(List<Integer> list) {
        return list.stream()                        // Stream<Integer>
                .min(Comparator.naturalOrder()) // Optional<Integer>
                .get();                         // Integer
    }

    private void deleteRadis(ArrayList<String> arrayListBill) {
        try {
            for (String keyPattern : arrayListBill) {
                Set<String> keys = redisService.getKeysWithPattern(keyPattern);
                if (keys != null && keys.size() > 0) {
                    for (String key : keys) {
                        redisService.delete(key);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error Radis Of Bill : {}", e.getMessage());
        }
    }

    private String dateFormatter() {
        final DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String seq = df.format(new Date());
        return seq;
    }
}