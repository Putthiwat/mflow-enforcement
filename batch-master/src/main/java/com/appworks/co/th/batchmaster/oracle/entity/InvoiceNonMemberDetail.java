package com.appworks.co.th.batchmaster.oracle.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@Data
@Entity
@ToString
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@EqualsAndHashCode(callSuper = false)
@Table(name = "MF_INVOICE_DETAIL_NONMEMBER")
@Where(clause = "DELETE_FLAG = 0")
public class InvoiceNonMemberDetail extends BaseEntity implements Serializable {
    @Id
    @Column(name = "ID")
    @SequenceGenerator(name = "seqInvoiceNonMemDetail", sequenceName = "SEQ_MF_INVOICE_NONM_DETAIL", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqInvoiceNonMemDetail")
    public Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(
            name = "INVOICE_NO",
            referencedColumnName = "INVOICE_NO",
            foreignKey = @ForeignKey(name = "MF_INVOICE_NONMEM_DETAIL_FK1"),
            nullable = false)
    private InvoiceNonMember invoice;

    @Column(name = "TRANSACTION_ID", columnDefinition = "VARCHAR2(50)")
    @NotNull
    private String transactionId;
    @Column(name = "TRANSACTION_DATE", columnDefinition = "TIMESTAMP")
    @NotNull
    private Instant transactionDate;
    @Column(name = "PLATE1", columnDefinition = "VARCHAR2(255)")
    @NotNull
    private String plate1;
    @Column(name = "PLATE2", columnDefinition = "VARCHAR2(255)")
    @NotNull
    private String plate2;
    @Column(name = "PROVINCE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String province;
    @Column(name = "PROVINCE_NAME", columnDefinition = "VARCHAR2(1500)")
    private String provinceName;
    @Column(name = "HQ_CODE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String hqCode;
    @Column(name = "HQ_NAME", columnDefinition = "VARCHAR2(150)")
    private String hqName;
    @Column(name = "PLAZA_CODE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String plazaCode;
    @Column(name = "PLAZA_NAME", columnDefinition = "VARCHAR2(150)")
    private String plazaName;
    @Column(name = "LANE_CODE", columnDefinition = "VARCHAR2(25)")
    @NotNull
    private String laneCode;
    @Column(name = "LANE_NAME", columnDefinition = "VARCHAR2(150)")
    private String laneName;
    @Column(name = "DEST_HQ_CODE", columnDefinition = "VARCHAR2(25)")
    private String destHqCode;
    @Column(name = "DEST_HQ_NAME", columnDefinition = "VARCHAR2(150)")
    private String destHqName;
    @Column(name = "DEST_PLAZA_CODE", columnDefinition = "VARCHAR2(25)")
    private String destPlazaCode;
    @Column(name = "DEST_PLAZA_NAME", columnDefinition = "VARCHAR2(150)")
    private String destPlazaName;
    @Column(name = "DEST_LANE_CODE", columnDefinition = "VARCHAR2(25)")
    private String destLaneCode;
    @Column(name = "DEST_LANE_NAME", columnDefinition = "VARCHAR2(150)")
    private String destLaneName;
    @Column(name = "FEE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    @NotNull
    private BigDecimal feeAmount;
    @Column(name = "FEE_AMOUNT_OLD", columnDefinition = "DECIMAL(5,2)")
    private BigDecimal feeAmountOld;
    @Column(name = "RAW_FEE", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal rawFee;


    @Column(name = "FINE_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal fineAmount;

    @Column(name = "COLLECTION_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal collectionAmount;

    @Column(name = "TOTAL_AMOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal totalAmount;

    @Column(name="OPERATION_FEE", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal operationFee;

    @Column(name="VAT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal vat;

    @Column(name = "DISCOUNT", columnDefinition = "DECIMAL(18,2)")
    private BigDecimal discount;

    @Column(name="VEHICLE_WHEEL", columnDefinition = "VARCHAR2(25)")
//    @NotNull
    private String vehicleWheel;

    @Column(name = "ORIGIN_TRAN_TYPE", columnDefinition = "VARCHAR2(25)")
    private String originTranType;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "detail", orphanRemoval = true, fetch = FetchType.LAZY)
    private List<InvoiceNonMemberEvidence> evidences;

//    public void setEvidences(List<InvoiceNonMemberEvidence> evidences) {
//        for (InvoiceNonMemberEvidence camera : evidences) {
//            camera.setDetail(this);
//            camera.setCreateBy(this.getCreateBy());
//            camera.setCreateChannel(this.getCreateChannel());
//        }
//        this.evidences = evidences;
//    }
}
