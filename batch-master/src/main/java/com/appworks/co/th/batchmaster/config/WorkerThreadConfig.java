package com.appworks.co.th.batchmaster.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@EnableAsync
@Configuration
public class WorkerThreadConfig {

    @Value("${vehicle.thread.pool.name-prefix}")
    private String vehiclePrefixName;

    @Value("${vehicle.thread.pool.queue-capacity:10000}")
    private int vehicleCapacity;

    @Value("${vehicle.thread.pool.core-size}")
    private int vehicleCoreSize;

    @Value("${vehicle.thread.pool.max-size}")
    private int vehicleMaxSize;

    @Bean(name = "generateMoveFileCardThreadPool")
    public TaskExecutor generateMoveFileCardThreadPool() {
        ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
        poolTaskExecutor.setCorePoolSize(vehicleCoreSize);
        poolTaskExecutor.setMaxPoolSize(vehicleMaxSize);
        poolTaskExecutor.setQueueCapacity(vehicleCapacity);
        poolTaskExecutor.setThreadNamePrefix(vehiclePrefixName);
        poolTaskExecutor.initialize();
        return poolTaskExecutor;
    }


    @Bean(name = "generateMoveFileThreadPool")
    public TaskExecutor generateMoveFileThreadPool() {
        ThreadPoolTaskExecutor poolTaskExecutor = new ThreadPoolTaskExecutor();
        poolTaskExecutor.setCorePoolSize(vehicleCoreSize);
        poolTaskExecutor.setMaxPoolSize(vehicleMaxSize);
        poolTaskExecutor.setQueueCapacity(vehicleCapacity);
        poolTaskExecutor.setThreadNamePrefix(vehiclePrefixName);
        poolTaskExecutor.initialize();
        return poolTaskExecutor;
    }

}
