package com.appworks.co.th.batchmaster;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.config.prop.AsyncFTPProperty;
import org.apache.activemq.broker.BrokerService;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
@EnableScheduling
@EnableBatchProcessing
@SpringBootApplication(scanBasePackages = "com.appworks.co.th")
@EntityScan("com.appworks.co.th.batchmaster.oracle.*")
@Import({
		BrokerConfiguration.class})
@EnableConfigurationProperties({AsyncFTPProperty.class})
public class BatchMasterApplication {
	@Value("${broker.url}")
	private String brokerUrl;

	public static void main(String[] args) {
		SpringApplication.run(BatchMasterApplication.class, args);
	}

//	@PostConstruct
//	public void init() throws Exception {
//		if ("tcp://127.0.0.1:61616".equals(brokerUrl)){
//			BrokerService broker = new BrokerService();
//			broker.addConnector(brokerUrl);
//			broker.start();
//		}
//	}
}
