package com.appworks.co.th.batchmaster.service.customerService;

public interface CustomerVehicleInfoService {
    String getBrand (String vehicleCode);
}
