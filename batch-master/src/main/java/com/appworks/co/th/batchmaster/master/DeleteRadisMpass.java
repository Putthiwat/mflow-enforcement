package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.JschConfig;
import com.appworks.co.th.batchmaster.service.RedisService;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Set;
import java.util.Vector;

import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_CARD;
import static com.appworks.co.th.batchmaster.commons.Constant.FilePayment.BATCH_MPASS;
import static com.appworks.co.th.batchmaster.commons.Constant.SummaryPayment.*;

public class DeleteRadisMpass implements Tasklet {


    private RedisService redisService;

    public static final Logger log = LoggerFactory.getLogger(DeleteRadisMpass.class);


    public DeleteRadisMpass( RedisService mredisService) {
        redisService=mredisService;
    }

    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception
    {
        log.info("open delete radis mpass");
        ArrayList<String> arrayListCard = new ArrayList<String>();
        arrayListCard.add(BATCH_MPASS + ":" + "*");
        arrayListCard.add(TOTAL_RECORD_MPASS + ":" + "*"+ ":" + "*");
        arrayListCard.add(TOTAL_AMOUNT_MPASS + ":" + "*"+ ":" + "*");
        try {
            for (String keyPattern: arrayListCard) {
                Set<String> keys = redisService.getKeysWithPattern(keyPattern);
                if (keys != null && keys.size() > 0) {
                    for (String key : keys) {
                        redisService.delete(key);
                    }
                }
            }
        }catch (Exception e){
            log.error("Error Radis Of Mpass : {}",e.getMessage());
        }
        return RepeatStatus.FINISHED;
    }


}