package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class MemberInvoiceProceedListBillTimeMasterJob {

    private final JobBuilderFactory jobBuilderFactory;
    public static final Logger log = LoggerFactory.getLogger(MemberInvoiceProceedListBillTimeMasterJob.class);

    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;
    public MemberInvoiceProceedListBillTimeMasterJob(JobBuilderFactory jobBuilderFactory,
                                                     RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    public ColumnRangePartitioner invoiceProceedListStagePartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_PROCEED_LIST invoiceProceed");
        columnRangePartitioner.setWhere("WHERE invoiceProceed.COMMAND = \'InvoiceMemberCommand\' AND invoiceProceed.STATE=\'APPROVAL_PENDING\' ");
        return columnRangePartitioner;
    }


    @Bean
    public Step memberInvoiceProceedListBillTime() {
        return this.masterStepBuilderFactory.get("memberInvoiceProceedListBillTime")
                .partitioner("memberInvoiceProceedListBillTimeStep", invoiceProceedListStagePartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(memberInvoiceProceedListBillTimeRequests())
                .inputChannel(memberInvoiceProceedListBillTimeReplies())
                .build();
    }


    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "memberInvoiceProceedListBillTimeRequests")
    public DirectChannel memberInvoiceProceedListBillTimeRequests() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceProceedListBillTimeRequestsOutboundFlow")
    public IntegrationFlow memberInvoiceProceedListBillTimeRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(memberInvoiceProceedListBillTimeRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("memberInvoiceProceedListBillTimeRequests"))
                .get();
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "memberInvoiceProceedListBillTimeReplies")
    public DirectChannel memberInvoiceProceedListBillTimeReplies() {
        return new DirectChannel();
    }

    @Bean(name = "memberInvoiceProceedListBillTimeRepliesInboundFlow")
    public IntegrationFlow memberInvoiceProceedListBillTimeRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("memberInvoiceProceedListBillTimeReplies"))
                .channel(memberInvoiceProceedListBillTimeReplies())
                .get();
    }


    @Bean
    public Job masterMemberInvoiceProceedListBillTimeJob() {
        return this.jobBuilderFactory.get("masterMemberInvoiceProceedListBillTimeJob")
                .incrementer(new RunIdIncrementer())
                .start(memberInvoiceProceedListBillTime())
                .build();
    }

}
