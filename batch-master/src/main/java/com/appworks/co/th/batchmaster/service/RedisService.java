package com.appworks.co.th.batchmaster.service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface RedisService {
    boolean delete(String key);

    void deleteList(Set<String> key);

    Object get(String key);

    Set<String> getKeysWithPattern(String pattern);

    Long setKeyIncrementBySetValue(String key, int delta, long timeout, TimeUnit unit);

    List<Object> getOpsForList(String key);

    public void setOfDay(String key, int value, long timeout, TimeUnit unit);
}
