package com.appworks.co.th.batchmaster.grpc;

import com.appworks.co.th.customerservice.CustomerVehicleInfoGrpc;
import com.appworks.co.th.customerservice.GetBrandRequest;
import com.appworks.co.th.customerservice.GetBrandResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

@Service
public class CustomerClient {

    private static final Logger logger = LoggerFactory.getLogger(CustomerClient.class);

    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;

    private static String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);


    @Value("${grpc.customerservice.host}")
    private String host;
    @Value("${grpc.customerservice.port}")
    private String port;


    private CustomerVehicleInfoGrpc.CustomerVehicleInfoBlockingStub customerVehicleInfoBlockingStub;

    @Autowired
    public void setCustomerVehicleInfoBlockingStub() {
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, Integer.parseInt(port))
                .defaultLoadBalancingPolicy("round_robin").usePlaintext().build();
        CustomerVehicleInfoGrpc.CustomerVehicleInfoBlockingStub stub = CustomerVehicleInfoGrpc.newBlockingStub(channel);
        this.customerVehicleInfoBlockingStub = stub;
    }

    public GetBrandResponse getBrand(GetBrandRequest request) {
        GetBrandResponse response = this.customerVehicleInfoBlockingStub.getBrand(request);
        return response;
    }

}
