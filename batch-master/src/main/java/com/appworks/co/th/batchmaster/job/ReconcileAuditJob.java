package com.appworks.co.th.batchmaster.job;

import com.appworks.co.th.batchmaster.config.prop.FTPProperties;
import com.appworks.co.th.batchmaster.service.GenMoveFileService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class ReconcileAuditJob {

    @Qualifier("oracleJdbcTemplate")
    private final JdbcTemplate jdbcTemplate;

    private final JobBuilderFactory jobBuilderFactory;
    private final GenMoveFileService genMoveFileService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    public static final String FILE_NAME_PREFIX_MEMBER = "billing_member_";
    public static final String FILE_NAME_PREFIX_NONMEMBER = "billing_nonmember_";
    public static final DateFormat df = new SimpleDateFormat("yyyyMMdd");
    public static final String FILE_NAME_TYPE = ".txt";
    private final FTPProperties ftpProperties;

    @Scheduled(cron = "${audit.reconcile.invoice.transaction.frequency}", zone = "Asia/Bangkok")
    private void  mainInvoiceJob(){
        String query = "SELECT INVOICE_NO FROM MF_INVOICE_AUDIT_DOH WHERE CREATE_DATE < TO_DATE(TO_CHAR (SYSDATE, 'YYYY-MM-DD'),'YYYY-MM-DD') AND CREATE_DATE > TO_DATE(TO_CHAR (SYSDATE-1, 'YYYY-MM-DD'),'YYYY-MM-DD') AND AUDIT_FLAG = 'Y' AND DELETE_FLAG = 0";
        List<String> data = jdbcTemplate.query(query, new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                return rs.getString(1);
            }
        });
        String filename = FILE_NAME_PREFIX_MEMBER+currentDate()+FILE_NAME_TYPE;
        List<String> stringList = new ArrayList<>();
        data.stream().forEach(obj -> {
            try {
                stringList.add(obj);
            } catch (Exception e) {
                log.error("AuditReconcileMemberTransactionItemWriter error : {}", e);
            }
        });
        InputStream inputStream = new ByteArrayInputStream(
                stringList.stream().map(this::convert).collect(Collectors.joining("\n")).getBytes());
        genMoveFileService.saveFileAsync(filename, inputStream, currentDate(), ftpProperties.getEnvPart());

    }

    @Scheduled(cron = "${audit.reconcile.invoicenonmem.transaction.frequency}", zone = "Asia/Bangkok")
    private void  mainInvoiceNonmemberJob(){
        String query = "SELECT INVOICE_NO FROM MF_INVOICE_NONMEMBER_AUDIT_DOH WHERE CREATE_DATE < TO_DATE(TO_CHAR (SYSDATE, 'YYYY-MM-DD'),'YYYY-MM-DD') AND CREATE_DATE > TO_DATE(TO_CHAR (SYSDATE-1, 'YYYY-MM-DD'),'YYYY-MM-DD') AND AUDIT_FLAG = 'Y' AND DELETE_FLAG = 0";
        List<String> data = jdbcTemplate.query(query, new RowMapper<String>(){
            public String mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                return rs.getString(1);
            }
        });
        String filename = FILE_NAME_PREFIX_NONMEMBER+currentDate()+FILE_NAME_TYPE;
        List<String> stringList = new ArrayList<>();
        data.stream().forEach(obj -> {
            try {
                stringList.add(obj);
            } catch (Exception e) {
                log.error("AuditReconcileMemberTransactionItemWriter error : {}", e);
            }
        });
        InputStream inputStream = new ByteArrayInputStream(
                stringList.stream().map(this::convert).collect(Collectors.joining("\n")).getBytes());
        genMoveFileService.saveFileAsync(filename, inputStream, currentDate(), ftpProperties.getEnvPart());

    }

    private String convert(String data) {
        return String.join(",", data);
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE,-1);
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        log.info("currentDate : {}", currentDate);
        return currentDateString;
    }

}
