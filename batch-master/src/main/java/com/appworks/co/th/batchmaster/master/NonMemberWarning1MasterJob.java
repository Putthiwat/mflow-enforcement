package com.appworks.co.th.batchmaster.master;

import com.appworks.co.th.batchmaster.config.BrokerConfiguration;
import com.appworks.co.th.batchmaster.partitioner.ColumnRangePartitioner;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.config.annotation.EnableBatchIntegration;
import org.springframework.batch.integration.partition.RemotePartitioningMasterStepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.jms.dsl.Jms;

import javax.sql.DataSource;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Configuration
@EnableBatchProcessing
@EnableBatchIntegration
@Import(value = {BrokerConfiguration.class})
public class NonMemberWarning1MasterJob {

    public final Logger log = LoggerFactory.getLogger(this.getClass());

    final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Value("${batch.master.grid:4}")
    private int GRID_SIZE;

    private final JobBuilderFactory jobBuilderFactory;

    private final RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory;


    @Autowired
    @Qualifier("oracleDataSource")
    private DataSource dataSource;

    public NonMemberWarning1MasterJob(JobBuilderFactory jobBuilderFactory,
                               RemotePartitioningMasterStepBuilderFactory masterStepBuilderFactory) {

        this.jobBuilderFactory = jobBuilderFactory;
        this.masterStepBuilderFactory = masterStepBuilderFactory;
    }

    /*
     * Configure outbound flow (requests going to workers)
     */
    @Bean(name = "nonMemberWarning1JobRequests")
    public DirectChannel nonMemberWarning1JobRequests() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning1JobRequestsOutboundFlow")
    public IntegrationFlow nonMemberWarning1JobRequestsOutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemberWarning1JobRequests())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemberWarning1JobRequests"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemberWarning1JobReplies")
    public DirectChannel nonMemberWarning1JobReplies() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemberWarning1JobRepliesInboundFlow")
    public IntegrationFlow nonMemberWarning1JobRepliesInboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemberWarning1JobReplies"))
                .channel(nonMemberWarning1JobReplies())
                .get();
    }



    public ColumnRangePartitioner nonMemberWarning1Partitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0  AND INVOICE_TYPE = 0 AND TOTAL_AMOUNT > 0 AND STATUS !='PAYMENT_SUCCESS' ");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberWarning1() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberWarning1")
                .partitioner("nonMemberWarning1Step", nonMemberWarning1Partitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemberWarning1JobRequests())
                .inputChannel(nonMemberWarning1JobReplies())
                .build();
    }

    public ColumnRangePartitioner invoiceNonMemEBillPartitioner()
    {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND E_BILL_FILE_ID IS NULL");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemGenerateEBillWarning1() {
        return this.masterStepBuilderFactory.get("masterStepNonMemGenerateEBillWarning1")
                .partitioner("updateInvoiceNonMemEBillWarning1Step", invoiceNonMemEBillPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemGenerateEBillRequestsWarning1())
                .inputChannel(nonMemGenerateEBillRepliesWarning1())
                .build();
    }

    @Bean(name = "nonMemGenerateEBillRequestsWarning1")
    public DirectChannel nonMemGenerateEBillRequestsWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRequestsWarning1OutboundFlow")
    public IntegrationFlow nonMemGenerateEBillRequestsWarning1OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateEBillRequestsWarning1())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateEBillRequestsWarning1"))
                .get();
    }

    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemGenerateEBillRepliesWarning1")
    public DirectChannel nonMemGenerateEBillRepliesWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateEBillRepliesWarning1InboundFlow")
    public IntegrationFlow nonMemGenerateEBillRepliesWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateEBillRepliesWarning1"))
                .channel(nonMemGenerateEBillRepliesWarning1())
                .get();
    }



    public ColumnRangePartitioner invoiceNonMemGroupPartitioner() {
        ColumnRangePartitioner columnRangePartitioner = new ColumnRangePartitioner();
        columnRangePartitioner.setColumn("id");
        columnRangePartitioner.setDataSource(dataSource);
        columnRangePartitioner.setTable("MF_INVOICE_NONMEMBER");
        columnRangePartitioner.setWhere("WHERE DELETE_FLAG = 0 AND TOTAL_AMOUNT > 0 AND REF_GROUP IS NULL");
        return columnRangePartitioner;
    }

    @Bean
    public Step masterStepNonMemberInvoiceGenerateGroupWarning1() {
        return this.masterStepBuilderFactory.get("masterStepNonMemberInvoiceGenerateGroupWarning1")
                .partitioner("updateNonMemInvoiceGroupWarning1Step", invoiceNonMemGroupPartitioner())
                .gridSize(GRID_SIZE)
                .outputChannel(nonMemGenerateGroupRequestsWarning1())
                .inputChannel(nonMemGenerateGroupRepliesWarning1())
                .build();
    }


    @Bean(name = "nonMemGenerateGroupRequestsWarning1")
    public DirectChannel nonMemGenerateGroupRequestsWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRequestsWarning1OutboundFlow")
    public IntegrationFlow nonMemGenerateGroupRequestsWarning1OutboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(nonMemGenerateGroupRequestsWarning1())
                .handle(Jms.outboundAdapter(connectionFactory).destination("nonMemGenerateGroupRequestsWarning1"))
                .get();
    }


    /*
     * Configure inbound flow (replies coming from workers)
     */
    @Bean(name = "nonMemGenerateGroupRepliesWarning1")
    public DirectChannel nonMemGenerateGroupRepliesWarning1() {
        return new DirectChannel();
    }

    @Bean(name = "nonMemGenerateGroupRepliesWarning1InboundFlow")
    public IntegrationFlow nonMemGenerateGroupRepliesWarning1InboundFlow(ActiveMQConnectionFactory connectionFactory) {
        return IntegrationFlows
                .from(Jms.messageDrivenChannelAdapter(connectionFactory).destination("nonMemGenerateGroupRepliesWarning1"))
                .channel(nonMemGenerateGroupRepliesWarning1())
                .get();
    }

    public String currentDate() {
        Calendar currentDate = Calendar.getInstance();
        Date date = currentDate.getTime();
        String currentDateString = df.format(date);
        return currentDateString;
    }


    @Bean
    public Job nonMemberWarning1Job() {
        return this.jobBuilderFactory.get("nonMemberWarning1Job")
                .incrementer(new RunIdIncrementer())
                .start(masterStepNonMemberWarning1())
                .next(masterStepNonMemGenerateEBillWarning1())
                .next(masterStepNonMemberInvoiceGenerateGroupWarning1())
                .build();
    }

}
