package com.appworks.co.th.batchmaster.partitioner;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ColumnRangePartitionerAuditDOH implements Partitioner {
    private static final Logger logger = LoggerFactory.getLogger(ColumnRangePartitioner.class);

    private JdbcOperations jdbcTemplate;
    private String table;
    private String column;
    private String rightJoin;
    private String on;
    private String where;


    public void setTable(String table) {
        this.table = table;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public void setDataSource(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void setWhere(String where) {
        this.where = where;
    }

    public void setRightJoin(String rightJoin) { this.rightJoin = rightJoin; }

    public void setOn(String on) { this.on = on;}

    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {
        System.out.println("SELECT MIN(" + column + ") FROM " + table + " RIGHT JOIN " + rightJoin + " ON " + on + " " + where);
        try {
            Integer min = jdbcTemplate.queryForObject("SELECT MIN(" + column + ") FROM " + table + " RIGHT JOIN " + rightJoin + " ON " + on + " " + where, Integer.class);

            Integer max = jdbcTemplate.queryForObject("SELECT MAX(" + column + ") FROM " + table + " RIGHT JOIN " + rightJoin + " ON " + on + " " + where, Integer.class);

            if (ObjectUtils.isEmpty(min))
                min = 0;
            if (ObjectUtils.isEmpty(max))
                max = 0;
            int targetSize = (max - min) / gridSize + 1;

            Map<String, ExecutionContext> result = new HashMap<>();

            int number = 0;
            int start = min;
            int end = start + targetSize - 1;

            while (start <= max) {
                ExecutionContext value = new ExecutionContext();
                result.put("partition" + number, value);

                if (end >= max) {
                    end = max;
                }

                value.putInt("minValue", start);
                value.putInt("maxValue", end);
                value.put("on", this.on);
                value.put("where", this.where);
                start += targetSize;
                end += targetSize;
                number++;
            }

            System.out.println(result.toString());
            return result;
        } catch (Exception e) {
            logger.error("Not found", e);
            return null;
        }
    }
}