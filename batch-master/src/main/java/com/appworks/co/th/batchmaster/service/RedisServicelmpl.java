package com.appworks.co.th.batchmaster.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class RedisServicelmpl implements RedisService{
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    public boolean delete(String key) {
        try {
            return redisTemplate.delete(key);
        } catch (Exception e) {
            log.error("Redis get : {}", e.getMessage());
            return false;
        }
    }


    public void deleteList(Set<String> key) {
        try {
            redisTemplate.delete(key);
        } catch (Exception e) {
            log.error("Redis get : {}", e.getMessage());
        }
    }


    public Object get(String key) {
        try {
            return redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            log.error("redis get", e);
            return null;
        }
    }


    public Set<String> getKeysWithPattern(String pattern) {
        try {
            return redisTemplate.keys(pattern);
        } catch (Exception e) {
            return new HashSet<>();
        }
    }

    @Override
    public Long setKeyIncrementBySetValue(String key, int delta, long timeout, TimeUnit unit) {
        Long keyIncre = redisTemplate.opsForValue().increment(key, delta);
        redisTemplate.expire(key, timeout, unit);
        return keyIncre;
    }


    @Override
    public List<Object> getOpsForList(String key) {
        try {
            return redisTemplate.opsForList().range(key, 0, -1);
        } catch (Exception e) {
            log.error("Redis setOpsForList : {}", e.getMessage());
        }
        return null;
    }


    public void setOfDay(String key, int value, long timeout, TimeUnit unit) {
        redisTemplate.opsForValue().set(key, value, timeout, unit);
    }
}
