package com.appworks.co.th.batchmaster.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "com.appworks.co.th.batchmaster.oracle.schema.repository",
        entityManagerFactoryRef = "oracleSchemaEntityManagerFactory",
        transactionManagerRef = "oracleSchemaTransactionManager"
)
public class JpaOracleSchemaConfig {

    @Bean(name = "oracleSchemaDataSource")
    @ConfigurationProperties("spring.datasource.oracle-schema.hikari")
    public DataSource oracleSchemaDataSource() {
        return new HikariDataSource();
    }

    @Bean(name = "oracleSchemaProperties")
    @ConfigurationProperties("spring.jpa.properties.oracle-schema")
    public Properties oracleSchemaProperties() {
        return new Properties();
    }

    @Bean(name = "oracleSchemaEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean oracleMainEntityManagerFactory(
            @Qualifier("oracleSchemaDataSource") DataSource oracleSchemaDataSource,
            @Qualifier("oracleSchemaProperties") Properties oracleSchemaProperties) {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(oracleSchemaDataSource);
        factory.setPackagesToScan("com.appworks.co.th.batchmaster.oracle.schema.entity");
        factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factory.setJpaProperties(oracleSchemaProperties);
        return factory;
    }

    @Bean(name = "oracleSchemaTransactionManager")
    public PlatformTransactionManager oracleSchemaTransactionManager(
            @Qualifier("oracleSchemaEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory){
        return new JpaTransactionManager(entityManagerFactory.getObject());
    }
}
