package com.appworks.co.th.batchmaster.service;

import java.io.InputStream;
import java.util.concurrent.CompletableFuture;


public interface GenMoveFileService {
    CompletableFuture<Void> saveFileAsync(String filename, InputStream inputStream, String datePath,
                                          String subPath);

    boolean saveFile(String filename, InputStream inputStream, String datePath,
                     String subPath);


}
