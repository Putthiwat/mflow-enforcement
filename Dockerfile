# Use official base image of Java Runtim
FROM openjdk:8-jre-alpine

WORKDIR /app

ENV TZ=Asia/Bangkok

EXPOSE 8080 6565

# Set application's JAR file
ARG JAR_FILE=batch-master/build/libs/batch-master.jar

# Add the application's JAR file to the container
COPY ${JAR_FILE} /app

# Run the JAR file
CMD java $JAVA_OPTS -jar batch-master.jar