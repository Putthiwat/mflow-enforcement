package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionType implements Serializable {
    private String code;
}
