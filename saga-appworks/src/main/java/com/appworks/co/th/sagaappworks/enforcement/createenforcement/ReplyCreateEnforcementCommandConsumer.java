package com.appworks.co.th.sagaappworks.enforcement.createenforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface ReplyCreateEnforcementCommandConsumer {

    @KafkaListener(topics = "${saga.kafka.enforcement.reply.command.topic:replyCreatedEnforcementCommand}",
            groupId = "${saga.kafka.service.groupId:enforcement-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyCreatedEnforcementCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
