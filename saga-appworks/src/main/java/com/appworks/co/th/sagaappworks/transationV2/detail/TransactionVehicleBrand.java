package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionVehicleBrand implements Serializable {
    private String code;
    private String description;
    private String descriptionEng;
    public TransactionVehicleBrand(String code) {
        this.code = code;
    }
}
