package com.appworks.co.th.sagaappworks.noncharge;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NonChargeTransactionCommand implements Command {
}
