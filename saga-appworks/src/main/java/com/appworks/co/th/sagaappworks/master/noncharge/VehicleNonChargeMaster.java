package com.appworks.co.th.sagaappworks.master.noncharge;

import lombok.Data;

@Data
public class VehicleNonChargeMaster {

    private String plate1;
    private String plate2;
    private String province;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;
    private String startDate;
    private String endDate;

}
