package com.appworks.co.th.sagaappworks.master.feewheel;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FeeWheelMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeFeeWheelMaster";
    private FeeWheelMaster detail;

    public FeeWheelMasterDomainEvent(Long id, String state, K header, FeeWheelMaster detail) {
        super(id, StoreFeeWheelMasterCommand.class, state, header);
        this.detail = detail;
    }
}
