package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class VerifyIllegalDetail implements Serializable {
    private boolean illegalFlag;
    private boolean verifyFlag;
    private List<String> reasons = new ArrayList<>();
}
