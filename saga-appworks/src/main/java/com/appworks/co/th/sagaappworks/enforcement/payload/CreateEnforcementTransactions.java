package com.appworks.co.th.sagaappworks.enforcement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateEnforcementTransactions implements Serializable {
    CreateEnforcementTransaction transaction;
}
