package com.appworks.co.th.sagaappworks.invoice;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class CreateInvoiceTransaction implements Serializable {

    private String id;
    private String transactionDate;
    private String plate1;
    private String plate2;
    private String province;
    private String provinceName;
    private String hqCode;
    private String hqName;
    private String plazaCode;
    private String plazaName;
    private String laneCode;
    private String laneName;
    private String destinationHqCode;
    private String destinationHqName;
    private String destinationPlazaCode;
    private String destinationPlazaName;
    private String destinationLaneCode;
    private String destinationLaneName;
    private Double feeAmount;
    private Double feeAmountOld;
    private Double discount;
    private Double vat;
    private Double fineAmount;
    private Double collectionAmount;
    private String vehicleWheel;
    private String originTranType;
    private Double operationFee;
    private List<CreateInvoiceEvidence> evidences;
}
