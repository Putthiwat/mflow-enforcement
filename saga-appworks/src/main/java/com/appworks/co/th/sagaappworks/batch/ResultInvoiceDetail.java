package com.appworks.co.th.sagaappworks.batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultInvoiceDetail implements Serializable {
    private Boolean status;
    private String message;

    List<InvoiceError> errors;
    List<InvoiceInfo> transactionIds;

    String serviceProvider;
    InvoiceHeader invoiceHeader;
    List<InvoiceTransaction> invoiceTransactions;
}
