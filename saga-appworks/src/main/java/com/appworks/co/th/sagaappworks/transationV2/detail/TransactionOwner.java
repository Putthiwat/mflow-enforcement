package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionOwner implements Serializable {
    private String customerId;
    private List<TransactionOwnerTitle> titles;
    private String firstName;
    private String middleName;
    private String lastName;
    private String companyName;
    private TransactionOwnerAddress address;

    private String docType;
    private String docNo;

}
