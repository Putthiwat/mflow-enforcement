package com.appworks.co.th.sagaappworks.invoice.payment.update;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateMemberInvoicePaymentStatusHeader implements Serializable {
    private String invoiceNo;
    private String customerId;
    private String paymentChannel;
    private Date paymentDate;
    private String updateBy;
    private String status;
    private String errorMessage;
}
