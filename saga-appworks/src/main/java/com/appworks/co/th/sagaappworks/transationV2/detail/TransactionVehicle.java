package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionVehicle implements Serializable {
    private String plate1;
    private String plate2;
    private String vehicleCode;
    private String vehicleChassis;
    private String vehicleWheel;
    private int wheelNo;
    private TransactionVehicleProvince province;
    private TransactionVehicleBrand brand;
    private List<TransactionVehicleColor> colors;
    private TransactionVehicleType type;

}
