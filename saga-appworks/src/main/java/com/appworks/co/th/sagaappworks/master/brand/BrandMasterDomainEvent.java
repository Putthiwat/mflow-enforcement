package com.appworks.co.th.sagaappworks.master.brand;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BrandMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeBrandMaster";
    private BrandMaster detail;

    public BrandMasterDomainEvent(Long id, String state, K header, BrandMaster detail) {
        super(id, StoreBrandMasterCommand.class, state, header);
        this.detail = detail;
    }
}
