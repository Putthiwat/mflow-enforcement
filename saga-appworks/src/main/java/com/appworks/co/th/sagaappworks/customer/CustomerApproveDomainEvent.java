package com.appworks.co.th.sagaappworks.customer;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CustomerApproveDomainEvent<T,K> extends DomainEvent {
    public static final String channel = "storeApproveCustomer";
    private T detail;

    public CustomerApproveDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
