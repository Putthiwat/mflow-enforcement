package com.appworks.co.th.sagaappworks.repository;

import com.appworks.co.th.sagaappworks.entity.SagaProceed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface SagaProceedRepository extends JpaRepository<SagaProceed,Long> {

    @Transactional
    @Query("UPDATE SagaProceed SET state = :state WHERE id = :id")
    @Modifying
    int updateStateById(@Param("state") String state, @Param("id") Long id);

    @Transactional
    @Query("UPDATE SagaProceed SET command = :command WHERE aggregateid = :aggregateId")
    @Modifying
    int updateCommandById(@Param("command") String command, @Param("aggregateId") String aggregateId);

    Optional<SagaProceed> findByTransactionsId(String transactionsId);
}
