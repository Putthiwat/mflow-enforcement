package com.appworks.co.th.sagaappworks.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "MF_SAGA_PROCEED", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"aggregateId"})
})
@Data
public class SagaProceed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "sagaProceedSequence")
    @SequenceGenerator(name = "sagaProceedSequence", sequenceName = "SAGA_PROCEED_SEQ", allocationSize = 1)
    private Long id;

    @Column(columnDefinition = "VARCHAR2(25)")
    private String orchestrator;

    private String aggregateId;

    private String transactionRefId;

    private String transactionsId;

    private String client;

    @Column(columnDefinition = "VARCHAR2(25)")
    private String state;

    @Column(name = "TRANSACTION_DATE", columnDefinition = "TIMESTAMP")
    private Date transactionDate;

    @Column(columnDefinition = "VARCHAR2(50)")
    private String command;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "sagaProceed", fetch = FetchType.LAZY)
    private List<SagaProceedList> sagaProceedLists;
}
