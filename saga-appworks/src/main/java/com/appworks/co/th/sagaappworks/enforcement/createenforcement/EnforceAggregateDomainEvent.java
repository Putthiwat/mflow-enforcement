package com.appworks.co.th.sagaappworks.enforcement.createenforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EnforceAggregateDomainEvent<K, H> extends DomainEvent {

    private String aggregateId;

    private Long proceedId;

    private Long proceedListId;

    private K detail;

    public EnforceAggregateDomainEvent(Long id, Class command, String state, K detail, H header) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
