package com.appworks.co.th.sagaappworks.vehicle;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerifyVehicleDomainEvent<T,K> extends DomainEvent {

    public static String channel = "vehicleService";

    private T detail;

    public VerifyVehicleDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
