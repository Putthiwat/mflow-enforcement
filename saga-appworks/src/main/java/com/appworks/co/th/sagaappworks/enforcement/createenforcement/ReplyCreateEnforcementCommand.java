package com.appworks.co.th.sagaappworks.enforcement.createenforcement;

import java.io.Serializable;

public class ReplyCreateEnforcementCommand implements Serializable {
    public static final String channel = "replyCreatedEnforcementCommand";
}
