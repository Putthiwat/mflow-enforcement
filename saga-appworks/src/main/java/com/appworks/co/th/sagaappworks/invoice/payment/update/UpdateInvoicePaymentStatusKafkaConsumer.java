package com.appworks.co.th.sagaappworks.invoice.payment.update;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface UpdateInvoicePaymentStatusKafkaConsumer {
    @KafkaListener(topics = "${saga.kafka.invoice.service.update.topic:updateInvoicePaymentStatus}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void updateInvoicePaymentStatus(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.invoice.service.member.reply.topic:replyUpdateMemberInvoicePaymentStatusCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyUpdateMemberInvoicePaymentStatusCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.invoice.service.nonmember.reply.topic:replyUpdateNonMemberInvoicePaymentStatusCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyUpdateNonMemberInvoicePaymentStatusCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
