package com.appworks.co.th.sagaappworks.master.brand;

import lombok.Data;

@Data
public class BrandMaster {

    private String brand;
    private String dltBrand;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;


}
