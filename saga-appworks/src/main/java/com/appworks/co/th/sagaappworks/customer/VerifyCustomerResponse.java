package com.appworks.co.th.sagaappworks.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyCustomerResponse {
    private String memberType;
    private String customerId;
    private String vehicleId;
    private String vehicleLicense;
    private String vehicleProvince;
    private String vehicleChassis;
    private boolean nonChargeFlag;
    private boolean illegalFlag;
    private List<String> illegalReason = new ArrayList<>();

}
