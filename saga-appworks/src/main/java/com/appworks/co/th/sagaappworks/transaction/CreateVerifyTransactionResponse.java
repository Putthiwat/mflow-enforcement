package com.appworks.co.th.sagaappworks.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CreateVerifyTransactionResponse {
    private String transactionId;
    private String status;
    private String message;
}
