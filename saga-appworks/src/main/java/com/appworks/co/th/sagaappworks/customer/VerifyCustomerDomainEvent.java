package com.appworks.co.th.sagaappworks.customer;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerifyCustomerDomainEvent<T, K> extends DomainEvent {
    public static String channel = "customerService";

    private T detail;

    public VerifyCustomerDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
