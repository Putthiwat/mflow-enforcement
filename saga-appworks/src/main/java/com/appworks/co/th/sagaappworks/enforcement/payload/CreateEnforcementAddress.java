package com.appworks.co.th.sagaappworks.enforcement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateEnforcementAddress implements Serializable {
    private String docNo;
    private String houseId;
    private String houseNo;
    private String houseType;
    private String houseTypeDesc;
    private String villageNo;
    private String alleyWayCode;
    private String alleyWayDesc;
    private String alleyCode;
    private String alleyDesc;
    private String roadCode;
    private String roadDesc;
    private String subdistrictCode;
    private String subdistrictDesc;
    private String districtCode;
    private String districtDesc;
    private String provinceCode;
    private String provinceDesc;
    private String postcode;
    private String rcodeCode;
    private String rcodeDesc;
    private String dateOfTerminate;
    private String createChannel;
    private String createBy;
}
