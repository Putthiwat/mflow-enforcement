package com.appworks.co.th.sagaappworks.transationV2.command;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class CreateTransactionCommand implements Command {
}
