package com.appworks.co.th.sagaappworks.config;

import com.appworks.co.th.sagaappworks.common.DomainEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class MessagePublisher {

    private static final Logger log = LoggerFactory.getLogger(MessagePublisher.class);

    @Autowired
    private KafkaTemplate<String, DomainEvent> template;

    public void sendDomainEvent(String channel, DomainEvent domainEventMessage) {
        ListenableFuture<SendResult<String, DomainEvent>> future = template.send(channel, domainEventMessage);
        future.addCallback(new ListenableFutureCallback<SendResult<String, DomainEvent>>() {
            @Override
            public void onSuccess(SendResult<String, DomainEvent> result) {
                log.info("sendDomainEvent Command : {} | State : {}", result.getProducerRecord().value().getCommand(), result.getProducerRecord().value().getState());

            }

            @Override
            public void onFailure(Throwable ex) {
               log.error("sendDomainEvent error : {}" , ex.getMessage());
            }
        });

    }



}