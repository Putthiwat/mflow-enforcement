package com.appworks.co.th.sagaappworks.master.account;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AccountInfoMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeAccountInfoMaster";
    private AccountInfoMaster detail;

    public AccountInfoMasterDomainEvent(Long id, String state, K header, AccountInfoMaster detail) {
        super(id, StoreAccountInfoMasterCommand.class, state, header);
        this.detail = detail;
    }
}
