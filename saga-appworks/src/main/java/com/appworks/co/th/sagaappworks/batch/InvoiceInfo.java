package com.appworks.co.th.sagaappworks.batch;

import lombok.Data;

import java.io.Serializable;

@Data
public class InvoiceInfo implements Serializable {
    private String id;
    private String invoiceNo;
}
