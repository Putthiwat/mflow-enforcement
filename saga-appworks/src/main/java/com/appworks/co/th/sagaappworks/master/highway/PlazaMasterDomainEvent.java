package com.appworks.co.th.sagaappworks.master.highway;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlazaMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storePlazaMaster";
    private PlazaMaster detail;

    public PlazaMasterDomainEvent(Long id, String state, K header, PlazaMaster detail) {
        super(id, StorePlazaMasterCommand.class, state, header);
        this.detail = detail;
    }
}
