package com.appworks.co.th.sagaappworks.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyCustomerRequest {
    private String plate1;
    private String plate2;
    private String province;
}
