package com.appworks.co.th.sagaappworks.master.feewheel;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreFeeWheelMasterConsumer {

    @KafkaListener(topics = "storeFeeWheelMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreFeeWheelMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
