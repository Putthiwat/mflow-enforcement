package com.appworks.co.th.sagaappworks.service;


import com.appworks.co.th.sagaappworks.common.DomainEvent;
import com.appworks.co.th.sagaappworks.entity.SagaProceed;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Service;


public interface SagaOrchestrator  {
    void handleEventDomain(ConsumerRecord<String, DomainEvent> cr);


}
