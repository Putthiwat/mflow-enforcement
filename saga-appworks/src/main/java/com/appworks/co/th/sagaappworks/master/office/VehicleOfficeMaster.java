package com.appworks.co.th.sagaappworks.master.office;

import lombok.Data;

@Data
public class VehicleOfficeMaster {

    private String office;
    private String dltOffice;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;

}
