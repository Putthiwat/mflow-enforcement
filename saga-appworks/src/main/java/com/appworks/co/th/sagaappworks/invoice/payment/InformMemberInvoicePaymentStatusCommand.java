package com.appworks.co.th.sagaappworks.invoice.payment;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

@Data
@AllArgsConstructor
public class InformMemberInvoicePaymentStatusCommand implements Command {
    public static String channel = "informMemberInvoicePaymentStatusCommand";
}
