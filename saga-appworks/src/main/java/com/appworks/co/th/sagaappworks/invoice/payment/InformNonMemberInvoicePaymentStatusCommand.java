package com.appworks.co.th.sagaappworks.invoice.payment;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InformNonMemberInvoicePaymentStatusCommand implements Command {
    public static String channel = "informNonMemberInvoicePaymentStatusCommand";
}
