package com.appworks.co.th.sagaappworks.customer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthServiceResult {
  private String status;
  private String action;
  private String accountId;
  private String language;
  private String approveDate;
  private String approveBy;
  private String approveChannel;
}
