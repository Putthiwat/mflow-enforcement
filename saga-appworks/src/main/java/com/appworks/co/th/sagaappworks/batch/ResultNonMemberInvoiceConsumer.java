package com.appworks.co.th.sagaappworks.batch;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface ResultNonMemberInvoiceConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:replyCreateNonMemberInvoiceService}",
            groupId = "${saga.kafka.service.groupId:batch-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createNonMemberInvoiceServiceResult(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
