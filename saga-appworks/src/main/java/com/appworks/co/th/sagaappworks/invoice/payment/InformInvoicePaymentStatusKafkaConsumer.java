package com.appworks.co.th.sagaappworks.invoice.payment;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface InformInvoicePaymentStatusKafkaConsumer {
    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:informInvoicePaymentStatus}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void informInvoicePaymentStatus(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:replyInformMemberInvoicePaymentStatusCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyInformMemberInvoicePaymentStatusCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:replyInformNonMemberInvoicePaymentStatusCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyInformNonMemberInvoicePaymentStatusCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
