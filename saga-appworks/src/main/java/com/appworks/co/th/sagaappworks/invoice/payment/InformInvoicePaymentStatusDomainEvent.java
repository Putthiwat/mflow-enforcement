package com.appworks.co.th.sagaappworks.invoice.payment;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class InformInvoicePaymentStatusDomainEvent<K> extends DomainEvent {

    public static String channel = "informInvoicePaymentStatus";

    private String invoiceNo;

    private Long invoiceProceedListId;

    private Long proceedListId;

    public InformInvoicePaymentStatusDomainEvent(Long id, Class command, String state, K header) {
        super(id, command, state, header);
    }
}
