package com.appworks.co.th.sagaappworks.batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceTransaction implements Serializable {
    private String id;
    private String invoiceNo;
    private String transactionDate;
    private String plate1;
    private String plate2;
    private String province;

    private String hqCode;
    private String plazaCode;
    private String laneCode;

    private String destinationHqCode;
    private String destinationPlazaCode;
    private String destinationLaneCode;

    private Double feeAmount;
    private Double feeAmountOld;
    private Double fineAmount;
    private Double collectionAmount;
    private Double totalAmount;
    private Double operationFee;
    private Double vat;
    private Double discount;

    private String originTranType;
    private String vehicleWheel;

    List<InvoiceEvidence> evidences;
}
