package com.appworks.co.th.sagaappworks.customer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface AuthServiceConsumer {
    @KafkaListener(topics = "${saga.kafka.createTransaction.service.topic:storeCustomerAuthServiceResult}",
            groupId = "${saga.kafka.service.groupId:customer-service}",
            containerGroup = "kafkaListenerContainerFactory",
            containerFactory = "authListenerContainerFactory"
    )
    void listenAuthServiceResult(ConsumerRecord<String, String> cr, Acknowledgment ack);
}
