package com.appworks.co.th.sagaappworks.repository;

import com.appworks.co.th.sagaappworks.entity.SagaProceed;
import com.appworks.co.th.sagaappworks.entity.SagaProceedList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface SagaProceedListRepository extends JpaRepository<SagaProceedList,Long> {

    @Transactional
    @Query("UPDATE SagaProceedList SET state = :state WHERE id = :id")
    @Modifying
    int updateStateById(@Param("state") String state, @Param("id") Long id);

    @Transactional
    @Query("UPDATE SagaProceedList SET state = :state , detail = :detail WHERE id = :id")
    @Modifying
    int updateStateAndDetailById(@Param("state") String state, @Param("detail") String detail, @Param("id") Long id);


    List<SagaProceedList> findAllBySagaProceed(SagaProceed sagaProceed);

}
