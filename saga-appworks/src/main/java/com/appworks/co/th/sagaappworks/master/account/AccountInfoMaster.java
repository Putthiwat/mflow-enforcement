package com.appworks.co.th.sagaappworks.master.account;

import lombok.Data;

@Data
public class AccountInfoMaster {

    private String accountId;
    private String accountType;
    private String fullName;
    private String status;
    private int deleteFlag;
}
