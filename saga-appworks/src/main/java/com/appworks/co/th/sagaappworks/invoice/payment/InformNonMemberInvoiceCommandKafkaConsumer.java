package com.appworks.co.th.sagaappworks.invoice.payment;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface InformNonMemberInvoiceCommandKafkaConsumer {
    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:informNonMemberInvoicePaymentStatusCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void informInvoicePaymentStatus(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
    
}
