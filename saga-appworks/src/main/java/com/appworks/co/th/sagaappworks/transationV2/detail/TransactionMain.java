package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionMain implements Serializable {
    private String client;
    private String action;
    private String serviceProvider;
    private String transactionId;
    private String refTransactionId;
    private String transactionDatetime;
    private String passageCode;
    private String agentCode;
    private String vehicleClass;
    private String paymentType;
    private Double feeAmount;
    private Double feeAmountOld;
    private Double balance;
    private Double vat;
    private Double discount;
    private String customerType;
    private Map<String,Object> discountMap;
    private String refPan;
    private String refTag;
    private String timestamp;
    private boolean flashAdjust;
    private String moveType;

    private TransactionType type;
    private TransactionSource source;
    private TransactionSource destination;
    private TransactionOwner owner;
    private TransactionVehicle vehicle;
    private TransactionInvoice invoice;
    private List<TransactionEvidences> evidences;

    private String originTranType;

    private List<TransactionFee> transactionFees;

    private Remark remark;
    private String suspectFlag;
    private String month;
    private String province;
    private String dueDate;

    private String originTranTypeManual;
    private TransactionOwner invoiceManual;
}
