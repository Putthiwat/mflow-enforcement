package com.appworks.co.th.sagaappworks.transaction;

import com.appworks.co.th.sagaappworks.entity.SagaProceed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionHeader implements Serializable {

    private SagaProceed sagaProceed;

    private Long transactionId;

    private String hqCode;
    private String plazaCode;
    private String laneCode;

    private TransactionVehicle vehicle;

    private String plate1;

    private String plate2;

    private String province;

    private Long type;

}
