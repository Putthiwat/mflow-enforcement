package com.appworks.co.th.sagaappworks.master.account;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreAccountInfoMasterConsumer {

    @KafkaListener(topics = "storeAccountInfoMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreAccountInfoMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
