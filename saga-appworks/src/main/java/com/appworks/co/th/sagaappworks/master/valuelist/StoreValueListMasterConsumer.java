package com.appworks.co.th.sagaappworks.master.valuelist;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreValueListMasterConsumer {

    @KafkaListener(topics = "storeValueListMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreValueListMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
