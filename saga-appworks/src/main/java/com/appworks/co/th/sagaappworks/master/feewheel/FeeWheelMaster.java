package com.appworks.co.th.sagaappworks.master.feewheel;

import lombok.Data;

@Data
public class FeeWheelMaster {
    private String code;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;

}
