package com.appworks.co.th.sagaappworks.enforcement.createenforcementaddress;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class CreateEnforcementDomainEvent<K> extends DomainEvent {
    private String aggregateId;

    private Long proceedId;

    private Long proceedListId;

    private K detail;

    public CreateEnforcementDomainEvent(Long id, Class command, String state, K detail) {
        super(id, command, state, null);
        this.detail = detail;
    }
}
