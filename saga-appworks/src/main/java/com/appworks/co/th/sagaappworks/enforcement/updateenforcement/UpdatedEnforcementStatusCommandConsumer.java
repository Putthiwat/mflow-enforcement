package com.appworks.co.th.sagaappworks.enforcement.updateenforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface UpdatedEnforcementStatusCommandConsumer {

    @KafkaListener(topics = "${saga.kafka.enforcement.update.command.topic:updatedEnforcementStatusCommand}",
            groupId = "${saga.kafka.service.groupId:enforcement-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void updatedEnforcementStatusCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
