package com.appworks.co.th.sagaappworks.enforcement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateEnforcementStatusDetail {
    private String invoiceNo;
    private String status;
    private String errorMessage;
    private String paymentChannel;
    private Date paymentDate;

}