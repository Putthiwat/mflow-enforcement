package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.batch.InvoiceError;
import com.appworks.co.th.sagaappworks.batch.InvoiceInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceUpdateHeader implements Serializable {
    private String invoiceNo;
    private String type;
    private String channel;
    private String updateBy;
    private String status;
}
