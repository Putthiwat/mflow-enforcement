package com.appworks.co.th.sagaappworks.master.office;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreVehicleOfficeMasterConsumer {

    @KafkaListener(topics = "storeVehicleOfficeMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreVehicleOfficeMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
