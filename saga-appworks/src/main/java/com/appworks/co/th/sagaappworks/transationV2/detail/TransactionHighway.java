package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionHighway implements Serializable {
    private String code;
    private String name;
    private String nameEn;
    private String description;
    private String descriptionEn;
    private String type;
    private String merchantId;
    private TransactionStation station;

    public TransactionHighway(String code) {
        this.code = code;
    }
}
