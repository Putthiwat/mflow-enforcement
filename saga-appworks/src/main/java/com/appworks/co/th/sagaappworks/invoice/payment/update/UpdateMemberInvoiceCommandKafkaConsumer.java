package com.appworks.co.th.sagaappworks.invoice.payment.update;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface UpdateMemberInvoiceCommandKafkaConsumer {
    @KafkaListener(topics = "${saga.kafka.invoice.service.member.topic:updateMemberInvoicePaymentStatusCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void updateInvoicePaymentStatus(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
