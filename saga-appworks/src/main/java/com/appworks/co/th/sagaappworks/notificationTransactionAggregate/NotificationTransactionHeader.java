package com.appworks.co.th.sagaappworks.notificationTransactionAggregate;

import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceTransaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationTransactionHeader implements Serializable {
    private String invoiceType;
    private String customerId;
    private Date issueDate;
    private Date dueDate;
    private String plate1;
    private String plate2;
    private String province;
    private String invoiceChannel;
    private String paymentType;
    private String serviceProvider;
    private String transactionDate;
    private String hqCode;
    private String hqName;
    private String hqNameEn;
    private String plazaCode;
    private String plazaName;
    private String plazaNameEn;
    private String laneCode;
    private String laneName;
    private String laneNameEn;
    private Double feeAmount;
    private String vehicleCode;
}
