package com.appworks.co.th.sagaappworks.vehicle;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface VehicleServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.vehicle.service.topic:vehicleService}",
            groupId = "${saga.kafka.service.groupId:vehicle-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenVehicleService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
