package com.appworks.co.th.sagaappworks.notification;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationManagementHeader implements Serializable {
    private Map<String, Object> data;
}
