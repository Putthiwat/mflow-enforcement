package com.appworks.co.th.sagaappworks.invoice.payment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InformInvoicePaymentStatusHeader implements Serializable {
    private String invoiceNo;
    private String type;
    private String channel;
    private String updateBy;
    private String status;
    private String customerId;

    private String paymentChannel;
}
