package com.appworks.co.th.sagaappworks.invoice;

import lombok.Data;

import java.io.Serializable;

@Data
public class CreateInvoiceEvidence implements Serializable {
    private String type;
    private String file;
}
