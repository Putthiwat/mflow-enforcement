package com.appworks.co.th.sagaappworks.batch;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface ResultMemberInvoiceConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:replyCreateMemberInvoiceService}",
            groupId = "${saga.kafka.service.groupId:batch-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createMemberInvoiceServiceResult(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
