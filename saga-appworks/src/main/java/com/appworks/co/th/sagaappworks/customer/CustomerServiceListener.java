package com.appworks.co.th.sagaappworks.customer;

import org.springframework.core.annotation.AliasFor;
import org.springframework.kafka.annotation.KafkaListener;

import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@KafkaListener
public @interface CustomerServiceListener {

    @AliasFor(annotation = KafkaListener.class, attribute = "id")
    String id() default "customer-service";

    @AliasFor(annotation = KafkaListener.class, attribute = "topics")
    String[] topics() default "customerService";

    @AliasFor(annotation = KafkaListener.class, attribute = "groupId")
    String groupId() default "customer-service";

}
