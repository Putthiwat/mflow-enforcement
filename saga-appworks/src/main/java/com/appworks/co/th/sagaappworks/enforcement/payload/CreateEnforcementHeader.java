package com.appworks.co.th.sagaappworks.enforcement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateEnforcementHeader implements Serializable {
    private String serviceProvider;
    private String hqCode;
    private String invoiceNo;
    private String invoiceNoRef;
    private String invoiceType;

    private String customerType;
    private String docType;
    private String docNo;
    private String customerId;
    private String fullName;
    private String address;

    private String email;
    private String phone;
    private String qrCode;

    private Date issueDate;
    private Date dueDate;
    private Date paymentDate;

    private Double feeAmount;
    private Double fineAmount;
    private Double collectionAmount;
    private Double totalAmount;
    private Double operationFee;
    private Double discount;
    private Double vat;

    private Integer printFlag;
    private String status;
    private String errorMessage;

}
