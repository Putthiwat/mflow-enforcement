package com.appworks.co.th.sagaappworks.master.vehicletype;

import lombok.Data;

@Data
public class VehicleTypeMaster {

    private String type;
    private String dltType;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;

}
