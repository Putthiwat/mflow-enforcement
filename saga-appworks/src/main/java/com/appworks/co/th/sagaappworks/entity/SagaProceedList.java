package com.appworks.co.th.sagaappworks.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "MF_SAGA_PROCEED_LIST", uniqueConstraints = {
//        @UniqueConstraint(columnNames = {"transactionId"})
})
@Data
public class SagaProceedList {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "sagaProceedListSequence")
    @SequenceGenerator(name = "sagaProceedListSequence", sequenceName = "SAGA_PROCEED_LIST_SEQ", allocationSize = 1)
    private Long id;

    @JsonIgnore
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "SAGA_PROCEED_ID")
    private SagaProceed sagaProceed;


    @Column(columnDefinition = "VARCHAR2(25)")
    private String state;


    @Column(columnDefinition = "VARCHAR2(50)")
    private String command;

    @Column(name = "DETAIL", columnDefinition = "NCLOB")
    @Lob
    private String detail;
}
