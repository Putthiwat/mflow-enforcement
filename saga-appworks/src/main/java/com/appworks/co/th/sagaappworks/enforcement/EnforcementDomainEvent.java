package com.appworks.co.th.sagaappworks.enforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EnforcementDomainEvent<T,K> extends DomainEvent {

    private T detail;

    public EnforcementDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }

}
