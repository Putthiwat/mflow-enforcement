package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class InvoiceMemberCommand implements Command {
}
