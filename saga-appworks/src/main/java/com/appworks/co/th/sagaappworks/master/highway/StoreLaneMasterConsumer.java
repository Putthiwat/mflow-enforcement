package com.appworks.co.th.sagaappworks.master.highway;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


/**
 * @author natthawut
 */
public interface StoreLaneMasterConsumer {

    @KafkaListener(topics = "storeLaneMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreLaneMasterMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
