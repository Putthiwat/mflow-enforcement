package com.appworks.co.th.sagaappworks.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyMasterRequest {
    private String hqCode;
    private String plazaCode;
    private String laneCode;
}
