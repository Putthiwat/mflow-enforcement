package com.appworks.co.th.sagaappworks.master.vehiclecolor;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VehicleColorMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeVehicleColorMaster";
    private VehicleColorMaster detail;

    public VehicleColorMasterDomainEvent(Long id, String state, K header, VehicleColorMaster detail) {
        super(id, StoreVehicleColorMasterCommand.class, state, header);
        this.detail = detail;
    }
}
