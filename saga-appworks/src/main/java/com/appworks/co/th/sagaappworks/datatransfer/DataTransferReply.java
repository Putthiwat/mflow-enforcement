package com.appworks.co.th.sagaappworks.datatransfer;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTransferReply {
    private String refTransId;
    private String status;

}
