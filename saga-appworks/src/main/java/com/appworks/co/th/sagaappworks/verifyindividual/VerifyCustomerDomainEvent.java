package com.appworks.co.th.sagaappworks.verifyindividual;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerifyCustomerDomainEvent<T> extends DomainEvent {
    public static final String channel = "customerVerify";
    private T detail;
    private int type;

    public VerifyCustomerDomainEvent(Long id, Class command, String state, T detail,int type) {
        super(id, command, state, type);
        this.detail = detail;
        this.type = type;
    }
}
