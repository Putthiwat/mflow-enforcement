package com.appworks.co.th.sagaappworks.transaction;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDetail implements Serializable {
    private String transactionId;

}
