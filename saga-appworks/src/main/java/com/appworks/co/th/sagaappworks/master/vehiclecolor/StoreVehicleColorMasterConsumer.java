package com.appworks.co.th.sagaappworks.master.vehiclecolor;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreVehicleColorMasterConsumer {

    @KafkaListener(topics = "storeVehicleColorMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreVehicleColorMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
