package com.appworks.co.th.sagaappworks.master.valuelist;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreValueListMasterCommand implements Command {
}
