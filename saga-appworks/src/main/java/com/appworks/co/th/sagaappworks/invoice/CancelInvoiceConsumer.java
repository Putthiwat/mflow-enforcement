package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface CancelInvoiceConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:cancelInvoiceService}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void cancelInvoiceService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

//    @KafkaListener(topics = "${saga.kafka.transactionmember.service.topic:createInvoiceService}",
//            groupId = "${saga.kafka.service.groupId:invoice-service}",
//            containerGroup = "kafkaListenerContainerFactory")
//    void createNonMemberInvoiceService(ConsumerRecord<String, DomainEvent> cr);
//
//    @KafkaListener(topics = "${saga.kafka.transactionmember.service.topic:cancelInvoiceService}",
//            groupId = "${saga.kafka.service.groupId:invoice-service}",
//            containerGroup = "kafkaListenerContainerFactory")
//    void cancelInvoiceService(ConsumerRecord<String, DomainEvent> cr);

}
