package com.appworks.co.th.sagaappworks.master.vehicletype;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreVehicleTypeMasterCommand implements Command {
}
