package com.appworks.co.th.sagaappworks.config;

import com.zaxxer.hikari.HikariDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@Configuration
@ConditionalOnProperty(
		name = "saga.orchestrator",
		havingValue = "true")
@EnableTransactionManagement
@EnableJpaRepositories(
		basePackages = "com.appworks.co.th.sagaappworks.repository",
		entityManagerFactoryRef = "sagaEntityManagerFactory",
		transactionManagerRef = "sagaTransactionManager"
)
public class SagaDatabaseConfig {

	@Value("${saga.datasource.driver.class}")
	private String driverClassName;

	@Value("${saga.datasource.url}")
	private String jdbcUrl;

	@Value("${saga.datasource.username}")
	private String username;

	@Value("${saga.datasource.password}")
	private String password;

	@Value("${saga.datasource.jpa.hibernate.ddl-auto}")
	private String hbm2ddlAuto;

	@Value("${saga.datasource.jpa.properties.hibernate.dialect}")
	private String dialect;


	@Value("${saga.datasource.hikari.connectionTimeout}")
	private long connectionTimeout;

	@Value("${saga.datasource.hikari.idleTimeout}")
	private long idleTimeout;

	@Value("${saga.datasource.hikari.maxLifetime}")
	private long maxLifetime;

	@Value("${saga.datasource.hikari.minimumIdle}")
	private int minimumIdle;

	@Value("${saga.datasource.hikari.maximumPoolSize}")
	private int maximumPoolSize;

	@Value("${saga.datasource.jndi}")
	private String jndiName;

	@Bean(name = "sagaDataSource")
	public DataSource dataSource() throws IllegalArgumentException, NamingException {

		if (StringUtils.isNotBlank(jndiName)) {
			JndiObjectFactoryBean jndiObj = new JndiObjectFactoryBean();
			jndiObj.setJndiName(jndiName);
			jndiObj.setProxyInterface(DataSource.class);
			jndiObj.setLookupOnStartup(false);
			jndiObj.afterPropertiesSet();
			return (DataSource) jndiObj.getObject();
		}else{
			HikariDataSource dataSource = new HikariDataSource();
			dataSource.setDriverClassName(driverClassName);
			dataSource.setJdbcUrl(jdbcUrl);
			dataSource.setUsername(username);
			dataSource.setPassword(password);
			dataSource.setConnectionTimeout(connectionTimeout);
			dataSource.setIdleTimeout(idleTimeout);
			dataSource.setMaxLifetime(maxLifetime);
			dataSource.setMinimumIdle(minimumIdle);
			dataSource.setMaximumPoolSize(maximumPoolSize);
			return dataSource;
		}
	}

	@Bean(name = "sagaEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean sagaEntityManagerFactory(
			@Qualifier("sagaDataSource") DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
		factory.setDataSource(dataSource);
		factory.setPackagesToScan("com.appworks.co.th.sagaappworks.entity");
		factory.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		Properties properties = new Properties();
		properties.put("hibernate.hbm2ddl.auto", hbm2ddlAuto);
		properties.put("hibernate.dialect", dialect);
		factory.setJpaProperties(properties);
		return factory;
	}
	@Bean(name = "sagaTransactionManager")
	public PlatformTransactionManager sagaTransactionManager(
			@Qualifier("sagaEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory){
		return new JpaTransactionManager(entityManagerFactory.getObject());
	}

}
