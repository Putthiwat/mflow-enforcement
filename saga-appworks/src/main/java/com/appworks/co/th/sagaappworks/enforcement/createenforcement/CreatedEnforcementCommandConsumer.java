package com.appworks.co.th.sagaappworks.enforcement.createenforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface CreatedEnforcementCommandConsumer {

    @KafkaListener(topics = "${saga.kafka.enforcement.command.topic:createdEnforcementCommand}",
            groupId = "${saga.kafka.service.groupId:enforcement-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createEnforcementCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
