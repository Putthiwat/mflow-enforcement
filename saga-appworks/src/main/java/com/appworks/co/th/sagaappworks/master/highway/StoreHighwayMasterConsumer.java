package com.appworks.co.th.sagaappworks.master.highway;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


/**
 * @author natthawut
 */
public interface StoreHighwayMasterConsumer {

    @KafkaListener(topics = "storeHighwayMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreHighwayMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
