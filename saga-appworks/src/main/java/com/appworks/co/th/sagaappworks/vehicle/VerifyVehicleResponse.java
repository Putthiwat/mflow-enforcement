package com.appworks.co.th.sagaappworks.vehicle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyVehicleResponse {
    private String licensePlate1;
    private String licensePlate2;
    private String provinceCode;
    private String brnName;
    private String engBrnName;
    private String titleCode;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String companyName;
    private String cardId;
    private String brandCode;
    private String typeCode;
    private String licenseNo;
    private String province;
    private String docNo;
    private String docType;
    private int wheel;

    private VehicleAddressResponse address;

    private List<VehicleColorResponse> colors;
    private boolean nonChargeFlag;
    private boolean illegalFlag;
    private List<String> illegalReason = new ArrayList<>();

}
