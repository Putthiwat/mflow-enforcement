package com.appworks.co.th.sagaappworks.notificationTransactionAggregate;

import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceTransaction;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationTransactionDetail implements Serializable {
    private String transactionDate;
    private String hqCode;
    private String hqName;
    private String plazaCode;
    private String plazaName;
    private String laneCode;
    private String laneName;
    private Double feeAmount;
}
