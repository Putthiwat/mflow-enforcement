package com.appworks.co.th.sagaappworks.master.valuelist;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ValueListMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeValueListMaster";
    private ValueListMaster detail;

    public ValueListMasterDomainEvent(Long id, String state, K header, ValueListMaster detail) {
        super(id, StoreValueListMasterCommand.class, state, header);
        this.detail = detail;
    }
}
