package com.appworks.co.th.sagaappworks.transaction;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface TransactionNonMemberConsumer {

    @KafkaListener(topics = "${saga.kafka.transactionnonmember.service.topic:transactionNonMember}",
            groupId = "${saga.kafka.service.groupId:transaction-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenNonMemberService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
