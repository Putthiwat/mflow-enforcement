package com.appworks.co.th.sagaappworks.verifyindividual;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerifyCustomerCommand implements Command {
}
