package com.appworks.co.th.sagaappworks.notification;

public class NotificationManagementConstants {
    private NotificationManagementConstants() {
    }

    public static class NotificationTypes {
        public static final String EMAIL = "Email";
        public static final String PUSH = "Push Notification";
        public static final String SMS = "Sms";
        public static final String LINE = "Line";
    }
}
