package com.appworks.co.th.sagaappworks.verifyillegal;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface VerifyIllegalServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:createVerifyTransaction}",
            groupId = "${saga.kafka.service.groupId:verifyillegal-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void createVerifyTransactionConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:createIllegalTransaction}",
            groupId = "${saga.kafka.service.groupId:verifyillegal-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void createIllegalTransactionConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:moveVerifyTransactionConsumer}",
            groupId = "${saga.kafka.service.groupId:verifyillegal-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void moveVerifyTransactionConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:moveIllegalTransactionConsumer}",
            groupId = "${saga.kafka.service.groupId:verifyillegal-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void moveIllegalTransactionConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
