package com.appworks.co.th.sagaappworks.master.vehiclewheel;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreVehicleWheelMasterConsumer {

    @KafkaListener(topics = "storeVehicleWheelMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreVehicleWheelMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
