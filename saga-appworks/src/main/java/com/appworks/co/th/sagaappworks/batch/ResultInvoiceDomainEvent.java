package com.appworks.co.th.sagaappworks.batch;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResultInvoiceDomainEvent<T,K> extends DomainEvent {

	public static String channel = "replyCreateMemberInvoiceService";

	private T detail;

	public ResultInvoiceDomainEvent(Long id, Class command, String state, K header, T detail) {
		super(id, command, state, header);
		this.detail = detail;
	}

}
