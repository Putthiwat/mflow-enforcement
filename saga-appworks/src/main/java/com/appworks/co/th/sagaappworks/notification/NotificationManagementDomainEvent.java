package com.appworks.co.th.sagaappworks.notification;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class NotificationManagementDomainEvent<K> extends DomainEvent {

    public static String channel = "sendNotificationManagement";

    private String invoiceNo;

    private Long invoiceProceedListId;

    private Long proceedListId;

    public NotificationManagementDomainEvent(Long id, Class command, String state, K header) {
        super(id, command, state, header);
    }
}
