package com.appworks.co.th.sagaappworks.datatransfer;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DataTransferDomainEvent<K, T> extends DomainEvent {
    public static String channel = "updateTransaction";
    private T detail;

    public DataTransferDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
