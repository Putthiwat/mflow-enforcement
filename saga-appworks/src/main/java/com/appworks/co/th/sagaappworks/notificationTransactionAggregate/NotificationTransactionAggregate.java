package com.appworks.co.th.sagaappworks.notificationTransactionAggregate;


import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class NotificationTransactionAggregate <NotificationTransactionHeader,K> extends DomainEvent {

    private K detail;
    public NotificationTransactionAggregate (Long id, Class command, String state, NotificationTransactionHeader header,K detail){
        super(id, command, state, header);
        this.detail = detail;
    }
}
