package com.appworks.co.th.sagaappworks.enforcement.payload;

import lombok.Data;

import java.io.Serializable;

@Data
public class CreateEnforcementEvidence implements Serializable {
    private String transactionId;
    private String type;
    private String file;
}
