package com.appworks.co.th.sagaappworks.master.brand;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreBrandMasterCommand implements Command {
}
