package com.appworks.co.th.sagaappworks.vehicle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyVehicleRequest {
    private String plate1;
    private String plate2;
    private String province;
    private Long type;
}
