package com.appworks.co.th.sagaappworks.master.vehicletype;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreVehicleTypeMasterConsumer {

    @KafkaListener(topics = "storeVehicleTypeMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreVehicleTypeMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
