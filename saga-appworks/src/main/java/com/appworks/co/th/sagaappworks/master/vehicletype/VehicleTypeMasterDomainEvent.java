package com.appworks.co.th.sagaappworks.master.vehicletype;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VehicleTypeMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeVehicleTypeMaster";
    private VehicleTypeMaster detail;

    public VehicleTypeMasterDomainEvent(Long id, String state, K header, VehicleTypeMaster detail) {
        super(id, StoreVehicleTypeMasterCommand.class, state, header);
        this.detail = detail;
    }
}
