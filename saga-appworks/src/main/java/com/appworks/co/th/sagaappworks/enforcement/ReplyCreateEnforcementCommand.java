package com.appworks.co.th.sagaappworks.enforcement;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReplyCreateEnforcementCommand implements Command {
    public static String channel = "replyCreatedEnforcementService";
}