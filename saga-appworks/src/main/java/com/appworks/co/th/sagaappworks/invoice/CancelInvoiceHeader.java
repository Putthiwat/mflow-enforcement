package com.appworks.co.th.sagaappworks.invoice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CancelInvoiceHeader implements Serializable {
    private String invoiceNo;
    private String channel;
    private String updateBy;
}
