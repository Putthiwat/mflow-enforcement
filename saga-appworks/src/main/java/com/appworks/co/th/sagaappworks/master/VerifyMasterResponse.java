package com.appworks.co.th.sagaappworks.master;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VerifyMasterResponse {
    private String message;
}
