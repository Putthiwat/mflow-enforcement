package com.appworks.co.th.sagaappworks.invoice.payment.update;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class UpdateInvoicePaymentStatusDomainEvent<K> extends DomainEvent {

    public static String channel = "updateInvoicePaymentStatus";

    private String invoiceNo;

    private Long invoiceProceedListId;

    private Long proceedListId;

    public UpdateInvoicePaymentStatusDomainEvent(Long id, Class command, String state, K header) {
        super(id, command, state, header);
    }
}
