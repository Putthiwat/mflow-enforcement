package com.appworks.co.th.sagaappworks.customer;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface CustomerServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.customer.service.topic:customerService}",
            groupId = "${saga.kafka.service.groupId:customer-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void listenCustomerService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:createMemberTransaction}",
            groupId = "${saga.kafka.service.groupId:customer-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void createMemberTransactionConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);



}
