package com.appworks.co.th.sagaappworks.master.vehiclewheel;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreVehicleWheelMasterCommand implements Command {
}
