package com.appworks.co.th.sagaappworks.transaction;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface TransactionServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.transaction.service.topic:transactionService}",
            groupId = "${saga.kafka.service.groupId:transaction-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenTransactionService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
