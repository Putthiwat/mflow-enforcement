package com.appworks.co.th.sagaappworks.master.vehiclecolor;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreVehicleColorMasterCommand implements Command {
}
