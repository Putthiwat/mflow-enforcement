package com.appworks.co.th.sagaappworks.customer;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface ApproveCustomerServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:storeApproveCustomer}",
            groupId = "${saga.kafka.service.groupId:customer-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void storeApproveCustomer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
