package com.appworks.co.th.sagaappworks.enforcement.createenforcementaddress;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface CreatedEnforcementAddressConsumer {

    @KafkaListener(topics = "${saga.kafka.enforcement.service.topic:createdEnforcementAddress}",
            groupId = "${saga.kafka.service.groupId:enforcement-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createEnforcementAddress(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
