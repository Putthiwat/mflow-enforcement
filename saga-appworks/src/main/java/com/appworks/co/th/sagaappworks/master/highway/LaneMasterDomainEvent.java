package com.appworks.co.th.sagaappworks.master.highway;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LaneMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeLaneMaster";
    private LaneMaster detail;

    public LaneMasterDomainEvent(Long id, String state, K header, LaneMaster detail) {
        super(id, StoreLaneMasterCommand.class, state, header);
        this.detail = detail;
    }
}
