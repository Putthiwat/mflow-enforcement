package com.appworks.co.th.sagaappworks.master.noncharge;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VehicleNonChargeMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeVehicleNonChargeMaster";
    private VehicleNonChargeMaster detail;

    public VehicleNonChargeMasterDomainEvent(Long id, String state, K header, VehicleNonChargeMaster detail) {
        super(id, StoreVehicleNonChargeMasterCommand.class, state, header);
        this.detail = detail;
    }
}
