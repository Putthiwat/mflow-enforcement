package com.appworks.co.th.sagaappworks.noncharge;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface NonChargeServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:createTransactionNonCharge}",
            groupId = "${saga.kafka.service.groupId:noncharge-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void createTransactionNonChargeConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
