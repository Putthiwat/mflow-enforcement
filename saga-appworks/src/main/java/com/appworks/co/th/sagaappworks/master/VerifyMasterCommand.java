package com.appworks.co.th.sagaappworks.master;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerifyMasterCommand implements Command {

}
