package com.appworks.co.th.sagaappworks.master.highway;

import lombok.Data;

/**
 * @author natthawut
 */
@Data
public class LaneMaster {
    private Long id;
    private String hqCode;
    private String stationCode;
    private String code;
    private String name;
    private String nameEn;
    private String description;
    private String descriptionEn;
    private String status;
    private int deleteFlag;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;
}