package com.appworks.co.th.sagaappworks.master;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface MasterServiceConsumer {

    @KafkaListener(topics = "${saga.kafka.master.service.topic:masterService}",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenMasterService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
