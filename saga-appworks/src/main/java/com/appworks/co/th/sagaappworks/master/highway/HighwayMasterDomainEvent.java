package com.appworks.co.th.sagaappworks.master.highway;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class HighwayMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeHighwayMaster";
    private HighwayMaster detail;

    public HighwayMasterDomainEvent(Long id, String state, K header, HighwayMaster detail) {
        super(id, StoreHighwayMasterCommand.class, state, header);
        this.detail = detail;
    }
}
