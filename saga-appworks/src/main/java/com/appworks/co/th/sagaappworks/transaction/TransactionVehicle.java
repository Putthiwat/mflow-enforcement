package com.appworks.co.th.sagaappworks.transaction;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class TransactionVehicle implements Serializable {
    private String brand;
    private List<String> color;
    private String type;

    private String dltBrand;
    private List<String> dltColor;
    private String dltType;

}
