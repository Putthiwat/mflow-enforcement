package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface CreatedNonMemberInvoiceConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.created.staging.topic:createdNonMemberInvoiceCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createdNonMemberInvoiceCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);



}
