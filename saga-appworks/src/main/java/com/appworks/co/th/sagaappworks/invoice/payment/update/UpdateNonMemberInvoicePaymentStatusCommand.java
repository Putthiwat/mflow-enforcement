package com.appworks.co.th.sagaappworks.invoice.payment.update;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UpdateNonMemberInvoicePaymentStatusCommand implements Command {
    public static String channel = "updateNonMemberInvoicePaymentStatusCommand";
}
