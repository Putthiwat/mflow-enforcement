package com.appworks.co.th.sagaappworks.master.feewheel;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreFeeWheelMasterCommand implements Command {
}
