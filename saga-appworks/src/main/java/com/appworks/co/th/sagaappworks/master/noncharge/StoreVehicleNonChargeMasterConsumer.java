package com.appworks.co.th.sagaappworks.master.noncharge;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreVehicleNonChargeMasterConsumer {

    @KafkaListener(topics = "storeVehicleNonChargeMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreVehicleNonChargeMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
