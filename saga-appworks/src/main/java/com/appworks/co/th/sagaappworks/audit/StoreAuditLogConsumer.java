package com.appworks.co.th.sagaappworks.audit;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreAuditLogConsumer {

    @KafkaListener(topics = "storeAuditLogService",
            groupId = "${saga.kafka.service.groupId:auditlog-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreAuditLog(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
