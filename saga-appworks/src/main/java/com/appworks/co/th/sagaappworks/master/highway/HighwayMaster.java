package com.appworks.co.th.sagaappworks.master.highway;

import lombok.Data;

/**
 * @author natthawut
 */
@Data
public class HighwayMaster {
    private Long id;
    private String code;
    private String name;
    private String nameEn;
    private String description;
    private String descriptionEn;
    private String status;
    private int deleteFlag;
    private String serviceProvider;
    private String type;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;
}
