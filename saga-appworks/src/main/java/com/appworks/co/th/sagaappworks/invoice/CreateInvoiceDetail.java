package com.appworks.co.th.sagaappworks.invoice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateInvoiceDetail implements Serializable {
    private List<CreateInvoiceTransaction> details;
}
