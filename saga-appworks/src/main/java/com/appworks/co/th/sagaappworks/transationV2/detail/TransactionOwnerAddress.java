package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionOwnerAddress implements Serializable {
    private String houseNo;
    private String building;
    private String floor;
    private String village;
    private String villageNo;
    private String alley;
    private String soi;
    private String road;
    private String subDistrict;
    private String district;
    private String province;
    private String postCode;

}
