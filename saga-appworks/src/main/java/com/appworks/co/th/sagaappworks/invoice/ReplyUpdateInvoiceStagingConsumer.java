package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface ReplyUpdateInvoiceStagingConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.created.staging.reply.topic:replyUpdateInvoiceStagingCommand}",
            groupId = "${saga.kafka.service.groupId}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyUpdateInvoiceStagingCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
