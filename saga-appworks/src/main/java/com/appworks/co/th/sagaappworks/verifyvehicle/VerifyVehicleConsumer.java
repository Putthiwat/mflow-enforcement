package com.appworks.co.th.sagaappworks.verifyvehicle;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface VerifyVehicleConsumer {
    @KafkaListener(topics = "${saga.kafka.vehicleVerify.service.topic:vehicleVerify}",
            groupId = "${saga.kafka.service.groupId:admin-operation-cmd-servic}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenVerifyVehicleService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
