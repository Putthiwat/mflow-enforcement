package com.appworks.co.th.sagaappworks.enforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface UpdateEnforcementStatusKafkaConsumer {

    @KafkaListener(topics = "${saga.kafka.enforcement.status.service.topic:updatedEnforcementStatus}",
            groupId = "${saga.kafka.service.groupId:enforcement-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void updateEnforcementStatus(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}