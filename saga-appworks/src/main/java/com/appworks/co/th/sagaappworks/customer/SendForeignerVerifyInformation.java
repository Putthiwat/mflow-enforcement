package com.appworks.co.th.sagaappworks.customer;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SendForeignerVerifyInformation<T,K> extends DomainEvent {
    public static final String channel = "foreignerVerify";
    private T detail;

    public SendForeignerVerifyInformation(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}

