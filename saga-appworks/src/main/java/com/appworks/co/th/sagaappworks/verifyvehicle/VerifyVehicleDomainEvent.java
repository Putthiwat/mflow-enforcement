package com.appworks.co.th.sagaappworks.verifyvehicle;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerifyVehicleDomainEvent <T> extends DomainEvent {
    public static final String channel = "vehicleVerify";
    private T detail;

    public VerifyVehicleDomainEvent(Long id, Class command, String state, T detail) {
        super(id, command, state, null);
        this.detail = detail;
    }
}
