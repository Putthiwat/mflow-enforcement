package com.appworks.co.th.sagaappworks.master.vehiclecolor;

import lombok.Data;

@Data
public class VehicleColorMaster {

    private String color;
    private String dltColor;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;

}
