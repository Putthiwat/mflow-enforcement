package com.appworks.co.th.sagaappworks.audit;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuditLogDomainEvent<K> extends DomainEvent {

    public static final String channel = "storeAuditLogService";
    private AuditLogEvent detail;

    public AuditLogDomainEvent(Long id, String state, K header, AuditLogEvent detail) {
        super(id, StoreAuditLogCommand.class, state, header);
        this.detail = detail;
    }
}
