package com.appworks.co.th.sagaappworks.transationV2.aggregate;

import com.appworks.co.th.sagaappworks.common.DomainEvent;

import com.appworks.co.th.sagaappworks.entity.SagaProceed;
import com.appworks.co.th.sagaappworks.entity.SagaProceedList;
import com.appworks.co.th.sagaappworks.transationV2.detail.TransactionHeader;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
public class TransactionAggregate<TransactionHeader,K> extends DomainEvent {

    private String aggregateId;

    private SagaProceed sagaProceed;

    private Long proceedListId;

    private K Detail;

    public TransactionAggregate(Long id, Class command, String state, TransactionHeader header, String aggregateId,SagaProceed sagaProceed) {
        super(id, command, state, header);
        this.aggregateId = aggregateId;
    }
}
