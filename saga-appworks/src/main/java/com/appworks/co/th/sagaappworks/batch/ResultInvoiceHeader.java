package com.appworks.co.th.sagaappworks.batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResultInvoiceHeader implements Serializable {
    private String type;
}
