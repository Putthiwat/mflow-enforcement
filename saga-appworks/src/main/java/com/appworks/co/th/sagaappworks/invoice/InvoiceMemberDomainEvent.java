package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InvoiceMemberDomainEvent<T,K> extends DomainEvent {

	public static String channel = "createMemberInvoiceService";

	private String aggregateId;

	private Long proceedListId;

	private T detail;

	public InvoiceMemberDomainEvent(Long id, Class command, String state, K header, T detail) {
		super(id, command, state, header);
		this.detail = detail;
	}

}
