package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class UpdateInvoiceStagingDomainEvent<K> extends DomainEvent {

    public static String channel = "updateInvoiceMemberCommand";

    private String invoiceNo;

    private Long invoiceProceedListId;

    private Long proceedListId;

    public UpdateInvoiceStagingDomainEvent(Long id, Class command, String state, K header) {
        super(id, command, state, header);
    }
}
