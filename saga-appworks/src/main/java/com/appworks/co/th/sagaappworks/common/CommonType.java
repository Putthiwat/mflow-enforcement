package com.appworks.co.th.sagaappworks.common;

public final class CommonType {
    public static final String MASTER_INVALID = "MASTER_INVALID";
    public static final String PERCENTAGE = "PERCENTAGE";
    public static final String DLT_NOT_FOUND = "DLT_NOT_FOUND ";
    public static final String LICENSE_INVALID = "LICENSE_INVALID";
}
