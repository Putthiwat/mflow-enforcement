package com.appworks.co.th.sagaappworks.transaction;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface TransactionMemberConsumer {

    @KafkaListener(topics = "${saga.kafka.transactionmember.service.topic:transactionMember}",
            groupId = "${saga.kafka.service.groupId:transaction-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenCustomerService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
