package com.appworks.co.th.sagaappworks.verifyvehicle;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class VerifyVehicleCommand implements Command {
}
