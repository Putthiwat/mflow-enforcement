package com.appworks.co.th.sagaappworks.master.vehiclewheel;

import lombok.Data;

@Data
public class VehicleWheelMaster {

    private String wheel;
    private String dltWheel;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;

}
