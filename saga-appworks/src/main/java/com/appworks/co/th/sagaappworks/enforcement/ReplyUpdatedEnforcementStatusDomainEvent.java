package com.appworks.co.th.sagaappworks.enforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ReplyUpdatedEnforcementStatusDomainEvent<T> extends DomainEvent {

    private T detail;

    public ReplyUpdatedEnforcementStatusDomainEvent(Long id, Class command, String state, T detail) {
        super(id, command, state, null);
        this.detail = detail;
    }

}
