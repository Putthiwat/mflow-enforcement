package com.appworks.co.th.sagaappworks.batch.generateInvoice;

import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceDetail;
import com.appworks.co.th.sagaappworks.invoice.CreateInvoiceHeader;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceObject {
    CreateInvoiceDetail createInvoiceDetail;
    CreateInvoiceHeader createInvoiceHeader;

}
