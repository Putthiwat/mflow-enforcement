package com.appworks.co.th.sagaappworks.audit;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreAuditLogCommand implements Command {
}
