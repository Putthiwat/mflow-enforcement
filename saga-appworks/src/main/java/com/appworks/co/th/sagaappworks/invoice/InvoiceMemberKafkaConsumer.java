package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface InvoiceMemberKafkaConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.member.service.topic:createMemberInvoiceService}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createMemberInvoiceService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
