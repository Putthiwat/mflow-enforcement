package com.appworks.co.th.sagaappworks.enforcement.updateenforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface ReplyUpdatedEnforcementStatusCommandConsumer {

    @KafkaListener(topics = "${saga.kafka.enforcement.reply.update.command.topic:replyUpdatedEnforcementStatusCommand}",
            groupId = "${saga.kafka.service.groupId:enforcement-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyUpdatedEnforcementStatusCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);
}
