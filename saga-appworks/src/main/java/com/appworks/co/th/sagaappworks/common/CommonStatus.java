package com.appworks.co.th.sagaappworks.common;

public final class CommonStatus {
    public static final String CUST_PENDING = "CUST_PENDING";
    public static final String CUST_COMPLETED = "CUST_COMPLETED";
    public static final String CUST_FAILED = "CUST_FAILED";
}
