package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface ReplyCreateInvoiceStagingConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.created.staging.reply.topic:replyCreateInvoiceStagingCommand}",
            groupId = "${saga.kafka.service.groupId}",
            containerGroup = "kafkaListenerContainerFactory")
    void replyCreateInvoiceStagingCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
