package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionVehicleColor implements Serializable {
    private String code;
    private String description;
    private String descriptionEng;
    public TransactionVehicleColor(String code) {
        this.code = code;
    }
}
