package com.appworks.co.th.sagaappworks.batch;

import lombok.Data;

import java.io.Serializable;

@Data
public class InvoiceError implements Serializable {
    private String code;
    private String transactionId;
    private String description;
}
