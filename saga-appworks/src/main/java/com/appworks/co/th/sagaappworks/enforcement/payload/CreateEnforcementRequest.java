package com.appworks.co.th.sagaappworks.enforcement.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateEnforcementRequest {

    CreateEnforcementHeader createEnforcementHeader;

}
