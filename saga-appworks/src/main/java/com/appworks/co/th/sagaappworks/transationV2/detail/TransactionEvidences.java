package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionEvidences implements Serializable {
    private String cameraCode;
    private Integer cameraNo;
    private String plate1;
    private String plate2;
    private Double percentageCharacter;
    private Double percentageProvince;
    private String plateNo1;
    private String plateNo2;
    private String plateNo3;
    private String plateNo4;
    private String plateNo5;
    private String plateColor;
    private String province;
    private String country;
    private String vehicleType;
    private String vehicleWheel;
    private String vehicleBrand;
    private List<String> vehicleColor;
    private String platePicName;
    private String platePicPath;
    private String bodyPicName;
    private String bodyPicPath;
    private String overallPicName;
    private String overallPicPath;
    private String timeStamp;
    private TransactionCrossLanes crossLane;
}
