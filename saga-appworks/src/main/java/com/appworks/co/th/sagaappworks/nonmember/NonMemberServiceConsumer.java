package com.appworks.co.th.sagaappworks.nonmember;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface NonMemberServiceConsumer {

//    @KafkaListener(topics = "${saga.kafka.customer.service.topic:nonMemberService}",
//            groupId = "${saga.kafka.service.groupId:non-member-service}",
//            containerGroup =  "kafkaListenerContainerFactory")
//    void listenCustomerService(ConsumerRecord<String, DomainEvent> cr);

    @KafkaListener(topics = "${saga.kafka.customer.service.create.trans.topic:createNonMemberTransaction}",
            groupId = "${saga.kafka.service.groupId:non-member-service}",
            containerGroup =  "kafkaListenerContainerFactory")
    void createNonMemberTransactionConsumer(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);



}
