package com.appworks.co.th.sagaappworks.master.valuelist;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Data
public class ValueListMaster {
    private String type;
    private String typeDescription;
    private String code;
    private String description;
    private String descriptionEn;
    private String status;
    private String createById;
    private String createBy;
    private String updateById;
    private String updateBy;
}
