package com.appworks.co.th.sagaappworks.verifyindividual;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface VerifyCustomerConsumer {

    @KafkaListener(topics = "${saga.kafka.customerVerify.service.topic:customerVerify}",
            groupId = "${saga.kafka.service.groupId:admin-operation-cmd-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenVerifyCustomerService(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
