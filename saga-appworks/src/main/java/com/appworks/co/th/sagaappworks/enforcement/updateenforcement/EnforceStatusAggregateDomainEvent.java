package com.appworks.co.th.sagaappworks.enforcement.updateenforcement;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class EnforceStatusAggregateDomainEvent<K> extends DomainEvent {

    private String aggregateId;

    private Long proceedId;

    private Long proceedListId;

    private K detail;

    public EnforceStatusAggregateDomainEvent(Long id, Class command, String state, K detail) {
        super(id, command, state, null);
        this.detail = detail;
    }
}
