package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.Data;

import java.io.Serializable;

@Data
public class TransactionHeader implements Serializable {
    private TransactionMain transaction;

}
