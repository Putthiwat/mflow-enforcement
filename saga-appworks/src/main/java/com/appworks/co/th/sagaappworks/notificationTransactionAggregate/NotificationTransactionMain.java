package com.appworks.co.th.sagaappworks.notificationTransactionAggregate;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NotificationTransactionMain implements Serializable {
    private String transactionId;
    private String transactionDate;
    private String invoiceType;
    private String customerId;
    private String fullName;
    private Date issueDate;
    private Date dueDate;
    private String plate1;
    private String plate2;
    private String province;
    private String hqCode;
    private String hqName;
    private String plazaCode;
    private String plazaName;
    private String laneCode;
    private String laneName;
    private Double feeAmount;
    private String invoiceChannel;
    private String paymentType;
}
