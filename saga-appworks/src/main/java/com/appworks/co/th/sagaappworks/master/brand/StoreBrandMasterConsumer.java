package com.appworks.co.th.sagaappworks.master.brand;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;

public interface StoreBrandMasterConsumer {

    @KafkaListener(topics = "storeBrandMaster",
            groupId = "${saga.kafka.service.groupId:master-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void listenStoreBrandMaster(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

}
