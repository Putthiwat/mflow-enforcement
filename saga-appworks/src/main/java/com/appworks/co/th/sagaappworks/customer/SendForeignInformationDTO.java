package com.appworks.co.th.sagaappworks.customer;

import lombok.Data;

@Data
public class SendForeignInformationDTO {
    private String customerId;
    private String nationalityCode;
    private String customerType;
}
