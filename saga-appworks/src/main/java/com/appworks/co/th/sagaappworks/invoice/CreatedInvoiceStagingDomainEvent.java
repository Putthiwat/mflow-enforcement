package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;


@Data
@NoArgsConstructor
public class CreatedInvoiceStagingDomainEvent <T,K> extends DomainEvent {

    public static String channel = "createdInvoiceMemberCommand";

    private String invoiceNo;

    private Long invoiceProceedListId;

    private Long invoiceProceedId;

    private Long proceedListId;

    private T detail;

    public CreatedInvoiceStagingDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
