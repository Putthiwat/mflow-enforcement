package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class InvoiceUpdateDomainEvent<K> extends DomainEvent {

	private static String channel = "updateInvoiceStagingService";

	public InvoiceUpdateDomainEvent(Long id, Class command, String state, K header) {
		super(id, command, state, header);
	}

}
