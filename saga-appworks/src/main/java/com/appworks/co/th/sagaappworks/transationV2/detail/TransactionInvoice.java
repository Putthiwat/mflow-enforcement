package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionInvoice implements Serializable {
    private String invoiceNo;
    private String invoiceStatus;
    private String invoiceStatusMsg;

}
