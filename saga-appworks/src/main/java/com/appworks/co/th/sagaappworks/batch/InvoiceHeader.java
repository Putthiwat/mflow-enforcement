package com.appworks.co.th.sagaappworks.batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceHeader implements Serializable {
    private String hqCode;
    private String invoiceNo;
    private String invoiceNoRef;
    private String invoiceType;
    private String invoiceChannel;

    private String customerType;
    private String customerId;
    private String fullName;
    private String address;

    private Date issueDate;
    private Date dueDate;
    private Date paymentDate;

    private Double feeAmount;
    private Double fineAmount;
    private Double collectionAmount;
    private Double totalAmount;
    private Double operationFee;
    private Double vat;
    private Double discount;

    private Integer printFlag;
    private String status;
    private String errorMessage;
}
