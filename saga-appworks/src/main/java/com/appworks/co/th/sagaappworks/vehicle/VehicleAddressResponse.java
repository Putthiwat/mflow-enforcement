package com.appworks.co.th.sagaappworks.vehicle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VehicleAddressResponse {
    private String houseNo;
    private String building;
    private String floor;
    private String village;
    private String villageNo;
    private String alley;
    private String soi;
    private String road;
    private String subdistrict;
    private String district;
    private String province;
    private String postCode;
}
