package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.batch.InvoiceError;
import com.appworks.co.th.sagaappworks.batch.InvoiceInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CancelInvoiceDetail implements Serializable {
    private Boolean status;
    private String message;
    List<InvoiceError> errors;
    List<InvoiceInfo> transactionIds;

}
