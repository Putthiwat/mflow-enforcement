package com.appworks.co.th.sagaappworks.common;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class DomainEvent<K> implements Serializable {
    private Long id;
    private Class command;
    private String state;
    private K header;

    public DomainEvent(Long id, Class command, String state, K header) {
        this.id = id;
        this.command = command;
        this.state = state;
        this.header = header;
    }
}
