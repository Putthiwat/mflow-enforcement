package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;


public interface CreatedInvoiceStagingConsumer {

    @KafkaListener(topics = "${saga.kafka.invoice.created.staging.topic:createdInvoiceMemberCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void createdInvoiceStagingCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);

    @KafkaListener(topics = "${saga.kafka.invoice.created.staging.topic:updateInvoiceMemberCommand}",
            groupId = "${saga.kafka.service.groupId:invoice-service}",
            containerGroup = "kafkaListenerContainerFactory")
    void updateInvoiceStagingCommand(ConsumerRecord<String, DomainEvent> cr, Acknowledgment ack);


}
