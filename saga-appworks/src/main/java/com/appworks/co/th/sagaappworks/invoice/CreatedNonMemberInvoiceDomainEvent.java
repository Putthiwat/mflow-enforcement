package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class CreatedNonMemberInvoiceDomainEvent<T,K> extends DomainEvent {

    public static String channel = "createdNonMemberInvoiceCommand";

    private String invoiceNo;

    private Long invoiceProceedListId;

    private Long invoiceProceedId;

    private Long proceedListId;

    private T detail;

    public CreatedNonMemberInvoiceDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
