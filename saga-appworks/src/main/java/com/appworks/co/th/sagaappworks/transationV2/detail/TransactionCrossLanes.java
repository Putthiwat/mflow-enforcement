package com.appworks.co.th.sagaappworks.transationV2.detail;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionCrossLanes implements Serializable {

    private String platePicName;
    private String platePicData;
    private String bodyPicName;
    private String bodyPicData;
    private String plate1;
    private String plate2;
    private String province;
}
