package com.appworks.co.th.sagaappworks.invoice;

import com.appworks.co.th.sagaappworks.entity.SagaProceed;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateInvoiceHeader implements Serializable {
    private String invoiceType;
    private String invoiceRefNo;
    private String customerId;
    private String fullName;
    private String address;
    private Date issueDate;
    private Date dueDate;
    private String channel;
    private String hqCode;
    private String createBy;
    private Double vat;
    private String serviceProvider;
    private String invoiceChannel;
    private String paymentType;

    private String plate1;
    private String plate2;
    private String plateProvince;
    private String brand;
    private List<String> color;

    private String docNo;
    private String docType;

    private String vehicleCode;
    private String vehicleTypeCode;
}
