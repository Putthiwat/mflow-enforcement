package com.appworks.co.th.sagaappworks.audit;

import lombok.Data;

import java.util.Map;

@Data
public class AuditLogEvent {
    private String type;
    private String accountId;
    private String logDatetime;
    private Map<String, Object> request;
    private Map<String, Object> response;
    private String createdById;
    private String createdBy;
}
