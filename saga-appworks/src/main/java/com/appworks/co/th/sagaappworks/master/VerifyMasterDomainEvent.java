package com.appworks.co.th.sagaappworks.master;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VerifyMasterDomainEvent<K, T> extends DomainEvent {
    public static String channel = "masterService";
    private T detail;

    public VerifyMasterDomainEvent(Long id, Class command, String state, K header, T detail) {
        super(id, command, state, header);
        this.detail = detail;
    }
}
