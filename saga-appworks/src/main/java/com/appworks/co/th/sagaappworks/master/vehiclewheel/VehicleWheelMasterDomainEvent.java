package com.appworks.co.th.sagaappworks.master.vehiclewheel;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VehicleWheelMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeVehicleWheelMaster";
    private VehicleWheelMaster detail;

    public VehicleWheelMasterDomainEvent(Long id, String state, K header, VehicleWheelMaster detail) {
        super(id, StoreVehicleWheelMasterCommand.class, state, header);
        this.detail = detail;
    }
}
