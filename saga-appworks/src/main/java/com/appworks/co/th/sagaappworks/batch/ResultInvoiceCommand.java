package com.appworks.co.th.sagaappworks.batch;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResultInvoiceCommand implements Command {
}
