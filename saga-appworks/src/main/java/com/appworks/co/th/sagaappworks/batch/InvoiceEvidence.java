package com.appworks.co.th.sagaappworks.batch;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceEvidence implements Serializable {
    private String transactionId;
    private String type;
    private String file;
}
