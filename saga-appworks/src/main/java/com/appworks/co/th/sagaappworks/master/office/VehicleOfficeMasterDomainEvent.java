package com.appworks.co.th.sagaappworks.master.office;

import com.appworks.co.th.sagaappworks.common.DomainEvent;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VehicleOfficeMasterDomainEvent<K> extends DomainEvent {
    public static final String channel = "storeVehicleOfficeMaster";
    private VehicleOfficeMaster detail;

    public VehicleOfficeMasterDomainEvent(Long id, String state, K header, VehicleOfficeMaster detail) {
        super(id, StoreVehicleOfficeMasterCommand.class, state, header);
        this.detail = detail;
    }
}
