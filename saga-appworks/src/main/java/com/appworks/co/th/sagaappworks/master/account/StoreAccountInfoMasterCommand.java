package com.appworks.co.th.sagaappworks.master.account;

import com.appworks.co.th.sagaappworks.common.Command;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StoreAccountInfoMasterCommand implements Command {
}
