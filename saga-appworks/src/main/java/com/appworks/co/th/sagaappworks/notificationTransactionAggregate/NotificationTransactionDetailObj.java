package com.appworks.co.th.sagaappworks.notificationTransactionAggregate;

import lombok.Data;

import java.io.Serializable;

@Data
public class NotificationTransactionDetailObj implements Serializable {
    private String id;
    private String transactionDate;
    private String hqCode;
    private String hqName;
    private String plazaCode;
    private String plazaName;
    private String laneCode;
    private String laneName;
    private Double feeAmount;
}
